
subroutine addintegralblock(twoeint,gout,ibfst,jbfst,kbfst,lbfst,ncarti,ncartj,ncartk,ncartl)
!-----------------------------------------------------------------------------+
!  Purpose: Add local integral block to full integral set 
!-----------------------------------------------------------------------------+

   use type_int4idx

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'
   include 'constants.h'

   ! dimensions
   integer, intent(in) :: ncarti,ncartj,ncartk,ncartl

   ! input
   integer,   intent(in) :: ibfst,jbfst,kbfst,lbfst
   real(kdp), intent(in) :: gout(ncarti,ncartj,ncartk,ncartl)

   ! output
   type(int4idx),   intent(inout) :: twoeint

   ! local
   integer :: icart,jcart,kcart,lcart,ind
   integer :: ibf,jbf,kbf,lbf 
   integer :: ioff,joff,koff,loff
  
   real(kdp) :: dval

   !---------------------------------------------------------------+
   ! copy local integral for shell quadruple to full set
   !---------------------------------------------------------------+

   loff = lbfst - 1
   koff = kbfst - 1
   joff = jbfst - 1
   ioff = ibfst - 1

   
   !$OMP PARALLEL DO SCHEDULE(AUTO) &
   !$OMP DEFAULT(NONE) & 
   !$OMP SHARED(ncartl,ncartk,ncartj,ncarti,gout,loff,koff,joff,ioff,twoeint) &
   !$OMP PRIVATE(lcart,kcart,jcart,icart,lbf,kbf,jbf,ibf,dval)
   do lcart = 1, ncartl
     lbf = lcart + loff
     do kcart = 1, ncartk
       kbf = kcart + koff
       do jcart = 1, ncartj
         jbf = jcart + joff
         do icart = 1, ncarti
           ibf = icart + ioff

           ! Permutational symmetry is accounted for in int4idx_setvalue   
           dval = gout(icart,jcart,kcart,lcart)
           call int4idx_setvalue(twoeint,ibf,jbf,kbf,lbf,dval,.true.) 

         end do
       end do 
     end do
   end do
   !$OMP END PARALLEL DO

end subroutine

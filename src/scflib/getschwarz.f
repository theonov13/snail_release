
subroutine getschwarz(intopt, &                                 ! control flag
                      gij,    &                                 ! out
                      boystab, lmax, ammax, shells, rab2,    &  ! inp
                      intbuf, eabt, eabu, eabv, eabtuv,      &  ! buffer
                      cnij, o2p, u, uA, uB,                  &  ! buffer
                      memmax, memetuv,                       &  ! dimensions (buffer)
                      nbf, nshell, mxcart, mxprim)              ! dimensions
!-----------------------------------------------------------------------------+
!  Purpose: Calculates the integrals (ij|ij) needed for Schwar-Screening 
!
!
!-----------------------------------------------------------------------------+

   use type_integ
   use type_cgto
   use type_shell
   use type_int4idx
   use IntegralBuffer
   use BoysTable

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'


   ! dimensions
   integer,      intent(in) :: nbf,nshell,mxcart,mxprim
   integer(kli), intent(in) :: memetuv(2)

   ! input
   type(integ_info), intent(in)   :: intopt 
   type(shell_info), intent(in)   :: shells(nshell)
   integer,          intent(in)   :: lmax,ammax
   integer(kli),     intent(in)   :: memmax
   real(kdp),        intent(in)   :: rab2(nshell*(nshell+1)/2)

   type(TBoysTable), intent(in)   :: boystab

   ! output
   real(kdp),        intent(out) :: gij(nshell,nshell)

   ! input/output
   type(TIntBuffer), intent(inout) :: intbuf

   real(kdp), intent(inout) :: eabt(memetuv(1), mxcart*mxcart),&
                               eabu(memetuv(1), mxcart*mxcart),&
                               eabv(memetuv(1), mxcart*mxcart),&
                               eabtuv(memetuv(2), mxcart*mxcart),&
                               cnij(mxprim*mxprim*mxcart*mxcart)

   real(kdp), intent(inout) :: o2p(mxprim*mxprim),u(mxprim*mxprim),uA(mxprim*mxprim),uB(mxprim*mxprim)

   ! local
   integer      :: ijcomp,ibf,jbf,ishl,jshl,ibfst,ibfnd,jbfst,jbfnd
   integer(kli) :: memused

   real(kdp) :: dtemp,dmax
   logical   :: use_os, use_transfer

   real(kdp), allocatable :: xyzp(:,:,:),Kab(:,:)
   real(kdp) :: alphai, alphaj
   integer   :: icomp, jcomp, nprimi, nprimj, ncompi, nprimij, nprimij2, iadr
   integer   :: ncarti, ncartj, angij, ndimij
 
   type(cgto_info), pointer :: bfi, bfj
   
   real(kdp), allocatable :: alpha(:),dx(:),dy(:),dz(:),pfac(:)

   real(kdp), allocatable :: gout(:)

   real(kdp), allocatable :: bfiexps(:), bficoefs(:), bfinorms(:), &
                             bfjexps(:), bfjcoefs(:), bfjnorms(:)

   integer,   allocatable :: bfila(:), bfima(:), bfina(:), &
                             bfjla(:), bfjma(:), bfjna(:)


   ! for timings
   real(kdp) :: cpur,cpuf,cpug1,cpug2,cpug3

   ! functions
   real(kdp) :: product_center_1D

   ! init
   use_os = .true.
   bfi => null()
   bfj => null()

   use_transfer = intopt%scheme == INT_MMDET

   !-----------------------------------------------------------------------+
   ! allocate buffer for local basis function info 
   !-----------------------------------------------------------------------+
   allocate( bfiexps(mxprim*mxcart), bficoefs(mxprim*mxcart), bfinorms(mxprim*mxcart), &
             bfjexps(mxprim*mxcart), bfjcoefs(mxprim*mxcart), bfjnorms(mxprim*mxcart)  )

   allocate( bfila(mxcart), bfima(mxcart), bfina(mxcart), &
             bfjla(mxcart), bfjma(mxcart), bfjna(mxcart)  )
 
   allocate( gout(mxcart*mxcart*mxcart*mxcart) ) 
 
   !---------------------------------------------------------------------+
   ! do not exploit permutation symmetry to reduce memory
   !---------------------------------------------------------------------+
   do ishl = 1, nshell
     nprimi = shells(ishl)%nprim
     ibfst = shells(ishl)%ibfstart; ibfnd = shells(ishl)%ibfend
     ncarti = shells(ishl)%ncart
     call copybasinfo(bfiexps, bficoefs, bfinorms, bfila, bfima, bfina, shells, ishl, nprimi, ncarti, nshell)

     do jshl = 1, ishl
       nprimj = shells(jshl)%nprim
       jbfst = shells(jshl)%ibfstart; jbfnd = shells(jshl)%ibfend
       ncartj = shells(jshl)%ncart
       call copybasinfo(bfjexps, bfjcoefs, bfjnorms, bfjla, bfjma, bfjna, shells, jshl, nprimj, ncartj, nshell)

       ! calculate the center of the shell pair primitives
       allocate(xyzp(3,nprimi,nprimj),Kab(nprimi,nprimj))
       call GetShellpairInfo(intopt, xyzp, Kab, eabt, eabu, eabv, eabtuv, cnij, shells, rab2, ishl, jshl,&
                             o2p,u,uA,uB,&
                             nprimi, nprimj, nshell, memetuv, ncarti, ncartj)


       ! just reference the first basis function in the shells 
       bfi => shells(ishl)%gtos(1); bfj => shells(jshl)%gtos(1)
      
       nprimij  = bfi%nprim * bfj%nprim
       nprimij2 = nprimij * nprimij

       allocate(pfac(nprimij2),alpha(nprimij2),dx(nprimij2),dy(nprimij2),dz(nprimij2))

       call GetShellQuadInfo(intopt,boystab,                 &   ! control flag and boys
                             intbuf%Fn,pfac,alpha,dx,dy,dz,  &   ! out
                             shells,ishl,jshl,ishl,jshl,     &   ! inp
                             nprimij,nprimij,nprimij2,       &   ! dim
                             intbuf%memboys,nshell)              ! dim
       
       dmax = zero 
       ncompi = ibfnd - ibfst + 1 
       do ibf = ibfst, ibfnd
         icomp = ibf - ibfst + 1
         bfi => shells(ishl)%gtos(icomp)

         do jbf = jbfst, jbfnd
           jcomp = jbf - jbfst + 1
           bfj => shells(jshl)%gtos(jcomp)

           if (intopt%scheme == INT_MMD .or. intopt%scheme == INT_MMDET) then
             ijcomp = (jcomp-1)*ncompi + icomp

             iadr = (jcomp-1)*ncompi*nprimij + (icomp-1)*nprimij + 1

             ! When using electron transfer relation
             ndimij = 1
             if (intbuf%intopt%scheme == INT_MMDET) then
               angij = shells(ishl)%angm + shells(jshl)%angm
               ndimij = (angij+1)*(angij+2)/2
             end if


             call mmd_eri(gout,&
                          bfi%exps,bfi%coefs,bfi%norms,bfi%origin,bfi%la,bfi%ma,bfi%na, &
                          bfj%exps,bfj%coefs,bfj%norms,bfj%origin,bfj%la,bfj%ma,bfj%na, &
                          bfi%exps,bfi%coefs,bfi%norms,bfi%origin,bfi%la,bfi%ma,bfi%na, &
                          bfj%exps,bfj%coefs,bfj%norms,bfj%origin,bfj%la,bfj%ma,bfj%na, &
                          eabtuv(1,ijcomp),cnij(iadr),eabtuv(1,ijcomp),cnij(iadr),&
                          use_transfer,ndimij,ndimij,&
                          intbuf,pfac,alpha,dx,dy,dz,&   
                          bfi%nprim,bfj%nprim,bfi%nprim,bfj%nprim,&
                          1,1,1,1,&
                          nprimij,nprimij,nprimij2,&
                          cpur,cpuf,cpug1,cpug2,cpug3,memetuv)                        
           else
             gout(1) = cgto_coulomb(bfi, bfj, bfi, bfj,&
                                    ibf, jbf, ibf, jbf, xyzp, xyzp, Kab, Kab, use_os,&
                                    intbuf%vrr_terms,intbuf%maxmtot,intbuf%maxam,intbuf%nbuf,&
                                    boystab%fntab, boystab%nxvals, boystab%nrange, boystab%kmax,&
                                    nprimi, nprimj, nprimi, nprimj)
           end if

           dmax = max(dmax,dabs(gout(1)))
         end do 
       end do

       gij(ishl,jshl) = dmax
       gij(jshl,ishl) = dmax

       deallocate(xyzp,Kab)
       deallocate(pfac,alpha,dx,dy,dz) ! McMurchie-Davidson
     end do
   end do 
   bfi => null()
   bfj => null()

end subroutine



subroutine GetFDSError(errmat,X,XT,F,S,D,nbf)
!-----------------------------------------------------------------+
!  Purpose:
!  --------
!
!   Calculate the error estimate fot the DIIS. Different choices
!   are possible. Here we calculate the difference FDS-SDF, which
!   should vanish for the exact solution.
!
!-----------------------------------------------------------------+
  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer, intent(in) :: nbf

  ! output
  real(kdp), intent(out) :: errmat(nbf,nbf)

  ! input
  real(kdp), intent(in) :: X(nbf,nbf),XT(nbf,nbf),&
                           F(nbf,nbf),S(nbf,nbf), &
                           D(nbf,nbf)
  ! local
  integer :: idx,jdx

  real(kdp), allocatable :: FDS(:,:), SDF(:,:),error(:,:)

  real(kdp), pointer :: FD(:,:),SD(:,:)
  
  target :: error

  allocate(FDS(nbf,nbf),SDF(nbf,nbf),error(nbf,nbf))
  
  ! 1) Build F x D x S (error = scratch)

  FD => error

  call dgemm('n','n',nbf,nbf,nbf,one,F,nbf,D,nbf,zero,FD,nbf)

  call dgemm('n','n',nbf,nbf,nbf,one,FD,nbf,S,nbf,zero,FDS,nbf)
  
  nullify(FD)

  ! 2) Build S x D x F
  
  SD => error

  call dgemm('n','n',nbf,nbf,nbf,one,S,nbf,D,nbf,zero,SD,nbf)

  call dgemm('n','n',nbf,nbf,nbf,one,SD,nbf,F,nbf,zero,SDF,nbf)

  nullify(SD)
  

  ! 3) error = FDS - SDF
  do jdx = 1, nbf
    do idx = 1, nbf
      error(idx, jdx) = FDS(idx, jdx) - SDF(idx, jdx)
    end do
  end do

  ! Transform to orthonormal basis (use FDS as scratch array)
  call orthobas(XT, error, X, errmat, FDS, nbf)

  deallocate(FDS,SDF,error)
end subroutine


subroutine getdiisweights(weights,errlst,nvec,nbf)
!-----------------------------------------------------------------------------+
!  Purpose:
!  --------
!
!  Determines DIIS weights to mix the previous densities by solving the 
!  linear system of equations:
!
!   A_11 A_12 .. A_1n -1   w_1      0
!   A_21 A_22 .. A_2n -1   w_2      0
!    .                   x      =   0
!    . 
!   A_n1 A_n2 .. A_nn -1   w_n      0
!    -1   -1      -1   0    l      -1 
!
!  with A_ij = <e_i|e_j> and e_i being the error estimate
!
!-----------------------------------------------------------------------------+

  use type_queue

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer, intent(in) :: nvec,nbf

  ! output
  real(kdp), intent(out) :: weights(nvec)

  ! input
  !real(kdp), intent(in) :: errlst(nbf*nbf,nvec)
  type(queueinfo), intent(in) :: errlst

  ! local
  integer   :: idx,jdx,kdx,ierr,ndim
  real(kdp) :: dtemp

  real(kdp), allocatable :: a(:,:),rhs(:)
  integer,   allocatable :: ipiv(:),imap(:)  

  ! functions
  real(kdp) :: ddot

  ndim = nvec+1

  allocate(a(ndim,ndim),ipiv(ndim),rhs(ndim),imap(errlst%nmax))

  ! get maping of indices in error queue
  call queue_getimap(errlst,imap,errlst%nmax)

  do jdx = 1, nvec
    do idx = jdx, nvec
      ! errij = err_i x err_j
      dtemp = ddot(nbf*nbf,errlst%list(1,imap(idx)),1,errlst%list(1,imap(jdx)),1)

      a(idx, jdx) = dtemp
      a(jdx, idx) = dtemp
    end do
  end do  

  a(ndim, ndim) = zero
  rhs           = zero
  rhs(ndim)     = -one
  do idx = 1, nvec
    a(idx, ndim) = -one
    a(ndim, idx) = -one
  end do

  ! solve linear system
  call dgesv(ndim,1,a,ndim,ipiv,rhs,ndim,ierr)

  ! sort solution into weights
  do idx = 1, nvec
    weights(idx) = rhs(idx) !rhs(ipiv(idx))
  end do

  ! Matrix is singular
  if (ierr > 0) then
    ! Take only density from the last two iterations
    weights = zero
    weights(nvec)   = half
    weights(nvec-1) = half
  end if

  deallocate(a,ipiv,rhs,imap)
end subroutine

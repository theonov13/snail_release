!========================================================================================
! Routines which are needed during the SCF procedure (e.g. building density)
!
!
!========================================================================================

 subroutine builddens(isuhf,dens,orb,nstart,nend,nbf)
!------------------------------------------------------------------+
! Purpose: Builds the density using the current CMO coefficients
!
! Input: 
!        orb     current CMO coefficients
!        nstart  start index in orb
!        nend    end index in orb (mainly number of occ. orbitals)
!        nbf     number of basis functions
!
! Output:
!       dens     Density = 2 * C x C^T  (RHF)
!       dens     Density =  C x C^T     (UHF)
!------------------------------------------------------------------+

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   logical, parameter :: use_blas = .true.
 
   integer,   intent(in) :: nstart,nend,nbf

   real(kdp), intent(inout) :: dens(nbf,nbf), orb(nbf,nbf)  

   logical,   intent(in) :: isuhf

   integer :: ibf,jbf,kbf

   real(kdp) :: dfac

   if (isuhf) then
     dfac = one
   else
     dfac = two
   end if

   if (use_blas) then
     call dgemm('n','t',nbf,nbf,nend,dfac,orb,nbf,orb,nbf,zero,dens,nbf)
   else
     do jbf = 1, nbf
       do ibf = 1, nbf
         dens(ibf,jbf) = zero
         do kbf = nstart, nend
           dens(ibf,jbf) = dens(ibf,jbf) + dfac * orb(ibf,kbf) * orb(jbf,kbf) 
         end do
       end do
     end do
   end if
 end subroutine


 subroutine orthobas(xao2ot,fock,xao2o,fockp,work,nbf)
!-----------------------------------------------------------------------------+
! Purpose: Transforms the Fock matrix to an orthogonal basis using the
!          the unitary transformation matrix in xao2ot
!-----------------------------------------------------------------------------+
   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   integer,   intent(in) :: nbf

   real(kdp), intent(in)    :: xao2ot(nbf,nbf),xao2o(nbf,nbf),fock(nbf,nbf)
   real(kdp), intent(inout) :: fockp(nbf,nbf),work(nbf,nbf)

   ! Build Work = F x X
   call dgemm('n', 'n', nbf, nbf, nbf, one, fock, nbf, xao2o, nbf, zero, work, nbf)
  
   ! Build FP = X^T x X
   call dgemm('n', 'n', nbf, nbf, nbf, one, xao2ot, nbf, work, nbf, zero, fockp, nbf)

 end subroutine


 function getenergy(hone,fock,dens,enuke,nbf,nspin) result(dret)
!-----------------------------------------------------------------------------+
!  Purpose: Calculates the RHF energy for a given density, 
!           one electron hamiltonian and Fock-operator 
!
!  E = 1/2 * sum(h_ij + F_ij) + e_nuke
!
!-----------------------------------------------------------------------------+
   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   real(kdp) :: dret

   integer, intent(in) :: nbf,nspin

   real(kdp), intent(in) :: hone(nbf,nbf), fock(nbf,nbf,nspin), dens(nbf,nbf,nspin), enuke

   integer   :: ibf,jbf,ispin
   real(kdp) :: eone,etwo

   eone = zero; etwo = zero

   do ibf = 1, nbf
     do jbf = 1, nbf
       do ispin = 1, nspin
         eone = eone + hone(ibf, jbf) * dens(ibf, jbf, ispin)
         etwo = etwo + fock(ibf, jbf, ispin) * dens(ibf, jbf, ispin)
       end do
     end do
   end do

   dret = half * (eone + etwo) + enuke

   return
 end function


 function getenuke(atoms,natoms) result(dret)
!-----------------------------------------------------------------------------+
!  Purpose: Calculates the electrostatic energy arising form the nuclei
!
!           E = Z_A * Z_B / R_AB 
!
!-----------------------------------------------------------------------------+
   use type_atom

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   real(kdp) :: dret

   integer,         intent(in) :: natoms
   type(atom_info), intent(in) :: atoms(natoms)

   integer :: aat,bat

   ! functions
   real(kdp) :: dist

   dret = zero

   do aat = 1, natoms
     do bat = aat + 1, natoms
       dret = dret +  atoms(aat)%Charge * atoms(bat)%Charge / dist(atoms(aat)%xyz, atoms(bat)%xyz)
     end do
   end do

   return
 end function

 function getekin(kin,dens,nbf,nspin) result(dret)
!-----------------------------------------------------------------------------+
!  Purpose: Calculates the kinetic energy 
!
!           E_kin = T_ij D_ij 
!
!-----------------------------------------------------------------------------+
   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   real(kdp) :: dret

   integer, intent(in) :: nbf,nspin

   real(kdp), intent(in) :: kin(nbf,nbf),  dens(nbf,nbf,nspin)

   integer   :: ibf,jbf,ispin

   dret = zero

   do ibf = 1, nbf
     do jbf = 1, nbf
       do ispin = 1, nspin
         dret = dret + kin(ibf, jbf) * dens(ibf, jbf, ispin)
       end do
     end do
   end do

   return
 end function


 subroutine diagfock(fock,orbs,orben,nbf)
!-----------------------------------------------------------------------------+
!  Purpose: Diagonalizes Fock-matrix 
!
!          fock  : Fock matrix won't be overwritten
!          orbs  : On output the eigenvectors of the Fock matrix
!          orben : On output the orbital energies
!          nbf   : Number of basis functions
!
!-----------------------------------------------------------------------------+
   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   integer, intent(in) :: nbf

   real(kdp), intent(in)    :: fock(nbf,nbf)
   real(kdp), intent(inout) :: orbs(nbf,nbf),orben(nbf)

   real(kdp), allocatable :: work(:)
   real(kdp) :: dlen
   integer   :: info,lwork


   call dcopy(nbf*nbf,fock,1,orbs,1)

   lwork = -1
   call dsyev('V','U',nbf,orbs,nbf,orben,dlen,lwork,info)
   lwork = int(dlen)
   allocate(work(lwork))

   call dsyev( 'V', 'U', nbf, orbs, nbf, orben, work, lwork, info )

   if (info.ne.0 ) then
      call matprt(fock,nbf,nbf)
      call quit('Problem during diagonalization of Fock-matrix','diagfock')
   end if

   deallocate(work)
 end subroutine 

 function getdensdev(dnew,dold,nbf,nspin) result(dret)
   implicit none
 
   include 'fortrankinds.h'
   include 'numbers.h'
 
   real(kdp) :: dret
 
   integer, intent(in) :: nbf,nspin
 
   real(kdp), intent(in) :: dnew(nbf,nbf,nspin),dold(nbf,nbf,nspin)
 
   integer   :: idx,jdx,ispin
   real(kdp) :: diff(nspin) 

   diff = zero

   do ispin = 1, nspin
     do jdx = 1, nbf
       do idx = 1, nbf
         diff(ispin) =  (dnew(idx, jdx, ispin) - dold(idx, jdx, ispin))**2
       end do
     end do
     diff(ispin) = sqrt(diff(ispin) / (nbf*nbf))
   end do

   if (nspin == 1) then
     dret = diff(1)
   else
     dret = half * ( diff(1) + diff(2) )
   end if

   return
 end function

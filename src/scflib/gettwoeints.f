
subroutine gettwoeints(twoeint,intbuf,fock,                    & ! out
                       buildfock,JKmode,use_cao,luhf,          & ! control flag
                       gab,have_schwartz,                      & ! for screening
                       dP,tolint,nscreen,memmax,lmax,shells,   & ! inp 
                       iprint,silent,                          & ! control
                       ncbf,nbf,nshell,mxprim,mxcart,mthreads)   ! dim 
!-----------------------------------------------------------------------------+
!  Purpose: Calculates the two electron repulsion integrals or the Fock matrix
!
!  Ideas: Sort shells according to l so that we can make best
!         use of hand optimized routines in mmd_eri (s1000 etc.)
!
!  Gunnar Schmitz 
!-----------------------------------------------------------------------------+

   use type_integ
   use type_cgto
   use type_shell
   use type_int4idx
   use mod_fileutil
   use BoysTable
   use IntegralBuffer

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'
   include 'constants.h'

   ! constants
   logical,   parameter :: use_schwarz = .true.
   logical,   parameter :: locdbg      = .false.
   integer,   parameter :: buildJK = 1, &
                           buildJ  = 2, &
                           buildk  = 3
   integer,   parameter :: ithd0 = 0

   ! dimensions
   integer, intent(in) :: ncbf,nbf,nshell,mxprim,mxcart,mthreads

   ! input
   type(shell_info), intent(in)   :: shells(nshell)
   integer,          intent(in)   :: lmax
   integer(kli),     intent(in)   :: memmax
   real(kdp),        intent(in)   :: tolint
   logical,          intent(in)   :: buildfock,have_schwartz,use_cao,luhf,silent
   integer,          intent(in)   :: JKmode,iprint
   real(kdp),        intent(in)   :: dP(nbf,nbf)

   ! output
   type(int4idx),    intent(inout) :: twoeint
   type(TIntBuffer), intent(inout) :: intbuf(0:mthreads-1)
   real(kdp),        intent(inout) :: fock(nbf,nbf)
   real(kdp),        intent(inout) :: gab(nshell,nshell)
   integer,          intent(out)   :: nscreen

   ! local
   integer      :: ibf,jbf,kbf,lbf,kl,ij,ind,&
                   ishl,jshl,kshl,lshl,&
                   lbfmax,lshlmax,nbuf,&
                   ibfst,ibfnd,jbfst,jbfnd,&
                   kbfst,kbfnd,lbfst,lbfnd,&
                   icomp,jcomp,kcomp,lcomp,&
                   ncompi,ncompj,ncompk,ncompl,&
                   ijcomp,klcomp,ammax,&
                   icart,jcart,kcart,lcart

   logical      :: use_transfer

   real(kdp)    :: s12_deg, s34_deg, s12_34_deg, s1234_deg, sfac

   integer(kli) :: memused,nints

   real(kdp), allocatable :: rab2(:)

   real(kdp) :: dijkl,xa,ya,za,xb,yb,zb
   logical   :: skipit

   real(kdp), allocatable :: xyzp(:,:,:), xyzq(:,:,:),&
                             Kab(:,:),Kcd(:,:)
   real(kdp) :: alphai, alphaj, alphak, alphal
   integer   :: nprimi, nprimj, nprimk, npriml
   integer   :: ncarti, ncartj, ncartk, ncartl
   integer   :: idx, jdx, kdx, ldx

   integer      :: tmax, umax, vmax
   integer(kli) :: memetuv(2)
   real(kdp)    :: dmem,dval
 
   type(cgto_info), pointer :: bfi, bfj, bfk, bfl

   real(kdp), allocatable :: eabt(:,:,:),eabu(:,:,:),eabv(:,:,:),&
                             ecdt(:,:,:),ecdu(:,:,:),ecdv(:,:,:),&
                             eabtuv(:,:,:),ecdtuv(:,:,:)
   real(kdp), allocatable :: cnij(:,:),cnkl(:,:)                         

   real(kdp), allocatable :: o2p(:,:), u(:,:), uA(:,:), uB(:,:) 

   ! for integral evaluation
   type(TBoysTable) :: boystab

   ! basis function information
   real(kdp), allocatable :: bfiexps(:,:), bficoefs(:,:), bfinorms(:,:), &
                             bfjexps(:,:), bfjcoefs(:,:), bfjnorms(:,:), &
                             bfkexps(:,:), bfkcoefs(:,:), bfknorms(:,:), &
                             bflexps(:,:), bflcoefs(:,:), bflnorms(:,:)

   integer,   allocatable :: bfila(:,:), bfima(:,:), bfina(:,:), &
                             bfjla(:,:), bfjma(:,:), bfjna(:,:), &
                             bfkla(:,:), bfkma(:,:), bfkna(:,:), &
                             bflla(:,:), bflma(:,:), bflna(:,:)

   real(kdp), allocatable :: gout(:,:),xtraf(:,:)

   integer :: nprimij,nprimkl,nprimijkl

   integer :: ndimab,ndimcd,angab,angcd

   integer   :: iprim,jprim,kprim,lprim,ijkl
   real(kdp) :: p, q, QXYZ(3), PXYZ(3), boysarg, rpq
   real(kdp), allocatable :: alpha(:),dx(:),dy(:),dz(:),pfac(:)

   real(kdp) :: bndmax,dmax
   real(kdp), allocatable :: dPshlmx(:,:)

   logical :: DoCoulomb,DoExchange

   integer :: ithrd,nthreads

   ! for timings
   real(kdp) :: cpur,cpuf,cpug1,cpug2,cpug3

   ! functions
   real(kdp) :: dist2_3d, product_center_1D, dist
   integer :: get_max_threads, get_thread_num

   !-----------------------------------------------------------------------+
   ! Define things for parrallel code 
   !-----------------------------------------------------------------------+
   nthreads = get_max_threads()

   if (mthreads .lt. nthreads) call quit("Numbers of threads for intbuf too small","gettwoeints")
 
   !-----------------------------------------------------------------------+
   ! init
   !-----------------------------------------------------------------------+
   memused = 0

   dmax = one

   DoCoulomb  = buildfock .and. (JKmode == buildJ .or. JKmode == buildJk)
   DoExchange = buildfock .and. (JKmode == buildK .or. JKmode == buildJk)

   ! integral scheme
   use_transfer = .false. !intbuf%intopt%scheme == INT_MMDET

   bfi => null()
   bfj => null()
   bfk => null()
   bfl => null()

   cpur  = zero
   cpuf  = zero 
   cpug1 = zero 
   cpug2 = zero 
   cpug3 = zero 

   !-----------------------------------------------------------------------+
   ! Calculate shell maxima of density matrix for screening
   !-----------------------------------------------------------------------+
   if (buildfock) then
     allocate(dPshlmx(nshell,nshell))
     dPshlmx = zero
     do ishl = 1, nshell
       ibfst = shells(ishl)%ibfstart; ibfnd = shells(ishl)%ibfend
       do jshl = 1, nshell
         jbfst = shells(jshl)%ibfstart; jbfnd = shells(jshl)%ibfend

         dval = zero
         do ibf = ibfst, ibfnd
           do jbf = jbfst, jbfnd
             dval = max(dval,abs(dP(ibf,jbf)))
           end do
         end do
         dPshlmx(ishl,jshl) = max(dPshlmx(ishl,jshl),dval)
       end do 
     end do
   end if

   !-----------------------------------------------------------------------+
   ! precompute centers   
   !-----------------------------------------------------------------------+
   allocate(rab2(nshell*(nshell+1)/2)) 

   do ishl = 1, nshell
     do jshl = 1, ishl
      
       xa = shells(ishl)%origin(1); xb = shells(jshl)%origin(1)
       ya = shells(ishl)%origin(2); yb = shells(jshl)%origin(2) 
       za = shells(ishl)%origin(3); zb = shells(jshl)%origin(3) 

       rab2(ishl*(ishl-1)/2 + jshl) = dist2_3d(xa, ya, za, xb, yb, zb)

     end do
   end do

   !-----------------------------------------------------------------------+
   ! allocate buffer for local basis function info 
   !-----------------------------------------------------------------------+
   allocate( bfiexps(mxprim*mxcart,0:nthreads-1), bficoefs(mxprim*mxcart,0:nthreads-1), bfinorms(mxprim*mxcart,0:nthreads-1), &
             bfjexps(mxprim*mxcart,0:nthreads-1), bfjcoefs(mxprim*mxcart,0:nthreads-1), bfjnorms(mxprim*mxcart,0:nthreads-1), &
             bfkexps(mxprim*mxcart,0:nthreads-1), bfkcoefs(mxprim*mxcart,0:nthreads-1), bfknorms(mxprim*mxcart,0:nthreads-1), &
             bflexps(mxprim*mxcart,0:nthreads-1), bflcoefs(mxprim*mxcart,0:nthreads-1), bflnorms(mxprim*mxcart,0:nthreads-1)  )

   allocate( bfila(mxcart,0:nthreads-1), bfima(mxcart,0:nthreads-1), bfina(mxcart,0:nthreads-1), &
             bfjla(mxcart,0:nthreads-1), bfjma(mxcart,0:nthreads-1), bfjna(mxcart,0:nthreads-1), &
             bfkla(mxcart,0:nthreads-1), bfkma(mxcart,0:nthreads-1), bfkna(mxcart,0:nthreads-1), &
             bflla(mxcart,0:nthreads-1), bflma(mxcart,0:nthreads-1), bflna(mxcart,0:nthreads-1)  )
  
   allocate( gout(mxcart*mxcart*mxcart*mxcart,0:nthreads-1) )

   allocate( cnij(mxprim*mxprim*mxcart*mxcart,0:nthreads-1), &
             cnkl(mxprim*mxprim*mxcart*mxcart,0:nthreads-1) )
 
   !-----------------------------------------------------------------------+
   ! allocate buffer for Integral schemes 
   !-----------------------------------------------------------------------+

   ! McMurrchie Davidson scheme additionally needs E coefficients (maybe include them also in intbuf??)
   if (intbuf(ithd0)%intopt%scheme == INT_MMD .or. intbuf(ithd0)%intopt%scheme == INT_MMDET) then
     memetuv(1) = mxprim*mxprim * (lmax+2) * (lmax+2) * (lmax+2) * 2  

     allocate(eabt(memetuv(1),mxcart*mxcart,0:nthreads-1), &
              eabu(memetuv(1),mxcart*mxcart,0:nthreads-1), &
              eabv(memetuv(1),mxcart*mxcart,0:nthreads-1), &   
              ecdt(memetuv(1),mxcart*mxcart,0:nthreads-1), &
              ecdu(memetuv(1),mxcart*mxcart,0:nthreads-1), &
              ecdv(memetuv(1),mxcart*mxcart,0:nthreads-1)  ) 
 
     allocate(o2p(mxprim*mxprim,0:nthreads-1), &
              u( mxprim*mxprim,0:nthreads-1),  &
              uA(mxprim*mxprim,0:nthreads-1),  &
              uB(mxprim*mxprim,0:nthreads-1))

     memetuv(2) = mxprim*mxprim * (lmax+2) * (lmax+2) * (lmax+2) * 8 
     allocate(eabtuv(memetuv(2),mxcart*mxcart,0:nthreads-1))
     allocate(ecdtuv(memetuv(2),mxcart*mxcart,0:nthreads-1))

   end if

   !-----------------------------------------------------------------------+
   ! read in pre-tabulated values for Boys-function 
   !-----------------------------------------------------------------------+

   call allocate_boystable(boystab)

   !-----------------------------------------------------------------------+
   ! calculate the diagonal elements (ij|ij) for Schwarz-Screening
   !-----------------------------------------------------------------------+
   bndmax = one
   if (use_schwarz) then

     if (.not. have_schwartz) &
       call getschwarz(intbuf(ithd0)%intopt, gab, boystab, lmax, ammax,        &
                       shells, rab2, intbuf(ithd0), eabt, eabu, eabv, eabtuv,  &
                       cnij, o2p, u, uA, uB, memmax, memetuv, nbf, nshell,     &
                       mxcart, mxprim)    

     bndmax = maxval(gab)
     if (buildfock) bndmax = bndmax * (half * 4.0 * maxval(dPshlmx))**2

   else
     gab = 99d+9  
   end if
   nscreen = 0
   nints   = nbf*nbf*nbf*nbf 

   !-----------------------------------------------------------------------+
   ! Prepare transformation matrix from cartesian to solid harmonics 
   !-----------------------------------------------------------------------+
   if (.not.use_cao) then
     allocate(xtraf(2*lmax+1,(lmax+1)*(lmax+2)/2))
     call trn2sphr(xtraf,lmax)  
   end if

   !-----------------------------------------------------------------------+
   ! calculate integrals (ij|kl) 
   !-----------------------------------------------------------------------+

   !$OMP PARALLEL DO SCHEDULE(AUTO) &
   !$OMP DEFAULT(NONE) & 
   !$OMP SHARED(shells,bndmax,gab,tolint,nshell,memetuv,gout,mxcart,&
   !$OMP        dPshlmx,dP,buildfock,use_cao,use_transfer,luhf,&
   !$OMP        docoulomb,doexchange,rab2,twoeint,fock, &
   !$OMP        bfiexps,bficoefs,bfinorms,bfila,bfima,bfina, &  
   !$OMP        bfjexps,bfjcoefs,bfjnorms,bfjla,bfjma,bfjna, &
   !$OMP        bfkexps,bfkcoefs,bfknorms,bfkla,bfkma,bfkna, &
   !$OMP        bflexps,bflcoefs,bflnorms,bflla,bflma,bflna, &
   !$OMP        eabt,eabu,eabv,eabtuv,cnij, &
   !$OMP        ecdt,ecdu,ecdv,ecdtuv,cnkl, &
   !$OMP        o2p,u,uA,uB,boystab,intbuf,JKmode) &
   !$OMP PRIVATE(ithrd,ishl,jshl,kshl,lshl,ibfst,lshlmax,skipit,&
   !$OMP         dijkl,dmax,xyzp,xyzq,Kab,Kcd,bfi,bfj,bfk,bfl,&
   !$OMP         s12_deg,s34_deg,s12_34_deg,s1234_deg,dval,sfac,&
   !$OMP         pfac,alpha,dx,dy,dz,angab,angcd,ndimab,ndimcd,& 
   !$OMP         ibfnd,nprimi,ncarti,jbfst,jbfnd,nprimj,ncartj,&
   !$OMP         kbfst,kbfnd,nprimk,ncartk,lbfst,lbfnd,npriml,ncartl,&
   !$OMP         ibf,jbf,lbf,kbf,nprimij,nprimkl,nprimijkl,ind) &
   !$OMP REDUCTION(+: cpur,cpuf,cpug1,cpug2,cpug3,nscreen) 
   do ishl = 1, nshell

     ithrd = get_thread_num() 
  
     ibfst = shells(ishl)%ibfstart; ibfnd = shells(ishl)%ibfend
     nprimi = shells(ishl)%nprim; ncarti = shells(ishl)%ncart
     call copybasinfo(bfiexps(1,ithrd), bficoefs(1,ithrd), bfinorms(1,ithrd), &
                      bfila(1,ithrd), bfima(1,ithrd), bfina(1,ithrd), shells, ishl, nprimi, ncarti, nshell)

     skipit = .false.  ! Reset logic to remove shell quadruples at an early stage

     do jshl = 1, ishl
       jbfst = shells(jshl)%ibfstart; jbfnd = shells(jshl)%ibfend
       nprimj = shells(jshl)%nprim; ncartj = shells(jshl)%ncart
       call copybasinfo(bfjexps(1,ithrd), bfjcoefs(1,ithrd), bfjnorms(1,ithrd), &
                        bfjla(1,ithrd), bfjma(1,ithrd), bfjna(1,ithrd), shells, jshl, nprimj, ncartj, nshell)

       skipit = ( sqrt(bndmax) * sqrt(gab(ishl,jshl)) .lt. tolint ) ! Check if shell pair (ij| will not form any 
                                                                    ! contribution with possible shell pairs |kl)...

       ! calculate the center of the shell pair primitives
       allocate(xyzp(3,nprimi,nprimj),Kab(nprimi,nprimj))
       if (.not.skipit) then
         call GetShellPairInfo(intbuf(ithrd)%intopt, xyzp, Kab, &
                               eabt(1,1,ithrd), eabu(1,1,ithrd), eabv(1,1,ithrd), eabtuv(1,1,ithrd), &
                               cnij(1,ithrd), shells, rab2, ishl, jshl, &
                               o2p(1,ithrd),u(1,ithrd),uA(1,ithrd),uB(1,ithrd),&
                               nprimi, nprimj, nshell, memetuv, ncarti, ncartj)
       end if

       do kshl = 1, ishl
         kbfst = shells(kshl)%ibfstart; kbfnd = shells(kshl)%ibfend
         nprimk = shells(kshl)%nprim; ncartk = shells(kshl)%ncart
         if (.not.skipit) &
           call copybasinfo(bfkexps(1,ithrd), bfkcoefs(1,ithrd), bfknorms(1,ithrd), bfkla(1,ithrd), &
                            bfkma(1,ithrd), bfkna(1,ithrd), shells, kshl, nprimk, ncartk, nshell)

         if (kshl == ishl) then; lshlmax = jshl; else; lshlmax = kshl; end if 

         do lshl = 1, lshlmax
           lbfst = shells(lshl)%ibfstart; lbfnd = shells(lshl)%ibfend
           npriml = shells(lshl)%nprim; ncartl = shells(lshl)%ncart
           if (.not.skipit) &
             call copybasinfo(bflexps(1,ithrd), bflcoefs(1,ithrd), bflnorms(1,ithrd), &
                              bflla(1,ithrd), bflma(1,ithrd), bflna(1,ithrd), shells, lshl, npriml, ncartl, nshell)

           ! calculate integral estimate (Schwarz-Screening)
           dijkl = dsqrt(gab(ishl,jshl)) * dsqrt(gab(lshl,kshl)) 
          
           if (buildfock) then
             ! the half comes from the fact that a factor of 2 is absorbed in the density (for RHF only!!)
             if (JKmode == buildJK) then
               dmax =  half * max(4*dPshlmx(ishl,jshl),4*dPshlmx(kshl,lshl), &  
                                    dPshlmx(ishl,kshl),  dPshlmx(ishl,lshl), &  
                                    dPshlmx(jshl,kshl),  dPshlmx(jshl,lshl))    
             elseif (JKmode == buildJ) then
               dmax =  half * max(4*dPshlmx(ishl,jshl),4*dPshlmx(kshl,lshl)) 

             elseif (JKmode == buildK) then
               dmax =  half * max(  dPshlmx(ishl,kshl),  dPshlmx(ishl,lshl), &  
                                    dPshlmx(jshl,kshl),  dPshlmx(jshl,lshl))    
             end if
             dijkl = dijkl * dmax 
           end if

           if (dijkl >= tolint ) then

             ! sanity check
             if (skipit) call quit("Something went wrong in the integral screening", "gettwoeints")

             ! calculate the center of the shell pair primitives
             allocate(xyzq(3,nprimk,npriml),Kcd(nprimk,npriml))

             call GetShellPairInfo(intbuf(ithrd)%intopt, xyzq, Kcd, &
                                   ecdt(1,1,ithrd), ecdu(1,1,ithrd), ecdv(1,1,ithrd), ecdtuv(1,1,ithrd), &
                                   cnkl(1,ithrd), shells, rab2, kshl, lshl, &
                                   o2p(1,ithrd),u(1,ithrd),uA(1,ithrd),uB(1,ithrd),&
                                   nprimk, npriml, nshell, memetuv, ncartk, ncartl)

             !-----------------------------------------------------------------------------------+ 
             ! calculate the Boys function for all possible shell quadruples
             !-----------------------------------------------------------------------------------+ 

             ! just reference the first basis function in the shells 
             bfi => shells(ishl)%gtos(1); bfj => shells(jshl)%gtos(1)
             bfk => shells(kshl)%gtos(1); bfl => shells(lshl)%gtos(1)

             nprimij = bfi%nprim * bfj%nprim
             nprimkl = bfk%nprim * bfl%nprim
             nprimijkl = nprimij * nprimkl

             allocate(pfac(nprimijkl),alpha(nprimijkl),dx(nprimijkl),dy(nprimijkl),dz(nprimijkl))

             call  GetShellQuadInfo(intbuf(ithrd)%intopt,boystab,           &  ! control flag and boys
                                    intbuf(ithrd)%Fn,pfac,alpha,dx,dy,dz,   &  ! out
                                    shells,ishl,jshl,kshl,lshl,             &  ! inp
                                    nprimij,nprimkl,nprimijkl,              &  ! dim
                                    intbuf(ithrd)%memboys,nshell)              ! dim


             if ( intbuf(ithrd)%intopt%scheme == INT_MMD .or. intbuf(ithrd)%intopt%scheme == INT_MMDET ) then
               ! McMurchie Davidson scheme

               ! When using electron transfer relation
               ndimab = 1; ndimcd = 1
               if (intbuf(ithrd)%intopt%scheme == INT_MMDET) then
                 angab = shells(ishl)%angm + shells(jshl)%angm 
                 angcd = shells(kshl)%angm + shells(lshl)%angm 
                 ndimab = (angab+1)*(angab+2)/2
                 ndimcd = (angcd+1)*(angcd+2)/2
               end if

               call mmd_eri(gout(1,ithrd),&
                            bfiexps(1,ithrd),bficoefs(1,ithrd),bfinorms(1,ithrd),&
                            shells(ishl)%origin,bfila(1,ithrd),bfima(1,ithrd),bfina(1,ithrd),   & 
                            bfjexps(1,ithrd),bfjcoefs(1,ithrd),bfjnorms(1,ithrd),&
                            shells(jshl)%origin,bfjla(1,ithrd),bfjma(1,ithrd),bfjna(1,ithrd),   &
                            bfkexps(1,ithrd),bfkcoefs(1,ithrd),bfknorms(1,ithrd),&
                            shells(kshl)%origin,bfkla(1,ithrd),bfkma(1,ithrd),bfkna(1,ithrd),   &
                            bflexps(1,ithrd),bflcoefs(1,ithrd),bflnorms(1,ithrd),&
                            shells(lshl)%origin,bflla(1,ithrd),bflma(1,ithrd),bflna(1,ithrd),   &
                            eabtuv(1,1,ithrd), cnij(1,ithrd),ecdtuv(1,1,ithrd), cnkl(1,ithrd), &
                            use_transfer,ndimab,ndimcd,&
                            intbuf(ithrd),pfac,alpha,dx,dy,dz,&   
                            nprimi,nprimj,nprimk,npriml,&
                            ncarti,ncartj,ncartk,ncartl,&
                            nprimij,nprimkl,nprimijkl,&
                            cpur,cpuf,cpug1,cpug2,cpug3,memetuv(2))
            
             elseif ( intbuf(ithrd)%intopt%scheme == INT_OS ) then
               ! Obara-Saika scheme
               call os_eri_shell(gout(1,ithrd),&
                                 bfiexps(1,ithrd),bficoefs(1,ithrd),bfinorms(1,ithrd),&
                                 shells(ishl)%origin,bfila(1,ithrd),bfima(1,ithrd),bfina(1,ithrd), &
                                 bfjexps(1,ithrd),bfjcoefs(1,ithrd),bfjnorms(1,ithrd),&
                                 shells(jshl)%origin,bfjla(1,ithrd),bfjma(1,ithrd),bfjna(1,ithrd), &
                                 bfkexps(1,ithrd),bfkcoefs(1,ithrd),bfknorms(1,ithrd),&
                                 shells(kshl)%origin,bfkla(1,ithrd),bfkma(1,ithrd),bfkna(1,ithrd), &
                                 bflexps(1,ithrd),bflcoefs(1,ithrd),bflnorms(1,ithrd),&
                                 shells(lshl)%origin,bflla(1,ithrd),bflma(1,ithrd),bflna(1,ithrd), &
                                 nprimi,nprimj,nprimk,npriml,ncarti,ncartj,ncartk,ncartl,rab2,&
                                 ibf,jbf,kbf,lbf,ishl,jshl,kshl,lshl,xyzp,xyzq,Kab,Kcd,&
                                 intbuf(ithrd)%vrr_terms,intbuf(ithrd)%maxmtot,intbuf(ithrd)%maxam,intbuf(ithrd)%nbuf,&
                                 boystab%fntab,boystab%nxvals,boystab%nrange,boystab%kmax,nshell)

             elseif ( intbuf(ithrd)%intopt%scheme == INT_HUZ ) then
               ! Huzinaga type scheme (extremely slow...)
               call huz_eri(gout(1,ithrd), &
                            bfiexps(1,ithrd),bficoefs(1,ithrd),bfinorms(1,ithrd),&
                            shells(ishl)%origin,bfila(1,ithrd),bfima(1,ithrd),bfina(1,ithrd), &
                            bfjexps(1,ithrd),bfjcoefs(1,ithrd),bfjnorms(1,ithrd),&
                            shells(jshl)%origin,bfjla(1,ithrd),bfjma(1,ithrd),bfjna(1,ithrd), &
                            bfkexps(1,ithrd),bfkcoefs(1,ithrd),bfknorms(1,ithrd),&
                            shells(kshl)%origin,bfkla(1,ithrd),bfkma(1,ithrd),bfkna(1,ithrd), &
                            bflexps(1,ithrd),bflcoefs(1,ithrd),bflnorms(1,ithrd),&
                            shells(lshl)%origin,bflla(1,ithrd),bflma(1,ithrd),bflna(1,ithrd), &
                            nprimi,nprimj,nprimk,npriml, &
                            ncarti,ncartj,ncartk,ncartl) 

             end if

             deallocate(xyzq,Kcd)
             deallocate(pfac,alpha,dx,dy,dz)
           else
             !gout = zero
             nscreen = nscreen + ncarti*ncartj*ncartk*ncartl
             call dcopy(ncarti*ncartj*ncartk*ncartl,zero,0,gout(1,ithrd),1)
           end if

           if (buildfock) then
          
             if (.not.use_cao) call quit("Use of solid harmonics not yet supported for Fock build!","gettwoeints")
 
             !---------------------------------------------------------------+
             !  1) each shell set of integrals contributes up to 
             !     6 shell sets of the Fock matrix:
             !     F(a,b) += (ab|cd) * D(c,d)
             !     F(c,d) += (ab|cd) * D(a,b)
             !     F(b,d) -= 1/4 * (ab|cd) * D(a,c)
             !     F(b,c) -= 1/4 * (ab|cd) * D(a,d)
             !     F(a,c) -= 1/4 * (ab|cd) * D(b,d)
             !     F(a,d) -= 1/4 * (ab|cd) * D(b,c)
             !  2) each permutationally-unique integral (shell set) must be
             !     scaled by its degeneracy, i.e. the number of the 
             !     integrals/sets equivalent to it
             !  3) the end result must be symmetrized
             !---------------------------------------------------------------+

             ! determine degeneracy for scaling
             if (ishl == jshl) then; s12_deg = 1.0; else; s12_deg = 2.0; end if 
             if (kshl == lshl) then; s34_deg = 1.0; else; s34_deg = 2.0; end if

             if (ishl == kshl) then
               if (jshl == lshl) then; s12_34_deg = 1.0; else; s12_34_deg = 2.0; end if
             else
               s12_34_deg = 2.0
             end if
             s1234_deg = s12_deg * s34_deg * s12_34_deg

             do lcart = 1, ncartl
               lbf = lcart + lbfst - 1
               do kcart = 1, ncartk
                 kbf = kcart + kbfst - 1
                 do jcart = 1, ncartj
                   jbf = jcart + jbfst - 1
                   do icart = 1, ncarti
                     ibf = icart + ibfst - 1
    
                     ind = (lcart-1) * ncartk * ncartj * ncarti &
                         + (kcart-1) * ncartj * ncarti          &
                         + (jcart-1) * ncarti + icart

                     dval = s1234_deg * gout(ind,ithrd)

                     ! ToDo This Critical section is rather large

                     !$OMP CRITICAL

                     ! See Almlof, Faegri, Korsell, 1981
                     !  -> Coulomb, Eq (4a,4b) of Korsell, 1981
                     if (DoCoulomb) then
                       fock(ibf,jbf) = fock(ibf,jbf) + dP(kbf,lbf) * dval
                       fock(kbf,lbf) = fock(kbf,lbf) + dP(ibf,jbf) * dval
                     end if
 
                     ! Exchange, Eq (5) of Korsell, 1981 
                     if (DoExchange) then
                       sfac = 0.25d0
                       if (luhf) sfac = 0.5d0
                       fock(jbf,lbf) = fock(jbf,lbf) - sfac * dP(ibf,kbf) * dval 
                       fock(jbf,kbf) = fock(jbf,kbf) - sfac * dP(ibf,lbf) * dval 
                       fock(ibf,kbf) = fock(ibf,kbf) - sfac * dP(jbf,lbf) * dval 
                       fock(ibf,lbf) = fock(ibf,lbf) - sfac * dP(jbf,kbf) * dval 
                     end if

                     !$OMP END CRITICAL

                   end do
                 end do 
               end do
             end do

           else
             !---------------------------------------------------------------+
             ! copy local integral for shell quadruple to full set
             !---------------------------------------------------------------+
             if (use_cao) then
               call addintegralblock(twoeint,gout(1,ithrd),ibfst,jbfst,kbfst,lbfst,ncarti,ncartj,ncartk,ncartl,mxcart)
             else 
               ! transform integral batch in gout to a solid harmonic basis
               call quit("Use of solid harmonics not yet supported!","gettwoeints")
             end if
           end if

         end do
       end do
       deallocate(xyzp,Kab)
     end do
   end do 
   !$OMP END PARALLEL DO

   bfi => null()
   bfj => null()
   bfk => null()
   bfl => null()

   if (locdbg) write(6,'(5x,a,f8.2)') 'Screening ratio (%)', real(nscreen,kdp) / real(nints,kdp) * 100.d0

   if (intbuf(ithd0)%intopt%scheme == INT_MMD .and. .not. buildfock .and. .not. silent) then
     write(6,'(/3x,a)')  'Profiling:'
     write(6,'(5x,a,f10.2)')  'R(t,u,v) CPU-time (sec)    : ', cpur
     write(6,'(5x,a,f10.2)')  'F(t,u,v) CPU-time (sec)    : ', cpuf
     write(6,'(5x,a,f10.2)')  'Cont. E1 CPU-time (sec)    : ', cpug1
     write(6,'(5x,a,f10.2)')  'Cont. Pr CPU-time (sec)    : ', cpug2
     write(6,'(5x,a,f10.2)')  'Cont. E2 CPU-time (sec)    : ', cpug3
   end if

   deallocate(rab2)

   call deallocate_boystable(boystab)

end subroutine

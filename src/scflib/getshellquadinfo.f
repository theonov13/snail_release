
subroutine GetShellQuadInfo(intopt,boystab,              &      ! control flag and boys
                            Fn,pfac,alpha,dx,dy,dz,      &      ! out
                            shells,ishl,jshl,kshl,lshl,  &      ! inp
                            nprimij,nprimkl,nprimijkl,   &      ! dim
                            memboys,nshell)                     ! dim
!------------------------------------------------------------------------+
!  Purpose: Precomputes shell information for different integral schemes
!
!------------------------------------------------------------------------+

   use type_integ
   use type_cgto
   use type_shell

   use BoysTable

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'
   include 'constants.h'

   ! dimensions
   integer,      intent(in) :: nprimij, nprimkl, nprimijkl, nshell
   integer(kli), intent(in) :: memboys

   ! output
   real(kdp), intent(out) :: Fn(memboys)
   real(kdp), intent(out) :: pfac(nprimijkl),alpha(nprimijkl),&
                             dx(nprimijkl),dy(nprimijkl),dz(nprimijkl)

   ! input
   type(integ_info), intent(in)   :: intopt
   type(shell_info), intent(in)   :: shells(nshell)
   integer,          intent(in)   :: ishl,jshl,kshl,lshl

   type(TBoysTable), intent(in)   :: boystab


   ! local
   integer   :: iprim, jprim, kprim, lprim,&
                ijkl, ammax
   real(kdp) :: p, q, Qxyz(3), Pxyz(3), boysarg, rpq, normijkl
 
   type(cgto_info), pointer :: bfi, bfj, bfk, bfl

   ! functions
   real(kdp) :: dist


   if (intopt%scheme == INT_MMD .or. intopt%scheme == INT_MMDET) then

     !-----------------------------------------------------------------------------------+ 
     ! calculate the Boys function for all possible shell quadruples
     !-----------------------------------------------------------------------------------+ 

     ! just reference the first basis function in the shells 
     bfi => shells(ishl)%gtos(1); bfj => shells(jshl)%gtos(1)
     bfk => shells(kshl)%gtos(1); bfl => shells(lshl)%gtos(1)

     ! maximum angular momentum to generate the required integrals
     !ammax = bfi%la + bfj%la + bfk%la + bfl%la + bfi%ma + bfj%ma + bfk%ma + bfl%ma + bfi%na + bfj%na + bfk%na + bfl%na
     ammax = shells(ishl)%angm + shells(jshl)%angm + shells(kshl)%angm + shells(lshl)%angm
     do iprim = 1, bfi%nprim
       do jprim = 1, bfj%nprim
         p = bfi%exps(iprim) + bfj%exps(jprim)
         call product_center(Pxyz,bfi%exps(iprim),bfi%origin,bfj%exps(jprim),bfj%origin)
         do kprim = 1, bfk%nprim
           do lprim = 1, bfl%nprim

             ijkl = (lprim-1) * bfi%nprim * bfj%nprim * bfk%nprim + & 
                    (kprim-1) * bfi%nprim * bfj%nprim + &
                    (jprim-1) * bfi%nprim + iprim 

             q = bfk%exps(kprim) + bfl%exps(lprim)
             alpha(ijkl) = p*q / (p+q)
             call product_center(Qxyz,bfk%exps(kprim),bfk%origin,bfl%exps(lprim),bfl%origin)
             rpq = dist(Pxyz,Qxyz)
             boysarg = alpha(ijkl) * rpq * rpq
             call BoysFArr(ammax,boysarg,Fn((ijkl-1)*(ammax+1) + 1), &
                           boystab%fntab,.true.,boystab%nxvals,boystab%nrange,boystab%kmax)
             
             !GS
             if (intopt%scheme == INT_MMDET) then
               normijkl = bfi%norms(iprim) * bfj%norms(jprim) * bfk%norms(kprim) * bfl%norms(lprim)
               call dscal(ammax,normijkl,Fn((ijkl-1)*(ammax+1) + 1),1)
             end if
             !GS
 
             dx(ijkl) = Pxyz(1)-Qxyz(1)
             dy(ijkl) = Pxyz(2)-Qxyz(2)
             dz(ijkl) = Pxyz(3)-Qxyz(3)
             pfac(ijkl) = two * pi**(2.5d0) / ( p * q * dsqrt (p+q) )
           end do
         end do
       end do
     end do

  else
    !write(6,*) 'No common intermediates'
  end if

end subroutine

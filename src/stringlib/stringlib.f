!==============================================================================
! Purpose:
! --------
!
!  routines for manipulating character strings.
!
!  Implemented:
!    + string_split:      splits string into substrings according to a delimiter
!    + string_2upper:     converts whole string in upper case string aBc -> ABC
!    + string_2lower:     converts whole string in lower case string aBc -> abc
!    + string_isnumeric:  checks if a string contains a numeric value
!    + string_replace:    replaces a substring in a string
!
!==============================================================================

subroutine string_split(nout,strout,strin,cdelimiter,ncin,ncout,nmax)
  implicit none

  ! dimensions
  integer, intent(in) :: ncin,ncout,nmax

  ! output
  integer,              intent(out) :: nout
  character(len=ncout), intent(out) :: strout(nmax)

  ! input
  character(len=ncin),  intent(in) :: strin
  character,            intent(in) :: cdelimiter

  ! local
  integer :: pos1,pos2,n
  character(len=ncin) :: slocal

  ! init
  pos1 = 1; n = 0

  slocal = trim(strin)

  do
    pos2 = index(slocal(pos1:), cdelimiter)
    if (pos2 == 0) then
       n = n + 1
       strout(n) = slocal(pos1:)
       exit
    end if
    n = n + 1
    strout(n) = slocal(pos1:pos1+pos2-2)
    pos1 = pos2+pos1

    if (n    >= nmax) exit
    if (pos1 >= ncin) exit 
 end do
 
 nout = n
end subroutine

subroutine string_2upper(str)
  implicit none

  character(*), intent(inout) :: str
  integer :: i

  do i = 1, len(str)
    select case(str(i:i))
      case("a":"z")
        str(i:i) = achar(iachar(str(i:i))-32)
    end select
  end do 
end subroutine string_2upper
 
subroutine string_2lower(str)
  implicit none

  character(*), intent(inout) :: str
  integer :: i

  do i = 1, len(str)
    select case(str(i:i))
      case("A":"Z")
        str(i:i) = achar(iachar(str(i:i))+32)
    end select
  end do  
end subroutine string_2lower

function string_isnumeric(str) result(is_numeric)
  implicit none

  ! output
  logical :: is_numeric

  ! input
  character(len=*), intent(in) :: str
  
  ! local
  real :: x
  integer :: e

  read(str,*,iostat=e) x
  is_numeric = e == 0

  return 
end function string_isnumeric


function string_replace (str,sub,rep)  result(sout)
  implicit none

  ! intput
  character(len=*) :: str,sub,rep 

  ! output
  character(len(str)+len(rep)) :: sout    ! provide extra spaces

  ! local
  integer :: i, nt, nr

  sout = str ; nt = len_trim(sub) ; nr = len_trim(rep)
  do
    i = index(sout,sub(:nt)) ; if (i == 0) exit
    sout = sout(:i-1) // rep(:nr) // sout(i+nt:)
  end do 

end function string_replace 

integer function string_count(str,text)
  implicit none

  character(len=*) :: str, text

  ! local
  integer :: i, nt

  string_count = 0; nt = len_trim(text)
  do i = 1, len(str) - nt + 1
    if (str(i:i+nt-1) == text(:nt)) string_count = string_count + 1
  end do

end function string_count

subroutine cleangradient(grad,coord,amass,ncoord,natoms)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: ncoord, natoms

  ! input
  real(kdp), intent(in) :: coord(3,natoms), amass(natoms)
 
  ! input/output
  real(kdp), intent(inout) :: grad(ncoord)

  ! local
  real(kdp), allocatable :: proj(:,:),tmp(:)

  allocate(proj(ncoord,ncoord),tmp(ncoord))

  call buildproj(proj,coord,amass,natoms,ncoord)

  call dcopy(ncoord,grad,1,tmp,1)

  call dsymv('l',ncoord,one,proj,ncoord,tmp,1,zero,grad,1)

end subroutine

subroutine cleanhessian(hess,coord,amass,ncoord,natoms)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in)    :: ncoord, natoms

  ! input
  real(kdp), intent(in)    :: coord(3,natoms), amass(natoms)
 
  ! input/output
  real(kdp), intent(inout) :: hess(ncoord,ncoord)

  ! local
  real(kdp), allocatable :: proj(:,:),tmp(:,:)

  allocate(proj(ncoord,ncoord))

  call buildproj(proj,coord,amass,natoms,ncoord)

  allocate(tmp(ncoord,ncoord)) 

  call dsymm('l','l',ncoord,ncoord,one,proj,ncoord,hess,ncoord,zero,tmp,ncoord)
  call dsymm('r','l',ncoord,ncoord,one,proj,ncoord,tmp,ncoord,zero,hess,ncoord)

  deallocate(proj,tmp)
 
end subroutine

subroutine rdhess(fc,filhess,ncoord)
  
  use mod_fileutil

  implicit none

  include 'fortrankinds.h'

  ! dimensions
  integer, intent(in) :: ncoord

  ! input
  character(len=80), intent(in) :: filhess

  ! output
  real(kdp), intent(out) :: fc(ncoord*(ncoord+1)/2)

  ! local
  integer :: i,j,ic,maxcol,mincol,k,kc
  integer :: iscrfl
  
  character(len=80) :: sdum

  iscrfl = openfile(filhess,'old')

  read(iscrfl,'(a)') sdum
  do i = 1, ncoord
    ic = 0
    maxcol = 0
    do while (maxcol < ncoord)
      mincol = maxcol+1
      maxcol = min(maxcol+5,ncoord)
      ic = ic + 1
      read(iscrfl,'(i3,i2,5f15.10)') k,kc,(fc((max(i,j)-3)*(max(i,j))/2+i+j),j=mincol,maxcol)
    end do  
  end do

  !write(iscrfl,'(a)') '$end'

  call closefile(iscrfl)


end subroutine

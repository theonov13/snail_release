subroutine freqana(hess,coord,amass,ncoord,natoms)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! constants
  real(kdp), parameter :: sqrtau2amu = 42.6952981642285
  real(kdp), parameter :: au2wav = 219474.63067

  ! dimensions
  integer,   intent(in) :: ncoord,natoms
  real(kdp), intent(in) :: hess(ncoord*(ncoord+1)/2)
  real(kdp), intent(in) :: coord(3,natoms),amass(natoms) 

  ! local
  real(kdp), allocatable :: fullhess(:,:),eig(:),work(:)
  integer   :: info, lwork, imode
  real(kdp) :: dlen,wav

  allocate(fullhess(ncoord,ncoord),eig(ncoord))

  !------------------------------------------------------------+
  ! Conversion, cleaning and printing
  !------------------------------------------------------------+

  ! expand hessian in trigonal form to full form
  call htosq(ncoord,fullhess,hess)

  ! Remove rotation and translation
  call cleanhessian(fullhess,coord,amass,ncoord,natoms)

  ! copy projected hessian back
  call sqtoh(ncoord,fullhess,hess)

  ! save projected hessian
  write(6,'(/3x,a,/)') 'I write hessian to file "hessian" in Turbomole format'
  call wrthess(hess,ncoord)

  !------------------------------------------------------------+
  ! Normalmode analysis
  !------------------------------------------------------------+

  ! first run to get dimension for work
  lwork = -1
  call dsyev( 'V', 'U', ncoord, fullhess, ncoord, eig, dlen, lwork, info)

  ! allocate memory for work
  lwork = int(dlen);
  allocate(work(lwork))

  ! do eigen decomposition
  call dsyev( 'V', 'U', ncoord, fullhess, ncoord, eig, work, lwork, info)

  ! print results
  write(6,'(/3x,a,/)') 'The harmonic frequencies are:'
  do imode = 1, ncoord
      if (eig(imode) > 0.0) then
        wav =  sqrt( eig(imode)) * au2wav / sqrtau2amu 
      else
        wav = -sqrt(-eig(imode)) * au2wav / sqrtau2amu 
      end if

      write(6,'(5x,i5,1x,f13.2,1x,a)') imode, wav, 'cm^-1'

  end do

  deallocate(work,fullhess,eig)

end subroutine

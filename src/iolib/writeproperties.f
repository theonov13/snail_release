subroutine writeproperties(escf)
!-----------------------------------------------------------------+
!  Purpose:
!  --------
!
!  Writes calculated properties to file 
!
!-----------------------------------------------------------------+

  use mod_fileutil

  implicit none

  include 'fortrankinds.h'

  ! constants
  character(len=80), parameter :: fprop  = 'properties_snail.txt'

  ! input
  real(kdp), intent(in) :: escf

  ! local
  integer :: ifil

  ifil = openfile(fprop,'unknown')

  write(ifil,*) '$scf_energy', escf 

  call closefile(ifil)

end subroutine



 subroutine ReadBas(fbas,iatom,atom,sbasname,memmax,memused,&
                    lstshls,nbf,nsbf,nshell,lmax,mxprim,mxcart,nmaxshls)
!======================================================================================
! Purpose: Reads basis set from file
!
!   input: 
!           fbas = filname containing the basis set
!           atom = atom for which the basis set is read in
!
!   output: 
!           nbf    = number of contraacted cartesian GTOs/basis functions
!           nsbf   = number of contraacted spherical GTOs/basis functions
!
!  Comment:
!    
!    This routine is far from being optimal. I wrote it fast and made it work,
!    but it is not fool-proof. Small differences in the format of the basis set
!    files will probably cause errors....
!
!
!======================================================================================
   use type_pgto
   use type_cgto
   use type_atom
   use type_shell
   use mod_fileutil

   implicit none

   include 'fortrankinds.h'
   include 'stdout.h'

   ! constants
   integer, parameter :: maxlines = 10000
   integer, parameter :: nstrlen  = 80
   integer, parameter :: narrlen  = 20

   character, parameter :: cdelim = " "

   ! dimensions
   integer, intent(in) :: nmaxshls

   ! input
   integer,           intent(in) :: iatom
   type(atom_info),   intent(in) :: atom
   character(len=80), intent(in) :: fbas
   character(len=*),  intent(in) :: sbasname
   integer(kli),      intent(in) :: memmax

   ! input/output
   integer,          intent(inout) :: nbf,nsbf,nshell,lmax,mxprim,mxcart             
   type(shell_info), intent(inout) :: lstshls(nmaxshls)
   integer(kli),     intent(inout) :: memused

   ! local
   integer :: iunbas, iline, ibas, nbas,ibf,jbf,iprim,i,j,&
              nx,ny,nz,iang,ncart,nsout,ios,irun,nrun,atnum,&
              ishell,nprim,ifound
   character(len=3)  :: satom
   character         :: ltyp
   logical           :: isfound
   real(kdp)         :: expo,coeff,xyz(3)
 

   real(kdp), allocatable :: exps(:),coefs(:)


   ! for handling strings
   character(len=nstrlen) :: sline, sbas, cdum
   character(len=narrlen) :: sarr(5)
   character(len=10)      :: sbasis
   character(len=20)      :: gtotype

   ! init
   atnum = -99

   ! open basis set file
   iunbas = openfile(fbas,'old')

   ! get number of basis sets contained in file
   nrun = 0
   do 
     read(iunbas,*,iostat=ios) sline
     if (ios /= 0) exit
     if (sline(1:6) == "$basis") then
       nrun = nrun + 1
     end if
   end do
   rewind(iunbas)
   read(iunbas,*,iostat=ios) sline

   isfound = .false.

   do irun = 1, nrun
     if (isfound) exit

     read(iunbas,'(a)',iostat=ios) sline; if (ios < 0) exit
 
     sline = trim(adjustl(sline))

     if (sline(1:6) == "$basis") then
       ! the nex two line have to be the 
       ! atom name and the name of the basis set
       read(iunbas,'(a)') sline
       if (sline(1:5) == "%atom") then
         call string_split(nsout,sarr,sline,cdelim,nstrlen,narrlen,5)
         satom = sarr(2)
         atnum = at_getatnum(satom)
       else
         write(istdout,*) "File: ", fbas
         call quit("atom name missing","readbas")
       end if
       read(iunbas,'(a)') sline
       if (sline(1:5) == "%name") then
         sbas = trim(adjustl(sline(6:)))
         
         ! check if we found the desired basis set
         if (trim(sbasname) == sbas .and. atnum == atom%atono) then
           isfound = .true.
         else
           do while (sline(1:4).ne."$end")
             read(iunbas,*) sline
           end do
           
           cycle
         end if
       else
         write(istdout,*) "File: ", fbas
         call quit("Basis set name missing","readbas")
       end if

       ! read in basis functions
       iline = 1
       do while (sline(1:4).ne."$end") 
         ! read in number of basis function for given angular momentum
         read(iunbas,*) nbas, ltyp
         ! read in exponents and contraction coefficients
         allocate(exps(nbas),coefs(nbas))
         do ibas = 1, nbas
           read(iunbas,*) exps(ibas),coefs(ibas)
         end do
         !---------------------------------------------------------------------+
         ! generate new contracted GTO(s)
         !---------------------------------------------------------------------+
         gtotype = "spdfghijklmnopqrstuwvxyz"

         ! convert to lower case
         call string_2lower(ltyp) 

         ! find index in gtotype. The angular momentum is the index - 1
         ifound = index(gtotype,ltyp)

         if (ifound > 0) then
           iang = ifound - 1
         else
           write(6,*) 'ltyp ', ltyp
           call quit('Problem reading angular momentum','readbas') 
         end if

         !---------------------------------------------------------------------+
         ! add shell of basis functions
         !---------------------------------------------------------------------+
         nshell = nshell + 1
         lmax   = max(lmax,iang)
         mxprim = max(mxprim,nbas)
         sbasis = trim(adjustl(sbasname))
         call allocate_shell(lstshls(nshell),iang,iatom,satom,atom,sbasis,exps,coefs,atom%xyz,nbf,nsbf,nbas)
         mxcart = max(mxcart,lstshls(nshell)%ncart)

         deallocate(exps,coefs)

         iline = iline + 1
         ! emergency exit
         if (iline.gt.maxlines) exit

         read(iunbas,*,iostat=ios) sline 
         if (ios < 0) then
           call quit("unexpected end of file",'readbas')
         end if

         ! go one line back
         backspace(iunbas)
 
       end do


     else
       write(istdout,*) "File: ", fbas
       call quit("Datagroup $basis is missing","readbas")
     end if
   end do

   ! close file containing basis set
   call closefile(iunbas) 

 end subroutine



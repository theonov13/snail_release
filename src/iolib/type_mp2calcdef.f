module type_mp2calcdef


 implicit none

  private

  include 'fortrankinds.h'
  include 'numbers.h'

  type mp2_calcdef

    logical   :: use_ri   = .false.
    logical   :: use_ovlp = .false.
    logical   :: use_eig  = .false.

    logical   :: use_frozen_core = .false. ! Should we use the Frozen core approximation
    real(kdp) :: freeze_point    = -3.0d0  ! Freezing point: All orbitals below are not included in correlation treatment
 
  end type

  public :: mp2_calcdef

!------------------------------------------------------------------------------!
!  contains
!------------------------------------------------------------------------------!

end module

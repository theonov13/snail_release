module type_scfcalcdef


 implicit none

  private

  include 'fortrankinds.h'
  include 'numbers.h'

  type scf_calcdef

    integer   :: maxiter  = 100 
    
    real(kdp) :: scftol = 1.d-5 
    
    logical   :: ldirect  = .false.  
    logical   :: use_diis = .true.
  
    real(kdp) :: dampst   = 0.9d0
    real(kdp) :: dampnd   = 0.4d0
    real(kdp) :: dampstep = 0.05d0

    character(len=5) :: cguess = "GWH"

  end type

  public :: scf_calcdef

!------------------------------------------------------------------------------!
!  contains
!------------------------------------------------------------------------------!

end module

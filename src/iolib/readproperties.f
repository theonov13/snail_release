subroutine readproperties(escf)
!-----------------------------------------------------------------+
!  Purpose:
!  --------
!
!  Read previously calculated properties from file 
!
!-----------------------------------------------------------------+

  use mod_fileutil

  implicit none

  include 'fortrankinds.h'

  ! constants
  character(len=80), parameter :: fprop  = 'properties_snail.txt'

  ! input
  real(kdp), intent(inout) :: escf

  ! local
  integer :: ifil,io,nlen,istr
  logical :: lexist

  character(len=80) :: sline

  inquire(file=trim(fprop), exist=lexist)

  if (lexist) then
    ifil = openfile(fprop,'old')

    do
      read(ifil,'(a)',iostat=io)  sline

      if (io > 0)  then
         write(6,*) io
         call quit("error during read","reaproperties")
      elseif (io < 0) then
         ! end of file reached
         exit
      else
        sline = trim(adjustl(sline))
        call string_2lower(sline)    ! convert to lower case to be case insensitive
        nlen  = len(sline)
        istr  = index(sline,"#")     ! find possibly position of comment
        if (istr > 0) then
          nlen = istr 
        end if

        if (sline(1:1) == "#") then
          ! comment => we just cycle
          cycle

        elseif (sline(1:11) == "$scf_energy") then
          read(sline(12:nlen),*) escf

        end if
      end if
    end do

    call closefile(ifil)
  else
    escf = 0.0d0
  end if

end subroutine

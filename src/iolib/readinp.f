
subroutine readinp(basis,cbas,jkbas,use_ri,use_rijk,              &
                   lscf,lmp2,lcis,lvib,lplot,lnumgrad,lnumhess,   &
                   do_opt,iprint,                                 &
                   ScfCalcDef,CisCalcDef,Mp2CalcDef,PltCalcDef,   &
                   OptCalcDef,                                    &
                   tolint,intopt,natoms,ncharge,wavefunc,title,   &
                   memmax,memused,atoms,filinp,fhess,mxatoms)

  use type_atom 
  use type_cgto
  use type_shell
  use type_basis
  use mod_fileutil
  use globaldata
  use type_integ

  use type_scfcalcdef 
  use type_ciscalcdef 
  use type_mp2calcdef 
  use type_pltcalcdef 
  use type_optcalcdef 

  implicit none

  include 'fortrankinds.h'
  
  ! constants
  integer, parameter :: maxlines = 2000
  integer, parameter :: nstrlen  = 80
  integer, parameter :: narrlen  = 20

  character, parameter :: cdelim = " "

  logical, parameter :: locdbg = .true.

  integer, parameter :: mxshls = 750   ! delete me in the future 

  ! dimensions
  integer, intent(in) :: mxatoms

  ! input
  character(len=80), intent(in)  :: filinp

  ! input/ouput
  type(scf_calcdef), intent(inout) :: ScfCalcDef
  type(cis_calcdef), intent(inout) :: CisCalcDef
  type(mp2_calcdef), intent(inout) :: Mp2CalcDef
  type(plt_calcdef), intent(inout) :: PltCalcDef
  type(opt_calcdef), intent(inout) :: OptCalcDef
  integer(kli),      intent(inout) :: memmax

  ! output
  integer,           intent(out) :: natoms,ncharge,iprint
  character(len=80), intent(out) :: wavefunc,title,fhess
  logical,           intent(out) :: lscf,lmp2,lcis,lvib,lplot,use_ri,use_rijk
  logical,           intent(out) :: lnumgrad,lnumhess,do_opt

  real(kdp),         intent(out) :: tolint
  
  type(basis_info),  intent(inout) :: basis
  type(basis_info),  intent(inout) :: cbas,jkbas
  type(atom_info),   intent(inout) :: atoms(mxatoms)
  type(integ_info),  intent(inout) :: intopt

  integer(kli),      intent(inout) :: memused

  ! local
  integer      :: iuninp,io,iat,nsout,nlen,istr 
  integer      :: nbf,nxbf,njkbf,nsbf,nxsbf,njksbf
  integer      :: ljkmax,lxmax,lmax
  integer      :: mxprim,mxcart
  integer      :: nshell,nxshell,njkshell

  integer      :: ival

  real(kdp) :: xcoord,ycoord,zcoord

  character(len=200) :: str
  character(len=80)  :: sline,basname,fbas,sread,xbasname,jkbasname
  character(len=3)   :: elem, a3
  character(len=2)   :: a2

  integer :: nsep,i
  integer :: nx,ny,nz,ibf,ibas,nbas,nprim,ishell,iprim
  real(kdp), allocatable :: exps(:),coefs(:)
  real(kdp) :: xyz(3)

  integer, allocatable :: ilst(:)

  ! basis functions shells
  type(shell_info), allocatable :: shells(:)
  ! for density fitting
  type(shell_info), allocatable :: xshells(:),jkshells(:)

  ! functions
  character(len=200) :: ExpandRange

  !--------------------------------------------------------------------+
  ! Initialization and setting of defaults
  !--------------------------------------------------------------------+

  !memmax =  int(3000,kli) * int(1024 * 128,kli) ! used at maximum 3 GiB
  memmax =  int(16000,kli) * int(1024 * 128,kli) ! used at maximum 16 GiB

  nbf        = -99
  natoms     = -99
  wavefunc   = "rhf"  ! default  
  title      = 'title not set'
  ncharge    = 0
  iprint     = 2
  lscf       = .true.
  lmp2       = .false.
  lcis       = .false.
  lvib       = .false.   
  lplot      = .false.

  lnumgrad   = .false.
  lnumhess   = .false.
  do_opt     = .false.

  ! integral scheme Mc-Murchie Davidson
  intopt%scheme = INT_MMD
 
  allocate(shells(mxshls),xshells(mxshls),jkshells(mxshls))

  ! RI approximation settings
  nxbf     = 0
  nxshell  = 0
  lxmax    = 0
  njkbf    = 0
  njkshell = 0
  ljkmax   = 0
  use_ri   = .false.
  use_rijk = .false.

  tolint = 1.d-10 ! Threshold for Schwarz-Screening

  !--------------------------------------------------------------------+
  ! Reading input file
  !--------------------------------------------------------------------+

  ! open input file
  if (locdbg) write(6,*) 'open file ', filinp
  iuninp = openfile(filinp,'old')

  do
    read(iuninp,'(a)',iostat=io)  sline

    if (io > 0)  then
       write(6,*) io
       call quit("error during read","reainp")
    elseif (io < 0) then
       ! end of file reached
       exit
    else
       ! do normal stuff ...

       sline = trim(adjustl(sline))
       call string_2lower(sline)    ! convert to lower case to be case insensitive
       nlen = len(sline)

       !---------------------------------------------------------------+
       ! General things
       !---------------------------------------------------------------+
       if (sline(1:nlen) == "$title") then
         read(iuninp,*,iostat=io) title 

       elseif (sline(1:nlen) == "$printlevel") then
         read(iuninp,*,iostat=io) iprint
 
         ! sanity check
         if (iprint < 0) call quit('Negative Print Level not allowed!','readinp')

       elseif (sline(1:nlen) == "$memmax") then
         read(iuninp,'(a)',iostat=io) sread

         sread = trim(adjustl(sread)) 
         call string_2lower(sread) ! convert to lower case to be case insensitive

         if (index(sread,'kb').gt.0) then
           ! convert memmax from KB to words
           read(sread,*,iostat=io) ival, a2
           memmax = int(ival,kli) * int(125,8)

         else if (index(sread,'mb').gt.0) then
           ! convert memmax from MB to words
           read(sread,*,iostat=io) ival, a2
           memmax = int(ival,kli) * int(1000*125,8)

         else if (index(sread,'gb').gt.0) then
           ! convert memmax from GB to words
           read(sread,*,iostat=io) ival, a2
           memmax = int(ival,kli) * int(1000*1000*125,8)

         else if (index(sread,'kib').gt.0) then
           ! convert memmax from KiB to words
           read(sread,*,iostat=io) ival, a3
           memmax = int(ival,kli) * int(128,8)

         else if (index(sread,'mib').gt.0) then
           ! convert memmax from MiB to words
           read(sread,*,iostat=io) ival, a3
           memmax = int(ival,kli) * int(1024*128,8)

         else if (index(sread,'gib').gt.0) then
           ! convert memmax from GiB to words
           read(sread,*,iostat=io) ival, a3
           memmax = int(ival,kli) * int(1024*1024*128,8)

         else ! default case
           ! convert memmax from MiB to words
           read(sread,*,iostat=io) ival
           memmax = int(ival,kli) * int(1024*128,8)
         end if

         if ( io .ne. 0 ) call quit("Can not read memmax","readinp")
 
       !---------------------------------------------------------------+
       ! SCF related things
       !---------------------------------------------------------------+
       elseif (sline(1:nlen) == "$wavefunction") then
         read(iuninp,*,iostat=io) sread

         sread = adjustl(sread)

         if (sread(1:3) == 'RHF' .or. sread(1:3) == 'rhf') then
           wavefunc = 'rhf'
         elseif (sread(1:3) == 'UHF' .or. sread(1:3) == 'uhf') then
           wavefunc = 'uhf'
         end if

       elseif (sline(1:nlen) == "$scf") then

         do
           read(iuninp,'(a)',iostat=io)  sline

           if (io > 0)  then
              write(6,*) io
              call quit("error during read","reainp")
           elseif (io < 0) then
              ! end of file reached
              exit
           else
             sline = trim(adjustl(sline))
             call string_2lower(sline)    ! convert to lower case to be case insensitive
             nlen  = len(sline)
             istr  = index(sline,"#")     ! find possibly position of comment
             if (istr > 0) then
               nlen = istr 
             end if

             if (sline(1:1) == "$") then
               backspace(iuninp)
               exit

             elseif (sline(1:1) == "#") then
               ! comment => we just cycle
               cycle

             elseif (sline(1:7) == "maxiter") then
               read(sline(8:nlen),*) ScfCalcDef%maxiter

             elseif (sline(1:6) == "no_scf") then
               lscf = .false.

             elseif (sline(1:6) == "scftol") then
               read(sline(7:nlen),*) ScfCalcDef%scftol

             elseif (sline(1:5) == "guess") then
               read(sline(6:nlen),*) sread
               sread =trim( adjustl(sread))

               select case (sread)
                 case ("gwh")
                   ScfCalcDef%cguess = "GWH"
                 case ("hcore")
                   ScfCalcDef%cguess = "HCORE"
                 case ("reuse")
                   ScfCalcDef%cguess = "REUSE"
                 case default
                   call quit("Unknown initial guess!","readinp")
               end select 

             elseif (sline(1:8) == 'direct') then
               ScfCalcDef%ldirect = .true.

             elseif (sline(1:11) == 'not_direct') then
               ScfCalcDef%ldirect = .false.
             end if
           end if
         end do

       !---------------------------------------------------------------+
       ! MP2 related things 
       !---------------------------------------------------------------+
       elseif (sline(1:nlen) == "$mp2") then
         lmp2 = .true.

         do
           read(iuninp,'(a)',iostat=io)  sline

           if (io > 0)  then
              write(6,*) io
              call quit("error during read","reainp")
           elseif (io < 0) then
              ! end of file reached
              exit
           else
             sline = trim(adjustl(sline))
             call string_2lower(sline)    ! convert to lower case to be case insensitive
             nlen  = len(sline)
             istr  = index(sline,"#")     ! find possibly position of comment
             if (istr > 0) then
               nlen = istr 
             end if

             if (sline(1:1) == "$") then
               backspace(iuninp)
               exit

             elseif (sline(1:1) == "#") then
               ! comment => we just cycle
               cycle

             elseif (sline(1:nlen) == "use_overlap") then
               Mp2CalcDef%use_ovlp = .true.

             elseif (sline(1:6) == "freeze") then
               read(sline(7:nlen),*) Mp2CalcDef%freeze_point
               Mp2CalcDef%use_frozen_core = .true.

             end if
           end if
         end do

       !---------------------------------------------------------------+
       ! Numerical derivatives 
       !---------------------------------------------------------------+
       elseif (sline(1:nlen) == "$numgrad") then
         lnumgrad = .true.

       elseif (sline(1:nlen) == "$numhess") then
         lnumhess = .true.
 
       elseif (sline(1:nlen) == "$optgeo") then
         do_opt = .true.
         lnumgrad = .true.

         do
           read(iuninp,'(a)',iostat=io)  sline

           if (io > 0)  then
              write(6,*) io
              call quit("error during read","reainp")
           elseif (io < 0) then
              ! end of file reached
              exit
           else
             sline = trim(adjustl(sline))
             call string_2lower(sline)    ! convert to lower case to be case insensitive
             nlen  = len(sline)
             istr  = index(sline,"#")     ! find possibly position of comment
             if (istr > 0) then
               nlen = istr 
             end if

             if (sline(1:1) == "$") then
               backspace(iuninp)
               exit

             elseif (sline(1:1) == "#") then
               ! comment => we just cycle
               cycle

             elseif (sline(1:7) == "maxiter") then
               read(sline(8:nlen),*) OptCalcDef%maxiter

             end if
           end if
         end do

       !---------------------------------------------------------------+
       ! Vibrational related things 
       !---------------------------------------------------------------+
       elseif (sline(1:nlen) == "$vib") then
         lvib = .true.
         read(iuninp,*,iostat=io) fhess

       !---------------------------------------------------------------+
       ! Plotting related things 
       !---------------------------------------------------------------+
       elseif (sline(1:nlen) == "$plot") then
         lplot = .true.

         do
           read(iuninp,'(a)',iostat=io)  sline

           if (io > 0)  then
              write(6,*) io
              call quit("error during read","reainp")
           elseif (io < 0) then
              ! end of file reached
              exit
           else
             sline = trim(adjustl(sline))
             call string_2lower(sline)    ! convert to lower case to be case insensitive
             nlen  = len(sline)
             istr  = index(sline,"#")     ! find possibly position of comment
             if (istr > 0) then
               nlen = istr 
             end if

             if (sline(1:1) == "$") then
               backspace(iuninp)
               exit

             elseif (sline(1:1) == "#") then
               ! comment => we just cycle
               cycle

             elseif (sline(1:7) == "density") then
               read(sline(8:nlen),*) PltCalcDef%filcube
               PltCalcDef%plot_density = .true.

             elseif (sline(1:3) == "mos") then
                
               nsep = 0
               str = ExpandRange(sline(4:nlen))
 
               do i = 1, len(str)
                 if (str(i:i)== ',') then
                   nsep = nsep + 1
                  end if
               end do

               allocate(ilst(nsep+1))
               read(str,*) ilst

               PltCalcDef%imos(1:nsep+1) = ilst(1:nsep+1)  
               PltCalcDef%plot_mos = .true.
               PltCalcDef%nmos = nsep + 1

             end if
           end if
         end do

       !---------------------------------------------------------------+
       ! CIS/RPA related things 
       !---------------------------------------------------------------+
       elseif (sline(1:nlen) == "$cis") then
         lcis = .true.

         do
           read(iuninp,'(a)',iostat=io)  sline

           if (io > 0)  then
              write(6,*) io
              call quit("error during read","reainp")
           elseif (io < 0) then
              ! end of file reached
              exit
           else
             sline = trim(adjustl(sline))
             call string_2lower(sline)    ! convert to lower case to be case insensitive
             nlen  = len(sline)
             istr  = index(sline,"#")     ! find possibly position of comment
             if (istr > 0) then
               nlen = istr 
             end if

             if (sline(1:1) == "$") then
               backspace(iuninp)
               exit

             elseif (sline(1:1) == "#") then
               ! comment => we just cycle
               cycle

             elseif (sline(1:6) == "nroots") then
               read(sline(7:nlen),*) CisCalcDef%nroots 

             elseif (sline(1:7) == "maxiter") then
               read(sline(8:nlen),*) CisCalcDef%maxiter

             elseif (sline(1:4) == "tdhf") then
               CisCalcDef%use_TDHF = .true.

             elseif (sline(1:8) == 'fulldiag') then
               CisCalcDef%use_fulldiag = .true.

             elseif (sline(1:11) == 'no_fulldiag') then
               CisCalcDef%use_fulldiag = .false.
             end if
           end if
         end do

       !---------------------------------------------------------------+
       ! Things related to integral handling  
       !---------------------------------------------------------------+
       elseif (sline(1:nlen) == "$intscheme") then
         read(iuninp,*,iostat=io) sread

         sread = adjustl(sread)

         if (sread(1:5) == 'MMDET') then
           intopt%scheme = INT_MMDET
         elseif (sread(1:3) == 'MMD') then
           intopt%scheme = INT_MMD
         elseif (sread(1:2) == 'OS') then
           intopt%scheme = INT_OS
         elseif (sread(1:3) == 'HUZ') then
           intopt%scheme = INT_HUZ
         end if

       elseif (sline(1:nlen) == "$tolint") then
         read(iuninp,*,iostat=io) tolint

       !---------------------------------------------------------------+
       ! basis set related things  
       !---------------------------------------------------------------+ 
       elseif (sline(1:nlen) == "$basis") then
         read(iuninp,*) basname

       elseif (sline(1:nlen) == "$cbas") then
         read(iuninp,*) xbasname
         use_ri = .true.

         Mp2CalcDef%use_ri = use_ri

       elseif (sline(1:nlen) == "$jkbas") then
         read(iuninp,*) jkbasname
         use_rijk = .true.

       !---------------------------------------------------------------+
       ! Molecule/Coordinate related things  
       !---------------------------------------------------------------+
       elseif (sline(1:nlen) == "$charge") then
         read(iuninp,*,iostat=io) ncharge 

       elseif (sline(1:nlen) == "$coord") then
       
         read(iuninp,*,iostat=io) natoms  

         if (natoms > mxatoms) then
           call quit('Too many atoms','readinp')
         end if  

         do iat = 1, natoms
           read(iuninp,*) elem,xcoord,ycoord,zcoord
  
           atoms(iat)%elem  = elem   
           atoms(iat)%atono = at_getatnum(elem)
           atoms(iat)%mass  = at_getatmass(elem)
           atoms(iat)%atoid = iat
           atoms(iat)%charge = real(atoms(iat)%atono,kdp)
           call at_setposition(atoms(iat),xcoord,ycoord,zcoord)      
         end do


       end if
    end if 
  end do

  ! close file containing basis set
  call closefile(iuninp)

  ! now assign basis set
  fbas = trim(homedir) // "/basis/all.basis"
  nbf = 0
  nsbf = 0
  nshell = 0
  lmax = 0
  mxprim = 0
  mxcart = 0
  !
  ! read in basis function in shells
  !
  do iat = 1, natoms
    call readbas(fbas,iat,atoms(iat),trim(adjustl(basname)),memmax,memused,&
                 shells,nbf,nsbf,nshell,lmax,mxprim,mxcart,mxshls)
  end do
  !
  ! Sort shells according to angular momenta (helps in integral routines)
  !
  call sort_shells(shells,nshell)

  !
  ! Copy shell information to basis set information
  !
  call allocate_basis(basis,lmax,nbf,nsbf,nshell,natoms,mxprim,mxcart)
  do ishell = 1, nshell
    call copy_shell(shells(ishell),basis%shells(ishell))
  end do

  ! now assign auxiliary basis set
  if (use_ri) then
    fbas = trim(homedir) // "/basis/all.cbas"
    nxbf  = 0
    nxsbf = 0
    lxmax = 0
    mxprim = 0
    mxcart = 0
    do iat = 1, natoms
      call readbas(fbas,iat,atoms(iat),trim(adjustl(xbasname)),memmax,memused,&
                   xshells,nxbf,nxsbf,nxshell,lxmax,mxprim,mxcart,mxshls)
    end do
    !
    ! Sort shells according to angular momenta (helps in integral routines)
    !
    call sort_shells(xshells,nxshell)

    !
    ! Copy shell information to basis set information
    !
    call allocate_basis(cbas,lxmax,nxbf,nxsbf,nxshell,natoms,mxprim,mxcart)
    do ishell = 1, nxshell
      call copy_shell(xshells(ishell),cbas%shells(ishell))
    end do

    mxprim = max(basis%mxprim,cbas%mxprim)
    mxcart = max(basis%mxcart,cbas%mxcart)

  end if

  ! now assign JK auxiliary basis set
  if (use_rijk) then
    fbas = trim(homedir) // "/basis/all.jkbas"
    njkbf  = 0
    njksbf = 0
    ljkmax = 0
    mxprim = 0
    mxcart = 0
    do iat = 1, natoms
      call readbas(fbas,iat,atoms(iat),trim(adjustl(jkbasname)),memmax,memused,&
                   jkshells,njkbf,njksbf,njkshell,ljkmax,mxprim,mxcart,mxshls)
    end do
    !
    ! Sort shells according to angular momenta (helps in integral routines)
    !
    call sort_shells(jkshells,njkshell)

    !
    ! Copy shell information to basis set information
    !
    call allocate_basis(jkbas,ljkmax,njkbf,njksbf,njkshell,natoms,mxprim,mxcart)
    do ishell = 1, njkshell
      call copy_shell(jkshells(ishell),jkbas%shells(ishell))
    end do

    mxprim = max(basis%mxprim,jkbas%mxprim,cbas%mxprim)
    mxcart = max(basis%mxcart,jkbas%mxcart,cbas%mxcart)

  end if

  ! Some checks

  if (nbf == 0) then
    write(6,'(/3x,a,i4)') trim(adjustl(basname)),natoms
    call quit('No basis set assigned','readinp')
  end if

  !-----------------------------------------------------------------------+
  ! clean up
  !-----------------------------------------------------------------------+
  do ishell = 1, basis%nshell
    call deallocate_shell(shells(ishell))
  end do
  deallocate(shells)

  do ishell = 1, cbas%nshell
    call deallocate_shell(xshells(ishell))
  end do
  deallocate(xshells)

  do ishell = 1, jkbas%nshell
    call deallocate_shell(jkshells(ishell))
  end do
  deallocate(jkshells)


end subroutine

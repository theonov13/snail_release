subroutine getpointgroup(atoms,threps,natoms)

  use type_atom

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'

  ! dimensions
  integer, intent(in) :: natoms

  ! input
  type(atom_info),  intent(in) :: atoms(natoms)
  real(kdp),        intent(in) :: threps

  ! local
  real(kdp) :: diff2, xvec(3), rot(3,3)
  integer   :: idx, iat, jat, isym, iaxis, igrp
  integer   :: symtab(8,8), iops(8), ivec(8)
  logical   :: found
  integer,   allocatable :: imap(:,:)
  real(kdp), allocatable :: xyz(:,:), xsym(:,:), sxyz(:,:)

  character(len=3) :: pgs(8), pgrp

  data pgs /'c1 ','ci ','cs ','c2 ','c2v','c2h','d2 ','d2h'/

  data symtab /1,0,0,0,0,0,0,0,  & ! c1
           &   1,0,0,0,1,0,0,0,  & ! ci
           &   1,0,0,0,0,1,0,0,  & ! cs
           &   1,1,0,0,0,0,0,0,  & ! c2 
           &   1,1,0,0,0,0,1,1,  & ! c2v
           &   1,1,0,0,1,1,0,0,  & ! c2h
           &   1,1,1,1,0,0,0,0,  & ! d2
           &   1,1,1,1,1,1,1,1/    ! d2h

  ! function
  real(kdp) :: ddot

  !---------------------------------------------------------------+
  ! main routine
  !---------------------------------------------------------------+
  allocate(xyz(3,natoms),xsym(3,natoms),sxyz(3,natoms))

  do iat = 1, natoms
    xyz(1,iat) = atoms(iat)%xyz(1) 
    xyz(2,iat) = atoms(iat)%xyz(2)
    xyz(3,iat) = atoms(iat)%xyz(3)
  end do

  allocate(imap(8,natoms))
  imap = 0
  iops = 0

  ! Build identity matrix and save it on rot
  rot = zero
  do idx = 1, 3
    rot(idx,idx) = one
  end do

  ! determine the highest order symmetry axis and make it the z axis 
  ! (only C2 rotations considered at the moment)
  found = .false.
  do isym = 1, 3
   select case(isym)
     case(1); call sym_c2z(xsym,xyz,natoms)
     case(2); call sym_c2y(xsym,xyz,natoms)
     case(3); call sym_c2x(xsym,xyz,natoms)
   end select

   iaxis = 1
   do iat = 1, natoms
     idx = 0
     do jat = 1, natoms
       if (atoms(iat)%elem /= atoms(jat)%elem) cycle
       xvec(1:3) = xyz(1:3,iat) - xsym(1:3,jat)
       diff2 = ddot(3, xvec, 1, xvec, 1)

       if (diff2 < threps .and. idx/=0) then
         call quit('Error while determing highest order symmetry axis', 'getpointgroup')
       end if

       if (diff2 < threps) idx = jat
     end do
     if (idx == 0) iaxis = 0
   end do

   if (iaxis == 1) then
    
    ! C2 axis is already along z axis don't change rot
    if (isym == 1) then
      found = .true.
      exit 
    end if

    ! rotate axis so that C2 axix is oriented along z axis
    ! (rotate y -> z and z -> -y)
    if (isym == 2) then 
      found = .true.
      rot = zero
      rot(1,1) = one ; rot(3,2) = one ; rot(2,3) = -one
      exit
    end if

    ! rotate axis so that C2 axix is oriented along z axis
    ! (rotate x -> z and z -> -x)
    if (isym == 3) then 
      found = .true.
      rot = zero
      rot(2,2) = one ; rot(3,1) = one ; rot(1,3) = -one
      exit
    end if

   end if
  end do

  ! if there are no rotations, identify symmetry planes 
  ! and ensure that they are in the xy plane
  if (.not.found) then
   do isym = 1, 3
    select case (isym)
      case(1); call sym_sigxy(xsym,xyz,natoms)
      case(2); call sym_sigxz(xsym,xyz,natoms)
      case(3); call sym_sigyz(xsym,xyz,natoms)
    end select

    iaxis = 1
    do iat = 1,natoms
      idx = 0
      do jat = 1,natoms
        if (atoms(iat)%elem /= atoms(jat)%elem) cycle
        xvec(1:3) = xyz(1:3,iat) - xsym(1:3,jat)
        diff2 = ddot(3, xvec, 1, xvec, 1)

        if (diff2 < threps .and. idx /= 0) then
          call quit('Error while detecting symmetry planes', 'getpointgroup')
        end if

        if (diff2 < threps) idx = jat
      end do
      if (idx == 0) iaxis = 0
    end do

    if (iaxis == 1) then
     
     ! sigma already xy plane => no change for rot
     if (isym == 1) then 
       found = .true.
       exit  
     end if

     ! rotate z -> y and y -> -z
     if (isym == 2) then 
       found = .true.
       rot = zero
       rot(1,1) = one ; rot(2,3) = one ; rot(3,2) = -one
       exit
     end if

     ! rotate z -> x and x -> -z
     if (isym == 3) then 
       found = .true.
       rot = zero
       rot(2,2) = one ; rot(1,3) = one ; rot(3,1) = -one
       exit
     end if

    end if
   end do
  end if

  ! rotate :  sxyz = rot^T xyz
  call dgemm('t','n',3,natoms,3,one,rot,3,xyz,3,zero,sxyz,3)

  ! loop over D2h symmetry operations and set up atom mappings
  do isym = 1, 8

    ! apply symmtery operation
    select case (isym)
     case(1) ; call sym_e(xsym,sxyz,natoms)
     case(2) ; call sym_c2z(xsym,sxyz,natoms)
     case(3) ; call sym_c2y(xsym,sxyz,natoms)
     case(4) ; call sym_c2x(xsym,sxyz,natoms)
     case(5) ; call sym_i(xsym,sxyz,natoms)
     case(6) ; call sym_sigxy(xsym,sxyz,natoms)
     case(7) ; call sym_sigxz(xsym,sxyz,natoms)
     case(8) ; call sym_sigyz(xsym,sxyz,natoms)
    end select

    ! determine which atoms maps to each other
    iops(isym) = 1
    do iat = 1,natoms
      do jat = 1,natoms
        if (atoms(iat)%elem /= atoms(jat)%elem) cycle
        xvec(1:3) = sxyz(1:3,iat) - xsym(1:3,jat)
        diff2 = ddot(3, xvec, 1, xvec, 1)

        if (diff2 < threps .and. imap(isym,iat) /= 0) then
          call quit('Error while identifying symmetry operation', 'getpointgroup')
        end if

        if (diff2 < threps) imap(isym,iat) = jat
      end do
      if (imap(isym,iat)==0) iops(isym)=0
    end do
  end do

  ! identify the point group
  pgrp='c1 '
  do igrp = 1, 8
    ivec(1:8) = iops(1:8) - symtab(1:8,igrp)
    idx = dot_product(ivec,ivec)
    if (idx == 0) pgrp = pgs(igrp)
  end do

  write(istdout,'(/7x,a,a)') 'Point group symmetry : ', pgrp

  deallocate(imap)
  deallocate(xyz,xsym,sxyz)

end subroutine
  
subroutine sym_c2z(xout, xin, ndim)
!---------------------------------------------------------+
! Purpose: Perform a C2z rotation for all vectors in xin
!          and return rotated set of vector in xout
!---------------------------------------------------------+

 implicit none

 include 'fortrankinds.h'
 
 ! dimensions
 integer, intent(in) :: ndim 

 ! output 
 real(kdp), intent(out) :: xout(3,ndim)

 ! input 
 real(kdp),  intent(in) :: xin (3,ndim)

 ! local
 integer :: idm

 do idm = 1, ndim
   xout(1,idm) = -xin(1,idm)
   xout(2,idm) = -xin(2,idm)
   xout(3,idm) =  xin(3,idm)
 end do

end subroutine sym_c2z

subroutine sym_c2x(xout, xin, ndim)
!---------------------------------------------------------+
! Purpose: Perform a C2x rotation for all vectors in xin
!          and return rotated set of vector in xout
!---------------------------------------------------------+

 implicit none
  logical   :: found

 include 'fortrankinds.h'

 ! dimensions
 integer, intent(in) :: ndim 

 ! output
 real(kdp), intent(out) :: xout(3,ndim)

 ! input
 real(kdp),  intent(in) :: xin( 3,ndim)

 ! local
 integer :: idm

 do idm = 1, ndim
   xout(1,idm) =  xin(1,idm)
   xout(2,idm) = -xin(2,idm)
   xout(3,idm) = -xin(3,idm)
 end do

end subroutine sym_c2x

subroutine sym_c2y(xout, xin, ndim)
!---------------------------------------------------------+
! Purpose: Perform a C2y rotation for all vectors in xin
!          and return rotated set of vector in xout
!---------------------------------------------------------+

 implicit none

 include 'fortrankinds.h'

 ! dimensions
 integer, intent(in) :: ndim 

 ! output
 real(kdp), intent(out) :: xout(3,ndim)

 ! input
 real(kdp),  intent(in) :: xin (3,ndim)

 ! local
 integer :: idm 

 do idm = 1, ndim
   xout(1,idm) = -xin(1,idm)
   xout(2,idm) =  xin(2,idm)
   xout(3,idm) = -xin(3,idm)
 end do

end subroutine sym_c2y

subroutine sym_sigxy(xout, xin, ndim)
!---------------------------------------------------------+
! Purpose: Apply a xy mirror plane for all vectors in xin
!          and return rotated set of vector in xout
!---------------------------------------------------------+

 implicit none

 include 'fortrankinds.h'

 ! dimensions
 integer, intent(in) :: ndim

 ! ouput
 real(kdp), intent(out) :: xout(3,ndim)

 ! input
 real(kdp),  intent(in) :: xin( 3,ndim)

 ! local
 integer :: idm

 do idm = 1, ndim
   xout(1,idm) =  xin(1,idm)
   xout(2,idm) =  xin(2,idm)
   xout(3,idm) = -xin(3,idm)
 end do

end subroutine sym_sigxy

subroutine sym_sigxz(xout, xin, ndim)
!---------------------------------------------------------+
! Purpose: Apply a xz mirror plane for all vectors in xin
!          and return rotated set of vector in xout
!---------------------------------------------------------+

 implicit none

 include 'fortrankinds.h'

 ! dimensions
 integer, intent(in) :: ndim

 ! output
 real(kdp), intent(out) :: xout(3,ndim)

 ! input
 real(kdp),  intent(in) :: xin( 3,ndim)

 ! local
 integer idm 

 do idm = 1, ndim
   xout(1,idm) =  xin(1,idm)
   xout(2,idm) = -xin(2,idm)
   xout(3,idm) =  xin(3,idm)
 end do

end subroutine sym_sigxz

subroutine sym_sigyz(xout, xin, ndim)
!---------------------------------------------------------+
! Purpose: Apply a yz mirror plane for all vectors in xin
!          and return rotated set of vector in xout
!---------------------------------------------------------+

 implicit none

 include 'fortrankinds.h'

 ! dimensions
 integer, intent(in) :: ndim

 ! output
 real(kdp), intent(out) :: xout(3,ndim)

 ! input
 real(kdp),  intent(in) :: xin (3,ndim)

 ! local
 integer :: idm

 do idm = 1, ndim
   xout(1,idm) = -xin(1,idm)
   xout(2,idm) =  xin(2,idm)
   xout(3,idm) =  xin(3,idm)
 end do

end subroutine sym_sigyz

subroutine sym_e(xout, xin, ndim)
!---------------------------------------------------------+
! Purpose: Apply identity operation for all vectors in xin
!          and return rotated set of vector in xout
!---------------------------------------------------------+

 implicit none
 
 include 'fortrankinds.h'

 ! dimensions
 integer, intent(in) :: ndim

 ! output
 real(kdp), intent(out) :: xout(3,ndim)

 ! input
 real(kdp),  intent(in) :: xin (3,ndim)

 ! local
 integer :: idm

 do idm = 1, ndim
   xout(1,idm) =  xin(1,idm)
   xout(2,idm) =  xin(2,idm)
   xout(3,idm) =  xin(3,idm)
 enddo

end subroutine sym_e

subroutine sym_i(xout, xin, ndim)
!---------------------------------------------------------+
! Purpose: Apply inversion operation for all vectors in xin
!          and return rotated set of vector in xout
!---------------------------------------------------------+

 implicit none
 
 include 'fortrankinds.h'

 ! dimensions
 integer, intent(in) :: ndim

 !output
 real(kdp), intent(out) :: xout(3,ndim)

 ! input
 real(kdp),  intent(in) :: xin (3,ndim)

 ! local
 integer :: idm

 do idm = 1, ndim
   xout(1,idm) = -xin(1,idm)
   xout(2,idm) = -xin(2,idm)
   xout(3,idm) = -xin(3,idm)
 end do

end subroutine sym_i

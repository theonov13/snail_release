subroutine prtcoord(atoms,natoms,mxatoms)

  use type_atom

  implicit none

  include 'fortrankinds.h'
  include 'stdout.h'
  include 'numbers.h'

  ! constants
  real(kdp), parameter :: deg2rad = 57.29578d+00
  real(kdp), parameter :: rotcm = 60.199686532d00
  real(kdp), parameter :: inmhz = 29.9792458d0


  ! dimensions
  integer, intent(in) :: natoms,mxatoms

  ! input
  type(atom_info),  intent(in) :: atoms(natoms)

  ! local
  integer   :: i, iat, lwork, info
  real(kdp) :: dlen, rot_const

  real(kdp),    allocatable :: xyz(:,:),geo(:,:), itensor(:,:), inertia(:), work(:)
  integer,      allocatable :: na(:),nb(:),nc(:),iz(:)
  character(4), allocatable :: elems(:)
 
 
  allocate(xyz(3,mxatoms),geo(3,mxatoms),elems(mxatoms),&
           na(mxatoms),nb(mxatoms),nc(mxatoms),iz(mxatoms))

  do iat = 1, natoms
    xyz(1:3,iat) = atoms(iat)%xyz(1:3)
    elems(iat)   = atoms(iat)%elem
  end do

  call xyzint(xyz,natoms,na,nb,nc,deg2rad,geo)

  write(istdout,'(/1x,a)') repeat('-',70)
  
  write(istdout,'(/4x,a,a,a)')'+', repeat('-',40) ,'+'
  write(istdout,'(5x,a)') 'Geometrical information on the molecule'
  write(istdout,'(4x,a,a,a)')'+', repeat('-',40) ,'+'

  write(istdout,'(/5x,a,/)')'internal coordinates: '

  do iat = 1, natoms
    if (geo(3,iat).gt.180.d0) geo(3,iat) = geo(3,iat)-360.d0

    write(istdout,'(5x,a3,i4,f10.4,i4,f10.4,i4,f10.4)') &
         elems(iat),na(iat),geo(1,iat),nb(iat),geo(2,iat),nc(iat),geo(3,iat)
  end do

  deallocate(xyz,geo,elems,na,nb,nc,iz)

  !
  ! Calculate moment of intertia tensor
  !
  allocate(inertia(3),itensor(3,3))
  call GetInertia(itensor,atoms,natoms) 

  !
  ! Diagonalize moment of intertia tensor
  !
  lwork = -1
  call dsyev('V','U',3,itensor,3,inertia,dlen,lwork,info)
  lwork = int(dlen)
  allocate(work(lwork))

  info = 0
  call dsyev( 'V', 'U', 3, itensor, 3, inertia, work, lwork, info )

  write(istdout,'(/5x,a,/)') "moments of inertia in a.u.:"
  write(istdout,'(5x,f14.8,2x,f14.8,2x,f14.8)')  inertia(1), inertia(2), inertia(3)

  write(istdout,'(/5x,a,/)') 'Rotational constants:'

  do i = 1, 3
    if(inertia(i) == zero )then
        write(istdout,'(5x,i10,a60)') i, 'undefined'
    else
        rot_const = rotcm / inertia(i)
        write(istdout,'(5x,i1,f11.5,2x,a6,f11.5,a6)') i, rot_const, 'cm**-1', &
                                                         rot_const*inmhz, 'GHz'
    endif
  enddo


end subroutine

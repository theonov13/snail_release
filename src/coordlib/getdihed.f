
subroutine getdihed(xyz,iat,jat,kat,lat,angle)
!--------------------------------------------------------------------+
! Purpose: Calculates the dihedral angle between atoms i, j, k and l
!
!--------------------------------------------------------------------+

    implicit none

    include 'fortrankinds.h'
    include 'numbers.h'
    include 'constants.h'

    ! input
    real(kdp), intent(in) :: xyz(3,*)
    integer,   intent(in) :: iat,jat,kat,lat

    ! output:
    real(kdp), intent(inout) :: angle
    
    ! local
    real(kdp) :: xi1, xj1, xl1, yi1, yj1, yl1, zi1, zj1, zl1
    real(kdp) :: xi2, xl2, yi2, yj2, yl2
    real(kdp) :: yi3, yj3, yl3
    real(kdp) :: dist, ddd, yxdist 
    real(kdp) :: cosa, cosph, sinph, costh, sinth 

    xi1 = xyz(1,iat) - xyz(1,kat)
    xj1 = xyz(1,jat) - xyz(1,kat)
    xl1 = xyz(1,lat) - xyz(1,kat)
    yi1 = xyz(2,iat) - xyz(2,kat)
    yj1 = xyz(2,jat) - xyz(2,kat)
    yl1 = xyz(2,lat) - xyz(2,kat)
    zi1 = xyz(3,iat) - xyz(3,kat)
    zj1 = xyz(3,jat) - xyz(3,kat)
    zl1 = xyz(3,lat) - xyz(3,kat)

    ! rotate around z axis to put kj along y axis
    dist = dsqrt(xj1**2 + yj1**2 + zj1**2)
    cosa = zj1 / dist

    if (cosa.gt.one)  cosa = one 
    if (cosa.lt.-one) cosa = -one

    ddd = one - cosa**2

    if (ddd.le.zero) then
      xi2 = yi1
      xl2 = xl1
      yi2 = yi1
      yl2 = yl1
      costh = cosa
      sinth = zero
    else
      yxdist = dist * dsqrt(ddd)

      if (yxdist.gt.1.0d-9) then
        cosph = yj1 / yxdist
        sinph = xj1 / yxdist

        xi2 = xi1 * cosph - yi1 * sinph 
        xl2 = xl1 * cosph - yl1 * sinph 
        yi2 = xi1 * sinph + yi1 * cosph 
        yj2 = xj1 * sinph + yj1 * cosph 
        yl2 = xl1 * sinph + yl1 * cosph
 
        ! rotate kj around the x axis so kj is along the z axis
        costh = cosa
        sinth = yj2 / dist
      else
        xi2 = yi1
        xl2 = xl1
        yi2 = yi1
        yl2 = yl1
        costh = cosa
        sinth = zero
      end if
    end if


    yi3 = yi2 * costh - zi1 * sinth
    yl3 = yl2 * costh - zl1 * sinth

    call dang(xl2,yl3,xi2,yi3,angle)

    if (angle .lt. zero) angle = twopi + angle
    if (angle .ge. twopi ) angle = zero 
end subroutine


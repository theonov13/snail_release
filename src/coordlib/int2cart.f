
subroutine intxyz(xyz,natoms,na,nb,nc,deg2rad,geo)
    implicit none

    include 'fortrankinds.h'
    include 'constants.h'
    include 'numbers.h'

    ! dimensions
    integer :: natoms

    real(kdp), intent(out) :: xyz(3,natoms)
    real(kdp), intent(in)  :: geo(3,natoms)
    integer,   intent(in)  :: na(natoms), nb(natoms), nc(natoms)
    real(kdp), intent(in)  :: deg2rad

    ! local
    integer   :: iat
    real(kdp) :: newX, newY, centerX, centerY, PointX, PointY
    real(kdp) :: alpha
  
    ! set first atom in origin
    xyz(1,1) = zero ; xyz(2,1) = zero ; xyz(3,1) = zero 

    ! set second atom along the x axis
    if (natoms.gt.1) then
      xyz(1,2) = -geo(1,2) ; xyz(2,2) = zero ; xyz(3,2) = zero
    end if   

    ! now the third atom   
    if (natoms.gt.2) then
      ! 1) add it along the x axis again
      xyz(1,3) = xyz(1,2) + geo(1,3)  
      xyz(2,3) = zero ; xyz(3,3) = zero
     
      ! 2) rotate atom to get right angle
      alpha = geo(2,3) * pi / 180.0d0
     
      centerX =  xyz(1,2) ; centerY = xyz(2,2)
      PointX  =  xyz(1,3) ; PointY  = xyz(2,3)   

      newX = centerX + (  dcos(-alpha) * (pointX-centerX) - dsin(-alpha) * (pointY-centerY))
      newY = centerY + (  dsin(-alpha) * (pointX-centerX) + dcos(-alpha) * (pointY-centerY))

      xyz(1,3) = newX ; xyz(2,3) = newY ; xyz(3,3) = zero
    end if

    ! add remaining atoms with NeRF algorithm
    do iat = 4, natoms
      call traf2xyz(xyz(1,iat),xyz(1,na(iat)), xyz(1,nb(iat)), xyz(1,nc(iat)),&
                    geo(1,iat), geo(2,iat), geo(3,iat))  
    end do

end subroutine 


subroutine traf2xyz(d,c,b,a,bond,angle,torsion)
    implicit none
  
    include 'fortrankinds.h'
    include 'constants.h'
  
    real(kdp), intent(out) :: d(3)
    real(kdp), intent(in)  :: c(3),b(3),a(3)
    real(kdp), intent(in)  :: bond,angle,torsion
  
    ! local
    real(kdp) :: d2(3), M(3,3), bc(3), n(3)
    real(kdp) :: dnrm, deg2rad
    integer   :: idx, jdx
  
    deg2rad = PI / 180.0d0
  
    ! GS: I don't really know why the minus sign has to be here (bond), 
    !     but otherwise it does not work properly.  
    d2(1) = -bond * dcos(-angle*deg2rad)
    d2(2) = -bond * dcos(-torsion*deg2rad) * dsin(-angle*deg2rad)
    d2(3) = -bond * dsin(-torsion*deg2rad) * dsin(-angle*deg2rad)
  
  
    ! bc = BC/|BC|
    bc(1) = c(1) - b(1)
    bc(2) = c(2) - b(2)
    bc(3) = c(3) - b(3)
  
    dnrm = dsqrt(bc(1)**2 + bc(2)**2 + bc(3)**2)
  
    bc(1) = bc(1) / dnrm 
    bc(2) = bc(2) / dnrm 
    bc(3) = bc(3) / dnrm 
    
  
    ! n = AB x bc / |AB x bc| 
    n(1) = (b(2)-a(2))*bc(3) - (b(3)-a(3))*bc(2)
    n(2) = (b(3)-a(3))*bc(1) - (b(1)-a(1))*bc(3)
    n(3) = (b(1)-a(1))*bc(2) - (b(2)-a(2))*bc(1)
  
    dnrm = dsqrt(n(1)**2 + n(2)**2 + n(3)**2)
  
    n(1) = n(1) / dnrm
    n(2) = n(2) / dnrm
    n(3) = n(3) / dnrm
  
    ! Build matrix
    M(1,1) = bc(1)
    M(2,1) = bc(2) 
    M(3,1) = bc(3)
  
    M(1,2) = n(2)*bc(3) - n(3)*bc(2)
    M(2,2) = n(3)*bc(1) - n(1)*bc(3) 
    M(3,2) = n(1)*bc(2) - n(2)*bc(1)
  
    M(1,3) = n(1)
    M(2,3) = n(2)
    M(3,3) = n(3)
  
  
    ! D = M*D2 + C  
    do idx = 1, 3
      d(idx) = c(idx) 
      do jdx = 1, 3
          d(idx) = d(idx) +  M(idx,jdx) * d2(jdx)
      end do
    end do
  

end subroutine



subroutine GetInertia(itensor,atoms,natoms) 
!----------------------------------------------------------------------------+
! Purpose: Calculates the moment of inertia tensor
!
!----------------------------------------------------------------------------+

  use type_atom

  implicit none

  include 'fortrankinds.h'
  include 'stdout.h'

  ! dimensions
  integer, intent(in) :: natoms

  ! output
  real(kdp), intent(out) :: itensor(3,3)

  ! input
  type(atom_info), intent(in) :: atoms(natoms)

  ! local
  real(kdp) :: I_xx, I_yy, I_zz, I_xy, I_xz, I_yz
  real(kdp) :: mass
  integer   :: iat

  I_xx = 0.0d0 
  I_yy = 0.0d0
  I_zz = 0.0d0
  I_xy = 0.0d0
  I_xz = 0.0d0
  I_yz = 0.0d0


  do iat = 1, natoms
      mass = atoms(iat)%mass
      I_xx = I_xx + mass * (atoms(iat)%xyz(2)**2 + atoms(iat)%xyz(3)**2)
      I_yy = I_yy + mass * (atoms(iat)%xyz(1)**2 + atoms(iat)%xyz(3)**2)
      I_zz = I_zz + mass * (atoms(iat)%xyz(1)**2 + atoms(iat)%xyz(2)**2)
      I_xy = I_xy - mass * atoms(iat)%xyz(1) * atoms(iat)%xyz(2)
      I_xz = I_xz - mass * atoms(iat)%xyz(1) * atoms(iat)%xyz(3)
      I_yz = I_yz - mass * atoms(iat)%xyz(2) * atoms(iat)%xyz(3)
  end do
  
  itensor = reshape((/ I_xx, I_xy, I_xz, I_xy, I_yy, I_yz, I_xz, I_yz, I_zz /), shape(itensor))  
end subroutine

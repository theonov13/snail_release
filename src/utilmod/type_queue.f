!----------------------------------------------------------------------------+
! Module, which features a simple implementation of the queue data structure
! 
! A queue is for instance used in the DIIS lib to manage the old densities
!
!----------------------------------------------------------------------------+
module type_queue 

  implicit none

  private

  include 'fortrankinds.h'

  type queueinfo
    real(kdp), pointer :: list(:,:) => null()

    integer :: nmax = 0
    integer :: ifront = 0
    integer :: irear  = 0
    integer :: nsize  = 0
  end type

  public :: queueinfo, allocate_queue, deallocate_queue, queue_add, queue_del, queue_getimap  

!------------------------------------------------------------------------+
  contains
!------------------------------------------------------------------------+

  subroutine allocate_queue(queue,nmax,nsize)
    implicit none
 
    type(queueinfo),  intent(inout) :: queue
    integer,          intent(in)    :: nmax,nsize

    allocate(queue%list(nsize,nmax))

    queue%nmax   = nmax
    queue%nsize  = nsize
    queue%ifront = 1
    queue%irear  = 0

  end subroutine

  subroutine deallocate_queue(queue)
    implicit none
 
    type(queueinfo), intent(inout) :: queue

    deallocate(queue%list)
    queue%nmax   = 0
    queue%nsize  = 0
    queue%ifront = 1
    queue%irear  = 0 
   
  end subroutine

  recursive subroutine queue_add(queue,item,ierr,nsize)
    implicit none

    include 'fortrankinds.h'

    ! dimensions
    integer, intent(in) :: nsize

    ! output
    type(queueinfo), intent(inout) :: queue 
    integer,         intent(inout) :: ierr

    ! input
    real(kdp),       intent(in) :: item(nsize)

    ! local
    integer :: idx

    ! sanity check
    if (nsize /= queue%nsize) call quit("wrong size","queue_add")


    if (queue%irear == 0 ) then
      ! init (first entry)
      queue%irear = 1 
      idx = queue%irear ; ierr = 0
    else
      if (queue%irear == queue%nmax) then
        queue%irear = 1  ! start from begining
      else
        queue%irear = queue%irear + 1
      end if

      idx = queue%irear
      if ( queue%ifront /= queue%irear ) then
        ierr = 0
      else
        ierr = 0
        if (queue%ifront == queue%nmax) then
          queue%ifront = 1
        else
          queue%ifront = queue%ifront + 1
        end if
      end if
    end if

    call dcopy(nsize,item,1,queue%list(1,idx),1)
  end subroutine

  subroutine queue_del(queue,ierr)
    implicit none

    ! output
    type(queueinfo), intent(inout) :: queue 
    integer,         intent(inout) :: ierr

    ! local
    integer :: idx


    if ( queue%ifront == queue%irear ) then
      ierr = -1 ! underflow
    else
      ierr = 0
      if ( queue%ifront == queue%nmax ) then
        queue%ifront = 1
      else
        queue%ifront  = queue%ifront + 1
      end if
    end if

  end subroutine

  subroutine queue_getimap(queue,imap,nmax)

    ! dimensions
    integer, intent(in)    :: nmax

    ! input
    type(queueinfo), intent(in) :: queue 

    ! output
    integer,         intent(inout) :: imap(nmax)

    ! local
    integer :: nend,istart,idx,jdx 

    if ( queue%ifront > queue%irear ) then
      nend = queue%nmax
    else
      nend = queue%irear 
    end if    

    istart = queue%ifront
    jdx = 0
    do idx = istart, nend
      jdx = jdx + 1
      imap(jdx) = idx
    end do

    if ( queue%ifront > queue%irear ) then
      do idx = 1, queue%ifront - 1
        jdx = jdx + 1
        imap(jdx) = idx
      end do
    end if

  end subroutine

end module

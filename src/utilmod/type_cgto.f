module type_cgto

 use type_pgto

 implicit none

  private

  include 'fortrankinds.h'
  include 'numbers.h'


  type cgto_info

    real(kdp) :: origin(3) = (/ zero, zero, zero /)
    integer   :: la = 0
    integer   :: ma = 0
    integer   :: na = 0

    integer   :: ishell = -1 ! to which shell does this GTO belong?

    integer   :: nprim
 
    type(pgto_info), pointer :: prim(:) => null()

    real(kdp),pointer :: exps(:)  => null()  ! exponents of the primitives
    real(kdp),pointer :: coefs(:) => null()  ! coefficients for the primivitives
    real(kdp),pointer :: norms(:) => null()  ! norms for the primitives
    real(kdp),pointer :: cn(:)    => null()  ! coefficients x norm 

    logical :: isallocated = .false.

  end type cgto_info 

  public :: allocate_cgto,deallocate_cgto,copy_cgto,&
            cgto_info,cgto_movecenter, cgto_overlap,&
            cgto_kinetic, cgto_nuclear, cgto_coulomb,&
            cgto_setprimitive, cgto_setprimitives,&
            cgto_multipole, cgto_normalize, cgto_shift_origin

!------------------------------------------------------------------------------!
  contains
!------------------------------------------------------------------------------!

  subroutine allocate_cgto(cgto,nprim)
    implicit none

    include 'fortrankinds.h'

    type(cgto_info), intent(inout) :: cgto
    integer,         intent(in)    :: nprim

    cgto%nprim = nprim

    allocate(cgto%prim(nprim))

    allocate(cgto%exps(nprim))

    allocate(cgto%norms(nprim))

    allocate(cgto%coefs(nprim))

    allocate(cgto%cn(nprim))

    cgto%isallocated = .true.
    
  end subroutine

  subroutine deallocate_cgto(cgto)
    implicit none

    include 'fortrankinds.h'

    type(cgto_info), intent(inout) :: cgto
    
    if (cgto%isallocated) then
      deallocate(cgto%prim) 
      deallocate(cgto%exps)
      deallocate(cgto%norms)
      deallocate(cgto%coefs)
      deallocate(cgto%cn)
      cgto%isallocated = .false.
    end if

    cgto%prim  => null()
    cgto%exps  => null()
    cgto%norms => null()
    cgto%coefs => null()
    cgto%cn    => null()

    cgto%nprim = 0 

  end subroutine

  subroutine copy_cgto(this,other)
    implicit none

    type(cgto_info), intent(inout) :: this, other

    ! local
    integer :: iprim,nprim

    ! destroy old object
    call deallocate_cgto(other)

    other%nprim = this%nprim

    nprim = this%nprim
    
    other%origin = this%origin
    other%la = this%la
    other%ma = this%ma
    other%na = this%na

    other%ishell = this%ishell

    allocate(other%prim(nprim), other%exps(nprim), other%norms(nprim), other%coefs(nprim), other%cn(nprim))

    do iprim = 1, nprim
      other%prim(iprim)   = this%prim(iprim)
      other%exps(iprim)   = this%exps(iprim) 
      other%norms(iprim)  = this%norms(iprim)
      other%coefs(iprim)  = this%coefs(iprim) 
      other%cn(iprim)     = this%cn(iprim) 
    end do

  end subroutine
  

  subroutine cgto_setprimitive(cgto,ind,expo,coef)
    use type_pgto

    implicit none

    include 'fortrankinds.h'

    type(cgto_info), intent(inout) :: cgto
    integer,   intent(in) :: ind
    real(kdp), intent(in) :: expo, coef

    cgto%exps(ind)  = expo
    cgto%coefs(ind) = coef

    cgto%prim(ind)%la = cgto%la
    cgto%prim(ind)%ma = cgto%ma
    cgto%prim(ind)%na = cgto%na

    cgto%prim(ind)%expo = expo
    cgto%prim(ind)%coef = coef

    cgto%prim(ind)%Origin = cgto%Origin

    ! normalize primitive
    call pgto_normalize(cgto%prim(ind))
    cgto%norms(ind) = cgto%prim(ind)%norm

    cgto%cn(ind) = cgto%norms(ind) * cgto%coefs(ind)

  end subroutine

  subroutine cgto_normalize(cgto)
    use type_pgto

    implicit none

    include 'fortrankinds.h'

    type(cgto_info), intent(inout) :: cgto

    real(kdp) :: Sij, norm
    integer   :: iprim, nprim

    ! normalize CGTO
    Sij = cgto_overlap(cgto,cgto)
    norm = 1.d0 / dsqrt(Sij)
   
    call dscal(cgto%nprim,norm,cgto%cn,1)
    call dscal(cgto%nprim,norm,cgto%coefs,1)

    nprim = cgto%nprim

    ! Also add new norm to primitives (GS I need to remove the primitive type since it is redundant)
    do iprim = 1, nprim
      cgto%prim(iprim)%norm = cgto%prim(iprim)%norm * norm 
    end do

  end subroutine

  subroutine cgto_setprimitives(cgto,exps,coefs)
    use type_pgto

    implicit none

    include 'fortrankinds.h'

    type(cgto_info), intent(inout) :: cgto
    real(kdp), intent(in) :: exps(cgto%nprim), coefs(cgto%nprim)

    ! local
    integer   :: iprim,nprim

    nprim = cgto%nprim

    do iprim = 1, nprim
      cgto%exps(iprim)  = exps(iprim)
      cgto%coefs(iprim) = coefs(iprim)

      cgto%prim(iprim)%la = cgto%la
      cgto%prim(iprim)%ma = cgto%ma
      cgto%prim(iprim)%na = cgto%na

      cgto%prim(iprim)%expo = exps(iprim)
      cgto%prim(iprim)%coef = coefs(iprim)

      cgto%prim(iprim)%Origin = cgto%Origin

      ! normalize primitive
      call pgto_normalize(cgto%prim(iprim))
      cgto%norms(iprim) = cgto%prim(iprim)%norm

      cgto%cn(iprim) = cgto%norms(iprim) * cgto%coefs(iprim)
    end do

  end subroutine
  
  subroutine cgto_movecenter(cgto,dx,dy,dz)
    use type_pgto
    
    implicit none

    include 'fortrankinds.h'

    real(kdp),       intent(in) :: dx,dy,dz
    type(cgto_info), intent(inout) :: cgto

    integer :: i

    do i = 1, cgto%nprim
      call pgto_movecenter(cgto%prim(i),dx,dy,dz)
    end do

  end subroutine


  function cgto_overlap(cgto,other) result(dret)
    use type_pgto
    
    implicit none

    include 'fortrankinds.h'

    real(kdp) :: dret

    type(cgto_info), intent(in) :: cgto,other

    integer :: i,j

    dret = 0.0d0

    do i = 1, cgto%nprim
      do j = 1, other%nprim
        dret = dret + cgto%prim(i)%coef * other%prim(j)%coef * pgto_overlap(cgto%prim(i),other%prim(j))
      end do
    end do


    return
  end function

  function cgto_kinetic(cgto,other) result(dret)
    use type_pgto
    
    implicit none

    include 'fortrankinds.h'

    real(kdp) :: dret


    type(cgto_info), intent(in) :: cgto,other

    
    integer :: i,j

    dret = 0.0d0

    do i = 1, cgto%nprim
      do j = 1, other%nprim
        dret = dret + cgto%prim(i)%coef * other%prim(j)%coef * pgto_kinetic(cgto%prim(i),other%prim(j))
      end do
    end do

    return
  end function

  function cgto_nuclear(cgto,other,xyz_nuc) result(dret)
    use type_pgto
    
    implicit none

    include 'fortrankinds.h'

    real(kdp) :: dret

    type(cgto_info), intent(in) :: cgto,other
    real(kdp),  intent(in) :: xyz_nuc(3)

    integer :: i,j

    dret = 0.0d0

    do i = 1, cgto%nprim
      do j = 1, other%nprim
        dret = dret + cgto%prim(i)%coef * other%prim(j)%coef * pgto_nuclear(cgto%prim(i),other%prim(j),xyz_nuc)
      end do
    end do

 
    return
  end function

  function cgto_coulomb(gA,gB,gC,gD,ibf,jbf,kbf,lbf,xyzp,xyzq,Kab,Kcd,&
                        use_os,vrr_terms,maxmtot,maxam,nbuf,&
                        fntab,nxvals,nrange,kmax,nprimi,nprimj,nprimk,npriml) result(dret)
    implicit none

    include 'fortrankinds.h'

    real(kdp) :: dret
  
    ! dimensions
    integer, intent(in) :: nxvals,nrange,kmax,nprimi,nprimj,nprimk,npriml

    ! input
    type(cgto_info), intent(in) :: gA,gB,gC,gD
    real(kdp),       intent(in) :: fntab(0:kmax,nxvals)
    integer,         intent(in) :: ibf,jbf,kbf,lbf

    ! when using Obara-Saika scheme
    integer,   intent(in)    :: maxmtot,maxam,nbuf
    logical,   intent(in)    :: use_os
    real(kdp), intent(inout) :: vrr_terms(nbuf)
    real(kdp), intent(in)    :: xyzp(3,nprimi,nprimj),xyzq(3,nprimk,npriml),&
                                Kab(nprimi,nprimj),Kcd(nprimk,npriml)

    ! functions
    real(kdp) :: contr_coulomb, os_contr_coulomb

    if (use_os) then
      dret = os_contr_coulomb(gA%exps,gA%coefs,gA%norms,gA%origin,gA%la,gA%ma,gA%na, &
                              gB%exps,gB%coefs,gB%norms,gB%origin,gB%la,gB%ma,gB%na, &
                              gC%exps,gC%coefs,gC%norms,gC%origin,gC%la,gC%ma,gc%na, &
                              gD%exps,gD%coefs,gD%norms,gD%origin,gD%la,gD%ma,gD%na, &
                              gA%nprim,gB%nprim,gC%nprim,gD%nprim,&
                              ibf,jbf,kbf,lbf,xyzp,xyzq,Kab,Kcd,&
                              vrr_terms,maxmtot,maxam,nbuf,&
                              fntab,nxvals,nrange,kmax)
    else
      dret = contr_coulomb(gA%exps,gA%coefs,gA%norms,gA%origin,gA%la,gA%ma,gA%na,&
                           gB%exps,gB%coefs,gB%norms,gB%origin,gB%la,gB%ma,gB%na,&
                           gC%exps,gC%coefs,gC%norms,gC%origin,gC%la,gC%ma,gC%na,&
                           gD%exps,gD%coefs,gD%norms,gD%origin,gD%la,gD%ma,gD%na,&
                           gA%nprim,gB%nprim,gC%nprim,gD%nprim)
    end if

    return
  end function


  function cgto_multipole(cgto,other,origin,lx,mx,nx) result(dret)
    use type_pgto
    
    implicit none

    include 'fortrankinds.h'

    real(kdp) :: dret

    ! input
    type(cgto_info), intent(in) :: cgto,other
    real(kdp),       intent(in) :: origin(3)
    integer,         intent(in) :: lx,mx,nx

    ! local
    integer :: i,j

    dret = 0.0d0

    do i = 1, cgto%nprim
      do j = 1, other%nprim
        dret = dret + cgto%prim(i)%coef * other%prim(j)%coef * &
                     pgto_multipole(cgto%prim(i),other%prim(j),origin,lx,mx,nx)
      end do
    end do


    return
  end function

  subroutine cgto_shift_origin(cgto,shift)
    implicit none

    include 'fortrankinds.h'

    ! input/output
    type(cgto_info), intent(inout) :: cgto
    real(kdp),       intent(in)    :: shift(3)         

    ! local 
    integer :: iprim

    cgto%origin = cgto%origin + shift

    do iprim = 1, cgto%nprim
      call pgto_shift_origin(cgto%prim(iprim),shift)
    end do

  end subroutine

end module

 

module mod_fileutil

  implicit none

  integer, parameter, private :: maxfil = 200 
  integer, parameter, private :: maxlen = 512

  character(len=maxlen), private :: filename(0:maxfil)
  logical, private               :: free(0:maxfil)
  logical, private               :: isinit = .false.

 public openfile, closefile

!---------------------------------------------------------------------+ 
 contains
!---------------------------------------------------------------------+ 

   subroutine init_modfileutil()
     free(0:10) = .false.        ! assume that the first 10 unit numerber are special
     free(11:maxfil) = .true.    ! that unitnumbers as available
     filename(0:maxfil) = "/--unkown file--/"
     isinit = .true.
   end subroutine
 
   function openfile(ffile,mode) result(ifile)
     implicit none

     include 'stdout.h'
     
     ! return value 
     integer :: ifile

     character(len=80), intent(in)  :: ffile
     character(len=*),  intent(in) :: mode

     ! sanity check
     if (.not.isinit) then
       call quit('mod_fileutil has not been initialized','openfile') 
     end if     

     ifile = getnextunit()
     filename(ifile) = trim(ffile)

     open(ifile,file=trim(ffile),form='formatted',status=trim(mode)) 
    
     return
   end function
 
   subroutine closefile(ifile)
     implicit none
     
     include 'stdout.h'

     integer, intent(in) :: ifile

     ! sanity check
     if (.not.isinit) then
       call quit('mod_fileutil has not been initialized','closefile')
     end if 

     close(ifile)

     free(ifile) = .true.
     filename(ifile) = "/--unkown file--/"
   end subroutine
 
   function getnextunit() result(ifil)
     implicit none

     include 'stdout.h'

     integer :: ifil,ind

     ! sanity check
     if (.not.isinit) then
       call quit('mod_fileutil has not been initialized','getnextunit')
     end if 

     do ind = 1, maxfil
       if (free(ind)) then
         free(ind) = .false.
         ifil = ind; exit
       end if
     end do

     return
   end function

  subroutine freeunit(iunit) 
    implicit none
 
    integer, intent(in) :: iunit

    free(iunit) = .true.
  end subroutine
end module

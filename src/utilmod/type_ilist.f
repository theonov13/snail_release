module type_ilist

  implicit none

  type node
     type(node), pointer :: next => null()
     type(node), pointer :: prev => null()
     integer :: data
  end type node
 
  type ilist
     type(node), pointer :: head => null()
     type(node), pointer :: tail => null()
     integer :: num_nodes = 0
  end type ilist
 
  public  :: node, ilist, ilst_append, ilst_prepend, ilst_insert, ilst_print, ilst_deallocate 
  private :: init
 
!------------------------------------------------------------------------------+
  contains
!------------------------------------------------------------------------------+

  !
  ! Create a new doubly-linked list
  !
  elemental type(ilist) function new_ilist()

    implicit none

    new_ilist = ilist(null(),null(),0)
    return
  end function new_ilist

  !
  ! Create a new doubly-linked list from string
  !
  elemental type(ilist) function ilst_fromstring(string)

    implicit none

   ! input
   character(len=80), intent(in) :: string

    ilst_fromstring = ilist(null(),null(),0)

    ! TODO

    return
  end function ilst_fromstring
 
  !
  ! Append an element to the end of the list
  !
  elemental subroutine ilst_append(ilst, value)

    implicit none

    ! input/output
    type(ilist), intent(inout) :: ilst

    ! input
    integer, intent(in)        :: value
 
    ! local
    type(node), pointer :: np
 
    ! If the list is empty
    if (ilst%num_nodes == 0) then
       call init(ilst, value)
       return
    end if
 
    ! Add new element to the end
    ilst%num_nodes = ilst%num_nodes + 1
    np => ilst%tail
    allocate(ilst%tail)
    ilst%tail%data = value
    ilst%tail%prev => np
    ilst%tail%prev%next => ilst%tail
  end subroutine ilst_append

  ! 
  ! Prepend an element to the beginning of the list
  !
  elemental subroutine ilst_prepend(ilst, value)

    implicit none

    ! input/output
    type(ilist), intent(inout) :: ilst

    ! input
    integer, intent(in)        :: value
 
    ! local
    type(node), pointer :: np
 
    if (ilst%num_nodes == 0) then
       call init(ilst, value)
       return
    end if
 
    ilst%num_nodes = ilst%num_nodes + 1
    np => ilst%head
    allocate(ilst%head)
    ilst%head%data = value
    ilst%head%next => np
    ilst%head%next%prev => ilst%head
  end subroutine ilst_prepend
 
  !
  ! Insert immediately before the given index
  !
  elemental subroutine ilst_insert(ilst, index, value)

    implicit none

    ! input/output
    type(ilist), intent(inout) :: ilst

    ! input
    integer, intent(in)        :: index
    integer, intent(in)        :: value

    ! local
    type(node), pointer :: element
    type(node), pointer :: np1, np2
    integer             :: i
 
    if (ilst%num_nodes == 0) then
       call init(ilst, value)
       return
    end if
 
    ! If index is beyond the end then append
    if (index > ilst%num_nodes) then
       call ilst_append(ilst, value)
       return
    end if
 
    ! If index is less than 1 then prepend
    if (index <= 1) then
       call ilst_prepend(ilst, value)
       return
    end if
 
    ! Find the node at position 'index' counting from 1
    np1 => ilst%head
    do i=1, index-2
       np1 => np1%next
    end do
    np2 => np1%next
 
    ! Create the new node
    allocate(element)
    element%data = value
 
    ! Connect it up
    element%prev => np1
    element%next => np2
    np1%next => element
    np2%prev => element
    ilst%num_nodes = ilst%num_nodes + 1
  end subroutine ilst_insert
 
  !
  ! print content of the list
  !
  subroutine ilst_print(ilst)

    implicit none

    include 'stdout.h'

    ! input
    type(ilist), intent(in) :: ilst

    ! local
    type(node), pointer     :: current
    integer :: i
 
    write(istdout,fmt='(a,i0,a)',advance='no') 'Doubly-linked list has ',ilst%num_nodes,' element - fwd = '
    current => ilst%head
    i = 1
    write(istdout,fmt='(i0,a)',advance='no') current%data,', '
    do
       current => current%next
       if (.not. associated(current)) then
          exit
       end if
       i = i + 1
       if (i == ilst%num_nodes) then
          write(istdout,'(i0)') current%data
       else
          write(istdout,fmt='(i0,a)',advance='no') current%data,', '
       end if
    end do

  end subroutine ilst_print

  ! 
  ! Deallocate all allocated memory
  !
  elemental subroutine ilst_deallocate(ilst)

    implicit none

    ! input/output
    type(ilist), intent(inout) :: ilst

    ! local
    type(node), pointer :: current, last
 
    current => ilst%head
    do
       last => current
       current => current%next
       if (associated(last)) then
          deallocate(last)
       end if
       if (associated(current, ilst%tail)) then
          deallocate(current)
          exit
       end if
    end do
  end subroutine ilst_deallocate 
 
  !
  ! Init list
  !
  elemental subroutine init(ilst, value)

    implicit none

    ! input/output
    type(ilist), intent(inout) :: ilst

    ! input
    integer, intent(in)        :: value

    allocate(ilst%head)
    ilst%tail => ilst%head
    ilst%tail%data = value
    ilst%num_nodes = 1
    return
  end subroutine init
 
end module type_ilist

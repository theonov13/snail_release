
subroutine get2ints(vpq,                                & ! out
                    verbose,tolint,memmax,lmax,xshells, & ! inp 
                    nxbf,nxshell,mxprimx,mxcartx)         ! dim 
!-----------------------------------------------------------------------------+
!  Purpose: Calculates the three index integrals
!
!  Gunnar Schmitz 
!-----------------------------------------------------------------------------+

   use type_integ
   use type_cgto
   use type_shell
   use mod_fileutil
   use BoysTable

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'
   include 'constants.h'

   ! constants
   logical,   parameter :: locdbg      = .false.

   ! dimensions
   integer, intent(in) :: nxbf,nxshell,mxprimx,mxcartx

   ! input
   type(shell_info), intent(in)   :: xshells(nxshell)
   integer,          intent(in)   :: lmax
   integer(kli),     intent(in)   :: memmax
   real(kdp),        intent(in)   :: tolint
   logical,          intent(in)   :: verbose

   ! output
   real(kdp),       intent(out)   :: vpq(nxbf,nxbf)

   ! local
   integer      :: ibf,kbf,ind,&
                   ishl,kshl,&
                   lbfmax,lshlmax,nbuf,&
                   ibfst,ibfnd,kbfst,kbfnd,&
                   icart,kcart,&
                   ncompi,ncompk,lmxshlpr,&
                   ammax,lammax,ik

   integer :: maxcart,maxprim

   integer(kli) :: memused

   integer :: maxmtot,maxam

   integer :: iunftab,k,reclen,irec

   real(kdp) :: dtemp,dtemp1,xa,ya,za,xb,yb,zb

   real(kdp) :: alphai, alphak
   integer   :: nprimi, nprimk, nprimik
   integer   :: ncarti, ncartk
   integer   :: idx,iadr

   integer      :: tmax, umax, vmax
   integer(kli) :: memetuv(2),memboys,memrtuv,memgtuv,memftuv,memgcdtuv
   real(kdp)    :: dmem
 
   type(cgto_info), pointer :: bfi, bfk

   real(kdp), allocatable :: eat(:,:),eau(:,:),eav(:,:),&
                             ect(:,:),ecu(:,:),ecv(:,:),&
                             eabtuv(:,:),ecdtuv(:,:),&
                             Fn(:),rtuv(:),gtuv(:),ftuv(:),&
                             gcdtuv(:)                           

   real(kdp), allocatable :: cni(:),cnk(:)

   real(kdp), allocatable :: o2p(:), u(:), uA(:), uB(:)

   ! for integral evaluation
   type(integ_info) :: intopt
   type(TBoysTable) :: boystab

   ! basis function information
   real(kdp), allocatable :: bfiexps(:), bficoefs(:), bfinorms(:), &
                             bfkexps(:), bfkcoefs(:), bfknorms(:)

   integer,   allocatable :: bfila(:), bfima(:), bfina(:), &
                             bfkla(:), bfkma(:), bfkna(:)

   real(kdp), allocatable :: gout(:)

   integer   :: iprim,kprim
   real(kdp) :: p, q, boysarg, rpq
   real(kdp), allocatable :: alpha(:),dx(:),dy(:),dz(:),pfac(:)

   ! functions
   real(kdp) :: dist2_3d, product_center_1D, dist

   ! init
   memused = 0

   ! integral scheme
   intopt%scheme = INT_MMD

   bfi => null()
   bfk => null()

   maxcart = mxcartx
   maxprim = mxprimx

   !-----------------------------------------------------------------------+
   ! allocate buffer for local basis function info 
   !-----------------------------------------------------------------------+
   allocate( bfiexps(maxprim*maxcart), bficoefs(maxprim*maxcart), bfinorms(maxprim*maxcart), &
             bfkexps(maxprim*maxcart), bfkcoefs(maxprim*maxcart), bfknorms(maxprim*maxcart)  )

   allocate( bfila(maxcart), bfima(maxcart), bfina(maxcart), &
             bfkla(maxcart), bfkma(maxcart), bfkna(maxcart)  )
  
   allocate( gout(maxcart*maxcart) )

   allocate(cni(maxprim*maxcart),cnk(maxprim*maxcart))

   !-----------------------------------------------------------------------+
   ! allocate buffer for Integral schemes 
   !-----------------------------------------------------------------------+

   ! McMurrchie Davidson scheme
   if (intopt%scheme == INT_MMD) then
     memetuv(1) = maxprim * (lmax+2) * (lmax+2) 

     allocate(eat(memetuv(1),maxcart),eau(memetuv(1),maxcart),eav(memetuv(1),maxcart), &   
              ect(memetuv(1),maxcart),ecu(memetuv(1),maxcart),ecv(memetuv(1),maxcart)  ) 

     memetuv(2) = maxprim * (lmax+2) * (lmax+2)

     allocate(eabtuv(memetuv(2),maxcart),ecdtuv(memetuv(2),maxcart))

     lmxshlpr = lmax * 2

     ammax   = 4 * lmax
     memboys = (3*(ammax+1)) * maxprim**4
     memrtuv = maxprim**4     * (lmxshlpr+1)**3 * (3*(lmxshlpr+1)) 
     memgtuv = maxprim**4     * (lmxshlpr+1)**3
     memftuv = maxprim**2     * (2*lmax)**3

     memgcdtuv = maxprim**2   * (2*lmax+2)**3 

     allocate(Fn(memboys))
     allocate(rtuv(memrtuv))
     allocate(gtuv(memgtuv))
     allocate(ftuv(memftuv))
     allocate(gcdtuv(memgcdtuv)) 

     dmem = (memetuv(1)*maxcart*6 + memboys + memrtuv + memgtuv + memftuv + memgcdtuv) * 8.0 / (1024*1024)

     if (verbose) write(6,'(5x,a,f10.2,2x,a)') "Allocated", dmem, " MB for int. evaluation"  

     allocate(o2p(maxprim),u(maxprim),uA(maxprim),uB(maxprim))

   end if

   !-----------------------------------------------------------------------+
   ! read in pre-tabulated values for Boys-function 
   !-----------------------------------------------------------------------+

   call allocate_boystable(boystab)

   !-----------------------------------------------------------------------+
   ! calculate integrals (I|K) 
   !-----------------------------------------------------------------------+

   do ishl = 1, nxshell
     ibfst = xshells(ishl)%ibfstart; ibfnd = xshells(ishl)%ibfend
     nprimi = xshells(ishl)%nprim; ncarti = xshells(ishl)%ncart
     ncarti = xshells(ishl)%ncart
     call copybasinfo(bfiexps, bficoefs, bfinorms, bfila, bfima, bfina, xshells, ishl, nprimi, ncarti, nxshell)

     ! Get E coefficients for I aux shell
     call GetAuxShellInfo(intopt,eat,eau,eav,eabtuv,cni,xshells,ishl,o2p,u,uA,uB,nprimi,nxshell,ncarti,memetuv,maxcart)

     do kshl = 1, nxshell 
       kbfst = xshells(kshl)%ibfstart; kbfnd = xshells(kshl)%ibfend
       nprimk = xshells(kshl)%nprim; ncartk = xshells(kshl)%ncart
       ncartk = xshells(kshl)%ncart
       call copybasinfo(bfkexps, bfkcoefs, bfknorms, bfkla, bfkma, bfkna, xshells, kshl, nprimk, ncartk, nxshell)

       ! Get E coefficients for K aux shell
       call GetAuxShellInfo(intopt,ect,ecu,ecv,ecdtuv,cnk,xshells,kshl,o2p,u,uA,uB,nprimk,nxshell,ncartk,memetuv,maxcart)

       !-----------------------------------------------------------------------------------+ 
       ! calculate the Boys function for shell triple (ij|K) 
       !-----------------------------------------------------------------------------------+ 

       nprimik = nprimi * nprimk

       allocate(pfac(nprimik),alpha(nprimik),dx(nprimik),dy(nprimik),dz(nprimik))

       ! just reference the first basis function in the shells 
       bfi => xshells(ishl)%gtos(1) 
       bfk => xshells(kshl)%gtos(1)

       ! maximum angular momentum to generate the required integrals
       lammax = bfi%la + bfk%la + bfi%ma + bfk%ma + bfi%na + bfk%na
       do iprim = 1, bfi%nprim
         p = bfi%exps(iprim) 
         do kprim = 1, bfk%nprim
             q = bfk%exps(kprim) 

             ik = (kprim-1) * bfi%nprim + iprim 

             alpha(ik) = p*q / (p+q)
             rpq = dist(bfi%origin,bfk%origin)
             boysarg = alpha(ik) * rpq * rpq
             call BoysFArr(lammax,boysarg,Fn((ik-1)*(lammax+1) + 1),  &
                           boystab%fntab,.true.,boystab%nxvals,boystab%nrange,boystab%kmax)
             
             dx(ik) = bfi%origin(1)-bfk%origin(1)
             dy(ik) = bfi%origin(2)-bfk%origin(2)
             dz(ik) = bfi%origin(3)-bfk%origin(3)
             pfac(ik) = two * pi**(2.5d0) / ( p * q * dsqrt (p+q) )

         end do
       end do

       if ( intopt%scheme == INT_MMD ) then
         ! McMurchie Davidson scheme
         call mmd_2int(gout,&
                       bfiexps,bficoefs,bfinorms,bfila,bfima,bfina,  &
                       bfkexps,bfkcoefs,bfknorms,bfkla,bfkma,bfkna,  &
                       eat,eau,eav,eabtuv,cni,ect,ecu,ecv,ecdtuv,cnk,&
                       Fn,rtuv,gtuv,gcdtuv,ftuv,pfac,&
                       alpha,dx,dy,dz,ammax,lmax,&
                       nprimi,nprimk,nprimik,ncarti,ncartk,memetuv) 
       
       else
         call quit("Currently only Mc-Murchie Davidson scheme for 3 index integrals!","get3ints")
       end if

       deallocate(pfac,alpha,dx,dy,dz)

       !---------------------------------------------------------------+
       ! copy local integral to full set
       !---------------------------------------------------------------+
       do kcart = 1, ncartk
         kbf = kcart + kbfst - 1
         do icart = 1, ncarti
           ibf = icart + ibfst - 1

           iadr = (kcart-1) * ncarti + icart

           vpq(kbf,ibf) = gout(iadr)
           vpq(ibf,kbf) = gout(iadr)
         end do
       end do 
     end do
   end do 

   bfi => null()
   bfk => null()

   call deallocate_boystable(boystab)

end subroutine

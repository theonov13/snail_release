
pure subroutine s000(gout,&                            ! out
                     eabtuv,cnab,ectuv,cnc,Fn,pfac,&   ! inp
                     nprimij,nprimk,nprimijkl)         ! dim 
  implicit none


  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'

  ! dimensions
  integer, intent(in) :: nprimij,nprimk,nprimijkl
 
  ! output:
  real(kdp), intent(out) :: gout(1)

  ! input:
  real(kdp), intent(in) :: eabtuv(nprimij), ectuv(nprimk) 

  real(kdp), intent(in) :: cnab(nprimij),cnc(nprimk)

  real(kdp), intent(in) :: Fn(0:0,nprimijkl)

  real(kdp), intent(in) :: pfac(nprimijkl)


  ! local
  integer   :: ijp,k,ijkl
  real(kdp) :: val,dfac,ftuv,gcdtuv

  !--------------------------------------------------------------------+
  !  Transform to Cartesian Coulomb integrals and contract 
  !--------------------------------------------------------------------+
  
  ! Build:
  !   F_{tau,nu,phi}^{C_r} = (-1)^(tau+nu+phi) * E_{tau,phi,nu}^{C_r}
  !
  ! Transform:
  !         (tuv|C_r] = \sum_{tau,phi,nu} (-1)^(tau+nu+phi) E_{tau,phi,nu}^{C_r} R_{t+tau,u+nu,v+phi}
  !   a.k.a (tuv|C_r] = \sum_{tau,phi,nu} F_{tau,phi,nu}^{C_r} R_{t+tau,u+nu,v+phi}
  !
  ! Contract:
  !   (tuv|C) =  \sum_{rs} T_r (tuv|C_r] 
  !
  ! Transform:
  !   [a_m b_n|C) = \sum_{tuv} E_{t,u,v}^{a_m b_n} (tuv|C)
  !
  ! Contract:
  !   (ab|C) = \sum_{mn} C_m C_n [a_m b_n|C) 

  val = zero
  do ijp = 1, nprimij

    gcdtuv = zero
    do k = 1, nprimk
      ftuv = ectuv(k)  
      dfac = cnc(k)
      ijkl = (k-1) * nprimij + ijp

      dfac = ftuv * pfac(ijkl)

      gcdtuv = gcdtuv + dfac * Fn(0,ijkl) * cnc(k)

    end do

    val = val + gcdtuv * eabtuv(ijp) * cnab(ijp)    

  end do

  gout(1) = val
  
end subroutine

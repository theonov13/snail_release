
subroutine GetAuxShellInfo(intopt,&                        ! Option for integral scheme
                           eabt,eabu,eabv,eabtuv,cnk,&     ! Output for McMurchie-Davidson scheme
                           shells, kshl,  &                ! input
                           o2p,u,uA,ub,&                   ! scratch  
                           nprimk, nshell, ncartk, memetuv, mxcart)

!------------------------------------------------------------------------+
!  Purpose: Precomputes shell information for different integral schemes
!------------------------------------------------------------------------+

   use type_integ
   use type_cgto
   use type_shell

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'
   include 'constants.h'

   ! dimensions
   integer,      intent(in) :: nprimk, nshell, ncartk, mxcart
   integer(kli), intent(in) :: memetuv(2)

   ! output
   real(kdp), intent(out) :: eabt(memetuv(1),mxcart*mxcart),&
                             eabu(memetuv(1),mxcart*mxcart),&
                             eabv(memetuv(1),mxcart*mxcart),&
                             eabtuv(memetuv(2),mxcart*mxcart),&
                             cnk(nprimk,ncartk) 

   ! input/output
   real(kdp), intent(inout) :: o2p(nprimk),u(nprimk),uA(nprimk),uB(nprimk)

   ! input
   type(integ_info), intent(in)   :: intopt
   type(shell_info), intent(in)   :: shells(nshell)
   integer,          intent(in)   :: kshl 

   ! local
   integer   :: kcomp, ncompk, kbfst, kbfnd, kprim, kcart
   real(kdp) :: distij(3),Kab,dK
 
   type(cgto_info), pointer :: bfk

   !-----------------------------------------------------------------+
   ! Actual subroutine
   !-----------------------------------------------------------------+

   ! pre-calculate the product of the primitive norm and coefficients for this shell pair
   do kcart = 1, ncartk
     do kprim = 1, nprimk
       dk = shells(kshl)%gtos(kcart)%coefs(kprim) * shells(kshl)%gtos(kcart)%norms(kprim) 

       cnk(kprim,kcart) = dk
     end do
   end do 

   if (intopt%scheme == INT_MMD .or. intopt%scheme == INT_MMDET) then
     ! McMurchie-Davidson Integral algorithm

     !Kab = abinv * dexp(-alphai * alphaj * abinv * rij2)
     Kab = 1.d0 ! Preset it another time...

     ! calculate some shell specific information
     kbfst = shells(kshl)%ibfstart; kbfnd = shells(kshl)%ibfend

     distij = zero 

     ! calculate Hermite expansion coefficients for shell pairs (ij) 
     ncompk = kbfnd - kbfst + 1
     do kcomp = 1, ncompk
       bfk => shells(kshl)%gtos(kcomp)

       call ecof_tij(eabt(1,kcomp),bfk%la,0,bfk%la,distij(1),bfk%exps,zero,Kab,o2p,u,uA,uB,nprimk,nprimk,1)
       call ecof_tij(eabu(1,kcomp),bfk%ma,0,bfk%ma,distij(2),bfk%exps,zero,Kab,o2p,u,uA,uB,nprimk,nprimk,1)
       call ecof_tij(eabv(1,kcomp),bfk%na,0,bfk%na,distij(3),bfk%exps,zero,Kab,o2p,u,uA,uB,nprimk,nprimk,1)

       ! Combine to one tensor
       call BuildETensor(eabtuv(1,kcomp),eabt(1,kcomp),eabu(1,kcomp),eabv(1,kcomp),  &
                         bfk%la,0,bfk%ma,0,bfk%na,0,bfk%la,bfk%ma,bfk%na,nprimk)

     end do
   else
     call quit("Unknown integral scheme!","GetAuxShellInfo")
   end if

end subroutine


subroutine mmd_3int(gout,&
                    aexps,acoefs,anorms,xyza,la,ma,na, &
                    bexps,bcoefs,bnorms,xyzb,lb,mb,nb, &
                    cexps,ccoefs,cnorms,xyzc,lc,mc,nc, &
                    eabt,eabu,eabv,eabtuv,cnab,&
                    ect,ecu,ecv,ectuv,cnc,&
                    Fn,rtuv,gtuv,gcdtuv,ftuv,pfac,&
                    alpha,dx,dy,dz,ammax,maxl,&
                    nprima,nprimb,nprimc,&
                    ncarta,ncartb,ncartc,&
                    nprimij,nprimkl,nprimijk,&
                    cpur,cpuf,cpug1,cpug2,cpug3,&
                    memetuv) 
  implicit none


  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'

  ! dimensions
  integer, intent(in) :: nprima,nprimb,nprimc
  integer, intent(in) :: ncarta,ncartb,ncartc
  integer, intent(in) :: nprimij,nprimkl,nprimijk
  integer, intent(in) :: ammax,maxl
 
  integer(kli), intent(in) :: memetuv(2)
 
  ! output:
  real(kdp), intent(out) :: gout(ncarta,ncartb,ncartc)

  ! input:
  real(kdp), intent(in) :: xyza(3),xyzb(3),xyzc(3)
  real(kdp), intent(in) :: anorms(nprima,ncarta),bnorms(nprimb,ncartb),&
                           cnorms(nprimc,ncartc)
  real(kdp), intent(in) :: aexps(nprima,ncarta),bexps(nprimb,ncartb),&
                           cexps(nprimc,ncartc)
  real(kdp), intent(in) :: acoefs(nprima,ncarta),bcoefs(nprimb,ncartb),&
                           ccoefs(nprimc,ncartc)
  integer,   intent(in) :: la(ncarta),ma(ncarta),na(ncarta),&
                           lb(ncartb),mb(ncartb),nb(ncartb),&
                           lc(ncartc),mc(ncartc),nc(ncartc)
  real(kdp), intent(in) :: eabt(memetuv(1),ncarta*ncartb),&
                           eabu(memetuv(1),ncarta*ncartb),&
                           eabv(memetuv(1),ncarta*ncartb),&
                           ect(memetuv(1),ncartc),&
                           ecu(memetuv(1),ncartc),&
                           ecv(memetuv(1),ncartc),&
                           eabtuv(memetuv(2),ncarta*ncartb),&
                           ectuv(memetuv(2),ncartc),&
                           cnab(nprimij,ncarta*ncartb),&
                           cnc(nprimc,ncartc)


  real(kdp), intent(inout) :: Fn(0:ammax,nprimijk), &
                              rtuv(nprimijk*(ammax+1)**3 * (3*(ammax+1)) ) , &
                              gtuv(nprimijk*(ammax+1)**3)                  , &
                              gcdtuv(nprimij*(1*maxl+2)**3)                , &
                              ftuv(nprimkl  *(2*maxl)**3)

  real(kdp), intent(in) :: pfac(nprimijk),alpha(nprimijk),&
                           dx(nprimijk),dy(nprimijk),dz(nprimijk)

  real(kdp), intent(inout) :: cpur,cpuf,cpug1,cpug2,cpug3

  ! local
  integer   :: i,j,k,ijp,tuv,tuv2,taunuphi,tuvbig,&
               labt,mabu,nabv,lcdt,mcdu,ncdv
  integer   :: icart,jcart,kcart,ijcart
  integer   :: t,u,v,tau,nu,phi,ijk
  integer   :: nnc,mmc,llc,&
               nna,mma,lla,&
               nnb,mmb,llb,&
               nab,mab,lab
  integer   :: iint
  integer   :: il,jl,kl,iprim,mxcartb
  integer   :: tmax,umax,vmax,lmax,nprijk
  real(kdp) :: val, dtemp, dfac
  real(kdp) :: cpu0,cpux

  real(kdp), allocatable :: gabcd(:)

  logical   :: atom12,atom23,atomic,case1
  integer   :: shellp,shellsum
  logical   :: equalij

  ! functions
  real(kdp) :: dist, ecof2

  nprijk = nprima*nprimb*nprimc

  allocate(gabcd(nprimij))

  il = la(1) + ma(1) + na(1) 
  jl = lb(1) + mb(1) + nb(1) 
  kl = lc(1) + mc(1) + nc(1) 

  iint = il * 100 + jl * 10 + kl 

  !------------------------------------------------------------------------------------------+
  ! decide on an early stage if integral is vanishing...
  !------------------------------------------------------------------------------------------+

  ! idea taken from libERD from Flocke et al.
  atom12 = (xyza(1) == xyzb(1)) .and. (xyza(2) == xyzb(2)) .and. (xyza(3) == xyzb(3))
  atom23 = (xyzb(1) == xyzc(1)) .and. (xyzb(2) == xyzc(2)) .and. (xyzb(3) == xyzc(3))

  atomic = (atom12 .and. atom23)

  shellp = il + jl  
  shellsum = shellp + kl

  case1 = mod(shellsum,2) .eq. 1

  if (atomic .and. (case1)) then
    gout = zero
    return
  end if

  !------------------------------------------------------------------------------------------+
  ! figure out if for shell pair ij  i = j  
  !------------------------------------------------------------------------------------------+

  equalij = atom12

  if (equalij) then
    equalij =  (il == jl) .and. (nprima == nprimb) .and. (ncarta == ncartb)
    if (equalij) then
      do iprim = 1, nprima
        equalij = equalij .and. (aexps(iprim,1) == bexps(iprim,1))
      end do
      if (equalij) then
        do jcart = 1, ncarta
          if (equalij) then
            do iprim = 1, nprima
              equalij = equalij .and. (acoefs(iprim,jcart) == bcoefs(iprim,jcart))
            end do
          end if
        end do
      end if
    end if
  end if

  select case(iint)

   !---------------------------------------------------------------------------+  
   !  Spcial cases
   !---------------------------------------------------------------------------+  

     case(000) ! (ss|s) 
       call s000(gout,&
                 eabtuv,cnab,ectuv,cnc,Fn,pfac,&
                 nprimij,nprimc,nprimijk) 

     case(100) ! (ps|s)
       call s100(gout,&
                 eabtuv(1,1),eabtuv(1,2),eabtuv(1,3),&
                 cnab,ectuv,cnc, &
                 Fn,rtuv,pfac,   &
                 alpha,dx,dy,dz, &
                 nprima,nprimb,nprimc,&
                 nprimij,nprimijk)
      
 
   !---------------------------------------------------------------------------+  
    case default   ! General cases...
   !---------------------------------------------------------------------------+  

      do icart = 1, ncarta
        nna = na(icart)
        mma = ma(icart)
        lla = la(icart)

        mxcartb = ncartb
        if (equalij) mxcartb = icart
        do jcart = 1, mxcartb
          nnb = nb(jcart) 
          mmb = mb(jcart) 
          llb = lb(jcart) 

          nab = nna + nnb
          mab = mma + mmb
          lab = lla + llb

          ijcart = (jcart-1)*ncarta + icart

          do kcart = 1, ncartc
            nnc = nc(kcart) 
            mmc = mc(kcart) 
            llc = lc(kcart) 

            !--------------------------------------------------------------------+
            !  Create Hermite Coulomb integrals via recursion from Boys-Function 
            !--------------------------------------------------------------------+

            tmax = lab + llc  
            umax = mab + mmc 
            vmax = nab + nnc

            lmax = tmax + umax + vmax 

            !call cpu_time(cpu0)
            call rcof_tuvn(rtuv,tmax,umax,vmax,0,alpha,dx,dy,dz,Fn,lmax,nprijk)
            !call cpu_time(cpux)
            !cpur = cpur + cpux - cpu0

            !--------------------------------------------------------------------+
            !  Transform to Cartesian Coulomb integrals and contract 
            !--------------------------------------------------------------------+
            
            ! Build:
            !   F_{tau,nu,phi}^{C_r} = (-1)^(tau+nu+phi) * E_{tau,phi,nu}^{C_r}
            !call cpu_time(cpu0)
            call contract_E_RHS(ftuv,ectuv(1,kcart),llc,0,mmc,0,nnc,0,nnc,mmc,llc,nprimc)
            !call cpu_time(cpux)
            !cpuf = cpuf + cpux - cpu0

            ! Transform:
            !         (tuv|C_r] = \sum_{tau,phi,nu} (-1)^(tau+nu+phi) E_{tau,phi,nu}^{C_r} R_{t+tau,u+nu,v+phi}
            !   a.k.a (tuv|C_r] = \sum_{tau,phi,nu} F_{tau,phi,nu}^{C_r} R_{t+tau,u+nu,v+phi}
            !call cpu_time(cpu0)

            call contract_HermTUV(gtuv,ftuv,rtuv,pfac,tmax,umax,vmax,eabt(1,ijcart),ect(1,kcart),&
                                  nnc,mmc,llc,nab,mab,lab,nprimijk,nprimij,nprimc)

            !call cpu_time(cpux)
            !cpug1 = cpug1 + cpux - cpu0

            ! Contract:
            !   (tuv|C) =  \sum_{rs} T_r (tuv|C_r] 
            !call cpu_time(cpu0)
            call contract_Prim_RHS(gcdtuv,gtuv,cnc(1,kcart),lab,mab,nab,nprimij,nprimc)
            !call cpu_time(cpux)
            !cpug2 = cpug2 + cpux - cpu0

            ! Transform:
            !   [a_m b_n|C) = \sum_{tuv} E_{t,u,v}^{a_m b_n} (tuv|C)
            !call cpu_time(cpu0)
            call contract_E_LHS(gabcd,eabtuv(1,ijcart),gcdtuv,nab,mab,lab,nprimij)
            !call cpu_time(cpux)
            !cpug3 = cpug3 + cpux - cpu0

            ! Contract:
            !   (ab|C) = \sum_{mn} C_m C_n [a_m b_n|C) 
            val = zero
            do ijp = 1, nprimij
              val = val + gabcd(ijp) * cnab(ijp,ijcart)
            end do
 
            gout(icart,jcart,kcart) = val

            if (equalij) gout(jcart,icart,kcart) = val

          end do ! kcart
        end do ! jcart
      end do ! icart
   end select 
 
end subroutine


!-----------------------------------------------------------------------
! Numbers defined as double precision (kdp) constants
! - include it after FORTRANKINDS
!-----------------------------------------------------------------------
      real(kdp),parameter :: zero    = 0.00_kdp
      real(kdp),parameter :: quarter = 0.25_kdp
      real(kdp),parameter :: half    = 0.50_kdp
      real(kdp),parameter :: one     = 1.00_kdp
      real(kdp),parameter :: thrhalf = 1.50_kdp
      real(kdp),parameter :: two     = 2.00_kdp
      real(kdp),parameter :: four    = 4.00_kdp
      real(kdp),parameter :: three   = 3.00_kdp
      real(kdp),parameter :: eight   = 8.00_kdp
      real(kdp),parameter :: sixteen = 16.00_kdp


      complex(kdp), parameter :: cplx_zero = (0.0_kdp,0.0_kdp)

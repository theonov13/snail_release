
recursive function ecof(i,j,t,Qx,a,b,n,Ax) result(dret)

  implicit none 

  include 'fortrankinds.h'
  include 'numbers.h'

  real(kdp) :: dret

  ! input
  integer,   intent(in) :: i, j, t, n
  real(kdp), intent(in) :: Qx, a, b, Ax

  ! local
  real(kdp) :: p, u 

  p = a + b
  u = a * b / p

  if (n == 0) then
    if ( t < 0 .or. t > (i+j) ) then
      dret = zero
    elseif ( i == j .and. j == t .and. t == 0) then
      dret = dexp(-u * Qx * Qx)
    elseif ( j == 0 ) then
      dret = (one/(two*p)) * ecof(i-1,j,t-1,Qx,a,b,n,Ax) - (u*Qx/a) * ecof(i-1,j,t,Qx,a,b,n,Ax) + (t+1) * ecof(i-1,j,t+1,Qx,a,b,n,Ax)
    else
      dret = (one/(two*p)) * ecof(i,j-1,t-1,Qx,a,b,n,Ax) + (u*Qx/b) * ecof(i,j-1,t,Qx,a,b,n,Ax) + (t+1) * ecof(i,j-1,t+1,Qx,a,b,n,Ax)
    end if
  else
    dret = ecof(i + 1, j, t, Qx, a, b, n - 1, Ax) + Ax * ecof(i, j, t, Qx, a, b, n - 1, Ax)
  end if

  return
end function

recursive function ecof2(i,j,t,Qx,a,b) result(dret)

  implicit none 

  include 'fortrankinds.h'
  include 'numbers.h'

  real(kdp) :: dret

  ! input
  integer,   intent(in) :: i, j, t
  real(kdp), intent(in) :: Qx, a, b

  ! local
  real(kdp) :: p, u 

  p = a + b
  u = a * b / p

  if ( t < 0 .or. t > (i+j) ) then
    dret = zero
  elseif ( i == j .and. j == t .and. t == 0) then
    dret = dexp(-u * Qx * Qx)
  elseif ( j == 0 ) then
    dret = (one/(two*p)) * ecof2(i-1,j,t-1,Qx,a,b) - (u*Qx/a) * ecof2(i-1,j,t,Qx,a,b) + (t+1) * ecof2(i-1,j,t+1,Qx,a,b)
  else
    dret = (one/(two*p)) * ecof2(i,j-1,t-1,Qx,a,b) + (u*Qx/b) * ecof2(i,j-1,t,Qx,a,b) + (t+1) * ecof2(i,j-1,t+1,Qx,a,b)
  end if

  return
end function

recursive function rcof(t, u, v, n, p, PCx, PCy, PCz, RPC, Fn, lmax, &
                        fntab,usetab,nxvals,nrange,kmax) result(dret)

  implicit none 

  include 'fortrankinds.h'
  include 'numbers.h'

  real(kdp) :: dret

  ! dimensions
  integer,   intent(in) :: nxvals,kmax,nrange

  ! input
  integer,   intent(in) :: t, u, v, n, lmax
  real(kdp), intent(in) :: p, PCx, PCy, PCz, RPC
  real(kdp), intent(in) :: Fn(0:n)

  logical,   intent(in) :: usetab
  real(kdp), intent(in) :: fntab(0:kmax,nxvals)

  ! local
  real(kdp) :: boysarg, dval

  ! functions
  real(kdp) :: BoysF

  boysarg = p * RPC * RPC

  dret = zero

  if ( t == u .and. u == v .and. v == 0 ) then
    dret = dret + (-two*p)**(n) * BoysF(n,boysarg,fntab,usetab,nxvals,nrange,kmax)
  else if ( t == u .and. u == 0 ) then
    if ( v > 1 ) then
      dret = dret + (v-1) * rcof(t,u,v-2,n+1,p,PCx,PCy,PCz,RPC,Fn,lmax,fntab,usetab,nxvals,nrange,kmax)
    end if
    dret = dret +  PCz  * rcof(t,u,v-1,n+1,p,PCx,PCy,PCz,RPC,Fn,lmax,fntab,usetab,nxvals,nrange,kmax)
  else if ( t == 0 ) then 
    if ( u > 1 ) then
      dret = dret + (u-1) * rcof(t,u-2,v,n+1,p,PCx,PCy,PCz,RPC,Fn,lmax,fntab,usetab,nxvals,nrange,kmax)
    end if 
    dret = dret + PCy * rcof(t,u-1,v,n+1,p,PCx,PCy,PCz,RPC,Fn,lmax,fntab,usetab,nxvals,nrange,kmax)
  else
    if ( t > 1 ) then
      dret = dret + (t-1) * rcof(t-2,u,v,n+1,p,PCx,PCy,PCz,RPC,Fn,lmax,fntab,usetab,nxvals,nrange,kmax)
    end if
    dret = dret + PCx * rcof(t-1,u,v,n+1,p,PCx,PCy,PCz,RPC,Fn,lmax,fntab,usetab,nxvals,nrange,kmax)

  end if

  return
end function

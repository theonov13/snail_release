
module IntegralBuffer

  use type_integ

  implicit none

  private

  include 'fortrankinds.h'
  include 'numbers.h'

  type TIntBuffer

    type(integ_info) :: intopt

    ! For Obara-Saika
    real(kdp), pointer :: vrr_terms(:)
    integer :: maxmtot = 0
    integer :: maxam   = 0

    ! For McMurchie-Davidson
    real(kdp), pointer :: Fn(:),     &
                          rtuv(:),   &
                          gtuv(:),   & 
                          ftuv(:),   &
                          gcdtuv(:), &
                          gabcd(:),  &
                          Kab(:),    &
                          Kcd(:),    &
                          gm0n0(:,:,:,:,:,:)

    integer, pointer  :: loopinfo(:,:)

    integer, pointer  :: cartcompij(:,:),&
                         cartcompkl(:,:)

    integer :: nbuf = 0

    integer :: ammax   = 0 
    integer :: memboys = 0 
    integer :: memrtuv = 0  
    integer :: memgtuv = 0 
    integer :: memftuv = 0 

    integer :: memgcdtuv = 0

  end type

  public :: TIntBuffer, allocate_intbuf, deallocate_intbuf 

!------------------------------------------------------------------------------!
  contains
!------------------------------------------------------------------------------!

  integer function megabytes(nsize)
    implicit none
    include 'fortrankinds.h'
    integer nsize    
    megabytes = nsize * 8.0 / (1024*1024)
  end function megabytes


  subroutine allocate_intbuf(intbuf,intopt,lmax,mxcart,mxprim,silent)

    implicit none

    include 'fortrankinds.h'

    logical, parameter :: verbose = .false.

    ! input/output
    type(TIntBuffer), intent(inout) :: intbuf 

    ! input
    type(integ_info), intent(in) :: intopt
    integer,          intent(in) :: lmax,mxcart,mxprim   
    logical,          intent(in) :: silent
 
    ! local
    real(kdp) :: dmem
    integer   :: maxam,maxmtot,nbuf,ammax,memboys,memrtuv,memgtuv,memftuv,memgcdtuv
    integer   :: lmxshlpr,mxcart2

    !
    ! Actual routine
    !

    intbuf%intopt = intopt

    !----------------------------------------------------------------------+
    ! Obara Saika recurrence relations
    !----------------------------------------------------------------------+
    if (intopt%scheme == INT_OS) then
      maxam   = (lmax+1) * 2 
      maxmtot = maxam * 7 
      nbuf = (maxmtot+1) * (maxam+1)**6 
    else
      maxam = 1
      maxmtot = 1
      nbuf = 1
    end if

    allocate(intbuf%vrr_terms(nbuf)) ! allocate anyway for range checker
    intbuf%maxam   = maxam
    intbuf%maxmtot = maxmtot
    intbuf%nbuf    = nbuf

    !----------------------------------------------------------------------+
    ! McMurrchie Davidson scheme
    !----------------------------------------------------------------------+
    if (intopt%scheme == INT_MMD .or. intopt%scheme == INT_MMDET) then

      lmxshlpr = lmax * 2   ! maximal angular momentum in shell pair  

      ! allocate memory
      ammax   = 4 * lmax
      memboys = (3*(ammax+1)) * mxprim**4 
      memrtuv = mxprim**4     * (lmxshlpr)**3 * (3*(lmxshlpr+1)) 
      memgtuv = mxprim**4     * (lmxshlpr+1)**3  
      memftuv = mxprim**2     * (2*lmax)**3 

      memgcdtuv = mxprim**2    * (2*lmax+2)**3  

      dmem = 0
      allocate(intbuf%Fn(memboys))              ! Boys function
      dmem = dmem + memboys

      allocate(intbuf%rtuv(memrtuv))            ! Hermitee Integrals
      dmem = dmem + memrtuv

      allocate(intbuf%ftuv(memftuv))            ! Modified E-coefficient tensor
      dmem = dmem + memftuv

      allocate(intbuf%gtuv(memgtuv))            ! Space needed after contraction of E coeff. on the RHS 
      dmem = dmem + memgtuv

      allocate(intbuf%gcdtuv(memgcdtuv))        ! Space needed after contracting the primitives on the RHS
      dmem = dmem + memgcdtuv

      allocate(intbuf%gabcd(mxprim*mxprim))     ! Space needed after contracting the primitives on the LHS
      dmem = dmem + mxprim*mxprim

      allocate(intbuf%Kab(mxprim*mxprim))       ! pre-exponential factor of primitives for bra (used for screening) 
      dmem = dmem + mxprim*mxprim

      allocate(intbuf%Kcd(mxprim*mxprim))       ! pre-exponential factor of primitives for ket (used for screening)
      dmem = dmem + mxprim*mxprim

      allocate(intbuf%loopinfo(5,mxcart*mxcart*mxcart*mxcart))

      if (intopt%scheme == INT_MMDET) then
        mxcart2 = (2*lmax + 1) * (2*lmax + 2) / 2
        !allocate(intbuf%gm0n0(mxcart2*mxcart2))
        allocate(intbuf%gm0n0(0:2*lmax,0:2*lmax,0:2*lmax,0:2*lmax,0:2*lmax,0:2*lmax))
        dmem = dmem + mxcart2*mxcart2

        intbuf%maxam = 2*lmax

        allocate(intbuf%cartcompij(3,mxcart2),&
                 intbuf%cartcompkl(3,mxcart2) )

      else
        ! for range checker 
        allocate(intbuf%cartcompij(3,1),&
                 intbuf%cartcompkl(3,1) )

      end if

      dmem = dmem * 8.0 / (1024*1024)

      if (.not. silent) write(6,'(5x,a,f10.2,2x,a)') "Allocated", dmem, " MB for int. evaluation"  

      ! save info on memory sizes
      intbuf%ammax   = ammax  
      intbuf%memboys = memboys
      intbuf%memrtuv = memrtuv  
      intbuf%memgtuv = memgtuv
      intbuf%memftuv = memftuv

      intbuf%memgcdtuv = memgcdtuv 

      if (verbose) then
        write(6,'(7x,a,f10.2,2x,a)') "Boys:        ", 8.0 * memboys / (1024*1024), " MB"
        write(6,'(7x,a,f10.2,2x,a)') "R(t,u,v):    ", 8.0 * memrtuv / (1024*1024), " MB"
        write(6,'(7x,a,f10.2,2x,a)') "F(t,u,v):    ", 8.0 * memftuv / (1024*1024), " MB"
        write(6,'(7x,a,f10.2,2x,a)') "G(t,u,v):    ", 8.0 * memgtuv / (1024*1024), " MB"
        write(6,'(7x,a,f10.2,2x,a)') "G_cd(t,u,v): ", 8.0 * memgcdtuv / (1024*1024), " MB"
        write(6,'(7x,a,f10.2,2x,a)') "G_abcd:      ", 8.0 * mxprim*mxprim / (1024*1024),  " MB"
      end if

    end if

  end subroutine

  subroutine deallocate_intbuf(intbuf)

    implicit none

    include 'fortrankinds.h'

    ! input
    type(TIntBuffer), intent(inout) :: intbuf 
    

    !
    ! Actual routine
    !

    !----------------------------------------------------------------------+
    ! Obara Saika recurrence relations
    !----------------------------------------------------------------------+
    deallocate(intbuf%vrr_terms)

    !----------------------------------------------------------------------+
    ! McMurrchie Davidson scheme
    !----------------------------------------------------------------------+
    if (intbuf%intopt%scheme == INT_MMD) then
      deallocate(intbuf%Fn)
      deallocate(intbuf%rtuv)
      deallocate(intbuf%gtuv)
      deallocate(intbuf%ftuv)
      deallocate(intbuf%gcdtuv) 
      deallocate(intbuf%gabcd)
      deallocate(intbuf%Kab)
      deallocate(intbuf%Kcd)

      deallocate(intbuf%loopinfo)

      if (intbuf%intopt%scheme == INT_MMDET) then
        deallocate(intbuf%gm0n0)
      end if
      deallocate(intbuf%cartcompij,intbuf%cartcompkl)


    end if

    !----------------------------------------------------------------------+
    ! reset info on memory sizes
    !----------------------------------------------------------------------+
    intbuf%maxam   = 0 
    intbuf%maxmtot = 0 
    intbuf%nbuf    = 0

    intbuf%ammax   = 0 
    intbuf%memboys = 0 
    intbuf%memrtuv = 0  
    intbuf%memgtuv = 0 
    intbuf%memftuv = 0 

    intbuf%memgcdtuv = 0

  end subroutine

end module


!-------------------------------------------------------------------------------------+
!  Purpose: Specialized code to calculate for a shell quadruple the integral (ss|ss)
!
!           Handoptimized and hopefully more efficient than the general code 
!
! Gunnar Schmitz
!-------------------------------------------------------------------------------------+

pure subroutine s0000(gout,&
                      eabtuv,cnab,&
                      ecdtuv,cncd,Fn,pfac,&
                      nprimij,nprimkl,nprimijkl) 
  implicit none


  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'

  ! dimensions
  integer, intent(in) :: nprimij,nprimkl,nprimijkl
 
  ! output:
  real(kdp), intent(out) :: gout(1)

  ! input:
  real(kdp), intent(in) :: eabtuv(nprimij),ecdtuv(nprimkl)

  real(kdp), intent(in) :: cnab(nprimij),cncd(nprimkl)

  real(kdp), intent(in) :: Fn(0:0,nprimijkl)

  real(kdp), intent(in) :: pfac(nprimijkl)


  ! local
  integer   :: ijp,klp,ijkl
  real(kdp) :: val,dfac,gcdtuv

  val = zero
  do klp = 1, nprimkl

    gcdtuv = zero
    ijkl = (klp-1) * nprimij 
    do ijp = 1, nprimij
      ijkl = ijkl + 1

      dfac = eabtuv(ijp) * pfac(ijkl)

      gcdtuv = gcdtuv + dfac * Fn(0,ijkl) * cnab(ijp)
    end do

    val = val + gcdtuv * ecdtuv(klp) * cncd(klp)    

  end do

  gout(1) = val
  
end subroutine

pure subroutine r0_0_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
  end do

end subroutine

pure subroutine r1_0_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001 
  rcof = zero 

  do k = 1, nprijkl
    r0001 = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001
    rcof(k,1,0,0,0) = PCx(k) * r0001
  end do

end subroutine

pure subroutine r0_1_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001 
  rcof = zero 

  do k = 1, nprijkl
    r0001 = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001
    rcof(k,0,1,0,0) = PCy(k) * r0001
  end do

end subroutine

pure subroutine r0_0_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer    :: k
  real(kdp) :: r0001
  rcof = zero 

  do k = 1, nprijkl
    r0001 = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001
    rcof(k,0,0,1,0) = PCz(k) * r0001
  end do

end subroutine

pure subroutine r1_1_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001,r0002
  real(kdp) :: PCyk,PCxk
  rcof = zero 

  do k = 1, nprijkl
    PCyk = PCy(k)
    PCxk = PCx(k)
    r0001 = -two * p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001
    rcof(k,0,0,0,2) = r0002
    rcof(k,0,1,0,1) = PCyk * r0002
    rcof(k,1,0,0,1) = PCxk * r0002
    !rcof(k,1,1,0,1) = zero
    rcof(k,0,1,0,0) = PCyk * r0001 
    rcof(k,1,0,0,0) = PCxk * r0001 
    rcof(k,1,1,0,0) = PCxk * rcof(k,0,1,0,1)
  end do

end subroutine

pure subroutine r1_0_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001,r0002 
  real(kdp) :: PCzk,PCxk
  rcof = zero 

  do k = 1, nprijkl
    PCzk = PCz(k)
    PCxk = PCx(k)
    r0001 = -two*p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001 
    rcof(k,0,0,0,2) = r0002 
    rcof(k,0,0,1,1) = PCzk * r0002 
    rcof(k,0,0,1,0) = PCzk * r0001
    rcof(k,1,0,0,1) = PCxk * r0002
    !rcof(k,1,0,1,1) = zero
    rcof(k,1,0,0,0) = PCxk * r0001
    rcof(k,1,0,1,0) = PCxk * rcof(k,0,0,1,1)
  end do

end subroutine

pure subroutine r0_1_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001,r0002
  real(kdp) :: PCzk,PCyk
  rcof = zero 

  do k = 1, nprijkl
    PCzk = PCz(k)
    PCyk = PCy(k)
    r0001 = -two * p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001 
    rcof(k,0,0,0,2) = r0002 
    rcof(k,0,0,1,1) = PCz(k) * r0002 
    rcof(k,0,0,1,0) = PCz(k) * r0001 
    rcof(k,0,1,0,1) = PCy(k) * r0002 
    !rcof(k,0,1,1,1) = zero
    rcof(k,0,1,0,0) = PCy(k) * r0001
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
  end do

end subroutine

pure subroutine r1_1_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001,r0002,r0003
  real(kdp) :: PCzk,PCxk,PCyk
  rcof = zero 

  do k = 1, nprijkl
    PCzk = PCz(k)
    PCxk = PCx(k)
    PCyk = PCy(k)
    r0001 = -two * p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    r0003 = -eight * p(k)* p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001 
    rcof(k,0,0,0,2) = r0002 
    rcof(k,0,0,0,3) = r0003 
    rcof(k,0,0,1,2) = PCzk * r0003 
    rcof(k,0,0,1,1) = PCzk * r0002
    rcof(k,0,0,1,0) = PCzk * r0001
    rcof(k,0,1,0,2) = PCyk * r0003
    !rcof(k,0,1,1,2) = zero
    rcof(k,1,0,0,2) = PCxk * r0003
    !rcof(k,1,1,0,2) = zero
    !rcof(k,1,0,1,2) = zero 
    !rcof(k,1,1,1,2) = zero 
    rcof(k,0,1,0,1) = PCyk * r0002
    rcof(k,0,1,1,1) = PCyk * rcof(k,0,0,1,2)
    rcof(k,1,0,0,1) = PCxk * r0002
    rcof(k,1,1,0,1) = PCxk * rcof(k,0,1,0,2)
    rcof(k,1,0,1,1) = PCxk * rcof(k,0,0,1,2)
    !rcof(k,1,1,1,1) = zero ! PCx(k) * rcof(k,0,1,1,2)
    rcof(k,0,1,0,0) = PCyk * r0001
    rcof(k,0,1,1,0) = PCyk * rcof(k,0,0,1,1)
    rcof(k,1,0,0,0) = PCxk * r0001
    rcof(k,1,1,0,0) = PCxk * rcof(k,0,1,0,1)
    rcof(k,1,0,1,0) = PCxk * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCxk * rcof(k,0,1,1,1)
  end do

end subroutine

pure subroutine r2_0_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001,r0002
  real(kdp) :: PCxk
  rcof = zero 

  do k = 1, nprijkl
    PCxk = PCx(k)
    r0001 = -two * p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001 
    rcof(k,0,0,0,2) = r0002 
    rcof(k,1,0,0,1) = PCxk * r0002
    rcof(k,2,0,0,1) = r0002 !PCx(k) * rcof(k,1,0,0,2) +  r0002
    rcof(k,1,0,0,0) = PCxk * r0001
    rcof(k,2,0,0,0) = PCxk * rcof(k,1,0,0,1) +  r0001
  end do

end subroutine

pure subroutine r0_2_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001,r0002
  real(kdp) :: PCyk
  rcof = zero 

  do k = 1, nprijkl
    PCyk = PCy(k)
    r0001 = -two * p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001 
    rcof(k,0,0,0,2) = r0002 
    rcof(k,0,1,0,1) = PCyk * r0002
    rcof(k,0,2,0,1) = r0002 !PCy(k) * rcof(k,0,1,0,2) +  r0002
    rcof(k,0,1,0,0) = PCyk * r0001
    rcof(k,0,2,0,0) = PCyk * rcof(k,0,1,0,1) +  r0001
  end do

end subroutine

pure subroutine r0_0_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001,r0002,r0011 
  rcof = zero 

  do k = 1, nprijkl
    r0001 = -two * p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001 
    rcof(k,0,0,0,2) = r0002 
    r0011 = PCz(k) * r0002
    rcof(k,0,0,1,1) = r0011
    rcof(k,0,0,1,0) = PCz(k) * r0001 
    rcof(k,0,0,2,1) = r0002 !PCz(k) * rcof(k,0,0,1,2) + r0002 
    rcof(k,0,0,2,0) = PCz(k) * r0011 + r0001 
  end do

end subroutine

pure subroutine r2_1_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001,r0002,r0003,r0101
  rcof = zero 

  do k = 1, nprijkl
    r0001 = -two * p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    r0003 = -eight * p(k)* p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001  
    rcof(k,0,0,0,2) = r0002 
    rcof(k,0,0,0,3) = r0003 
    rcof(k,0,1,0,2) = PCy(k) * r0003 
    rcof(k,1,0,0,2) = PCx(k) * r0003 
    !rcof(k,1,1,0,2) = zero ! PCx(k) * rcof(k,0,1,0,3)
    rcof(k,2,0,0,2) = r0003 ! PCx(k) * rcof(k,1,0,0,3) +  r0003 
    !rcof(k,2,1,0,2) = zero ! PCx(k) * rcof(k,1,1,0,3) +  rcof(k,0,1,0,3)
    r0101 = PCy(k) * r0002
    rcof(k,0,1,0,1) = r0101
    rcof(k,1,0,0,1) = PCx(k) * r0002
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) +  r0002
    rcof(k,2,1,0,1) = PCy(k) * r0003  !PCx(k) * rcof(k,1,1,0,2) +  rcof(k,0,1,0,2)
    rcof(k,0,1,0,0) = PCy(k) * r0001 
    rcof(k,1,0,0,0) = PCx(k) * r0001 
    rcof(k,1,1,0,0) = PCx(k) * r0101
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) +  r0001 
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) +  r0101
  end do

end subroutine

pure subroutine r1_2_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001,r0002,r0003
  rcof = zero 

  do k = 1, nprijkl
    r0001 = -two * p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    r0003 = -eight * p(k)* p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001 
    rcof(k,0,0,0,2) = r0002 
    rcof(k,0,0,0,3) = r0003  
    rcof(k,0,1,0,2) = PCy(k) * r0003 
    rcof(k,0,2,0,2) = r0003 !PCy(k) * rcof(k,0,1,0,3) +  r0003
    rcof(k,1,0,0,2) = PCx(k) * r0003
    !rcof(k,1,1,0,2) = zero !PCx(k) * rcof(k,0,1,0,3)
    !rcof(k,1,2,0,2) = zero !PCx(k) * rcof(k,0,2,0,3)
    rcof(k,0,1,0,1) = PCy(k) * r0002
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) +  r0002
    rcof(k,1,0,0,1) = PCx(k) * r0002
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,0,1,0,0) = PCy(k) * r0001
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) +  r0001
    rcof(k,1,0,0,0) = PCx(k) * r0001
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
  end do

end subroutine

pure subroutine r2_0_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001,r0002,r0003
  rcof = zero 

  do k = 1, nprijkl
    r0001 = -two * p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    r0003 = -eight * p(k)* p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001 
    rcof(k,0,0,0,2) = r0002 
    rcof(k,0,0,0,3) = r0003 
    rcof(k,0,0,1,2) = PCz(k) * r0003 
    rcof(k,0,0,1,1) = PCz(k) * r0002
    rcof(k,0,0,1,0) = PCz(k) * r0001
    rcof(k,1,0,0,2) = PCx(k) * r0003
    !rcof(k,1,0,1,2) = zero !PCx(k) * rcof(k,0,0,1,3)
    rcof(k,2,0,0,2) = r0003 !PCx(k) * rcof(k,1,0,0,3) +  r0003 
    !rcof(k,2,0,1,2) = zero !PCx(k) * rcof(k,1,0,1,3) +  rcof(k,0,0,1,3)
    rcof(k,1,0,0,1) = PCx(k) * r0002
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) +  r0002
    rcof(k,2,0,1,1) = PCz(k) * r0003 !PCx(k) * rcof(k,1,0,1,2) +  rcof(k,0,0,1,2)
    rcof(k,1,0,0,0) = PCx(k) * r0001
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) +  r0001
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) +  rcof(k,0,0,1,1)
  end do

end subroutine

pure subroutine r0_2_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001,r0002,r0003
  rcof = zero 

  do k = 1, nprijkl
    r0001 = -two * p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    r0003 = -eight * p(k)* p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001 
    rcof(k,0,0,0,2) = r0002 
    rcof(k,0,0,0,3) = r0003 
    rcof(k,0,0,1,2) = PCz(k) * r0003 
    rcof(k,0,0,1,1) = PCz(k) * r0002 
    rcof(k,0,0,1,0) = PCz(k) * r0001 
    rcof(k,0,1,0,2) = PCy(k) * r0003 
    !rcof(k,0,1,1,2) = zero !PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,2,0,2) = r0003 !PCy(k) * rcof(k,0,1,0,3) +  r0003 
    !rcof(k,0,2,1,2) = zero !PCy(k) * rcof(k,0,1,1,3) +  rcof(k,0,0,1,3)
    rcof(k,0,1,0,1) = PCy(k) * r0002
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) +  r0002
    rcof(k,0,2,1,1) = PCz(k) * r0003 !PCy(k) * rcof(k,0,1,1,2) +  rcof(k,0,0,1,2)
    rcof(k,0,1,0,0) = PCy(k) * r0001
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) +  r0001
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) +  rcof(k,0,0,1,1)
  end do

end subroutine

pure subroutine r1_0_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001,r0002,r0003
  real(kdp) :: PCzk,PCxk
  rcof = zero 

  do k = 1, nprijkl
    PCzk = PCz(k)
    PCxk = PCx(k)
    r0001 = -two * p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    r0003 = -eight * p(k)* p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001 
    rcof(k,0,0,0,2) = r0002 
    rcof(k,0,0,0,3) = r0003 
    rcof(k,0,0,1,2) = PCzk * r0003 
    rcof(k,0,0,1,1) = PCzk * r0002 
    rcof(k,0,0,1,0) = PCzk * r0001 
    rcof(k,0,0,2,2) = r0003 !PCz(k) * rcof(k,0,0,1,3) +  r0003
    rcof(k,1,0,0,2) = PCxk * r0003
    !rcof(k,1,0,1,2) = zero !PCx(k) * rcof(k,0,0,1,3)
    !rcof(k,1,0,2,2) = zero !PCx(k) * rcof(k,0,0,2,3)
    rcof(k,0,0,2,1) = PCzk * rcof(k,0,0,1,2) +  r0002
    rcof(k,1,0,0,1) = PCxk * r0002
    rcof(k,1,0,1,1) = PCxk * rcof(k,0,0,1,2)
    rcof(k,1,0,2,1) = PCxk * r0003
    rcof(k,0,0,2,0) = PCzk * rcof(k,0,0,1,1) +  r0001
    rcof(k,1,0,0,0) = PCxk * r0001
    rcof(k,1,0,1,0) = PCxk * rcof(k,0,0,1,1)
    rcof(k,1,0,2,0) = PCxk * rcof(k,0,0,2,1)
  end do

end subroutine

pure subroutine r0_1_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001,r0002,r0003
  real(kdp) :: PCzk,PCyk
  rcof = zero 

  do k = 1, nprijkl
    PCzk = PCz(k)
    PCyk = PCy(k)

    r0001 = -two * p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    r0003 = -eight * p(k)* p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001 
    rcof(k,0,0,0,2) = r0002 
    rcof(k,0,0,0,3) = r0003
    rcof(k,0,0,1,2) = PCzk * r0003 
    rcof(k,0,0,1,1) = PCzk * r0002 
    rcof(k,0,0,1,0) = PCzk * r0001 
    rcof(k,0,0,2,2) = r0003 ! PCzk * rcof(k,0,0,1,3) +  r0003 
    rcof(k,0,1,0,2) = PCyk * r0003
    !rcof(k,0,1,1,2) = zero ! PCyk * rcof(k,0,0,1,3)
    !rcof(k,0,1,2,2) = zero ! PCyk * rcof(k,0,0,2,3)
    rcof(k,0,0,2,1) = PCzk * rcof(k,0,0,1,2) +  r0002
    rcof(k,0,1,0,1) = PCyk * r0002
    rcof(k,0,1,1,1) = PCyk * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCyk * rcof(k,0,0,2,2)
    rcof(k,0,0,2,0) = PCzk * rcof(k,0,0,1,1) +  r0001
    rcof(k,0,1,0,0) = PCyk * r0001
    rcof(k,0,1,1,0) = PCyk * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCyk * rcof(k,0,0,2,1)
  end do

end subroutine

subroutine r2_1_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output
  real(kdp) :: r0001,r0002,r0003,r0004  
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    r0001 = -two * p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    r0003 = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    r0004 = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)

    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001
    rcof(k,0,0,0,2) = r0002 
    rcof(k,0,0,0,3) = r0003 
    rcof(k,0,0,0,4) = r0004 
    rcof(k,0,0,1,3) = PCz(k) * r0004
    rcof(k,0,0,1,2) = PCz(k) * r0003
    rcof(k,0,0,1,1) = PCz(k) * r0002 
    rcof(k,0,0,1,0) = PCz(k) * r0001 
    rcof(k,0,1,0,3) = PCy(k) * r0004 
    !rcof(k,0,1,1,3) = zero ! PCy(k) * rcof(k,0,0,1,4)
    rcof(k,1,0,0,3) = PCx(k) * r0004 
    !rcof(k,1,1,0,3) = zero ! PCx(k) * rcof(k,0,1,0,4)
    !rcof(k,1,0,1,3) = zero ! PCx(k) * rcof(k,0,0,1,4)
    !rcof(k,1,1,1,3) = zero ! PCx(k) * rcof(k,0,1,1,4)
    rcof(k,2,0,0,3) = r0004 ! PCx(k) * rcof(k,1,0,0,4) +  r0004 
    !rcof(k,2,1,0,3) = zero ! PCx(k) * rcof(k,1,1,0,4) +  rcof(k,0,1,0,4)
    !rcof(k,2,0,1,3) = zero ! PCx(k) * rcof(k,1,0,1,4) +  rcof(k,0,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) +  rcof(k,0,1,1,4)
    rcof(k,0,1,0,2) = PCy(k) * r0003 
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,1,0,0,2) = PCx(k) * r0003 
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    !rcof(k,1,1,1,2) = zero ! PCx(k) * rcof(k,0,1,1,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) +  r0003 
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) +  rcof(k,0,1,0,3)
    !rcof(k,2,0,1,2) = zero ! PCx(k) * rcof(k,1,0,1,3) +  rcof(k,0,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) ! +  rcof(k,0,1,1,3)
    rcof(k,0,1,0,1) = PCy(k) * r0002 
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,1,0,0,1) = PCx(k) * r0002 
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) +  r0002 
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) +  rcof(k,0,1,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) +  rcof(k,0,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) +  rcof(k,0,1,1,2)
    rcof(k,0,1,0,0) = PCy(k) * r0001 
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,1,0,0,0) = PCx(k) * r0001 
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) +  r0001 
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) +  rcof(k,0,1,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) +  rcof(k,0,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) +  rcof(k,0,1,1,1)
  end do

end subroutine

pure subroutine r1_2_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer   :: k
  real(kdp) :: r0001,r0002,r0003,r0004
  rcof = zero 

  do k = 1, nprijkl
    r0001 = -two * p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)
    r0003 = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    r0004 = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)

    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001 
    rcof(k,0,0,0,2) = r0002 
    rcof(k,0,0,0,3) = r0003 
    rcof(k,0,0,0,4) = r0004 
    rcof(k,0,0,1,3) = PCz(k) * r0004 
    rcof(k,0,0,1,2) = PCz(k) * r0003 
    rcof(k,0,0,1,1) = PCz(k) * r0002 
    rcof(k,0,0,1,0) = PCz(k) * r0001 
    rcof(k,0,1,0,3) = PCy(k) * r0004 
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) +  r0004 
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) +  rcof(k,0,0,1,4)
    rcof(k,1,0,0,3) = PCx(k) * r0004
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,0,1,0,2) = PCy(k) * r0003
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) +  r0003 
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) +  rcof(k,0,0,1,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,0,1,0,1) = PCy(k) * r0002
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) +  r0002 
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) +  rcof(k,0,0,1,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,0,1,0,0) = PCy(k) * r0001
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) +  r0001
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) +  rcof(k,0,0,1,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
  end do

end subroutine

pure subroutine r1_1_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) +  rcof(k,0,0,0,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) +  rcof(k,0,0,0,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) +  rcof(k,0,0,0,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) +  rcof(k,0,0,0,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
  end do

end subroutine

pure subroutine r2_2_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) +  rcof(k,0,0,0,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) +  rcof(k,0,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) +  rcof(k,0,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) +  rcof(k,0,2,0,4)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) +  rcof(k,0,0,0,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) +  rcof(k,0,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) +  rcof(k,0,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) +  rcof(k,0,2,0,3)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) +  rcof(k,0,0,0,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) +  rcof(k,0,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) +  rcof(k,0,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) +  rcof(k,0,2,0,2)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) +  rcof(k,0,0,0,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) +  rcof(k,0,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) +  rcof(k,0,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) +  rcof(k,0,2,0,1)
  end do

end subroutine

pure subroutine r2_0_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) +  rcof(k,0,0,0,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) +  rcof(k,0,0,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) +  rcof(k,0,0,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) +  rcof(k,0,0,2,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) +  rcof(k,0,0,0,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) +  rcof(k,0,0,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) +  rcof(k,0,0,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) +  rcof(k,0,0,2,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) +  rcof(k,0,0,0,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) +  rcof(k,0,0,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) +  rcof(k,0,0,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) +  rcof(k,0,0,2,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) +  rcof(k,0,0,0,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) +  rcof(k,0,0,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) +  rcof(k,0,0,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) +  rcof(k,0,0,2,1)
  end do

end subroutine

pure subroutine r0_2_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) +  rcof(k,0,0,0,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) +  rcof(k,0,0,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) +  rcof(k,0,0,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) +  rcof(k,0,0,2,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) +  rcof(k,0,0,0,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) +  rcof(k,0,0,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) +  rcof(k,0,0,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) +  rcof(k,0,0,2,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) +  rcof(k,0,0,0,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) +  rcof(k,0,0,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) +  rcof(k,0,0,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) +  rcof(k,0,0,2,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) +  rcof(k,0,0,0,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) +  rcof(k,0,0,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) +  rcof(k,0,0,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) +  rcof(k,0,0,2,1)
  end do

end subroutine

pure subroutine r2_2_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) +  rcof(k,0,0,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) +  rcof(k,0,0,1,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) +  rcof(k,0,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) +  rcof(k,0,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) +  rcof(k,0,2,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) +  rcof(k,0,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) +  rcof(k,0,1,1,5)
    rcof(k,2,2,1,4) = PCx(k) * rcof(k,1,2,1,5) +  rcof(k,0,2,1,5)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) +  rcof(k,0,0,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) +  rcof(k,0,0,1,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) +  rcof(k,0,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) +  rcof(k,0,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) +  rcof(k,0,2,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) +  rcof(k,0,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) +  rcof(k,0,1,1,4)
    rcof(k,2,2,1,3) = PCx(k) * rcof(k,1,2,1,4) +  rcof(k,0,2,1,4)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) +  rcof(k,0,0,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) +  rcof(k,0,0,1,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) +  rcof(k,0,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) +  rcof(k,0,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) +  rcof(k,0,2,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) +  rcof(k,0,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) +  rcof(k,0,1,1,3)
    rcof(k,2,2,1,2) = PCx(k) * rcof(k,1,2,1,3) +  rcof(k,0,2,1,3)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) +  rcof(k,0,0,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) +  rcof(k,0,0,1,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) +  rcof(k,0,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) +  rcof(k,0,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) +  rcof(k,0,2,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) +  rcof(k,0,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) +  rcof(k,0,1,1,2)
    rcof(k,2,2,1,1) = PCx(k) * rcof(k,1,2,1,2) +  rcof(k,0,2,1,2)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) +  rcof(k,0,0,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) +  rcof(k,0,0,1,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) +  rcof(k,0,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) +  rcof(k,0,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) +  rcof(k,0,2,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) +  rcof(k,0,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) +  rcof(k,0,1,1,1)
    rcof(k,2,2,1,0) = PCx(k) * rcof(k,1,2,1,1) +  rcof(k,0,2,1,1)
  end do

end subroutine

pure subroutine r2_1_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,2,1,2,4) = PCx(k) * rcof(k,1,1,2,5) + 1 * rcof(k,0,1,2,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,2,1,2,3) = PCx(k) * rcof(k,1,1,2,4) + 1 * rcof(k,0,1,2,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,2,1,2,2) = PCx(k) * rcof(k,1,1,2,3) + 1 * rcof(k,0,1,2,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,2,1,2,1) = PCx(k) * rcof(k,1,1,2,2) + 1 * rcof(k,0,1,2,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,2,1,2,0) = PCx(k) * rcof(k,1,1,2,1) + 1 * rcof(k,0,1,2,1)
  end do

end subroutine

pure subroutine r1_2_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,2,2,4) = PCx(k) * rcof(k,0,2,2,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,2,2,3) = PCx(k) * rcof(k,0,2,2,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,2,2,2) = PCx(k) * rcof(k,0,2,2,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,2,2,1) = PCx(k) * rcof(k,0,2,2,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,2,2,0) = PCx(k) * rcof(k,0,2,2,1)
  end do

end subroutine

pure subroutine r2_2_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,0,2,2,5) = PCy(k) * rcof(k,0,1,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,2,1,5) = PCx(k) * rcof(k,0,2,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,1,2,5) = PCx(k) * rcof(k,0,1,2,6)
    rcof(k,1,2,2,5) = PCx(k) * rcof(k,0,2,2,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,2,2,0,5) = PCx(k) * rcof(k,1,2,0,6) + 1 * rcof(k,0,2,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,2,1,1,5) = PCx(k) * rcof(k,1,1,1,6) + 1 * rcof(k,0,1,1,6)
    rcof(k,2,2,1,5) = PCx(k) * rcof(k,1,2,1,6) + 1 * rcof(k,0,2,1,6)
    rcof(k,2,0,2,5) = PCx(k) * rcof(k,1,0,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,2,1,2,5) = PCx(k) * rcof(k,1,1,2,6) + 1 * rcof(k,0,1,2,6)
    rcof(k,2,2,2,5) = PCx(k) * rcof(k,1,2,2,6) + 1 * rcof(k,0,2,2,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,2,2,4) = PCx(k) * rcof(k,0,2,2,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) + 1 * rcof(k,0,2,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,2,2,1,4) = PCx(k) * rcof(k,1,2,1,5) + 1 * rcof(k,0,2,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,2,1,2,4) = PCx(k) * rcof(k,1,1,2,5) + 1 * rcof(k,0,1,2,5)
    rcof(k,2,2,2,4) = PCx(k) * rcof(k,1,2,2,5) + 1 * rcof(k,0,2,2,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,2,2,3) = PCx(k) * rcof(k,0,2,2,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) + 1 * rcof(k,0,2,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,2,2,1,3) = PCx(k) * rcof(k,1,2,1,4) + 1 * rcof(k,0,2,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,2,1,2,3) = PCx(k) * rcof(k,1,1,2,4) + 1 * rcof(k,0,1,2,4)
    rcof(k,2,2,2,3) = PCx(k) * rcof(k,1,2,2,4) + 1 * rcof(k,0,2,2,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,2,2,2) = PCx(k) * rcof(k,0,2,2,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) + 1 * rcof(k,0,2,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,2,2,1,2) = PCx(k) * rcof(k,1,2,1,3) + 1 * rcof(k,0,2,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,2,1,2,2) = PCx(k) * rcof(k,1,1,2,3) + 1 * rcof(k,0,1,2,3)
    rcof(k,2,2,2,2) = PCx(k) * rcof(k,1,2,2,3) + 1 * rcof(k,0,2,2,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,2,2,1) = PCx(k) * rcof(k,0,2,2,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) + 1 * rcof(k,0,2,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,2,2,1,1) = PCx(k) * rcof(k,1,2,1,2) + 1 * rcof(k,0,2,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,2,1,2,1) = PCx(k) * rcof(k,1,1,2,2) + 1 * rcof(k,0,1,2,2)
    rcof(k,2,2,2,1) = PCx(k) * rcof(k,1,2,2,2) + 1 * rcof(k,0,2,2,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,2,2,0) = PCx(k) * rcof(k,0,2,2,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) + 1 * rcof(k,0,2,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,2,2,1,0) = PCx(k) * rcof(k,1,2,1,1) + 1 * rcof(k,0,2,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,2,1,2,0) = PCx(k) * rcof(k,1,1,2,1) + 1 * rcof(k,0,1,2,1)
    rcof(k,2,2,2,0) = PCx(k) * rcof(k,1,2,2,1) + 1 * rcof(k,0,2,2,1)
  end do

end subroutine

pure subroutine r3_0_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight * p(k)* p(k) * p(k) * Fn(3,k)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
  end do

end subroutine

pure subroutine r0_3_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight * p(k)* p(k) * p(k) * Fn(3,k)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
  end do

end subroutine

pure subroutine r0_0_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight * p(k)* p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
  end do

end subroutine

pure subroutine r3_1_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,3,1,0,3) = PCx(k) * rcof(k,2,1,0,4) + 2 * rcof(k,1,1,0,4)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,3,1,0,2) = PCx(k) * rcof(k,2,1,0,3) + 2 * rcof(k,1,1,0,3)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,3,1,0,1) = PCx(k) * rcof(k,2,1,0,2) + 2 * rcof(k,1,1,0,2)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,3,1,0,0) = PCx(k) * rcof(k,2,1,0,1) + 2 * rcof(k,1,1,0,1)
  end do

end subroutine

pure subroutine r1_3_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,3,0,3) = PCx(k) * rcof(k,0,3,0,4)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,3,0,2) = PCx(k) * rcof(k,0,3,0,3)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,3,0,1) = PCx(k) * rcof(k,0,3,0,2)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,3,0,0) = PCx(k) * rcof(k,0,3,0,1)
  end do

end subroutine

pure subroutine r3_0_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,3,0,1,3) = PCx(k) * rcof(k,2,0,1,4) + 2 * rcof(k,1,0,1,4)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,3,0,1,2) = PCx(k) * rcof(k,2,0,1,3) + 2 * rcof(k,1,0,1,3)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,3,0,1,1) = PCx(k) * rcof(k,2,0,1,2) + 2 * rcof(k,1,0,1,2)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,3,0,1,0) = PCx(k) * rcof(k,2,0,1,1) + 2 * rcof(k,1,0,1,1)
  end do

end subroutine

pure subroutine r0_3_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,3,1,3) = PCy(k) * rcof(k,0,2,1,4) + 2 * rcof(k,0,1,1,4)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,3,1,2) = PCy(k) * rcof(k,0,2,1,3) + 2 * rcof(k,0,1,1,3)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,3,1,1) = PCy(k) * rcof(k,0,2,1,2) + 2 * rcof(k,0,1,1,2)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,3,1,0) = PCy(k) * rcof(k,0,2,1,1) + 2 * rcof(k,0,1,1,1)
  end do

end subroutine

pure subroutine r1_0_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,0,3,3) = PCx(k) * rcof(k,0,0,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,0,3,2) = PCx(k) * rcof(k,0,0,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,0,3,1) = PCx(k) * rcof(k,0,0,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,0,3,0) = PCx(k) * rcof(k,0,0,3,1)
  end do

end subroutine

pure subroutine r0_1_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,1,3,3) = PCy(k) * rcof(k,0,0,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,1,3,2) = PCy(k) * rcof(k,0,0,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,1,3,1) = PCy(k) * rcof(k,0,0,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,1,3,0) = PCy(k) * rcof(k,0,0,3,1)
  end do

end subroutine

pure subroutine r3_1_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two * p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,3,0,0,4) = PCx(k) * rcof(k,2,0,0,5) + 2 * rcof(k,1,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,3,1,0,4) = PCx(k) * rcof(k,2,1,0,5) + 2 * rcof(k,1,1,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,3,0,1,4) = PCx(k) * rcof(k,2,0,1,5) + 2 * rcof(k,1,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,3,1,1,4) = PCx(k) * rcof(k,2,1,1,5) + 2 * rcof(k,1,1,1,5)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,3,1,0,3) = PCx(k) * rcof(k,2,1,0,4) + 2 * rcof(k,1,1,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,3,0,1,3) = PCx(k) * rcof(k,2,0,1,4) + 2 * rcof(k,1,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,3,1,1,3) = PCx(k) * rcof(k,2,1,1,4) + 2 * rcof(k,1,1,1,4)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,3,1,0,2) = PCx(k) * rcof(k,2,1,0,3) + 2 * rcof(k,1,1,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,3,0,1,2) = PCx(k) * rcof(k,2,0,1,3) + 2 * rcof(k,1,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,3,1,1,2) = PCx(k) * rcof(k,2,1,1,3) + 2 * rcof(k,1,1,1,3)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,3,1,0,1) = PCx(k) * rcof(k,2,1,0,2) + 2 * rcof(k,1,1,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,3,0,1,1) = PCx(k) * rcof(k,2,0,1,2) + 2 * rcof(k,1,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,3,1,1,1) = PCx(k) * rcof(k,2,1,1,2) + 2 * rcof(k,1,1,1,2)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,3,1,0,0) = PCx(k) * rcof(k,2,1,0,1) + 2 * rcof(k,1,1,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,3,0,1,0) = PCx(k) * rcof(k,2,0,1,1) + 2 * rcof(k,1,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,3,1,1,0) = PCx(k) * rcof(k,2,1,1,1) + 2 * rcof(k,1,1,1,1)
  end do

end subroutine

pure subroutine r1_3_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,3,0,4) = PCy(k) * rcof(k,0,2,0,5) + 2 * rcof(k,0,1,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,3,1,4) = PCy(k) * rcof(k,0,2,1,5) + 2 * rcof(k,0,1,1,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,3,0,4) = PCx(k) * rcof(k,0,3,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,3,1,4) = PCx(k) * rcof(k,0,3,1,5)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,3,1,3) = PCy(k) * rcof(k,0,2,1,4) + 2 * rcof(k,0,1,1,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,3,0,3) = PCx(k) * rcof(k,0,3,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,3,1,3) = PCx(k) * rcof(k,0,3,1,4)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,3,1,2) = PCy(k) * rcof(k,0,2,1,3) + 2 * rcof(k,0,1,1,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,3,0,2) = PCx(k) * rcof(k,0,3,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,3,1,2) = PCx(k) * rcof(k,0,3,1,3)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,3,1,1) = PCy(k) * rcof(k,0,2,1,2) + 2 * rcof(k,0,1,1,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,3,0,1) = PCx(k) * rcof(k,0,3,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,3,1,1) = PCx(k) * rcof(k,0,3,1,2)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,3,1,0) = PCy(k) * rcof(k,0,2,1,1) + 2 * rcof(k,0,1,1,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,3,0,0) = PCx(k) * rcof(k,0,3,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,3,1,0) = PCx(k) * rcof(k,0,3,1,1)
  end do

end subroutine

pure subroutine r1_1_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,0,3,4) = PCz(k) * rcof(k,0,0,2,5) + 2 * rcof(k,0,0,1,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,1,3,4) = PCy(k) * rcof(k,0,0,3,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,0,3,4) = PCx(k) * rcof(k,0,0,3,5)
    rcof(k,1,1,3,4) = PCx(k) * rcof(k,0,1,3,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,1,3,3) = PCy(k) * rcof(k,0,0,3,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,0,3,3) = PCx(k) * rcof(k,0,0,3,4)
    rcof(k,1,1,3,3) = PCx(k) * rcof(k,0,1,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,1,3,2) = PCy(k) * rcof(k,0,0,3,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,0,3,2) = PCx(k) * rcof(k,0,0,3,3)
    rcof(k,1,1,3,2) = PCx(k) * rcof(k,0,1,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,1,3,1) = PCy(k) * rcof(k,0,0,3,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,0,3,1) = PCx(k) * rcof(k,0,0,3,2)
    rcof(k,1,1,3,1) = PCx(k) * rcof(k,0,1,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,1,3,0) = PCy(k) * rcof(k,0,0,3,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,0,3,0) = PCx(k) * rcof(k,0,0,3,1)
    rcof(k,1,1,3,0) = PCx(k) * rcof(k,0,1,3,1)
  end do

end subroutine

pure subroutine r3_2_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,3,0,0,4) = PCx(k) * rcof(k,2,0,0,5) + 2 * rcof(k,1,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,3,1,0,4) = PCx(k) * rcof(k,2,1,0,5) + 2 * rcof(k,1,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) + 1 * rcof(k,0,2,0,5)
    rcof(k,3,2,0,4) = PCx(k) * rcof(k,2,2,0,5) + 2 * rcof(k,1,2,0,5)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,3,1,0,3) = PCx(k) * rcof(k,2,1,0,4) + 2 * rcof(k,1,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) + 1 * rcof(k,0,2,0,4)
    rcof(k,3,2,0,3) = PCx(k) * rcof(k,2,2,0,4) + 2 * rcof(k,1,2,0,4)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,3,1,0,2) = PCx(k) * rcof(k,2,1,0,3) + 2 * rcof(k,1,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) + 1 * rcof(k,0,2,0,3)
    rcof(k,3,2,0,2) = PCx(k) * rcof(k,2,2,0,3) + 2 * rcof(k,1,2,0,3)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,3,1,0,1) = PCx(k) * rcof(k,2,1,0,2) + 2 * rcof(k,1,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) + 1 * rcof(k,0,2,0,2)
    rcof(k,3,2,0,1) = PCx(k) * rcof(k,2,2,0,2) + 2 * rcof(k,1,2,0,2)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,3,1,0,0) = PCx(k) * rcof(k,2,1,0,1) + 2 * rcof(k,1,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) + 1 * rcof(k,0,2,0,1)
    rcof(k,3,2,0,0) = PCx(k) * rcof(k,2,2,0,1) + 2 * rcof(k,1,2,0,1)
  end do

end subroutine

pure subroutine r2_3_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,3,0,4) = PCy(k) * rcof(k,0,2,0,5) + 2 * rcof(k,0,1,0,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,3,0,4) = PCx(k) * rcof(k,0,3,0,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) + 1 * rcof(k,0,2,0,5)
    rcof(k,2,3,0,4) = PCx(k) * rcof(k,1,3,0,5) + 1 * rcof(k,0,3,0,5)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,3,0,3) = PCx(k) * rcof(k,0,3,0,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) + 1 * rcof(k,0,2,0,4)
    rcof(k,2,3,0,3) = PCx(k) * rcof(k,1,3,0,4) + 1 * rcof(k,0,3,0,4)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,3,0,2) = PCx(k) * rcof(k,0,3,0,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) + 1 * rcof(k,0,2,0,3)
    rcof(k,2,3,0,2) = PCx(k) * rcof(k,1,3,0,3) + 1 * rcof(k,0,3,0,3)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,3,0,1) = PCx(k) * rcof(k,0,3,0,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) + 1 * rcof(k,0,2,0,2)
    rcof(k,2,3,0,1) = PCx(k) * rcof(k,1,3,0,2) + 1 * rcof(k,0,3,0,2)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,3,0,0) = PCx(k) * rcof(k,0,3,0,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) + 1 * rcof(k,0,2,0,1)
    rcof(k,2,3,0,0) = PCx(k) * rcof(k,1,3,0,1) + 1 * rcof(k,0,3,0,1)
  end do

end subroutine

pure subroutine r3_0_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,3,0,0,4) = PCx(k) * rcof(k,2,0,0,5) + 2 * rcof(k,1,0,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,3,0,1,4) = PCx(k) * rcof(k,2,0,1,5) + 2 * rcof(k,1,0,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,3,0,2,4) = PCx(k) * rcof(k,2,0,2,5) + 2 * rcof(k,1,0,2,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,3,0,1,3) = PCx(k) * rcof(k,2,0,1,4) + 2 * rcof(k,1,0,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,3,0,2,3) = PCx(k) * rcof(k,2,0,2,4) + 2 * rcof(k,1,0,2,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,3,0,1,2) = PCx(k) * rcof(k,2,0,1,3) + 2 * rcof(k,1,0,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,3,0,2,2) = PCx(k) * rcof(k,2,0,2,3) + 2 * rcof(k,1,0,2,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,3,0,1,1) = PCx(k) * rcof(k,2,0,1,2) + 2 * rcof(k,1,0,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,3,0,2,1) = PCx(k) * rcof(k,2,0,2,2) + 2 * rcof(k,1,0,2,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,3,0,1,0) = PCx(k) * rcof(k,2,0,1,1) + 2 * rcof(k,1,0,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,3,0,2,0) = PCx(k) * rcof(k,2,0,2,1) + 2 * rcof(k,1,0,2,1)
  end do

end subroutine

pure subroutine r0_3_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k)
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,3,0,4) = PCy(k) * rcof(k,0,2,0,5) + 2 * rcof(k,0,1,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,3,1,4) = PCy(k) * rcof(k,0,2,1,5) + 2 * rcof(k,0,1,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,0,3,2,4) = PCy(k) * rcof(k,0,2,2,5) + 2 * rcof(k,0,1,2,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,3,1,3) = PCy(k) * rcof(k,0,2,1,4) + 2 * rcof(k,0,1,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,0,3,2,3) = PCy(k) * rcof(k,0,2,2,4) + 2 * rcof(k,0,1,2,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,3,1,2) = PCy(k) * rcof(k,0,2,1,3) + 2 * rcof(k,0,1,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,0,3,2,2) = PCy(k) * rcof(k,0,2,2,3) + 2 * rcof(k,0,1,2,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,3,1,1) = PCy(k) * rcof(k,0,2,1,2) + 2 * rcof(k,0,1,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,0,3,2,1) = PCy(k) * rcof(k,0,2,2,2) + 2 * rcof(k,0,1,2,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,3,1,0) = PCy(k) * rcof(k,0,2,1,1) + 2 * rcof(k,0,1,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,0,3,2,0) = PCy(k) * rcof(k,0,2,2,1) + 2 * rcof(k,0,1,2,1)
  end do

end subroutine

pure subroutine r2_0_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,0,3,4) = PCz(k) * rcof(k,0,0,2,5) + 2 * rcof(k,0,0,1,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,0,3,4) = PCx(k) * rcof(k,0,0,3,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,2,0,3,4) = PCx(k) * rcof(k,1,0,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,0,3,3) = PCx(k) * rcof(k,0,0,3,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,2,0,3,3) = PCx(k) * rcof(k,1,0,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,0,3,2) = PCx(k) * rcof(k,0,0,3,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,2,0,3,2) = PCx(k) * rcof(k,1,0,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,0,3,1) = PCx(k) * rcof(k,0,0,3,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,2,0,3,1) = PCx(k) * rcof(k,1,0,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,0,3,0) = PCx(k) * rcof(k,0,0,3,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,2,0,3,0) = PCx(k) * rcof(k,1,0,3,1) + 1 * rcof(k,0,0,3,1)
  end do

end subroutine

pure subroutine r0_2_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,0,3,4) = PCz(k) * rcof(k,0,0,2,5) + 2 * rcof(k,0,0,1,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,1,3,4) = PCy(k) * rcof(k,0,0,3,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,0,2,3,4) = PCy(k) * rcof(k,0,1,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,1,3,3) = PCy(k) * rcof(k,0,0,3,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,0,2,3,3) = PCy(k) * rcof(k,0,1,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,1,3,2) = PCy(k) * rcof(k,0,0,3,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,0,2,3,2) = PCy(k) * rcof(k,0,1,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,1,3,1) = PCy(k) * rcof(k,0,0,3,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,0,2,3,1) = PCy(k) * rcof(k,0,1,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,1,3,0) = PCy(k) * rcof(k,0,0,3,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,0,2,3,0) = PCy(k) * rcof(k,0,1,3,1) + 1 * rcof(k,0,0,3,1)
  end do

end subroutine

pure subroutine r3_2_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k)* p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,2,1,5) = PCx(k) * rcof(k,0,2,1,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,3,0,0,5) = PCx(k) * rcof(k,2,0,0,6) + 2 * rcof(k,1,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,3,1,0,5) = PCx(k) * rcof(k,2,1,0,6) + 2 * rcof(k,1,1,0,6)
    rcof(k,2,2,0,5) = PCx(k) * rcof(k,1,2,0,6) + 1 * rcof(k,0,2,0,6)
    rcof(k,3,2,0,5) = PCx(k) * rcof(k,2,2,0,6) + 2 * rcof(k,1,2,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,3,0,1,5) = PCx(k) * rcof(k,2,0,1,6) + 2 * rcof(k,1,0,1,6)
    rcof(k,2,1,1,5) = PCx(k) * rcof(k,1,1,1,6) + 1 * rcof(k,0,1,1,6)
    rcof(k,3,1,1,5) = PCx(k) * rcof(k,2,1,1,6) + 2 * rcof(k,1,1,1,6)
    rcof(k,2,2,1,5) = PCx(k) * rcof(k,1,2,1,6) + 1 * rcof(k,0,2,1,6)
    rcof(k,3,2,1,5) = PCx(k) * rcof(k,2,2,1,6) + 2 * rcof(k,1,2,1,6)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,3,0,0,4) = PCx(k) * rcof(k,2,0,0,5) + 2 * rcof(k,1,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,3,1,0,4) = PCx(k) * rcof(k,2,1,0,5) + 2 * rcof(k,1,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) + 1 * rcof(k,0,2,0,5)
    rcof(k,3,2,0,4) = PCx(k) * rcof(k,2,2,0,5) + 2 * rcof(k,1,2,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,3,0,1,4) = PCx(k) * rcof(k,2,0,1,5) + 2 * rcof(k,1,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,3,1,1,4) = PCx(k) * rcof(k,2,1,1,5) + 2 * rcof(k,1,1,1,5)
    rcof(k,2,2,1,4) = PCx(k) * rcof(k,1,2,1,5) + 1 * rcof(k,0,2,1,5)
    rcof(k,3,2,1,4) = PCx(k) * rcof(k,2,2,1,5) + 2 * rcof(k,1,2,1,5)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,3,1,0,3) = PCx(k) * rcof(k,2,1,0,4) + 2 * rcof(k,1,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) + 1 * rcof(k,0,2,0,4)
    rcof(k,3,2,0,3) = PCx(k) * rcof(k,2,2,0,4) + 2 * rcof(k,1,2,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,3,0,1,3) = PCx(k) * rcof(k,2,0,1,4) + 2 * rcof(k,1,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,3,1,1,3) = PCx(k) * rcof(k,2,1,1,4) + 2 * rcof(k,1,1,1,4)
    rcof(k,2,2,1,3) = PCx(k) * rcof(k,1,2,1,4) + 1 * rcof(k,0,2,1,4)
    rcof(k,3,2,1,3) = PCx(k) * rcof(k,2,2,1,4) + 2 * rcof(k,1,2,1,4)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,3,1,0,2) = PCx(k) * rcof(k,2,1,0,3) + 2 * rcof(k,1,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) + 1 * rcof(k,0,2,0,3)
    rcof(k,3,2,0,2) = PCx(k) * rcof(k,2,2,0,3) + 2 * rcof(k,1,2,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,3,0,1,2) = PCx(k) * rcof(k,2,0,1,3) + 2 * rcof(k,1,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,3,1,1,2) = PCx(k) * rcof(k,2,1,1,3) + 2 * rcof(k,1,1,1,3)
    rcof(k,2,2,1,2) = PCx(k) * rcof(k,1,2,1,3) + 1 * rcof(k,0,2,1,3)
    rcof(k,3,2,1,2) = PCx(k) * rcof(k,2,2,1,3) + 2 * rcof(k,1,2,1,3)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,3,1,0,1) = PCx(k) * rcof(k,2,1,0,2) + 2 * rcof(k,1,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) + 1 * rcof(k,0,2,0,2)
    rcof(k,3,2,0,1) = PCx(k) * rcof(k,2,2,0,2) + 2 * rcof(k,1,2,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,3,0,1,1) = PCx(k) * rcof(k,2,0,1,2) + 2 * rcof(k,1,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,3,1,1,1) = PCx(k) * rcof(k,2,1,1,2) + 2 * rcof(k,1,1,1,2)
    rcof(k,2,2,1,1) = PCx(k) * rcof(k,1,2,1,2) + 1 * rcof(k,0,2,1,2)
    rcof(k,3,2,1,1) = PCx(k) * rcof(k,2,2,1,2) + 2 * rcof(k,1,2,1,2)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,3,1,0,0) = PCx(k) * rcof(k,2,1,0,1) + 2 * rcof(k,1,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) + 1 * rcof(k,0,2,0,1)
    rcof(k,3,2,0,0) = PCx(k) * rcof(k,2,2,0,1) + 2 * rcof(k,1,2,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,3,0,1,0) = PCx(k) * rcof(k,2,0,1,1) + 2 * rcof(k,1,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,3,1,1,0) = PCx(k) * rcof(k,2,1,1,1) + 2 * rcof(k,1,1,1,1)
    rcof(k,2,2,1,0) = PCx(k) * rcof(k,1,2,1,1) + 1 * rcof(k,0,2,1,1)
    rcof(k,3,2,1,0) = PCx(k) * rcof(k,2,2,1,1) + 2 * rcof(k,1,2,1,1)
  end do

end subroutine

pure subroutine r2_3_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,3,0,5) = PCy(k) * rcof(k,0,2,0,6) + 2 * rcof(k,0,1,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,0,3,1,5) = PCy(k) * rcof(k,0,2,1,6) + 2 * rcof(k,0,1,1,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,3,0,5) = PCx(k) * rcof(k,0,3,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,2,1,5) = PCx(k) * rcof(k,0,2,1,6)
    rcof(k,1,3,1,5) = PCx(k) * rcof(k,0,3,1,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,2,2,0,5) = PCx(k) * rcof(k,1,2,0,6) + 1 * rcof(k,0,2,0,6)
    rcof(k,2,3,0,5) = PCx(k) * rcof(k,1,3,0,6) + 1 * rcof(k,0,3,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,2,1,1,5) = PCx(k) * rcof(k,1,1,1,6) + 1 * rcof(k,0,1,1,6)
    rcof(k,2,2,1,5) = PCx(k) * rcof(k,1,2,1,6) + 1 * rcof(k,0,2,1,6)
    rcof(k,2,3,1,5) = PCx(k) * rcof(k,1,3,1,6) + 1 * rcof(k,0,3,1,6)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,3,0,4) = PCy(k) * rcof(k,0,2,0,5) + 2 * rcof(k,0,1,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,3,1,4) = PCy(k) * rcof(k,0,2,1,5) + 2 * rcof(k,0,1,1,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,3,0,4) = PCx(k) * rcof(k,0,3,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,3,1,4) = PCx(k) * rcof(k,0,3,1,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) + 1 * rcof(k,0,2,0,5)
    rcof(k,2,3,0,4) = PCx(k) * rcof(k,1,3,0,5) + 1 * rcof(k,0,3,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,2,2,1,4) = PCx(k) * rcof(k,1,2,1,5) + 1 * rcof(k,0,2,1,5)
    rcof(k,2,3,1,4) = PCx(k) * rcof(k,1,3,1,5) + 1 * rcof(k,0,3,1,5)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,3,1,3) = PCy(k) * rcof(k,0,2,1,4) + 2 * rcof(k,0,1,1,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,3,0,3) = PCx(k) * rcof(k,0,3,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,3,1,3) = PCx(k) * rcof(k,0,3,1,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) + 1 * rcof(k,0,2,0,4)
    rcof(k,2,3,0,3) = PCx(k) * rcof(k,1,3,0,4) + 1 * rcof(k,0,3,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,2,2,1,3) = PCx(k) * rcof(k,1,2,1,4) + 1 * rcof(k,0,2,1,4)
    rcof(k,2,3,1,3) = PCx(k) * rcof(k,1,3,1,4) + 1 * rcof(k,0,3,1,4)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,3,1,2) = PCy(k) * rcof(k,0,2,1,3) + 2 * rcof(k,0,1,1,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,3,0,2) = PCx(k) * rcof(k,0,3,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,3,1,2) = PCx(k) * rcof(k,0,3,1,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) + 1 * rcof(k,0,2,0,3)
    rcof(k,2,3,0,2) = PCx(k) * rcof(k,1,3,0,3) + 1 * rcof(k,0,3,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,2,2,1,2) = PCx(k) * rcof(k,1,2,1,3) + 1 * rcof(k,0,2,1,3)
    rcof(k,2,3,1,2) = PCx(k) * rcof(k,1,3,1,3) + 1 * rcof(k,0,3,1,3)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,3,1,1) = PCy(k) * rcof(k,0,2,1,2) + 2 * rcof(k,0,1,1,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,3,0,1) = PCx(k) * rcof(k,0,3,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,3,1,1) = PCx(k) * rcof(k,0,3,1,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) + 1 * rcof(k,0,2,0,2)
    rcof(k,2,3,0,1) = PCx(k) * rcof(k,1,3,0,2) + 1 * rcof(k,0,3,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,2,2,1,1) = PCx(k) * rcof(k,1,2,1,2) + 1 * rcof(k,0,2,1,2)
    rcof(k,2,3,1,1) = PCx(k) * rcof(k,1,3,1,2) + 1 * rcof(k,0,3,1,2)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,3,1,0) = PCy(k) * rcof(k,0,2,1,1) + 2 * rcof(k,0,1,1,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,3,0,0) = PCx(k) * rcof(k,0,3,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,3,1,0) = PCx(k) * rcof(k,0,3,1,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) + 1 * rcof(k,0,2,0,1)
    rcof(k,2,3,0,0) = PCx(k) * rcof(k,1,3,0,1) + 1 * rcof(k,0,3,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,2,2,1,0) = PCx(k) * rcof(k,1,2,1,1) + 1 * rcof(k,0,2,1,1)
    rcof(k,2,3,1,0) = PCx(k) * rcof(k,1,3,1,1) + 1 * rcof(k,0,3,1,1)
  end do

end subroutine

pure subroutine r3_1_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,1,2,5) = PCx(k) * rcof(k,0,1,2,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,3,0,0,5) = PCx(k) * rcof(k,2,0,0,6) + 2 * rcof(k,1,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,3,1,0,5) = PCx(k) * rcof(k,2,1,0,6) + 2 * rcof(k,1,1,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,3,0,1,5) = PCx(k) * rcof(k,2,0,1,6) + 2 * rcof(k,1,0,1,6)
    rcof(k,2,1,1,5) = PCx(k) * rcof(k,1,1,1,6) + 1 * rcof(k,0,1,1,6)
    rcof(k,3,1,1,5) = PCx(k) * rcof(k,2,1,1,6) + 2 * rcof(k,1,1,1,6)
    rcof(k,2,0,2,5) = PCx(k) * rcof(k,1,0,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,3,0,2,5) = PCx(k) * rcof(k,2,0,2,6) + 2 * rcof(k,1,0,2,6)
    rcof(k,2,1,2,5) = PCx(k) * rcof(k,1,1,2,6) + 1 * rcof(k,0,1,2,6)
    rcof(k,3,1,2,5) = PCx(k) * rcof(k,2,1,2,6) + 2 * rcof(k,1,1,2,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,3,0,0,4) = PCx(k) * rcof(k,2,0,0,5) + 2 * rcof(k,1,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,3,1,0,4) = PCx(k) * rcof(k,2,1,0,5) + 2 * rcof(k,1,1,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,3,0,1,4) = PCx(k) * rcof(k,2,0,1,5) + 2 * rcof(k,1,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,3,1,1,4) = PCx(k) * rcof(k,2,1,1,5) + 2 * rcof(k,1,1,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,3,0,2,4) = PCx(k) * rcof(k,2,0,2,5) + 2 * rcof(k,1,0,2,5)
    rcof(k,2,1,2,4) = PCx(k) * rcof(k,1,1,2,5) + 1 * rcof(k,0,1,2,5)
    rcof(k,3,1,2,4) = PCx(k) * rcof(k,2,1,2,5) + 2 * rcof(k,1,1,2,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,3,1,0,3) = PCx(k) * rcof(k,2,1,0,4) + 2 * rcof(k,1,1,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,3,0,1,3) = PCx(k) * rcof(k,2,0,1,4) + 2 * rcof(k,1,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,3,1,1,3) = PCx(k) * rcof(k,2,1,1,4) + 2 * rcof(k,1,1,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,3,0,2,3) = PCx(k) * rcof(k,2,0,2,4) + 2 * rcof(k,1,0,2,4)
    rcof(k,2,1,2,3) = PCx(k) * rcof(k,1,1,2,4) + 1 * rcof(k,0,1,2,4)
    rcof(k,3,1,2,3) = PCx(k) * rcof(k,2,1,2,4) + 2 * rcof(k,1,1,2,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,3,1,0,2) = PCx(k) * rcof(k,2,1,0,3) + 2 * rcof(k,1,1,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,3,0,1,2) = PCx(k) * rcof(k,2,0,1,3) + 2 * rcof(k,1,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,3,1,1,2) = PCx(k) * rcof(k,2,1,1,3) + 2 * rcof(k,1,1,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,3,0,2,2) = PCx(k) * rcof(k,2,0,2,3) + 2 * rcof(k,1,0,2,3)
    rcof(k,2,1,2,2) = PCx(k) * rcof(k,1,1,2,3) + 1 * rcof(k,0,1,2,3)
    rcof(k,3,1,2,2) = PCx(k) * rcof(k,2,1,2,3) + 2 * rcof(k,1,1,2,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,3,1,0,1) = PCx(k) * rcof(k,2,1,0,2) + 2 * rcof(k,1,1,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,3,0,1,1) = PCx(k) * rcof(k,2,0,1,2) + 2 * rcof(k,1,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,3,1,1,1) = PCx(k) * rcof(k,2,1,1,2) + 2 * rcof(k,1,1,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,3,0,2,1) = PCx(k) * rcof(k,2,0,2,2) + 2 * rcof(k,1,0,2,2)
    rcof(k,2,1,2,1) = PCx(k) * rcof(k,1,1,2,2) + 1 * rcof(k,0,1,2,2)
    rcof(k,3,1,2,1) = PCx(k) * rcof(k,2,1,2,2) + 2 * rcof(k,1,1,2,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,3,1,0,0) = PCx(k) * rcof(k,2,1,0,1) + 2 * rcof(k,1,1,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,3,0,1,0) = PCx(k) * rcof(k,2,0,1,1) + 2 * rcof(k,1,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,3,1,1,0) = PCx(k) * rcof(k,2,1,1,1) + 2 * rcof(k,1,1,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,3,0,2,0) = PCx(k) * rcof(k,2,0,2,1) + 2 * rcof(k,1,0,2,1)
    rcof(k,2,1,2,0) = PCx(k) * rcof(k,1,1,2,1) + 1 * rcof(k,0,1,2,1)
    rcof(k,3,1,2,0) = PCx(k) * rcof(k,2,1,2,1) + 2 * rcof(k,1,1,2,1)
  end do

end subroutine

pure subroutine r1_3_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,3,0,5) = PCy(k) * rcof(k,0,2,0,6) + 2 * rcof(k,0,1,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,0,3,1,5) = PCy(k) * rcof(k,0,2,1,6) + 2 * rcof(k,0,1,1,6)
    rcof(k,0,2,2,5) = PCy(k) * rcof(k,0,1,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,0,3,2,5) = PCy(k) * rcof(k,0,2,2,6) + 2 * rcof(k,0,1,2,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,3,0,5) = PCx(k) * rcof(k,0,3,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,2,1,5) = PCx(k) * rcof(k,0,2,1,6)
    rcof(k,1,3,1,5) = PCx(k) * rcof(k,0,3,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,1,2,5) = PCx(k) * rcof(k,0,1,2,6)
    rcof(k,1,2,2,5) = PCx(k) * rcof(k,0,2,2,6)
    rcof(k,1,3,2,5) = PCx(k) * rcof(k,0,3,2,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,3,0,4) = PCy(k) * rcof(k,0,2,0,5) + 2 * rcof(k,0,1,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,3,1,4) = PCy(k) * rcof(k,0,2,1,5) + 2 * rcof(k,0,1,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,0,3,2,4) = PCy(k) * rcof(k,0,2,2,5) + 2 * rcof(k,0,1,2,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,3,0,4) = PCx(k) * rcof(k,0,3,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,3,1,4) = PCx(k) * rcof(k,0,3,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,2,2,4) = PCx(k) * rcof(k,0,2,2,5)
    rcof(k,1,3,2,4) = PCx(k) * rcof(k,0,3,2,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,3,1,3) = PCy(k) * rcof(k,0,2,1,4) + 2 * rcof(k,0,1,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,0,3,2,3) = PCy(k) * rcof(k,0,2,2,4) + 2 * rcof(k,0,1,2,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,3,0,3) = PCx(k) * rcof(k,0,3,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,3,1,3) = PCx(k) * rcof(k,0,3,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,2,2,3) = PCx(k) * rcof(k,0,2,2,4)
    rcof(k,1,3,2,3) = PCx(k) * rcof(k,0,3,2,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,3,1,2) = PCy(k) * rcof(k,0,2,1,3) + 2 * rcof(k,0,1,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,0,3,2,2) = PCy(k) * rcof(k,0,2,2,3) + 2 * rcof(k,0,1,2,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,3,0,2) = PCx(k) * rcof(k,0,3,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,3,1,2) = PCx(k) * rcof(k,0,3,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,2,2,2) = PCx(k) * rcof(k,0,2,2,3)
    rcof(k,1,3,2,2) = PCx(k) * rcof(k,0,3,2,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,3,1,1) = PCy(k) * rcof(k,0,2,1,2) + 2 * rcof(k,0,1,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,0,3,2,1) = PCy(k) * rcof(k,0,2,2,2) + 2 * rcof(k,0,1,2,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,3,0,1) = PCx(k) * rcof(k,0,3,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,3,1,1) = PCx(k) * rcof(k,0,3,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,2,2,1) = PCx(k) * rcof(k,0,2,2,2)
    rcof(k,1,3,2,1) = PCx(k) * rcof(k,0,3,2,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,3,1,0) = PCy(k) * rcof(k,0,2,1,1) + 2 * rcof(k,0,1,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,0,3,2,0) = PCy(k) * rcof(k,0,2,2,1) + 2 * rcof(k,0,1,2,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,3,0,0) = PCx(k) * rcof(k,0,3,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,3,1,0) = PCx(k) * rcof(k,0,3,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,2,2,0) = PCx(k) * rcof(k,0,2,2,1)
    rcof(k,1,3,2,0) = PCx(k) * rcof(k,0,3,2,1)
  end do

end subroutine

pure subroutine r2_1_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,0,3,5) = PCz(k) * rcof(k,0,0,2,6) + 2 * rcof(k,0,0,1,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,0,1,3,5) = PCy(k) * rcof(k,0,0,3,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,1,2,5) = PCx(k) * rcof(k,0,1,2,6)
    rcof(k,1,0,3,5) = PCx(k) * rcof(k,0,0,3,6)
    rcof(k,1,1,3,5) = PCx(k) * rcof(k,0,1,3,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,2,1,1,5) = PCx(k) * rcof(k,1,1,1,6) + 1 * rcof(k,0,1,1,6)
    rcof(k,2,0,2,5) = PCx(k) * rcof(k,1,0,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,2,1,2,5) = PCx(k) * rcof(k,1,1,2,6) + 1 * rcof(k,0,1,2,6)
    rcof(k,2,0,3,5) = PCx(k) * rcof(k,1,0,3,6) + 1 * rcof(k,0,0,3,6)
    rcof(k,2,1,3,5) = PCx(k) * rcof(k,1,1,3,6) + 1 * rcof(k,0,1,3,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,0,3,4) = PCz(k) * rcof(k,0,0,2,5) + 2 * rcof(k,0,0,1,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,1,3,4) = PCy(k) * rcof(k,0,0,3,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,0,3,4) = PCx(k) * rcof(k,0,0,3,5)
    rcof(k,1,1,3,4) = PCx(k) * rcof(k,0,1,3,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,2,1,2,4) = PCx(k) * rcof(k,1,1,2,5) + 1 * rcof(k,0,1,2,5)
    rcof(k,2,0,3,4) = PCx(k) * rcof(k,1,0,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,2,1,3,4) = PCx(k) * rcof(k,1,1,3,5) + 1 * rcof(k,0,1,3,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,1,3,3) = PCy(k) * rcof(k,0,0,3,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,0,3,3) = PCx(k) * rcof(k,0,0,3,4)
    rcof(k,1,1,3,3) = PCx(k) * rcof(k,0,1,3,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,2,1,2,3) = PCx(k) * rcof(k,1,1,2,4) + 1 * rcof(k,0,1,2,4)
    rcof(k,2,0,3,3) = PCx(k) * rcof(k,1,0,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,2,1,3,3) = PCx(k) * rcof(k,1,1,3,4) + 1 * rcof(k,0,1,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,1,3,2) = PCy(k) * rcof(k,0,0,3,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,0,3,2) = PCx(k) * rcof(k,0,0,3,3)
    rcof(k,1,1,3,2) = PCx(k) * rcof(k,0,1,3,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,2,1,2,2) = PCx(k) * rcof(k,1,1,2,3) + 1 * rcof(k,0,1,2,3)
    rcof(k,2,0,3,2) = PCx(k) * rcof(k,1,0,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,2,1,3,2) = PCx(k) * rcof(k,1,1,3,3) + 1 * rcof(k,0,1,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,1,3,1) = PCy(k) * rcof(k,0,0,3,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,0,3,1) = PCx(k) * rcof(k,0,0,3,2)
    rcof(k,1,1,3,1) = PCx(k) * rcof(k,0,1,3,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,2,1,2,1) = PCx(k) * rcof(k,1,1,2,2) + 1 * rcof(k,0,1,2,2)
    rcof(k,2,0,3,1) = PCx(k) * rcof(k,1,0,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,2,1,3,1) = PCx(k) * rcof(k,1,1,3,2) + 1 * rcof(k,0,1,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,1,3,0) = PCy(k) * rcof(k,0,0,3,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,0,3,0) = PCx(k) * rcof(k,0,0,3,1)
    rcof(k,1,1,3,0) = PCx(k) * rcof(k,0,1,3,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,2,1,2,0) = PCx(k) * rcof(k,1,1,2,1) + 1 * rcof(k,0,1,2,1)
    rcof(k,2,0,3,0) = PCx(k) * rcof(k,1,0,3,1) + 1 * rcof(k,0,0,3,1)
    rcof(k,2,1,3,0) = PCx(k) * rcof(k,1,1,3,1) + 1 * rcof(k,0,1,3,1)
  end do

end subroutine

pure subroutine r1_2_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,0,3,5) = PCz(k) * rcof(k,0,0,2,6) + 2 * rcof(k,0,0,1,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,0,1,3,5) = PCy(k) * rcof(k,0,0,3,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,0,2,2,5) = PCy(k) * rcof(k,0,1,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,0,2,3,5) = PCy(k) * rcof(k,0,1,3,6) + 1 * rcof(k,0,0,3,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,2,1,5) = PCx(k) * rcof(k,0,2,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,1,2,5) = PCx(k) * rcof(k,0,1,2,6)
    rcof(k,1,2,2,5) = PCx(k) * rcof(k,0,2,2,6)
    rcof(k,1,0,3,5) = PCx(k) * rcof(k,0,0,3,6)
    rcof(k,1,1,3,5) = PCx(k) * rcof(k,0,1,3,6)
    rcof(k,1,2,3,5) = PCx(k) * rcof(k,0,2,3,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,0,3,4) = PCz(k) * rcof(k,0,0,2,5) + 2 * rcof(k,0,0,1,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,1,3,4) = PCy(k) * rcof(k,0,0,3,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,0,2,3,4) = PCy(k) * rcof(k,0,1,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,2,2,4) = PCx(k) * rcof(k,0,2,2,5)
    rcof(k,1,0,3,4) = PCx(k) * rcof(k,0,0,3,5)
    rcof(k,1,1,3,4) = PCx(k) * rcof(k,0,1,3,5)
    rcof(k,1,2,3,4) = PCx(k) * rcof(k,0,2,3,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,1,3,3) = PCy(k) * rcof(k,0,0,3,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,0,2,3,3) = PCy(k) * rcof(k,0,1,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,2,2,3) = PCx(k) * rcof(k,0,2,2,4)
    rcof(k,1,0,3,3) = PCx(k) * rcof(k,0,0,3,4)
    rcof(k,1,1,3,3) = PCx(k) * rcof(k,0,1,3,4)
    rcof(k,1,2,3,3) = PCx(k) * rcof(k,0,2,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,1,3,2) = PCy(k) * rcof(k,0,0,3,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,0,2,3,2) = PCy(k) * rcof(k,0,1,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,2,2,2) = PCx(k) * rcof(k,0,2,2,3)
    rcof(k,1,0,3,2) = PCx(k) * rcof(k,0,0,3,3)
    rcof(k,1,1,3,2) = PCx(k) * rcof(k,0,1,3,3)
    rcof(k,1,2,3,2) = PCx(k) * rcof(k,0,2,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,1,3,1) = PCy(k) * rcof(k,0,0,3,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,0,2,3,1) = PCy(k) * rcof(k,0,1,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,2,2,1) = PCx(k) * rcof(k,0,2,2,2)
    rcof(k,1,0,3,1) = PCx(k) * rcof(k,0,0,3,2)
    rcof(k,1,1,3,1) = PCx(k) * rcof(k,0,1,3,2)
    rcof(k,1,2,3,1) = PCx(k) * rcof(k,0,2,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,1,3,0) = PCy(k) * rcof(k,0,0,3,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,0,2,3,0) = PCy(k) * rcof(k,0,1,3,1) + 1 * rcof(k,0,0,3,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,2,2,0) = PCx(k) * rcof(k,0,2,2,1)
    rcof(k,1,0,3,0) = PCx(k) * rcof(k,0,0,3,1)
    rcof(k,1,1,3,0) = PCx(k) * rcof(k,0,1,3,1)
    rcof(k,1,2,3,0) = PCx(k) * rcof(k,0,2,3,1)
  end do

end subroutine

pure subroutine r3_2_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,0,7) = (-two*p(k))**7 * Fn(7,k)
    rcof(k,0,0,1,6) = PCz(k) * rcof(k,0,0,0,7)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,6) = PCz(k) * rcof(k,0,0,1,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,1,0,6) = PCy(k) * rcof(k,0,0,0,7)
    rcof(k,0,1,1,6) = PCy(k) * rcof(k,0,0,1,7)
    rcof(k,0,1,2,6) = PCy(k) * rcof(k,0,0,2,7)
    rcof(k,0,2,0,6) = PCy(k) * rcof(k,0,1,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,2,1,6) = PCy(k) * rcof(k,0,1,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,0,2,2,6) = PCy(k) * rcof(k,0,1,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,1,0,0,6) = PCx(k) * rcof(k,0,0,0,7)
    rcof(k,1,1,0,6) = PCx(k) * rcof(k,0,1,0,7)
    rcof(k,1,2,0,6) = PCx(k) * rcof(k,0,2,0,7)
    rcof(k,1,0,1,6) = PCx(k) * rcof(k,0,0,1,7)
    rcof(k,1,1,1,6) = PCx(k) * rcof(k,0,1,1,7)
    rcof(k,1,2,1,6) = PCx(k) * rcof(k,0,2,1,7)
    rcof(k,1,0,2,6) = PCx(k) * rcof(k,0,0,2,7)
    rcof(k,1,1,2,6) = PCx(k) * rcof(k,0,1,2,7)
    rcof(k,1,2,2,6) = PCx(k) * rcof(k,0,2,2,7)
    rcof(k,2,0,0,6) = PCx(k) * rcof(k,1,0,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,3,0,0,6) = PCx(k) * rcof(k,2,0,0,7) + 2 * rcof(k,1,0,0,7)
    rcof(k,2,1,0,6) = PCx(k) * rcof(k,1,1,0,7) + 1 * rcof(k,0,1,0,7)
    rcof(k,3,1,0,6) = PCx(k) * rcof(k,2,1,0,7) + 2 * rcof(k,1,1,0,7)
    rcof(k,2,2,0,6) = PCx(k) * rcof(k,1,2,0,7) + 1 * rcof(k,0,2,0,7)
    rcof(k,3,2,0,6) = PCx(k) * rcof(k,2,2,0,7) + 2 * rcof(k,1,2,0,7)
    rcof(k,2,0,1,6) = PCx(k) * rcof(k,1,0,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,3,0,1,6) = PCx(k) * rcof(k,2,0,1,7) + 2 * rcof(k,1,0,1,7)
    rcof(k,2,1,1,6) = PCx(k) * rcof(k,1,1,1,7) + 1 * rcof(k,0,1,1,7)
    rcof(k,3,1,1,6) = PCx(k) * rcof(k,2,1,1,7) + 2 * rcof(k,1,1,1,7)
    rcof(k,2,2,1,6) = PCx(k) * rcof(k,1,2,1,7) + 1 * rcof(k,0,2,1,7)
    rcof(k,3,2,1,6) = PCx(k) * rcof(k,2,2,1,7) + 2 * rcof(k,1,2,1,7)
    rcof(k,2,0,2,6) = PCx(k) * rcof(k,1,0,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,3,0,2,6) = PCx(k) * rcof(k,2,0,2,7) + 2 * rcof(k,1,0,2,7)
    rcof(k,2,1,2,6) = PCx(k) * rcof(k,1,1,2,7) + 1 * rcof(k,0,1,2,7)
    rcof(k,3,1,2,6) = PCx(k) * rcof(k,2,1,2,7) + 2 * rcof(k,1,1,2,7)
    rcof(k,2,2,2,6) = PCx(k) * rcof(k,1,2,2,7) + 1 * rcof(k,0,2,2,7)
    rcof(k,3,2,2,6) = PCx(k) * rcof(k,2,2,2,7) + 2 * rcof(k,1,2,2,7)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,0,2,2,5) = PCy(k) * rcof(k,0,1,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,2,1,5) = PCx(k) * rcof(k,0,2,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,1,2,5) = PCx(k) * rcof(k,0,1,2,6)
    rcof(k,1,2,2,5) = PCx(k) * rcof(k,0,2,2,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,3,0,0,5) = PCx(k) * rcof(k,2,0,0,6) + 2 * rcof(k,1,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,3,1,0,5) = PCx(k) * rcof(k,2,1,0,6) + 2 * rcof(k,1,1,0,6)
    rcof(k,2,2,0,5) = PCx(k) * rcof(k,1,2,0,6) + 1 * rcof(k,0,2,0,6)
    rcof(k,3,2,0,5) = PCx(k) * rcof(k,2,2,0,6) + 2 * rcof(k,1,2,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,3,0,1,5) = PCx(k) * rcof(k,2,0,1,6) + 2 * rcof(k,1,0,1,6)
    rcof(k,2,1,1,5) = PCx(k) * rcof(k,1,1,1,6) + 1 * rcof(k,0,1,1,6)
    rcof(k,3,1,1,5) = PCx(k) * rcof(k,2,1,1,6) + 2 * rcof(k,1,1,1,6)
    rcof(k,2,2,1,5) = PCx(k) * rcof(k,1,2,1,6) + 1 * rcof(k,0,2,1,6)
    rcof(k,3,2,1,5) = PCx(k) * rcof(k,2,2,1,6) + 2 * rcof(k,1,2,1,6)
    rcof(k,2,0,2,5) = PCx(k) * rcof(k,1,0,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,3,0,2,5) = PCx(k) * rcof(k,2,0,2,6) + 2 * rcof(k,1,0,2,6)
    rcof(k,2,1,2,5) = PCx(k) * rcof(k,1,1,2,6) + 1 * rcof(k,0,1,2,6)
    rcof(k,3,1,2,5) = PCx(k) * rcof(k,2,1,2,6) + 2 * rcof(k,1,1,2,6)
    rcof(k,2,2,2,5) = PCx(k) * rcof(k,1,2,2,6) + 1 * rcof(k,0,2,2,6)
    rcof(k,3,2,2,5) = PCx(k) * rcof(k,2,2,2,6) + 2 * rcof(k,1,2,2,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,2,2,4) = PCx(k) * rcof(k,0,2,2,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,3,0,0,4) = PCx(k) * rcof(k,2,0,0,5) + 2 * rcof(k,1,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,3,1,0,4) = PCx(k) * rcof(k,2,1,0,5) + 2 * rcof(k,1,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) + 1 * rcof(k,0,2,0,5)
    rcof(k,3,2,0,4) = PCx(k) * rcof(k,2,2,0,5) + 2 * rcof(k,1,2,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,3,0,1,4) = PCx(k) * rcof(k,2,0,1,5) + 2 * rcof(k,1,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,3,1,1,4) = PCx(k) * rcof(k,2,1,1,5) + 2 * rcof(k,1,1,1,5)
    rcof(k,2,2,1,4) = PCx(k) * rcof(k,1,2,1,5) + 1 * rcof(k,0,2,1,5)
    rcof(k,3,2,1,4) = PCx(k) * rcof(k,2,2,1,5) + 2 * rcof(k,1,2,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,3,0,2,4) = PCx(k) * rcof(k,2,0,2,5) + 2 * rcof(k,1,0,2,5)
    rcof(k,2,1,2,4) = PCx(k) * rcof(k,1,1,2,5) + 1 * rcof(k,0,1,2,5)
    rcof(k,3,1,2,4) = PCx(k) * rcof(k,2,1,2,5) + 2 * rcof(k,1,1,2,5)
    rcof(k,2,2,2,4) = PCx(k) * rcof(k,1,2,2,5) + 1 * rcof(k,0,2,2,5)
    rcof(k,3,2,2,4) = PCx(k) * rcof(k,2,2,2,5) + 2 * rcof(k,1,2,2,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,2,2,3) = PCx(k) * rcof(k,0,2,2,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,3,1,0,3) = PCx(k) * rcof(k,2,1,0,4) + 2 * rcof(k,1,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) + 1 * rcof(k,0,2,0,4)
    rcof(k,3,2,0,3) = PCx(k) * rcof(k,2,2,0,4) + 2 * rcof(k,1,2,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,3,0,1,3) = PCx(k) * rcof(k,2,0,1,4) + 2 * rcof(k,1,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,3,1,1,3) = PCx(k) * rcof(k,2,1,1,4) + 2 * rcof(k,1,1,1,4)
    rcof(k,2,2,1,3) = PCx(k) * rcof(k,1,2,1,4) + 1 * rcof(k,0,2,1,4)
    rcof(k,3,2,1,3) = PCx(k) * rcof(k,2,2,1,4) + 2 * rcof(k,1,2,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,3,0,2,3) = PCx(k) * rcof(k,2,0,2,4) + 2 * rcof(k,1,0,2,4)
    rcof(k,2,1,2,3) = PCx(k) * rcof(k,1,1,2,4) + 1 * rcof(k,0,1,2,4)
    rcof(k,3,1,2,3) = PCx(k) * rcof(k,2,1,2,4) + 2 * rcof(k,1,1,2,4)
    rcof(k,2,2,2,3) = PCx(k) * rcof(k,1,2,2,4) + 1 * rcof(k,0,2,2,4)
    rcof(k,3,2,2,3) = PCx(k) * rcof(k,2,2,2,4) + 2 * rcof(k,1,2,2,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,2,2,2) = PCx(k) * rcof(k,0,2,2,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,3,1,0,2) = PCx(k) * rcof(k,2,1,0,3) + 2 * rcof(k,1,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) + 1 * rcof(k,0,2,0,3)
    rcof(k,3,2,0,2) = PCx(k) * rcof(k,2,2,0,3) + 2 * rcof(k,1,2,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,3,0,1,2) = PCx(k) * rcof(k,2,0,1,3) + 2 * rcof(k,1,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,3,1,1,2) = PCx(k) * rcof(k,2,1,1,3) + 2 * rcof(k,1,1,1,3)
    rcof(k,2,2,1,2) = PCx(k) * rcof(k,1,2,1,3) + 1 * rcof(k,0,2,1,3)
    rcof(k,3,2,1,2) = PCx(k) * rcof(k,2,2,1,3) + 2 * rcof(k,1,2,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,3,0,2,2) = PCx(k) * rcof(k,2,0,2,3) + 2 * rcof(k,1,0,2,3)
    rcof(k,2,1,2,2) = PCx(k) * rcof(k,1,1,2,3) + 1 * rcof(k,0,1,2,3)
    rcof(k,3,1,2,2) = PCx(k) * rcof(k,2,1,2,3) + 2 * rcof(k,1,1,2,3)
    rcof(k,2,2,2,2) = PCx(k) * rcof(k,1,2,2,3) + 1 * rcof(k,0,2,2,3)
    rcof(k,3,2,2,2) = PCx(k) * rcof(k,2,2,2,3) + 2 * rcof(k,1,2,2,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,2,2,1) = PCx(k) * rcof(k,0,2,2,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,3,1,0,1) = PCx(k) * rcof(k,2,1,0,2) + 2 * rcof(k,1,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) + 1 * rcof(k,0,2,0,2)
    rcof(k,3,2,0,1) = PCx(k) * rcof(k,2,2,0,2) + 2 * rcof(k,1,2,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,3,0,1,1) = PCx(k) * rcof(k,2,0,1,2) + 2 * rcof(k,1,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,3,1,1,1) = PCx(k) * rcof(k,2,1,1,2) + 2 * rcof(k,1,1,1,2)
    rcof(k,2,2,1,1) = PCx(k) * rcof(k,1,2,1,2) + 1 * rcof(k,0,2,1,2)
    rcof(k,3,2,1,1) = PCx(k) * rcof(k,2,2,1,2) + 2 * rcof(k,1,2,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,3,0,2,1) = PCx(k) * rcof(k,2,0,2,2) + 2 * rcof(k,1,0,2,2)
    rcof(k,2,1,2,1) = PCx(k) * rcof(k,1,1,2,2) + 1 * rcof(k,0,1,2,2)
    rcof(k,3,1,2,1) = PCx(k) * rcof(k,2,1,2,2) + 2 * rcof(k,1,1,2,2)
    rcof(k,2,2,2,1) = PCx(k) * rcof(k,1,2,2,2) + 1 * rcof(k,0,2,2,2)
    rcof(k,3,2,2,1) = PCx(k) * rcof(k,2,2,2,2) + 2 * rcof(k,1,2,2,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,2,2,0) = PCx(k) * rcof(k,0,2,2,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,3,1,0,0) = PCx(k) * rcof(k,2,1,0,1) + 2 * rcof(k,1,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) + 1 * rcof(k,0,2,0,1)
    rcof(k,3,2,0,0) = PCx(k) * rcof(k,2,2,0,1) + 2 * rcof(k,1,2,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,3,0,1,0) = PCx(k) * rcof(k,2,0,1,1) + 2 * rcof(k,1,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,3,1,1,0) = PCx(k) * rcof(k,2,1,1,1) + 2 * rcof(k,1,1,1,1)
    rcof(k,2,2,1,0) = PCx(k) * rcof(k,1,2,1,1) + 1 * rcof(k,0,2,1,1)
    rcof(k,3,2,1,0) = PCx(k) * rcof(k,2,2,1,1) + 2 * rcof(k,1,2,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,3,0,2,0) = PCx(k) * rcof(k,2,0,2,1) + 2 * rcof(k,1,0,2,1)
    rcof(k,2,1,2,0) = PCx(k) * rcof(k,1,1,2,1) + 1 * rcof(k,0,1,2,1)
    rcof(k,3,1,2,0) = PCx(k) * rcof(k,2,1,2,1) + 2 * rcof(k,1,1,2,1)
    rcof(k,2,2,2,0) = PCx(k) * rcof(k,1,2,2,1) + 1 * rcof(k,0,2,2,1)
    rcof(k,3,2,2,0) = PCx(k) * rcof(k,2,2,2,1) + 2 * rcof(k,1,2,2,1)
  end do

end subroutine

pure subroutine r2_3_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,0,7) = (-two*p(k))**7 * Fn(7,k)
    rcof(k,0,0,1,6) = PCz(k) * rcof(k,0,0,0,7)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,6) = PCz(k) * rcof(k,0,0,1,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,1,0,6) = PCy(k) * rcof(k,0,0,0,7)
    rcof(k,0,1,1,6) = PCy(k) * rcof(k,0,0,1,7)
    rcof(k,0,1,2,6) = PCy(k) * rcof(k,0,0,2,7)
    rcof(k,0,2,0,6) = PCy(k) * rcof(k,0,1,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,3,0,6) = PCy(k) * rcof(k,0,2,0,7) + 2 * rcof(k,0,1,0,7)
    rcof(k,0,2,1,6) = PCy(k) * rcof(k,0,1,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,0,3,1,6) = PCy(k) * rcof(k,0,2,1,7) + 2 * rcof(k,0,1,1,7)
    rcof(k,0,2,2,6) = PCy(k) * rcof(k,0,1,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,0,3,2,6) = PCy(k) * rcof(k,0,2,2,7) + 2 * rcof(k,0,1,2,7)
    rcof(k,1,0,0,6) = PCx(k) * rcof(k,0,0,0,7)
    rcof(k,1,1,0,6) = PCx(k) * rcof(k,0,1,0,7)
    rcof(k,1,2,0,6) = PCx(k) * rcof(k,0,2,0,7)
    rcof(k,1,3,0,6) = PCx(k) * rcof(k,0,3,0,7)
    rcof(k,1,0,1,6) = PCx(k) * rcof(k,0,0,1,7)
    rcof(k,1,1,1,6) = PCx(k) * rcof(k,0,1,1,7)
    rcof(k,1,2,1,6) = PCx(k) * rcof(k,0,2,1,7)
    rcof(k,1,3,1,6) = PCx(k) * rcof(k,0,3,1,7)
    rcof(k,1,0,2,6) = PCx(k) * rcof(k,0,0,2,7)
    rcof(k,1,1,2,6) = PCx(k) * rcof(k,0,1,2,7)
    rcof(k,1,2,2,6) = PCx(k) * rcof(k,0,2,2,7)
    rcof(k,1,3,2,6) = PCx(k) * rcof(k,0,3,2,7)
    rcof(k,2,0,0,6) = PCx(k) * rcof(k,1,0,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,2,1,0,6) = PCx(k) * rcof(k,1,1,0,7) + 1 * rcof(k,0,1,0,7)
    rcof(k,2,2,0,6) = PCx(k) * rcof(k,1,2,0,7) + 1 * rcof(k,0,2,0,7)
    rcof(k,2,3,0,6) = PCx(k) * rcof(k,1,3,0,7) + 1 * rcof(k,0,3,0,7)
    rcof(k,2,0,1,6) = PCx(k) * rcof(k,1,0,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,2,1,1,6) = PCx(k) * rcof(k,1,1,1,7) + 1 * rcof(k,0,1,1,7)
    rcof(k,2,2,1,6) = PCx(k) * rcof(k,1,2,1,7) + 1 * rcof(k,0,2,1,7)
    rcof(k,2,3,1,6) = PCx(k) * rcof(k,1,3,1,7) + 1 * rcof(k,0,3,1,7)
    rcof(k,2,0,2,6) = PCx(k) * rcof(k,1,0,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,2,1,2,6) = PCx(k) * rcof(k,1,1,2,7) + 1 * rcof(k,0,1,2,7)
    rcof(k,2,2,2,6) = PCx(k) * rcof(k,1,2,2,7) + 1 * rcof(k,0,2,2,7)
    rcof(k,2,3,2,6) = PCx(k) * rcof(k,1,3,2,7) + 1 * rcof(k,0,3,2,7)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,3,0,5) = PCy(k) * rcof(k,0,2,0,6) + 2 * rcof(k,0,1,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,0,3,1,5) = PCy(k) * rcof(k,0,2,1,6) + 2 * rcof(k,0,1,1,6)
    rcof(k,0,2,2,5) = PCy(k) * rcof(k,0,1,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,0,3,2,5) = PCy(k) * rcof(k,0,2,2,6) + 2 * rcof(k,0,1,2,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,3,0,5) = PCx(k) * rcof(k,0,3,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,2,1,5) = PCx(k) * rcof(k,0,2,1,6)
    rcof(k,1,3,1,5) = PCx(k) * rcof(k,0,3,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,1,2,5) = PCx(k) * rcof(k,0,1,2,6)
    rcof(k,1,2,2,5) = PCx(k) * rcof(k,0,2,2,6)
    rcof(k,1,3,2,5) = PCx(k) * rcof(k,0,3,2,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,2,2,0,5) = PCx(k) * rcof(k,1,2,0,6) + 1 * rcof(k,0,2,0,6)
    rcof(k,2,3,0,5) = PCx(k) * rcof(k,1,3,0,6) + 1 * rcof(k,0,3,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,2,1,1,5) = PCx(k) * rcof(k,1,1,1,6) + 1 * rcof(k,0,1,1,6)
    rcof(k,2,2,1,5) = PCx(k) * rcof(k,1,2,1,6) + 1 * rcof(k,0,2,1,6)
    rcof(k,2,3,1,5) = PCx(k) * rcof(k,1,3,1,6) + 1 * rcof(k,0,3,1,6)
    rcof(k,2,0,2,5) = PCx(k) * rcof(k,1,0,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,2,1,2,5) = PCx(k) * rcof(k,1,1,2,6) + 1 * rcof(k,0,1,2,6)
    rcof(k,2,2,2,5) = PCx(k) * rcof(k,1,2,2,6) + 1 * rcof(k,0,2,2,6)
    rcof(k,2,3,2,5) = PCx(k) * rcof(k,1,3,2,6) + 1 * rcof(k,0,3,2,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,3,0,4) = PCy(k) * rcof(k,0,2,0,5) + 2 * rcof(k,0,1,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,3,1,4) = PCy(k) * rcof(k,0,2,1,5) + 2 * rcof(k,0,1,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,0,3,2,4) = PCy(k) * rcof(k,0,2,2,5) + 2 * rcof(k,0,1,2,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,3,0,4) = PCx(k) * rcof(k,0,3,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,3,1,4) = PCx(k) * rcof(k,0,3,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,2,2,4) = PCx(k) * rcof(k,0,2,2,5)
    rcof(k,1,3,2,4) = PCx(k) * rcof(k,0,3,2,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) + 1 * rcof(k,0,2,0,5)
    rcof(k,2,3,0,4) = PCx(k) * rcof(k,1,3,0,5) + 1 * rcof(k,0,3,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,2,2,1,4) = PCx(k) * rcof(k,1,2,1,5) + 1 * rcof(k,0,2,1,5)
    rcof(k,2,3,1,4) = PCx(k) * rcof(k,1,3,1,5) + 1 * rcof(k,0,3,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,2,1,2,4) = PCx(k) * rcof(k,1,1,2,5) + 1 * rcof(k,0,1,2,5)
    rcof(k,2,2,2,4) = PCx(k) * rcof(k,1,2,2,5) + 1 * rcof(k,0,2,2,5)
    rcof(k,2,3,2,4) = PCx(k) * rcof(k,1,3,2,5) + 1 * rcof(k,0,3,2,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,3,1,3) = PCy(k) * rcof(k,0,2,1,4) + 2 * rcof(k,0,1,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,0,3,2,3) = PCy(k) * rcof(k,0,2,2,4) + 2 * rcof(k,0,1,2,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,3,0,3) = PCx(k) * rcof(k,0,3,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,3,1,3) = PCx(k) * rcof(k,0,3,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,2,2,3) = PCx(k) * rcof(k,0,2,2,4)
    rcof(k,1,3,2,3) = PCx(k) * rcof(k,0,3,2,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) + 1 * rcof(k,0,2,0,4)
    rcof(k,2,3,0,3) = PCx(k) * rcof(k,1,3,0,4) + 1 * rcof(k,0,3,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,2,2,1,3) = PCx(k) * rcof(k,1,2,1,4) + 1 * rcof(k,0,2,1,4)
    rcof(k,2,3,1,3) = PCx(k) * rcof(k,1,3,1,4) + 1 * rcof(k,0,3,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,2,1,2,3) = PCx(k) * rcof(k,1,1,2,4) + 1 * rcof(k,0,1,2,4)
    rcof(k,2,2,2,3) = PCx(k) * rcof(k,1,2,2,4) + 1 * rcof(k,0,2,2,4)
    rcof(k,2,3,2,3) = PCx(k) * rcof(k,1,3,2,4) + 1 * rcof(k,0,3,2,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,3,1,2) = PCy(k) * rcof(k,0,2,1,3) + 2 * rcof(k,0,1,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,0,3,2,2) = PCy(k) * rcof(k,0,2,2,3) + 2 * rcof(k,0,1,2,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,3,0,2) = PCx(k) * rcof(k,0,3,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,3,1,2) = PCx(k) * rcof(k,0,3,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,2,2,2) = PCx(k) * rcof(k,0,2,2,3)
    rcof(k,1,3,2,2) = PCx(k) * rcof(k,0,3,2,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) + 1 * rcof(k,0,2,0,3)
    rcof(k,2,3,0,2) = PCx(k) * rcof(k,1,3,0,3) + 1 * rcof(k,0,3,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,2,2,1,2) = PCx(k) * rcof(k,1,2,1,3) + 1 * rcof(k,0,2,1,3)
    rcof(k,2,3,1,2) = PCx(k) * rcof(k,1,3,1,3) + 1 * rcof(k,0,3,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,2,1,2,2) = PCx(k) * rcof(k,1,1,2,3) + 1 * rcof(k,0,1,2,3)
    rcof(k,2,2,2,2) = PCx(k) * rcof(k,1,2,2,3) + 1 * rcof(k,0,2,2,3)
    rcof(k,2,3,2,2) = PCx(k) * rcof(k,1,3,2,3) + 1 * rcof(k,0,3,2,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,3,1,1) = PCy(k) * rcof(k,0,2,1,2) + 2 * rcof(k,0,1,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,0,3,2,1) = PCy(k) * rcof(k,0,2,2,2) + 2 * rcof(k,0,1,2,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,3,0,1) = PCx(k) * rcof(k,0,3,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,3,1,1) = PCx(k) * rcof(k,0,3,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,2,2,1) = PCx(k) * rcof(k,0,2,2,2)
    rcof(k,1,3,2,1) = PCx(k) * rcof(k,0,3,2,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) + 1 * rcof(k,0,2,0,2)
    rcof(k,2,3,0,1) = PCx(k) * rcof(k,1,3,0,2) + 1 * rcof(k,0,3,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,2,2,1,1) = PCx(k) * rcof(k,1,2,1,2) + 1 * rcof(k,0,2,1,2)
    rcof(k,2,3,1,1) = PCx(k) * rcof(k,1,3,1,2) + 1 * rcof(k,0,3,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,2,1,2,1) = PCx(k) * rcof(k,1,1,2,2) + 1 * rcof(k,0,1,2,2)
    rcof(k,2,2,2,1) = PCx(k) * rcof(k,1,2,2,2) + 1 * rcof(k,0,2,2,2)
    rcof(k,2,3,2,1) = PCx(k) * rcof(k,1,3,2,2) + 1 * rcof(k,0,3,2,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,3,1,0) = PCy(k) * rcof(k,0,2,1,1) + 2 * rcof(k,0,1,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,0,3,2,0) = PCy(k) * rcof(k,0,2,2,1) + 2 * rcof(k,0,1,2,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,3,0,0) = PCx(k) * rcof(k,0,3,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,3,1,0) = PCx(k) * rcof(k,0,3,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,2,2,0) = PCx(k) * rcof(k,0,2,2,1)
    rcof(k,1,3,2,0) = PCx(k) * rcof(k,0,3,2,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) + 1 * rcof(k,0,2,0,1)
    rcof(k,2,3,0,0) = PCx(k) * rcof(k,1,3,0,1) + 1 * rcof(k,0,3,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,2,2,1,0) = PCx(k) * rcof(k,1,2,1,1) + 1 * rcof(k,0,2,1,1)
    rcof(k,2,3,1,0) = PCx(k) * rcof(k,1,3,1,1) + 1 * rcof(k,0,3,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,2,1,2,0) = PCx(k) * rcof(k,1,1,2,1) + 1 * rcof(k,0,1,2,1)
    rcof(k,2,2,2,0) = PCx(k) * rcof(k,1,2,2,1) + 1 * rcof(k,0,2,2,1)
    rcof(k,2,3,2,0) = PCx(k) * rcof(k,1,3,2,1) + 1 * rcof(k,0,3,2,1)
  end do

end subroutine

pure subroutine r2_2_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,0,7) = (-two*p(k))**7 * Fn(7,k)
    rcof(k,0,0,1,6) = PCz(k) * rcof(k,0,0,0,7)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,6) = PCz(k) * rcof(k,0,0,1,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,0,3,6) = PCz(k) * rcof(k,0,0,2,7) + 2 * rcof(k,0,0,1,7)
    rcof(k,0,1,0,6) = PCy(k) * rcof(k,0,0,0,7)
    rcof(k,0,1,1,6) = PCy(k) * rcof(k,0,0,1,7)
    rcof(k,0,1,2,6) = PCy(k) * rcof(k,0,0,2,7)
    rcof(k,0,1,3,6) = PCy(k) * rcof(k,0,0,3,7)
    rcof(k,0,2,0,6) = PCy(k) * rcof(k,0,1,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,2,1,6) = PCy(k) * rcof(k,0,1,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,0,2,2,6) = PCy(k) * rcof(k,0,1,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,0,2,3,6) = PCy(k) * rcof(k,0,1,3,7) + 1 * rcof(k,0,0,3,7)
    rcof(k,1,0,0,6) = PCx(k) * rcof(k,0,0,0,7)
    rcof(k,1,1,0,6) = PCx(k) * rcof(k,0,1,0,7)
    rcof(k,1,2,0,6) = PCx(k) * rcof(k,0,2,0,7)
    rcof(k,1,0,1,6) = PCx(k) * rcof(k,0,0,1,7)
    rcof(k,1,1,1,6) = PCx(k) * rcof(k,0,1,1,7)
    rcof(k,1,2,1,6) = PCx(k) * rcof(k,0,2,1,7)
    rcof(k,1,0,2,6) = PCx(k) * rcof(k,0,0,2,7)
    rcof(k,1,1,2,6) = PCx(k) * rcof(k,0,1,2,7)
    rcof(k,1,2,2,6) = PCx(k) * rcof(k,0,2,2,7)
    rcof(k,1,0,3,6) = PCx(k) * rcof(k,0,0,3,7)
    rcof(k,1,1,3,6) = PCx(k) * rcof(k,0,1,3,7)
    rcof(k,1,2,3,6) = PCx(k) * rcof(k,0,2,3,7)
    rcof(k,2,0,0,6) = PCx(k) * rcof(k,1,0,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,2,1,0,6) = PCx(k) * rcof(k,1,1,0,7) + 1 * rcof(k,0,1,0,7)
    rcof(k,2,2,0,6) = PCx(k) * rcof(k,1,2,0,7) + 1 * rcof(k,0,2,0,7)
    rcof(k,2,0,1,6) = PCx(k) * rcof(k,1,0,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,2,1,1,6) = PCx(k) * rcof(k,1,1,1,7) + 1 * rcof(k,0,1,1,7)
    rcof(k,2,2,1,6) = PCx(k) * rcof(k,1,2,1,7) + 1 * rcof(k,0,2,1,7)
    rcof(k,2,0,2,6) = PCx(k) * rcof(k,1,0,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,2,1,2,6) = PCx(k) * rcof(k,1,1,2,7) + 1 * rcof(k,0,1,2,7)
    rcof(k,2,2,2,6) = PCx(k) * rcof(k,1,2,2,7) + 1 * rcof(k,0,2,2,7)
    rcof(k,2,0,3,6) = PCx(k) * rcof(k,1,0,3,7) + 1 * rcof(k,0,0,3,7)
    rcof(k,2,1,3,6) = PCx(k) * rcof(k,1,1,3,7) + 1 * rcof(k,0,1,3,7)
    rcof(k,2,2,3,6) = PCx(k) * rcof(k,1,2,3,7) + 1 * rcof(k,0,2,3,7)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,0,3,5) = PCz(k) * rcof(k,0,0,2,6) + 2 * rcof(k,0,0,1,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,0,1,3,5) = PCy(k) * rcof(k,0,0,3,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,0,2,2,5) = PCy(k) * rcof(k,0,1,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,0,2,3,5) = PCy(k) * rcof(k,0,1,3,6) + 1 * rcof(k,0,0,3,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,2,1,5) = PCx(k) * rcof(k,0,2,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,1,2,5) = PCx(k) * rcof(k,0,1,2,6)
    rcof(k,1,2,2,5) = PCx(k) * rcof(k,0,2,2,6)
    rcof(k,1,0,3,5) = PCx(k) * rcof(k,0,0,3,6)
    rcof(k,1,1,3,5) = PCx(k) * rcof(k,0,1,3,6)
    rcof(k,1,2,3,5) = PCx(k) * rcof(k,0,2,3,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,2,2,0,5) = PCx(k) * rcof(k,1,2,0,6) + 1 * rcof(k,0,2,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,2,1,1,5) = PCx(k) * rcof(k,1,1,1,6) + 1 * rcof(k,0,1,1,6)
    rcof(k,2,2,1,5) = PCx(k) * rcof(k,1,2,1,6) + 1 * rcof(k,0,2,1,6)
    rcof(k,2,0,2,5) = PCx(k) * rcof(k,1,0,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,2,1,2,5) = PCx(k) * rcof(k,1,1,2,6) + 1 * rcof(k,0,1,2,6)
    rcof(k,2,2,2,5) = PCx(k) * rcof(k,1,2,2,6) + 1 * rcof(k,0,2,2,6)
    rcof(k,2,0,3,5) = PCx(k) * rcof(k,1,0,3,6) + 1 * rcof(k,0,0,3,6)
    rcof(k,2,1,3,5) = PCx(k) * rcof(k,1,1,3,6) + 1 * rcof(k,0,1,3,6)
    rcof(k,2,2,3,5) = PCx(k) * rcof(k,1,2,3,6) + 1 * rcof(k,0,2,3,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,0,3,4) = PCz(k) * rcof(k,0,0,2,5) + 2 * rcof(k,0,0,1,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,1,3,4) = PCy(k) * rcof(k,0,0,3,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,0,2,3,4) = PCy(k) * rcof(k,0,1,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,2,2,4) = PCx(k) * rcof(k,0,2,2,5)
    rcof(k,1,0,3,4) = PCx(k) * rcof(k,0,0,3,5)
    rcof(k,1,1,3,4) = PCx(k) * rcof(k,0,1,3,5)
    rcof(k,1,2,3,4) = PCx(k) * rcof(k,0,2,3,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) + 1 * rcof(k,0,2,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,2,2,1,4) = PCx(k) * rcof(k,1,2,1,5) + 1 * rcof(k,0,2,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,2,1,2,4) = PCx(k) * rcof(k,1,1,2,5) + 1 * rcof(k,0,1,2,5)
    rcof(k,2,2,2,4) = PCx(k) * rcof(k,1,2,2,5) + 1 * rcof(k,0,2,2,5)
    rcof(k,2,0,3,4) = PCx(k) * rcof(k,1,0,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,2,1,3,4) = PCx(k) * rcof(k,1,1,3,5) + 1 * rcof(k,0,1,3,5)
    rcof(k,2,2,3,4) = PCx(k) * rcof(k,1,2,3,5) + 1 * rcof(k,0,2,3,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,1,3,3) = PCy(k) * rcof(k,0,0,3,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,0,2,3,3) = PCy(k) * rcof(k,0,1,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,2,2,3) = PCx(k) * rcof(k,0,2,2,4)
    rcof(k,1,0,3,3) = PCx(k) * rcof(k,0,0,3,4)
    rcof(k,1,1,3,3) = PCx(k) * rcof(k,0,1,3,4)
    rcof(k,1,2,3,3) = PCx(k) * rcof(k,0,2,3,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) + 1 * rcof(k,0,2,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,2,2,1,3) = PCx(k) * rcof(k,1,2,1,4) + 1 * rcof(k,0,2,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,2,1,2,3) = PCx(k) * rcof(k,1,1,2,4) + 1 * rcof(k,0,1,2,4)
    rcof(k,2,2,2,3) = PCx(k) * rcof(k,1,2,2,4) + 1 * rcof(k,0,2,2,4)
    rcof(k,2,0,3,3) = PCx(k) * rcof(k,1,0,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,2,1,3,3) = PCx(k) * rcof(k,1,1,3,4) + 1 * rcof(k,0,1,3,4)
    rcof(k,2,2,3,3) = PCx(k) * rcof(k,1,2,3,4) + 1 * rcof(k,0,2,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,1,3,2) = PCy(k) * rcof(k,0,0,3,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,0,2,3,2) = PCy(k) * rcof(k,0,1,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,2,2,2) = PCx(k) * rcof(k,0,2,2,3)
    rcof(k,1,0,3,2) = PCx(k) * rcof(k,0,0,3,3)
    rcof(k,1,1,3,2) = PCx(k) * rcof(k,0,1,3,3)
    rcof(k,1,2,3,2) = PCx(k) * rcof(k,0,2,3,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) + 1 * rcof(k,0,2,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,2,2,1,2) = PCx(k) * rcof(k,1,2,1,3) + 1 * rcof(k,0,2,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,2,1,2,2) = PCx(k) * rcof(k,1,1,2,3) + 1 * rcof(k,0,1,2,3)
    rcof(k,2,2,2,2) = PCx(k) * rcof(k,1,2,2,3) + 1 * rcof(k,0,2,2,3)
    rcof(k,2,0,3,2) = PCx(k) * rcof(k,1,0,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,2,1,3,2) = PCx(k) * rcof(k,1,1,3,3) + 1 * rcof(k,0,1,3,3)
    rcof(k,2,2,3,2) = PCx(k) * rcof(k,1,2,3,3) + 1 * rcof(k,0,2,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,1,3,1) = PCy(k) * rcof(k,0,0,3,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,0,2,3,1) = PCy(k) * rcof(k,0,1,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,2,2,1) = PCx(k) * rcof(k,0,2,2,2)
    rcof(k,1,0,3,1) = PCx(k) * rcof(k,0,0,3,2)
    rcof(k,1,1,3,1) = PCx(k) * rcof(k,0,1,3,2)
    rcof(k,1,2,3,1) = PCx(k) * rcof(k,0,2,3,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) + 1 * rcof(k,0,2,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,2,2,1,1) = PCx(k) * rcof(k,1,2,1,2) + 1 * rcof(k,0,2,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,2,1,2,1) = PCx(k) * rcof(k,1,1,2,2) + 1 * rcof(k,0,1,2,2)
    rcof(k,2,2,2,1) = PCx(k) * rcof(k,1,2,2,2) + 1 * rcof(k,0,2,2,2)
    rcof(k,2,0,3,1) = PCx(k) * rcof(k,1,0,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,2,1,3,1) = PCx(k) * rcof(k,1,1,3,2) + 1 * rcof(k,0,1,3,2)
    rcof(k,2,2,3,1) = PCx(k) * rcof(k,1,2,3,2) + 1 * rcof(k,0,2,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,1,3,0) = PCy(k) * rcof(k,0,0,3,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,0,2,3,0) = PCy(k) * rcof(k,0,1,3,1) + 1 * rcof(k,0,0,3,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,2,2,0) = PCx(k) * rcof(k,0,2,2,1)
    rcof(k,1,0,3,0) = PCx(k) * rcof(k,0,0,3,1)
    rcof(k,1,1,3,0) = PCx(k) * rcof(k,0,1,3,1)
    rcof(k,1,2,3,0) = PCx(k) * rcof(k,0,2,3,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) + 1 * rcof(k,0,2,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,2,2,1,0) = PCx(k) * rcof(k,1,2,1,1) + 1 * rcof(k,0,2,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,2,1,2,0) = PCx(k) * rcof(k,1,1,2,1) + 1 * rcof(k,0,1,2,1)
    rcof(k,2,2,2,0) = PCx(k) * rcof(k,1,2,2,1) + 1 * rcof(k,0,2,2,1)
    rcof(k,2,0,3,0) = PCx(k) * rcof(k,1,0,3,1) + 1 * rcof(k,0,0,3,1)
    rcof(k,2,1,3,0) = PCx(k) * rcof(k,1,1,3,1) + 1 * rcof(k,0,1,3,1)
    rcof(k,2,2,3,0) = PCx(k) * rcof(k,1,2,3,1) + 1 * rcof(k,0,2,3,1)
  end do

end subroutine

pure subroutine r3_3_0(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,3,0,5) = PCy(k) * rcof(k,0,2,0,6) + 2 * rcof(k,0,1,0,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,3,0,5) = PCx(k) * rcof(k,0,3,0,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,3,0,0,5) = PCx(k) * rcof(k,2,0,0,6) + 2 * rcof(k,1,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,3,1,0,5) = PCx(k) * rcof(k,2,1,0,6) + 2 * rcof(k,1,1,0,6)
    rcof(k,2,2,0,5) = PCx(k) * rcof(k,1,2,0,6) + 1 * rcof(k,0,2,0,6)
    rcof(k,3,2,0,5) = PCx(k) * rcof(k,2,2,0,6) + 2 * rcof(k,1,2,0,6)
    rcof(k,2,3,0,5) = PCx(k) * rcof(k,1,3,0,6) + 1 * rcof(k,0,3,0,6)
    rcof(k,3,3,0,5) = PCx(k) * rcof(k,2,3,0,6) + 2 * rcof(k,1,3,0,6)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,3,0,4) = PCy(k) * rcof(k,0,2,0,5) + 2 * rcof(k,0,1,0,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,3,0,4) = PCx(k) * rcof(k,0,3,0,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,3,0,0,4) = PCx(k) * rcof(k,2,0,0,5) + 2 * rcof(k,1,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,3,1,0,4) = PCx(k) * rcof(k,2,1,0,5) + 2 * rcof(k,1,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) + 1 * rcof(k,0,2,0,5)
    rcof(k,3,2,0,4) = PCx(k) * rcof(k,2,2,0,5) + 2 * rcof(k,1,2,0,5)
    rcof(k,2,3,0,4) = PCx(k) * rcof(k,1,3,0,5) + 1 * rcof(k,0,3,0,5)
    rcof(k,3,3,0,4) = PCx(k) * rcof(k,2,3,0,5) + 2 * rcof(k,1,3,0,5)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,3,0,3) = PCx(k) * rcof(k,0,3,0,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,3,1,0,3) = PCx(k) * rcof(k,2,1,0,4) + 2 * rcof(k,1,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) + 1 * rcof(k,0,2,0,4)
    rcof(k,3,2,0,3) = PCx(k) * rcof(k,2,2,0,4) + 2 * rcof(k,1,2,0,4)
    rcof(k,2,3,0,3) = PCx(k) * rcof(k,1,3,0,4) + 1 * rcof(k,0,3,0,4)
    rcof(k,3,3,0,3) = PCx(k) * rcof(k,2,3,0,4) + 2 * rcof(k,1,3,0,4)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,3,0,2) = PCx(k) * rcof(k,0,3,0,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,3,1,0,2) = PCx(k) * rcof(k,2,1,0,3) + 2 * rcof(k,1,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) + 1 * rcof(k,0,2,0,3)
    rcof(k,3,2,0,2) = PCx(k) * rcof(k,2,2,0,3) + 2 * rcof(k,1,2,0,3)
    rcof(k,2,3,0,2) = PCx(k) * rcof(k,1,3,0,3) + 1 * rcof(k,0,3,0,3)
    rcof(k,3,3,0,2) = PCx(k) * rcof(k,2,3,0,3) + 2 * rcof(k,1,3,0,3)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,3,0,1) = PCx(k) * rcof(k,0,3,0,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,3,1,0,1) = PCx(k) * rcof(k,2,1,0,2) + 2 * rcof(k,1,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) + 1 * rcof(k,0,2,0,2)
    rcof(k,3,2,0,1) = PCx(k) * rcof(k,2,2,0,2) + 2 * rcof(k,1,2,0,2)
    rcof(k,2,3,0,1) = PCx(k) * rcof(k,1,3,0,2) + 1 * rcof(k,0,3,0,2)
    rcof(k,3,3,0,1) = PCx(k) * rcof(k,2,3,0,2) + 2 * rcof(k,1,3,0,2)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,3,0,0) = PCx(k) * rcof(k,0,3,0,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,3,1,0,0) = PCx(k) * rcof(k,2,1,0,1) + 2 * rcof(k,1,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) + 1 * rcof(k,0,2,0,1)
    rcof(k,3,2,0,0) = PCx(k) * rcof(k,2,2,0,1) + 2 * rcof(k,1,2,0,1)
    rcof(k,2,3,0,0) = PCx(k) * rcof(k,1,3,0,1) + 1 * rcof(k,0,3,0,1)
    rcof(k,3,3,0,0) = PCx(k) * rcof(k,2,3,0,1) + 2 * rcof(k,1,3,0,1)
  end do

end subroutine

pure subroutine r3_0_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,0,3,5) = PCz(k) * rcof(k,0,0,2,6) + 2 * rcof(k,0,0,1,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,0,3,5) = PCx(k) * rcof(k,0,0,3,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,3,0,0,5) = PCx(k) * rcof(k,2,0,0,6) + 2 * rcof(k,1,0,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,3,0,1,5) = PCx(k) * rcof(k,2,0,1,6) + 2 * rcof(k,1,0,1,6)
    rcof(k,2,0,2,5) = PCx(k) * rcof(k,1,0,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,3,0,2,5) = PCx(k) * rcof(k,2,0,2,6) + 2 * rcof(k,1,0,2,6)
    rcof(k,2,0,3,5) = PCx(k) * rcof(k,1,0,3,6) + 1 * rcof(k,0,0,3,6)
    rcof(k,3,0,3,5) = PCx(k) * rcof(k,2,0,3,6) + 2 * rcof(k,1,0,3,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,0,3,4) = PCz(k) * rcof(k,0,0,2,5) + 2 * rcof(k,0,0,1,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,0,3,4) = PCx(k) * rcof(k,0,0,3,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,3,0,0,4) = PCx(k) * rcof(k,2,0,0,5) + 2 * rcof(k,1,0,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,3,0,1,4) = PCx(k) * rcof(k,2,0,1,5) + 2 * rcof(k,1,0,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,3,0,2,4) = PCx(k) * rcof(k,2,0,2,5) + 2 * rcof(k,1,0,2,5)
    rcof(k,2,0,3,4) = PCx(k) * rcof(k,1,0,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,3,0,3,4) = PCx(k) * rcof(k,2,0,3,5) + 2 * rcof(k,1,0,3,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,0,3,3) = PCx(k) * rcof(k,0,0,3,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,3,0,1,3) = PCx(k) * rcof(k,2,0,1,4) + 2 * rcof(k,1,0,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,3,0,2,3) = PCx(k) * rcof(k,2,0,2,4) + 2 * rcof(k,1,0,2,4)
    rcof(k,2,0,3,3) = PCx(k) * rcof(k,1,0,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,3,0,3,3) = PCx(k) * rcof(k,2,0,3,4) + 2 * rcof(k,1,0,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,0,3,2) = PCx(k) * rcof(k,0,0,3,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,3,0,1,2) = PCx(k) * rcof(k,2,0,1,3) + 2 * rcof(k,1,0,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,3,0,2,2) = PCx(k) * rcof(k,2,0,2,3) + 2 * rcof(k,1,0,2,3)
    rcof(k,2,0,3,2) = PCx(k) * rcof(k,1,0,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,3,0,3,2) = PCx(k) * rcof(k,2,0,3,3) + 2 * rcof(k,1,0,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,0,3,1) = PCx(k) * rcof(k,0,0,3,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,3,0,1,1) = PCx(k) * rcof(k,2,0,1,2) + 2 * rcof(k,1,0,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,3,0,2,1) = PCx(k) * rcof(k,2,0,2,2) + 2 * rcof(k,1,0,2,2)
    rcof(k,2,0,3,1) = PCx(k) * rcof(k,1,0,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,3,0,3,1) = PCx(k) * rcof(k,2,0,3,2) + 2 * rcof(k,1,0,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,0,3,0) = PCx(k) * rcof(k,0,0,3,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,3,0,1,0) = PCx(k) * rcof(k,2,0,1,1) + 2 * rcof(k,1,0,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,3,0,2,0) = PCx(k) * rcof(k,2,0,2,1) + 2 * rcof(k,1,0,2,1)
    rcof(k,2,0,3,0) = PCx(k) * rcof(k,1,0,3,1) + 1 * rcof(k,0,0,3,1)
    rcof(k,3,0,3,0) = PCx(k) * rcof(k,2,0,3,1) + 2 * rcof(k,1,0,3,1)
  end do

end subroutine

pure subroutine r0_3_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,0,3,5) = PCz(k) * rcof(k,0,0,2,6) + 2 * rcof(k,0,0,1,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,0,1,3,5) = PCy(k) * rcof(k,0,0,3,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,3,0,5) = PCy(k) * rcof(k,0,2,0,6) + 2 * rcof(k,0,1,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,0,3,1,5) = PCy(k) * rcof(k,0,2,1,6) + 2 * rcof(k,0,1,1,6)
    rcof(k,0,2,2,5) = PCy(k) * rcof(k,0,1,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,0,3,2,5) = PCy(k) * rcof(k,0,2,2,6) + 2 * rcof(k,0,1,2,6)
    rcof(k,0,2,3,5) = PCy(k) * rcof(k,0,1,3,6) + 1 * rcof(k,0,0,3,6)
    rcof(k,0,3,3,5) = PCy(k) * rcof(k,0,2,3,6) + 2 * rcof(k,0,1,3,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,0,3,4) = PCz(k) * rcof(k,0,0,2,5) + 2 * rcof(k,0,0,1,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,1,3,4) = PCy(k) * rcof(k,0,0,3,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,3,0,4) = PCy(k) * rcof(k,0,2,0,5) + 2 * rcof(k,0,1,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,3,1,4) = PCy(k) * rcof(k,0,2,1,5) + 2 * rcof(k,0,1,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,0,3,2,4) = PCy(k) * rcof(k,0,2,2,5) + 2 * rcof(k,0,1,2,5)
    rcof(k,0,2,3,4) = PCy(k) * rcof(k,0,1,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,0,3,3,4) = PCy(k) * rcof(k,0,2,3,5) + 2 * rcof(k,0,1,3,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,1,3,3) = PCy(k) * rcof(k,0,0,3,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,3,1,3) = PCy(k) * rcof(k,0,2,1,4) + 2 * rcof(k,0,1,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,0,3,2,3) = PCy(k) * rcof(k,0,2,2,4) + 2 * rcof(k,0,1,2,4)
    rcof(k,0,2,3,3) = PCy(k) * rcof(k,0,1,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,0,3,3,3) = PCy(k) * rcof(k,0,2,3,4) + 2 * rcof(k,0,1,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,1,3,2) = PCy(k) * rcof(k,0,0,3,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,3,1,2) = PCy(k) * rcof(k,0,2,1,3) + 2 * rcof(k,0,1,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,0,3,2,2) = PCy(k) * rcof(k,0,2,2,3) + 2 * rcof(k,0,1,2,3)
    rcof(k,0,2,3,2) = PCy(k) * rcof(k,0,1,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,0,3,3,2) = PCy(k) * rcof(k,0,2,3,3) + 2 * rcof(k,0,1,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,1,3,1) = PCy(k) * rcof(k,0,0,3,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,3,1,1) = PCy(k) * rcof(k,0,2,1,2) + 2 * rcof(k,0,1,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,0,3,2,1) = PCy(k) * rcof(k,0,2,2,2) + 2 * rcof(k,0,1,2,2)
    rcof(k,0,2,3,1) = PCy(k) * rcof(k,0,1,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,0,3,3,1) = PCy(k) * rcof(k,0,2,3,2) + 2 * rcof(k,0,1,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,1,3,0) = PCy(k) * rcof(k,0,0,3,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,3,1,0) = PCy(k) * rcof(k,0,2,1,1) + 2 * rcof(k,0,1,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,0,3,2,0) = PCy(k) * rcof(k,0,2,2,1) + 2 * rcof(k,0,1,2,1)
    rcof(k,0,2,3,0) = PCy(k) * rcof(k,0,1,3,1) + 1 * rcof(k,0,0,3,1)
    rcof(k,0,3,3,0) = PCy(k) * rcof(k,0,2,3,1) + 2 * rcof(k,0,1,3,1)
  end do

end subroutine

pure subroutine r3_3_1(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k)* p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k)* p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,0,7) = (-two*p(k))**7 * Fn(7,k)
    rcof(k,0,0,1,6) = PCz(k) * rcof(k,0,0,0,7)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,0,6) = PCy(k) * rcof(k,0,0,0,7)
    rcof(k,0,1,1,6) = PCy(k) * rcof(k,0,0,1,7)
    rcof(k,0,2,0,6) = PCy(k) * rcof(k,0,1,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,3,0,6) = PCy(k) * rcof(k,0,2,0,7) + 2 * rcof(k,0,1,0,7)
    rcof(k,0,2,1,6) = PCy(k) * rcof(k,0,1,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,0,3,1,6) = PCy(k) * rcof(k,0,2,1,7) + 2 * rcof(k,0,1,1,7)
    rcof(k,1,0,0,6) = PCx(k) * rcof(k,0,0,0,7)
    rcof(k,1,1,0,6) = PCx(k) * rcof(k,0,1,0,7)
    rcof(k,1,2,0,6) = PCx(k) * rcof(k,0,2,0,7)
    rcof(k,1,3,0,6) = PCx(k) * rcof(k,0,3,0,7)
    rcof(k,1,0,1,6) = PCx(k) * rcof(k,0,0,1,7)
    rcof(k,1,1,1,6) = PCx(k) * rcof(k,0,1,1,7)
    rcof(k,1,2,1,6) = PCx(k) * rcof(k,0,2,1,7)
    rcof(k,1,3,1,6) = PCx(k) * rcof(k,0,3,1,7)
    rcof(k,2,0,0,6) = PCx(k) * rcof(k,1,0,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,3,0,0,6) = PCx(k) * rcof(k,2,0,0,7) + 2 * rcof(k,1,0,0,7)
    rcof(k,2,1,0,6) = PCx(k) * rcof(k,1,1,0,7) + 1 * rcof(k,0,1,0,7)
    rcof(k,3,1,0,6) = PCx(k) * rcof(k,2,1,0,7) + 2 * rcof(k,1,1,0,7)
    rcof(k,2,2,0,6) = PCx(k) * rcof(k,1,2,0,7) + 1 * rcof(k,0,2,0,7)
    rcof(k,3,2,0,6) = PCx(k) * rcof(k,2,2,0,7) + 2 * rcof(k,1,2,0,7)
    rcof(k,2,3,0,6) = PCx(k) * rcof(k,1,3,0,7) + 1 * rcof(k,0,3,0,7)
    rcof(k,3,3,0,6) = PCx(k) * rcof(k,2,3,0,7) + 2 * rcof(k,1,3,0,7)
    rcof(k,2,0,1,6) = PCx(k) * rcof(k,1,0,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,3,0,1,6) = PCx(k) * rcof(k,2,0,1,7) + 2 * rcof(k,1,0,1,7)
    rcof(k,2,1,1,6) = PCx(k) * rcof(k,1,1,1,7) + 1 * rcof(k,0,1,1,7)
    rcof(k,3,1,1,6) = PCx(k) * rcof(k,2,1,1,7) + 2 * rcof(k,1,1,1,7)
    rcof(k,2,2,1,6) = PCx(k) * rcof(k,1,2,1,7) + 1 * rcof(k,0,2,1,7)
    rcof(k,3,2,1,6) = PCx(k) * rcof(k,2,2,1,7) + 2 * rcof(k,1,2,1,7)
    rcof(k,2,3,1,6) = PCx(k) * rcof(k,1,3,1,7) + 1 * rcof(k,0,3,1,7)
    rcof(k,3,3,1,6) = PCx(k) * rcof(k,2,3,1,7) + 2 * rcof(k,1,3,1,7)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,3,0,5) = PCy(k) * rcof(k,0,2,0,6) + 2 * rcof(k,0,1,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,0,3,1,5) = PCy(k) * rcof(k,0,2,1,6) + 2 * rcof(k,0,1,1,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,3,0,5) = PCx(k) * rcof(k,0,3,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,2,1,5) = PCx(k) * rcof(k,0,2,1,6)
    rcof(k,1,3,1,5) = PCx(k) * rcof(k,0,3,1,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,3,0,0,5) = PCx(k) * rcof(k,2,0,0,6) + 2 * rcof(k,1,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,3,1,0,5) = PCx(k) * rcof(k,2,1,0,6) + 2 * rcof(k,1,1,0,6)
    rcof(k,2,2,0,5) = PCx(k) * rcof(k,1,2,0,6) + 1 * rcof(k,0,2,0,6)
    rcof(k,3,2,0,5) = PCx(k) * rcof(k,2,2,0,6) + 2 * rcof(k,1,2,0,6)
    rcof(k,2,3,0,5) = PCx(k) * rcof(k,1,3,0,6) + 1 * rcof(k,0,3,0,6)
    rcof(k,3,3,0,5) = PCx(k) * rcof(k,2,3,0,6) + 2 * rcof(k,1,3,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,3,0,1,5) = PCx(k) * rcof(k,2,0,1,6) + 2 * rcof(k,1,0,1,6)
    rcof(k,2,1,1,5) = PCx(k) * rcof(k,1,1,1,6) + 1 * rcof(k,0,1,1,6)
    rcof(k,3,1,1,5) = PCx(k) * rcof(k,2,1,1,6) + 2 * rcof(k,1,1,1,6)
    rcof(k,2,2,1,5) = PCx(k) * rcof(k,1,2,1,6) + 1 * rcof(k,0,2,1,6)
    rcof(k,3,2,1,5) = PCx(k) * rcof(k,2,2,1,6) + 2 * rcof(k,1,2,1,6)
    rcof(k,2,3,1,5) = PCx(k) * rcof(k,1,3,1,6) + 1 * rcof(k,0,3,1,6)
    rcof(k,3,3,1,5) = PCx(k) * rcof(k,2,3,1,6) + 2 * rcof(k,1,3,1,6)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,3,0,4) = PCy(k) * rcof(k,0,2,0,5) + 2 * rcof(k,0,1,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,3,1,4) = PCy(k) * rcof(k,0,2,1,5) + 2 * rcof(k,0,1,1,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,3,0,4) = PCx(k) * rcof(k,0,3,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,3,1,4) = PCx(k) * rcof(k,0,3,1,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,3,0,0,4) = PCx(k) * rcof(k,2,0,0,5) + 2 * rcof(k,1,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,3,1,0,4) = PCx(k) * rcof(k,2,1,0,5) + 2 * rcof(k,1,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) + 1 * rcof(k,0,2,0,5)
    rcof(k,3,2,0,4) = PCx(k) * rcof(k,2,2,0,5) + 2 * rcof(k,1,2,0,5)
    rcof(k,2,3,0,4) = PCx(k) * rcof(k,1,3,0,5) + 1 * rcof(k,0,3,0,5)
    rcof(k,3,3,0,4) = PCx(k) * rcof(k,2,3,0,5) + 2 * rcof(k,1,3,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,3,0,1,4) = PCx(k) * rcof(k,2,0,1,5) + 2 * rcof(k,1,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,3,1,1,4) = PCx(k) * rcof(k,2,1,1,5) + 2 * rcof(k,1,1,1,5)
    rcof(k,2,2,1,4) = PCx(k) * rcof(k,1,2,1,5) + 1 * rcof(k,0,2,1,5)
    rcof(k,3,2,1,4) = PCx(k) * rcof(k,2,2,1,5) + 2 * rcof(k,1,2,1,5)
    rcof(k,2,3,1,4) = PCx(k) * rcof(k,1,3,1,5) + 1 * rcof(k,0,3,1,5)
    rcof(k,3,3,1,4) = PCx(k) * rcof(k,2,3,1,5) + 2 * rcof(k,1,3,1,5)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,3,1,3) = PCy(k) * rcof(k,0,2,1,4) + 2 * rcof(k,0,1,1,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,3,0,3) = PCx(k) * rcof(k,0,3,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,3,1,3) = PCx(k) * rcof(k,0,3,1,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,3,1,0,3) = PCx(k) * rcof(k,2,1,0,4) + 2 * rcof(k,1,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) + 1 * rcof(k,0,2,0,4)
    rcof(k,3,2,0,3) = PCx(k) * rcof(k,2,2,0,4) + 2 * rcof(k,1,2,0,4)
    rcof(k,2,3,0,3) = PCx(k) * rcof(k,1,3,0,4) + 1 * rcof(k,0,3,0,4)
    rcof(k,3,3,0,3) = PCx(k) * rcof(k,2,3,0,4) + 2 * rcof(k,1,3,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,3,0,1,3) = PCx(k) * rcof(k,2,0,1,4) + 2 * rcof(k,1,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,3,1,1,3) = PCx(k) * rcof(k,2,1,1,4) + 2 * rcof(k,1,1,1,4)
    rcof(k,2,2,1,3) = PCx(k) * rcof(k,1,2,1,4) + 1 * rcof(k,0,2,1,4)
    rcof(k,3,2,1,3) = PCx(k) * rcof(k,2,2,1,4) + 2 * rcof(k,1,2,1,4)
    rcof(k,2,3,1,3) = PCx(k) * rcof(k,1,3,1,4) + 1 * rcof(k,0,3,1,4)
    rcof(k,3,3,1,3) = PCx(k) * rcof(k,2,3,1,4) + 2 * rcof(k,1,3,1,4)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,3,1,2) = PCy(k) * rcof(k,0,2,1,3) + 2 * rcof(k,0,1,1,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,3,0,2) = PCx(k) * rcof(k,0,3,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,3,1,2) = PCx(k) * rcof(k,0,3,1,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,3,1,0,2) = PCx(k) * rcof(k,2,1,0,3) + 2 * rcof(k,1,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) + 1 * rcof(k,0,2,0,3)
    rcof(k,3,2,0,2) = PCx(k) * rcof(k,2,2,0,3) + 2 * rcof(k,1,2,0,3)
    rcof(k,2,3,0,2) = PCx(k) * rcof(k,1,3,0,3) + 1 * rcof(k,0,3,0,3)
    rcof(k,3,3,0,2) = PCx(k) * rcof(k,2,3,0,3) + 2 * rcof(k,1,3,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,3,0,1,2) = PCx(k) * rcof(k,2,0,1,3) + 2 * rcof(k,1,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,3,1,1,2) = PCx(k) * rcof(k,2,1,1,3) + 2 * rcof(k,1,1,1,3)
    rcof(k,2,2,1,2) = PCx(k) * rcof(k,1,2,1,3) + 1 * rcof(k,0,2,1,3)
    rcof(k,3,2,1,2) = PCx(k) * rcof(k,2,2,1,3) + 2 * rcof(k,1,2,1,3)
    rcof(k,2,3,1,2) = PCx(k) * rcof(k,1,3,1,3) + 1 * rcof(k,0,3,1,3)
    rcof(k,3,3,1,2) = PCx(k) * rcof(k,2,3,1,3) + 2 * rcof(k,1,3,1,3)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,3,1,1) = PCy(k) * rcof(k,0,2,1,2) + 2 * rcof(k,0,1,1,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,3,0,1) = PCx(k) * rcof(k,0,3,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,3,1,1) = PCx(k) * rcof(k,0,3,1,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,3,1,0,1) = PCx(k) * rcof(k,2,1,0,2) + 2 * rcof(k,1,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) + 1 * rcof(k,0,2,0,2)
    rcof(k,3,2,0,1) = PCx(k) * rcof(k,2,2,0,2) + 2 * rcof(k,1,2,0,2)
    rcof(k,2,3,0,1) = PCx(k) * rcof(k,1,3,0,2) + 1 * rcof(k,0,3,0,2)
    rcof(k,3,3,0,1) = PCx(k) * rcof(k,2,3,0,2) + 2 * rcof(k,1,3,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,3,0,1,1) = PCx(k) * rcof(k,2,0,1,2) + 2 * rcof(k,1,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,3,1,1,1) = PCx(k) * rcof(k,2,1,1,2) + 2 * rcof(k,1,1,1,2)
    rcof(k,2,2,1,1) = PCx(k) * rcof(k,1,2,1,2) + 1 * rcof(k,0,2,1,2)
    rcof(k,3,2,1,1) = PCx(k) * rcof(k,2,2,1,2) + 2 * rcof(k,1,2,1,2)
    rcof(k,2,3,1,1) = PCx(k) * rcof(k,1,3,1,2) + 1 * rcof(k,0,3,1,2)
    rcof(k,3,3,1,1) = PCx(k) * rcof(k,2,3,1,2) + 2 * rcof(k,1,3,1,2)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,3,1,0) = PCy(k) * rcof(k,0,2,1,1) + 2 * rcof(k,0,1,1,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,3,0,0) = PCx(k) * rcof(k,0,3,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,3,1,0) = PCx(k) * rcof(k,0,3,1,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,3,1,0,0) = PCx(k) * rcof(k,2,1,0,1) + 2 * rcof(k,1,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) + 1 * rcof(k,0,2,0,1)
    rcof(k,3,2,0,0) = PCx(k) * rcof(k,2,2,0,1) + 2 * rcof(k,1,2,0,1)
    rcof(k,2,3,0,0) = PCx(k) * rcof(k,1,3,0,1) + 1 * rcof(k,0,3,0,1)
    rcof(k,3,3,0,0) = PCx(k) * rcof(k,2,3,0,1) + 2 * rcof(k,1,3,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,3,0,1,0) = PCx(k) * rcof(k,2,0,1,1) + 2 * rcof(k,1,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,3,1,1,0) = PCx(k) * rcof(k,2,1,1,1) + 2 * rcof(k,1,1,1,1)
    rcof(k,2,2,1,0) = PCx(k) * rcof(k,1,2,1,1) + 1 * rcof(k,0,2,1,1)
    rcof(k,3,2,1,0) = PCx(k) * rcof(k,2,2,1,1) + 2 * rcof(k,1,2,1,1)
    rcof(k,2,3,1,0) = PCx(k) * rcof(k,1,3,1,1) + 1 * rcof(k,0,3,1,1)
    rcof(k,3,3,1,0) = PCx(k) * rcof(k,2,3,1,1) + 2 * rcof(k,1,3,1,1)
  end do

end subroutine

pure subroutine r3_1_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,0,7) = (-two*p(k))**7 * Fn(7,k)
    rcof(k,0,0,1,6) = PCz(k) * rcof(k,0,0,0,7)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,6) = PCz(k) * rcof(k,0,0,1,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,0,3,6) = PCz(k) * rcof(k,0,0,2,7) + 2 * rcof(k,0,0,1,7)
    rcof(k,0,1,0,6) = PCy(k) * rcof(k,0,0,0,7)
    rcof(k,0,1,1,6) = PCy(k) * rcof(k,0,0,1,7)
    rcof(k,0,1,2,6) = PCy(k) * rcof(k,0,0,2,7)
    rcof(k,0,1,3,6) = PCy(k) * rcof(k,0,0,3,7)
    rcof(k,1,0,0,6) = PCx(k) * rcof(k,0,0,0,7)
    rcof(k,1,1,0,6) = PCx(k) * rcof(k,0,1,0,7)
    rcof(k,1,0,1,6) = PCx(k) * rcof(k,0,0,1,7)
    rcof(k,1,1,1,6) = PCx(k) * rcof(k,0,1,1,7)
    rcof(k,1,0,2,6) = PCx(k) * rcof(k,0,0,2,7)
    rcof(k,1,1,2,6) = PCx(k) * rcof(k,0,1,2,7)
    rcof(k,1,0,3,6) = PCx(k) * rcof(k,0,0,3,7)
    rcof(k,1,1,3,6) = PCx(k) * rcof(k,0,1,3,7)
    rcof(k,2,0,0,6) = PCx(k) * rcof(k,1,0,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,3,0,0,6) = PCx(k) * rcof(k,2,0,0,7) + 2 * rcof(k,1,0,0,7)
    rcof(k,2,1,0,6) = PCx(k) * rcof(k,1,1,0,7) + 1 * rcof(k,0,1,0,7)
    rcof(k,3,1,0,6) = PCx(k) * rcof(k,2,1,0,7) + 2 * rcof(k,1,1,0,7)
    rcof(k,2,0,1,6) = PCx(k) * rcof(k,1,0,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,3,0,1,6) = PCx(k) * rcof(k,2,0,1,7) + 2 * rcof(k,1,0,1,7)
    rcof(k,2,1,1,6) = PCx(k) * rcof(k,1,1,1,7) + 1 * rcof(k,0,1,1,7)
    rcof(k,3,1,1,6) = PCx(k) * rcof(k,2,1,1,7) + 2 * rcof(k,1,1,1,7)
    rcof(k,2,0,2,6) = PCx(k) * rcof(k,1,0,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,3,0,2,6) = PCx(k) * rcof(k,2,0,2,7) + 2 * rcof(k,1,0,2,7)
    rcof(k,2,1,2,6) = PCx(k) * rcof(k,1,1,2,7) + 1 * rcof(k,0,1,2,7)
    rcof(k,3,1,2,6) = PCx(k) * rcof(k,2,1,2,7) + 2 * rcof(k,1,1,2,7)
    rcof(k,2,0,3,6) = PCx(k) * rcof(k,1,0,3,7) + 1 * rcof(k,0,0,3,7)
    rcof(k,3,0,3,6) = PCx(k) * rcof(k,2,0,3,7) + 2 * rcof(k,1,0,3,7)
    rcof(k,2,1,3,6) = PCx(k) * rcof(k,1,1,3,7) + 1 * rcof(k,0,1,3,7)
    rcof(k,3,1,3,6) = PCx(k) * rcof(k,2,1,3,7) + 2 * rcof(k,1,1,3,7)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,0,3,5) = PCz(k) * rcof(k,0,0,2,6) + 2 * rcof(k,0,0,1,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,0,1,3,5) = PCy(k) * rcof(k,0,0,3,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,1,2,5) = PCx(k) * rcof(k,0,1,2,6)
    rcof(k,1,0,3,5) = PCx(k) * rcof(k,0,0,3,6)
    rcof(k,1,1,3,5) = PCx(k) * rcof(k,0,1,3,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,3,0,0,5) = PCx(k) * rcof(k,2,0,0,6) + 2 * rcof(k,1,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,3,1,0,5) = PCx(k) * rcof(k,2,1,0,6) + 2 * rcof(k,1,1,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,3,0,1,5) = PCx(k) * rcof(k,2,0,1,6) + 2 * rcof(k,1,0,1,6)
    rcof(k,2,1,1,5) = PCx(k) * rcof(k,1,1,1,6) + 1 * rcof(k,0,1,1,6)
    rcof(k,3,1,1,5) = PCx(k) * rcof(k,2,1,1,6) + 2 * rcof(k,1,1,1,6)
    rcof(k,2,0,2,5) = PCx(k) * rcof(k,1,0,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,3,0,2,5) = PCx(k) * rcof(k,2,0,2,6) + 2 * rcof(k,1,0,2,6)
    rcof(k,2,1,2,5) = PCx(k) * rcof(k,1,1,2,6) + 1 * rcof(k,0,1,2,6)
    rcof(k,3,1,2,5) = PCx(k) * rcof(k,2,1,2,6) + 2 * rcof(k,1,1,2,6)
    rcof(k,2,0,3,5) = PCx(k) * rcof(k,1,0,3,6) + 1 * rcof(k,0,0,3,6)
    rcof(k,3,0,3,5) = PCx(k) * rcof(k,2,0,3,6) + 2 * rcof(k,1,0,3,6)
    rcof(k,2,1,3,5) = PCx(k) * rcof(k,1,1,3,6) + 1 * rcof(k,0,1,3,6)
    rcof(k,3,1,3,5) = PCx(k) * rcof(k,2,1,3,6) + 2 * rcof(k,1,1,3,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,0,3,4) = PCz(k) * rcof(k,0,0,2,5) + 2 * rcof(k,0,0,1,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,1,3,4) = PCy(k) * rcof(k,0,0,3,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,0,3,4) = PCx(k) * rcof(k,0,0,3,5)
    rcof(k,1,1,3,4) = PCx(k) * rcof(k,0,1,3,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,3,0,0,4) = PCx(k) * rcof(k,2,0,0,5) + 2 * rcof(k,1,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,3,1,0,4) = PCx(k) * rcof(k,2,1,0,5) + 2 * rcof(k,1,1,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,3,0,1,4) = PCx(k) * rcof(k,2,0,1,5) + 2 * rcof(k,1,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,3,1,1,4) = PCx(k) * rcof(k,2,1,1,5) + 2 * rcof(k,1,1,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,3,0,2,4) = PCx(k) * rcof(k,2,0,2,5) + 2 * rcof(k,1,0,2,5)
    rcof(k,2,1,2,4) = PCx(k) * rcof(k,1,1,2,5) + 1 * rcof(k,0,1,2,5)
    rcof(k,3,1,2,4) = PCx(k) * rcof(k,2,1,2,5) + 2 * rcof(k,1,1,2,5)
    rcof(k,2,0,3,4) = PCx(k) * rcof(k,1,0,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,3,0,3,4) = PCx(k) * rcof(k,2,0,3,5) + 2 * rcof(k,1,0,3,5)
    rcof(k,2,1,3,4) = PCx(k) * rcof(k,1,1,3,5) + 1 * rcof(k,0,1,3,5)
    rcof(k,3,1,3,4) = PCx(k) * rcof(k,2,1,3,5) + 2 * rcof(k,1,1,3,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,1,3,3) = PCy(k) * rcof(k,0,0,3,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,0,3,3) = PCx(k) * rcof(k,0,0,3,4)
    rcof(k,1,1,3,3) = PCx(k) * rcof(k,0,1,3,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,3,1,0,3) = PCx(k) * rcof(k,2,1,0,4) + 2 * rcof(k,1,1,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,3,0,1,3) = PCx(k) * rcof(k,2,0,1,4) + 2 * rcof(k,1,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,3,1,1,3) = PCx(k) * rcof(k,2,1,1,4) + 2 * rcof(k,1,1,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,3,0,2,3) = PCx(k) * rcof(k,2,0,2,4) + 2 * rcof(k,1,0,2,4)
    rcof(k,2,1,2,3) = PCx(k) * rcof(k,1,1,2,4) + 1 * rcof(k,0,1,2,4)
    rcof(k,3,1,2,3) = PCx(k) * rcof(k,2,1,2,4) + 2 * rcof(k,1,1,2,4)
    rcof(k,2,0,3,3) = PCx(k) * rcof(k,1,0,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,3,0,3,3) = PCx(k) * rcof(k,2,0,3,4) + 2 * rcof(k,1,0,3,4)
    rcof(k,2,1,3,3) = PCx(k) * rcof(k,1,1,3,4) + 1 * rcof(k,0,1,3,4)
    rcof(k,3,1,3,3) = PCx(k) * rcof(k,2,1,3,4) + 2 * rcof(k,1,1,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,1,3,2) = PCy(k) * rcof(k,0,0,3,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,0,3,2) = PCx(k) * rcof(k,0,0,3,3)
    rcof(k,1,1,3,2) = PCx(k) * rcof(k,0,1,3,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,3,1,0,2) = PCx(k) * rcof(k,2,1,0,3) + 2 * rcof(k,1,1,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,3,0,1,2) = PCx(k) * rcof(k,2,0,1,3) + 2 * rcof(k,1,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,3,1,1,2) = PCx(k) * rcof(k,2,1,1,3) + 2 * rcof(k,1,1,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,3,0,2,2) = PCx(k) * rcof(k,2,0,2,3) + 2 * rcof(k,1,0,2,3)
    rcof(k,2,1,2,2) = PCx(k) * rcof(k,1,1,2,3) + 1 * rcof(k,0,1,2,3)
    rcof(k,3,1,2,2) = PCx(k) * rcof(k,2,1,2,3) + 2 * rcof(k,1,1,2,3)
    rcof(k,2,0,3,2) = PCx(k) * rcof(k,1,0,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,3,0,3,2) = PCx(k) * rcof(k,2,0,3,3) + 2 * rcof(k,1,0,3,3)
    rcof(k,2,1,3,2) = PCx(k) * rcof(k,1,1,3,3) + 1 * rcof(k,0,1,3,3)
    rcof(k,3,1,3,2) = PCx(k) * rcof(k,2,1,3,3) + 2 * rcof(k,1,1,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,1,3,1) = PCy(k) * rcof(k,0,0,3,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,0,3,1) = PCx(k) * rcof(k,0,0,3,2)
    rcof(k,1,1,3,1) = PCx(k) * rcof(k,0,1,3,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,3,1,0,1) = PCx(k) * rcof(k,2,1,0,2) + 2 * rcof(k,1,1,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,3,0,1,1) = PCx(k) * rcof(k,2,0,1,2) + 2 * rcof(k,1,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,3,1,1,1) = PCx(k) * rcof(k,2,1,1,2) + 2 * rcof(k,1,1,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,3,0,2,1) = PCx(k) * rcof(k,2,0,2,2) + 2 * rcof(k,1,0,2,2)
    rcof(k,2,1,2,1) = PCx(k) * rcof(k,1,1,2,2) + 1 * rcof(k,0,1,2,2)
    rcof(k,3,1,2,1) = PCx(k) * rcof(k,2,1,2,2) + 2 * rcof(k,1,1,2,2)
    rcof(k,2,0,3,1) = PCx(k) * rcof(k,1,0,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,3,0,3,1) = PCx(k) * rcof(k,2,0,3,2) + 2 * rcof(k,1,0,3,2)
    rcof(k,2,1,3,1) = PCx(k) * rcof(k,1,1,3,2) + 1 * rcof(k,0,1,3,2)
    rcof(k,3,1,3,1) = PCx(k) * rcof(k,2,1,3,2) + 2 * rcof(k,1,1,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,1,3,0) = PCy(k) * rcof(k,0,0,3,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,0,3,0) = PCx(k) * rcof(k,0,0,3,1)
    rcof(k,1,1,3,0) = PCx(k) * rcof(k,0,1,3,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,3,1,0,0) = PCx(k) * rcof(k,2,1,0,1) + 2 * rcof(k,1,1,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,3,0,1,0) = PCx(k) * rcof(k,2,0,1,1) + 2 * rcof(k,1,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,3,1,1,0) = PCx(k) * rcof(k,2,1,1,1) + 2 * rcof(k,1,1,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,3,0,2,0) = PCx(k) * rcof(k,2,0,2,1) + 2 * rcof(k,1,0,2,1)
    rcof(k,2,1,2,0) = PCx(k) * rcof(k,1,1,2,1) + 1 * rcof(k,0,1,2,1)
    rcof(k,3,1,2,0) = PCx(k) * rcof(k,2,1,2,1) + 2 * rcof(k,1,1,2,1)
    rcof(k,2,0,3,0) = PCx(k) * rcof(k,1,0,3,1) + 1 * rcof(k,0,0,3,1)
    rcof(k,3,0,3,0) = PCx(k) * rcof(k,2,0,3,1) + 2 * rcof(k,1,0,3,1)
    rcof(k,2,1,3,0) = PCx(k) * rcof(k,1,1,3,1) + 1 * rcof(k,0,1,3,1)
    rcof(k,3,1,3,0) = PCx(k) * rcof(k,2,1,3,1) + 2 * rcof(k,1,1,3,1)
  end do

end subroutine

pure subroutine r1_3_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,0,7) = (-two*p(k))**7 * Fn(7,k)
    rcof(k,0,0,1,6) = PCz(k) * rcof(k,0,0,0,7)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,6) = PCz(k) * rcof(k,0,0,1,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,0,3,6) = PCz(k) * rcof(k,0,0,2,7) + 2 * rcof(k,0,0,1,7)
    rcof(k,0,1,0,6) = PCy(k) * rcof(k,0,0,0,7)
    rcof(k,0,1,1,6) = PCy(k) * rcof(k,0,0,1,7)
    rcof(k,0,1,2,6) = PCy(k) * rcof(k,0,0,2,7)
    rcof(k,0,1,3,6) = PCy(k) * rcof(k,0,0,3,7)
    rcof(k,0,2,0,6) = PCy(k) * rcof(k,0,1,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,3,0,6) = PCy(k) * rcof(k,0,2,0,7) + 2 * rcof(k,0,1,0,7)
    rcof(k,0,2,1,6) = PCy(k) * rcof(k,0,1,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,0,3,1,6) = PCy(k) * rcof(k,0,2,1,7) + 2 * rcof(k,0,1,1,7)
    rcof(k,0,2,2,6) = PCy(k) * rcof(k,0,1,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,0,3,2,6) = PCy(k) * rcof(k,0,2,2,7) + 2 * rcof(k,0,1,2,7)
    rcof(k,0,2,3,6) = PCy(k) * rcof(k,0,1,3,7) + 1 * rcof(k,0,0,3,7)
    rcof(k,0,3,3,6) = PCy(k) * rcof(k,0,2,3,7) + 2 * rcof(k,0,1,3,7)
    rcof(k,1,0,0,6) = PCx(k) * rcof(k,0,0,0,7)
    rcof(k,1,1,0,6) = PCx(k) * rcof(k,0,1,0,7)
    rcof(k,1,2,0,6) = PCx(k) * rcof(k,0,2,0,7)
    rcof(k,1,3,0,6) = PCx(k) * rcof(k,0,3,0,7)
    rcof(k,1,0,1,6) = PCx(k) * rcof(k,0,0,1,7)
    rcof(k,1,1,1,6) = PCx(k) * rcof(k,0,1,1,7)
    rcof(k,1,2,1,6) = PCx(k) * rcof(k,0,2,1,7)
    rcof(k,1,3,1,6) = PCx(k) * rcof(k,0,3,1,7)
    rcof(k,1,0,2,6) = PCx(k) * rcof(k,0,0,2,7)
    rcof(k,1,1,2,6) = PCx(k) * rcof(k,0,1,2,7)
    rcof(k,1,2,2,6) = PCx(k) * rcof(k,0,2,2,7)
    rcof(k,1,3,2,6) = PCx(k) * rcof(k,0,3,2,7)
    rcof(k,1,0,3,6) = PCx(k) * rcof(k,0,0,3,7)
    rcof(k,1,1,3,6) = PCx(k) * rcof(k,0,1,3,7)
    rcof(k,1,2,3,6) = PCx(k) * rcof(k,0,2,3,7)
    rcof(k,1,3,3,6) = PCx(k) * rcof(k,0,3,3,7)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,0,3,5) = PCz(k) * rcof(k,0,0,2,6) + 2 * rcof(k,0,0,1,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,0,1,3,5) = PCy(k) * rcof(k,0,0,3,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,3,0,5) = PCy(k) * rcof(k,0,2,0,6) + 2 * rcof(k,0,1,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,0,3,1,5) = PCy(k) * rcof(k,0,2,1,6) + 2 * rcof(k,0,1,1,6)
    rcof(k,0,2,2,5) = PCy(k) * rcof(k,0,1,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,0,3,2,5) = PCy(k) * rcof(k,0,2,2,6) + 2 * rcof(k,0,1,2,6)
    rcof(k,0,2,3,5) = PCy(k) * rcof(k,0,1,3,6) + 1 * rcof(k,0,0,3,6)
    rcof(k,0,3,3,5) = PCy(k) * rcof(k,0,2,3,6) + 2 * rcof(k,0,1,3,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,3,0,5) = PCx(k) * rcof(k,0,3,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,2,1,5) = PCx(k) * rcof(k,0,2,1,6)
    rcof(k,1,3,1,5) = PCx(k) * rcof(k,0,3,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,1,2,5) = PCx(k) * rcof(k,0,1,2,6)
    rcof(k,1,2,2,5) = PCx(k) * rcof(k,0,2,2,6)
    rcof(k,1,3,2,5) = PCx(k) * rcof(k,0,3,2,6)
    rcof(k,1,0,3,5) = PCx(k) * rcof(k,0,0,3,6)
    rcof(k,1,1,3,5) = PCx(k) * rcof(k,0,1,3,6)
    rcof(k,1,2,3,5) = PCx(k) * rcof(k,0,2,3,6)
    rcof(k,1,3,3,5) = PCx(k) * rcof(k,0,3,3,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,0,3,4) = PCz(k) * rcof(k,0,0,2,5) + 2 * rcof(k,0,0,1,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,1,3,4) = PCy(k) * rcof(k,0,0,3,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,3,0,4) = PCy(k) * rcof(k,0,2,0,5) + 2 * rcof(k,0,1,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,3,1,4) = PCy(k) * rcof(k,0,2,1,5) + 2 * rcof(k,0,1,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,0,3,2,4) = PCy(k) * rcof(k,0,2,2,5) + 2 * rcof(k,0,1,2,5)
    rcof(k,0,2,3,4) = PCy(k) * rcof(k,0,1,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,0,3,3,4) = PCy(k) * rcof(k,0,2,3,5) + 2 * rcof(k,0,1,3,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,3,0,4) = PCx(k) * rcof(k,0,3,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,3,1,4) = PCx(k) * rcof(k,0,3,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,2,2,4) = PCx(k) * rcof(k,0,2,2,5)
    rcof(k,1,3,2,4) = PCx(k) * rcof(k,0,3,2,5)
    rcof(k,1,0,3,4) = PCx(k) * rcof(k,0,0,3,5)
    rcof(k,1,1,3,4) = PCx(k) * rcof(k,0,1,3,5)
    rcof(k,1,2,3,4) = PCx(k) * rcof(k,0,2,3,5)
    rcof(k,1,3,3,4) = PCx(k) * rcof(k,0,3,3,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,1,3,3) = PCy(k) * rcof(k,0,0,3,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,3,1,3) = PCy(k) * rcof(k,0,2,1,4) + 2 * rcof(k,0,1,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,0,3,2,3) = PCy(k) * rcof(k,0,2,2,4) + 2 * rcof(k,0,1,2,4)
    rcof(k,0,2,3,3) = PCy(k) * rcof(k,0,1,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,0,3,3,3) = PCy(k) * rcof(k,0,2,3,4) + 2 * rcof(k,0,1,3,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,3,0,3) = PCx(k) * rcof(k,0,3,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,3,1,3) = PCx(k) * rcof(k,0,3,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,2,2,3) = PCx(k) * rcof(k,0,2,2,4)
    rcof(k,1,3,2,3) = PCx(k) * rcof(k,0,3,2,4)
    rcof(k,1,0,3,3) = PCx(k) * rcof(k,0,0,3,4)
    rcof(k,1,1,3,3) = PCx(k) * rcof(k,0,1,3,4)
    rcof(k,1,2,3,3) = PCx(k) * rcof(k,0,2,3,4)
    rcof(k,1,3,3,3) = PCx(k) * rcof(k,0,3,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,1,3,2) = PCy(k) * rcof(k,0,0,3,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,3,1,2) = PCy(k) * rcof(k,0,2,1,3) + 2 * rcof(k,0,1,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,0,3,2,2) = PCy(k) * rcof(k,0,2,2,3) + 2 * rcof(k,0,1,2,3)
    rcof(k,0,2,3,2) = PCy(k) * rcof(k,0,1,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,0,3,3,2) = PCy(k) * rcof(k,0,2,3,3) + 2 * rcof(k,0,1,3,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,3,0,2) = PCx(k) * rcof(k,0,3,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,3,1,2) = PCx(k) * rcof(k,0,3,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,2,2,2) = PCx(k) * rcof(k,0,2,2,3)
    rcof(k,1,3,2,2) = PCx(k) * rcof(k,0,3,2,3)
    rcof(k,1,0,3,2) = PCx(k) * rcof(k,0,0,3,3)
    rcof(k,1,1,3,2) = PCx(k) * rcof(k,0,1,3,3)
    rcof(k,1,2,3,2) = PCx(k) * rcof(k,0,2,3,3)
    rcof(k,1,3,3,2) = PCx(k) * rcof(k,0,3,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,1,3,1) = PCy(k) * rcof(k,0,0,3,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,3,1,1) = PCy(k) * rcof(k,0,2,1,2) + 2 * rcof(k,0,1,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,0,3,2,1) = PCy(k) * rcof(k,0,2,2,2) + 2 * rcof(k,0,1,2,2)
    rcof(k,0,2,3,1) = PCy(k) * rcof(k,0,1,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,0,3,3,1) = PCy(k) * rcof(k,0,2,3,2) + 2 * rcof(k,0,1,3,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,3,0,1) = PCx(k) * rcof(k,0,3,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,3,1,1) = PCx(k) * rcof(k,0,3,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,2,2,1) = PCx(k) * rcof(k,0,2,2,2)
    rcof(k,1,3,2,1) = PCx(k) * rcof(k,0,3,2,2)
    rcof(k,1,0,3,1) = PCx(k) * rcof(k,0,0,3,2)
    rcof(k,1,1,3,1) = PCx(k) * rcof(k,0,1,3,2)
    rcof(k,1,2,3,1) = PCx(k) * rcof(k,0,2,3,2)
    rcof(k,1,3,3,1) = PCx(k) * rcof(k,0,3,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,1,3,0) = PCy(k) * rcof(k,0,0,3,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,3,1,0) = PCy(k) * rcof(k,0,2,1,1) + 2 * rcof(k,0,1,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,0,3,2,0) = PCy(k) * rcof(k,0,2,2,1) + 2 * rcof(k,0,1,2,1)
    rcof(k,0,2,3,0) = PCy(k) * rcof(k,0,1,3,1) + 1 * rcof(k,0,0,3,1)
    rcof(k,0,3,3,0) = PCy(k) * rcof(k,0,2,3,1) + 2 * rcof(k,0,1,3,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,3,0,0) = PCx(k) * rcof(k,0,3,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,3,1,0) = PCx(k) * rcof(k,0,3,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,2,2,0) = PCx(k) * rcof(k,0,2,2,1)
    rcof(k,1,3,2,0) = PCx(k) * rcof(k,0,3,2,1)
    rcof(k,1,0,3,0) = PCx(k) * rcof(k,0,0,3,1)
    rcof(k,1,1,3,0) = PCx(k) * rcof(k,0,1,3,1)
    rcof(k,1,2,3,0) = PCx(k) * rcof(k,0,2,3,1)
    rcof(k,1,3,3,0) = PCx(k) * rcof(k,0,3,3,1)
  end do

end subroutine

pure subroutine r3_3_2(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,0,7) = (-two*p(k))**7 * Fn(7,k)
    rcof(k,0,0,0,8) = (-two*p(k))**8 * Fn(8,k)
    rcof(k,0,0,1,7) = PCz(k) * rcof(k,0,0,0,8)
    rcof(k,0,0,1,6) = PCz(k) * rcof(k,0,0,0,7)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,7) = PCz(k) * rcof(k,0,0,1,8) + 1 * rcof(k,0,0,0,8)
    rcof(k,0,1,0,7) = PCy(k) * rcof(k,0,0,0,8)
    rcof(k,0,1,1,7) = PCy(k) * rcof(k,0,0,1,8)
    rcof(k,0,1,2,7) = PCy(k) * rcof(k,0,0,2,8)
    rcof(k,0,2,0,7) = PCy(k) * rcof(k,0,1,0,8) + 1 * rcof(k,0,0,0,8)
    rcof(k,0,3,0,7) = PCy(k) * rcof(k,0,2,0,8) + 2 * rcof(k,0,1,0,8)
    rcof(k,0,2,1,7) = PCy(k) * rcof(k,0,1,1,8) + 1 * rcof(k,0,0,1,8)
    rcof(k,0,3,1,7) = PCy(k) * rcof(k,0,2,1,8) + 2 * rcof(k,0,1,1,8)
    rcof(k,0,2,2,7) = PCy(k) * rcof(k,0,1,2,8) + 1 * rcof(k,0,0,2,8)
    rcof(k,0,3,2,7) = PCy(k) * rcof(k,0,2,2,8) + 2 * rcof(k,0,1,2,8)
    rcof(k,1,0,0,7) = PCx(k) * rcof(k,0,0,0,8)
    rcof(k,1,1,0,7) = PCx(k) * rcof(k,0,1,0,8)
    rcof(k,1,2,0,7) = PCx(k) * rcof(k,0,2,0,8)
    rcof(k,1,3,0,7) = PCx(k) * rcof(k,0,3,0,8)
    rcof(k,1,0,1,7) = PCx(k) * rcof(k,0,0,1,8)
    rcof(k,1,1,1,7) = PCx(k) * rcof(k,0,1,1,8)
    rcof(k,1,2,1,7) = PCx(k) * rcof(k,0,2,1,8)
    rcof(k,1,3,1,7) = PCx(k) * rcof(k,0,3,1,8)
    rcof(k,1,0,2,7) = PCx(k) * rcof(k,0,0,2,8)
    rcof(k,1,1,2,7) = PCx(k) * rcof(k,0,1,2,8)
    rcof(k,1,2,2,7) = PCx(k) * rcof(k,0,2,2,8)
    rcof(k,1,3,2,7) = PCx(k) * rcof(k,0,3,2,8)
    rcof(k,2,0,0,7) = PCx(k) * rcof(k,1,0,0,8) + 1 * rcof(k,0,0,0,8)
    rcof(k,3,0,0,7) = PCx(k) * rcof(k,2,0,0,8) + 2 * rcof(k,1,0,0,8)
    rcof(k,2,1,0,7) = PCx(k) * rcof(k,1,1,0,8) + 1 * rcof(k,0,1,0,8)
    rcof(k,3,1,0,7) = PCx(k) * rcof(k,2,1,0,8) + 2 * rcof(k,1,1,0,8)
    rcof(k,2,2,0,7) = PCx(k) * rcof(k,1,2,0,8) + 1 * rcof(k,0,2,0,8)
    rcof(k,3,2,0,7) = PCx(k) * rcof(k,2,2,0,8) + 2 * rcof(k,1,2,0,8)
    rcof(k,2,3,0,7) = PCx(k) * rcof(k,1,3,0,8) + 1 * rcof(k,0,3,0,8)
    rcof(k,3,3,0,7) = PCx(k) * rcof(k,2,3,0,8) + 2 * rcof(k,1,3,0,8)
    rcof(k,2,0,1,7) = PCx(k) * rcof(k,1,0,1,8) + 1 * rcof(k,0,0,1,8)
    rcof(k,3,0,1,7) = PCx(k) * rcof(k,2,0,1,8) + 2 * rcof(k,1,0,1,8)
    rcof(k,2,1,1,7) = PCx(k) * rcof(k,1,1,1,8) + 1 * rcof(k,0,1,1,8)
    rcof(k,3,1,1,7) = PCx(k) * rcof(k,2,1,1,8) + 2 * rcof(k,1,1,1,8)
    rcof(k,2,2,1,7) = PCx(k) * rcof(k,1,2,1,8) + 1 * rcof(k,0,2,1,8)
    rcof(k,3,2,1,7) = PCx(k) * rcof(k,2,2,1,8) + 2 * rcof(k,1,2,1,8)
    rcof(k,2,3,1,7) = PCx(k) * rcof(k,1,3,1,8) + 1 * rcof(k,0,3,1,8)
    rcof(k,3,3,1,7) = PCx(k) * rcof(k,2,3,1,8) + 2 * rcof(k,1,3,1,8)
    rcof(k,2,0,2,7) = PCx(k) * rcof(k,1,0,2,8) + 1 * rcof(k,0,0,2,8)
    rcof(k,3,0,2,7) = PCx(k) * rcof(k,2,0,2,8) + 2 * rcof(k,1,0,2,8)
    rcof(k,2,1,2,7) = PCx(k) * rcof(k,1,1,2,8) + 1 * rcof(k,0,1,2,8)
    rcof(k,3,1,2,7) = PCx(k) * rcof(k,2,1,2,8) + 2 * rcof(k,1,1,2,8)
    rcof(k,2,2,2,7) = PCx(k) * rcof(k,1,2,2,8) + 1 * rcof(k,0,2,2,8)
    rcof(k,3,2,2,7) = PCx(k) * rcof(k,2,2,2,8) + 2 * rcof(k,1,2,2,8)
    rcof(k,2,3,2,7) = PCx(k) * rcof(k,1,3,2,8) + 1 * rcof(k,0,3,2,8)
    rcof(k,3,3,2,7) = PCx(k) * rcof(k,2,3,2,8) + 2 * rcof(k,1,3,2,8)
    rcof(k,0,0,2,6) = PCz(k) * rcof(k,0,0,1,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,1,0,6) = PCy(k) * rcof(k,0,0,0,7)
    rcof(k,0,1,1,6) = PCy(k) * rcof(k,0,0,1,7)
    rcof(k,0,1,2,6) = PCy(k) * rcof(k,0,0,2,7)
    rcof(k,0,2,0,6) = PCy(k) * rcof(k,0,1,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,3,0,6) = PCy(k) * rcof(k,0,2,0,7) + 2 * rcof(k,0,1,0,7)
    rcof(k,0,2,1,6) = PCy(k) * rcof(k,0,1,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,0,3,1,6) = PCy(k) * rcof(k,0,2,1,7) + 2 * rcof(k,0,1,1,7)
    rcof(k,0,2,2,6) = PCy(k) * rcof(k,0,1,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,0,3,2,6) = PCy(k) * rcof(k,0,2,2,7) + 2 * rcof(k,0,1,2,7)
    rcof(k,1,0,0,6) = PCx(k) * rcof(k,0,0,0,7)
    rcof(k,1,1,0,6) = PCx(k) * rcof(k,0,1,0,7)
    rcof(k,1,2,0,6) = PCx(k) * rcof(k,0,2,0,7)
    rcof(k,1,3,0,6) = PCx(k) * rcof(k,0,3,0,7)
    rcof(k,1,0,1,6) = PCx(k) * rcof(k,0,0,1,7)
    rcof(k,1,1,1,6) = PCx(k) * rcof(k,0,1,1,7)
    rcof(k,1,2,1,6) = PCx(k) * rcof(k,0,2,1,7)
    rcof(k,1,3,1,6) = PCx(k) * rcof(k,0,3,1,7)
    rcof(k,1,0,2,6) = PCx(k) * rcof(k,0,0,2,7)
    rcof(k,1,1,2,6) = PCx(k) * rcof(k,0,1,2,7)
    rcof(k,1,2,2,6) = PCx(k) * rcof(k,0,2,2,7)
    rcof(k,1,3,2,6) = PCx(k) * rcof(k,0,3,2,7)
    rcof(k,2,0,0,6) = PCx(k) * rcof(k,1,0,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,3,0,0,6) = PCx(k) * rcof(k,2,0,0,7) + 2 * rcof(k,1,0,0,7)
    rcof(k,2,1,0,6) = PCx(k) * rcof(k,1,1,0,7) + 1 * rcof(k,0,1,0,7)
    rcof(k,3,1,0,6) = PCx(k) * rcof(k,2,1,0,7) + 2 * rcof(k,1,1,0,7)
    rcof(k,2,2,0,6) = PCx(k) * rcof(k,1,2,0,7) + 1 * rcof(k,0,2,0,7)
    rcof(k,3,2,0,6) = PCx(k) * rcof(k,2,2,0,7) + 2 * rcof(k,1,2,0,7)
    rcof(k,2,3,0,6) = PCx(k) * rcof(k,1,3,0,7) + 1 * rcof(k,0,3,0,7)
    rcof(k,3,3,0,6) = PCx(k) * rcof(k,2,3,0,7) + 2 * rcof(k,1,3,0,7)
    rcof(k,2,0,1,6) = PCx(k) * rcof(k,1,0,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,3,0,1,6) = PCx(k) * rcof(k,2,0,1,7) + 2 * rcof(k,1,0,1,7)
    rcof(k,2,1,1,6) = PCx(k) * rcof(k,1,1,1,7) + 1 * rcof(k,0,1,1,7)
    rcof(k,3,1,1,6) = PCx(k) * rcof(k,2,1,1,7) + 2 * rcof(k,1,1,1,7)
    rcof(k,2,2,1,6) = PCx(k) * rcof(k,1,2,1,7) + 1 * rcof(k,0,2,1,7)
    rcof(k,3,2,1,6) = PCx(k) * rcof(k,2,2,1,7) + 2 * rcof(k,1,2,1,7)
    rcof(k,2,3,1,6) = PCx(k) * rcof(k,1,3,1,7) + 1 * rcof(k,0,3,1,7)
    rcof(k,3,3,1,6) = PCx(k) * rcof(k,2,3,1,7) + 2 * rcof(k,1,3,1,7)
    rcof(k,2,0,2,6) = PCx(k) * rcof(k,1,0,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,3,0,2,6) = PCx(k) * rcof(k,2,0,2,7) + 2 * rcof(k,1,0,2,7)
    rcof(k,2,1,2,6) = PCx(k) * rcof(k,1,1,2,7) + 1 * rcof(k,0,1,2,7)
    rcof(k,3,1,2,6) = PCx(k) * rcof(k,2,1,2,7) + 2 * rcof(k,1,1,2,7)
    rcof(k,2,2,2,6) = PCx(k) * rcof(k,1,2,2,7) + 1 * rcof(k,0,2,2,7)
    rcof(k,3,2,2,6) = PCx(k) * rcof(k,2,2,2,7) + 2 * rcof(k,1,2,2,7)
    rcof(k,2,3,2,6) = PCx(k) * rcof(k,1,3,2,7) + 1 * rcof(k,0,3,2,7)
    rcof(k,3,3,2,6) = PCx(k) * rcof(k,2,3,2,7) + 2 * rcof(k,1,3,2,7)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,3,0,5) = PCy(k) * rcof(k,0,2,0,6) + 2 * rcof(k,0,1,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,0,3,1,5) = PCy(k) * rcof(k,0,2,1,6) + 2 * rcof(k,0,1,1,6)
    rcof(k,0,2,2,5) = PCy(k) * rcof(k,0,1,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,0,3,2,5) = PCy(k) * rcof(k,0,2,2,6) + 2 * rcof(k,0,1,2,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,3,0,5) = PCx(k) * rcof(k,0,3,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,2,1,5) = PCx(k) * rcof(k,0,2,1,6)
    rcof(k,1,3,1,5) = PCx(k) * rcof(k,0,3,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,1,2,5) = PCx(k) * rcof(k,0,1,2,6)
    rcof(k,1,2,2,5) = PCx(k) * rcof(k,0,2,2,6)
    rcof(k,1,3,2,5) = PCx(k) * rcof(k,0,3,2,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,3,0,0,5) = PCx(k) * rcof(k,2,0,0,6) + 2 * rcof(k,1,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,3,1,0,5) = PCx(k) * rcof(k,2,1,0,6) + 2 * rcof(k,1,1,0,6)
    rcof(k,2,2,0,5) = PCx(k) * rcof(k,1,2,0,6) + 1 * rcof(k,0,2,0,6)
    rcof(k,3,2,0,5) = PCx(k) * rcof(k,2,2,0,6) + 2 * rcof(k,1,2,0,6)
    rcof(k,2,3,0,5) = PCx(k) * rcof(k,1,3,0,6) + 1 * rcof(k,0,3,0,6)
    rcof(k,3,3,0,5) = PCx(k) * rcof(k,2,3,0,6) + 2 * rcof(k,1,3,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,3,0,1,5) = PCx(k) * rcof(k,2,0,1,6) + 2 * rcof(k,1,0,1,6)
    rcof(k,2,1,1,5) = PCx(k) * rcof(k,1,1,1,6) + 1 * rcof(k,0,1,1,6)
    rcof(k,3,1,1,5) = PCx(k) * rcof(k,2,1,1,6) + 2 * rcof(k,1,1,1,6)
    rcof(k,2,2,1,5) = PCx(k) * rcof(k,1,2,1,6) + 1 * rcof(k,0,2,1,6)
    rcof(k,3,2,1,5) = PCx(k) * rcof(k,2,2,1,6) + 2 * rcof(k,1,2,1,6)
    rcof(k,2,3,1,5) = PCx(k) * rcof(k,1,3,1,6) + 1 * rcof(k,0,3,1,6)
    rcof(k,3,3,1,5) = PCx(k) * rcof(k,2,3,1,6) + 2 * rcof(k,1,3,1,6)
    rcof(k,2,0,2,5) = PCx(k) * rcof(k,1,0,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,3,0,2,5) = PCx(k) * rcof(k,2,0,2,6) + 2 * rcof(k,1,0,2,6)
    rcof(k,2,1,2,5) = PCx(k) * rcof(k,1,1,2,6) + 1 * rcof(k,0,1,2,6)
    rcof(k,3,1,2,5) = PCx(k) * rcof(k,2,1,2,6) + 2 * rcof(k,1,1,2,6)
    rcof(k,2,2,2,5) = PCx(k) * rcof(k,1,2,2,6) + 1 * rcof(k,0,2,2,6)
    rcof(k,3,2,2,5) = PCx(k) * rcof(k,2,2,2,6) + 2 * rcof(k,1,2,2,6)
    rcof(k,2,3,2,5) = PCx(k) * rcof(k,1,3,2,6) + 1 * rcof(k,0,3,2,6)
    rcof(k,3,3,2,5) = PCx(k) * rcof(k,2,3,2,6) + 2 * rcof(k,1,3,2,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,3,0,4) = PCy(k) * rcof(k,0,2,0,5) + 2 * rcof(k,0,1,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,3,1,4) = PCy(k) * rcof(k,0,2,1,5) + 2 * rcof(k,0,1,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,0,3,2,4) = PCy(k) * rcof(k,0,2,2,5) + 2 * rcof(k,0,1,2,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,3,0,4) = PCx(k) * rcof(k,0,3,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,3,1,4) = PCx(k) * rcof(k,0,3,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,2,2,4) = PCx(k) * rcof(k,0,2,2,5)
    rcof(k,1,3,2,4) = PCx(k) * rcof(k,0,3,2,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,3,0,0,4) = PCx(k) * rcof(k,2,0,0,5) + 2 * rcof(k,1,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,3,1,0,4) = PCx(k) * rcof(k,2,1,0,5) + 2 * rcof(k,1,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) + 1 * rcof(k,0,2,0,5)
    rcof(k,3,2,0,4) = PCx(k) * rcof(k,2,2,0,5) + 2 * rcof(k,1,2,0,5)
    rcof(k,2,3,0,4) = PCx(k) * rcof(k,1,3,0,5) + 1 * rcof(k,0,3,0,5)
    rcof(k,3,3,0,4) = PCx(k) * rcof(k,2,3,0,5) + 2 * rcof(k,1,3,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,3,0,1,4) = PCx(k) * rcof(k,2,0,1,5) + 2 * rcof(k,1,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,3,1,1,4) = PCx(k) * rcof(k,2,1,1,5) + 2 * rcof(k,1,1,1,5)
    rcof(k,2,2,1,4) = PCx(k) * rcof(k,1,2,1,5) + 1 * rcof(k,0,2,1,5)
    rcof(k,3,2,1,4) = PCx(k) * rcof(k,2,2,1,5) + 2 * rcof(k,1,2,1,5)
    rcof(k,2,3,1,4) = PCx(k) * rcof(k,1,3,1,5) + 1 * rcof(k,0,3,1,5)
    rcof(k,3,3,1,4) = PCx(k) * rcof(k,2,3,1,5) + 2 * rcof(k,1,3,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,3,0,2,4) = PCx(k) * rcof(k,2,0,2,5) + 2 * rcof(k,1,0,2,5)
    rcof(k,2,1,2,4) = PCx(k) * rcof(k,1,1,2,5) + 1 * rcof(k,0,1,2,5)
    rcof(k,3,1,2,4) = PCx(k) * rcof(k,2,1,2,5) + 2 * rcof(k,1,1,2,5)
    rcof(k,2,2,2,4) = PCx(k) * rcof(k,1,2,2,5) + 1 * rcof(k,0,2,2,5)
    rcof(k,3,2,2,4) = PCx(k) * rcof(k,2,2,2,5) + 2 * rcof(k,1,2,2,5)
    rcof(k,2,3,2,4) = PCx(k) * rcof(k,1,3,2,5) + 1 * rcof(k,0,3,2,5)
    rcof(k,3,3,2,4) = PCx(k) * rcof(k,2,3,2,5) + 2 * rcof(k,1,3,2,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,3,1,3) = PCy(k) * rcof(k,0,2,1,4) + 2 * rcof(k,0,1,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,0,3,2,3) = PCy(k) * rcof(k,0,2,2,4) + 2 * rcof(k,0,1,2,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,3,0,3) = PCx(k) * rcof(k,0,3,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,3,1,3) = PCx(k) * rcof(k,0,3,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,2,2,3) = PCx(k) * rcof(k,0,2,2,4)
    rcof(k,1,3,2,3) = PCx(k) * rcof(k,0,3,2,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,3,1,0,3) = PCx(k) * rcof(k,2,1,0,4) + 2 * rcof(k,1,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) + 1 * rcof(k,0,2,0,4)
    rcof(k,3,2,0,3) = PCx(k) * rcof(k,2,2,0,4) + 2 * rcof(k,1,2,0,4)
    rcof(k,2,3,0,3) = PCx(k) * rcof(k,1,3,0,4) + 1 * rcof(k,0,3,0,4)
    rcof(k,3,3,0,3) = PCx(k) * rcof(k,2,3,0,4) + 2 * rcof(k,1,3,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,3,0,1,3) = PCx(k) * rcof(k,2,0,1,4) + 2 * rcof(k,1,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,3,1,1,3) = PCx(k) * rcof(k,2,1,1,4) + 2 * rcof(k,1,1,1,4)
    rcof(k,2,2,1,3) = PCx(k) * rcof(k,1,2,1,4) + 1 * rcof(k,0,2,1,4)
    rcof(k,3,2,1,3) = PCx(k) * rcof(k,2,2,1,4) + 2 * rcof(k,1,2,1,4)
    rcof(k,2,3,1,3) = PCx(k) * rcof(k,1,3,1,4) + 1 * rcof(k,0,3,1,4)
    rcof(k,3,3,1,3) = PCx(k) * rcof(k,2,3,1,4) + 2 * rcof(k,1,3,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,3,0,2,3) = PCx(k) * rcof(k,2,0,2,4) + 2 * rcof(k,1,0,2,4)
    rcof(k,2,1,2,3) = PCx(k) * rcof(k,1,1,2,4) + 1 * rcof(k,0,1,2,4)
    rcof(k,3,1,2,3) = PCx(k) * rcof(k,2,1,2,4) + 2 * rcof(k,1,1,2,4)
    rcof(k,2,2,2,3) = PCx(k) * rcof(k,1,2,2,4) + 1 * rcof(k,0,2,2,4)
    rcof(k,3,2,2,3) = PCx(k) * rcof(k,2,2,2,4) + 2 * rcof(k,1,2,2,4)
    rcof(k,2,3,2,3) = PCx(k) * rcof(k,1,3,2,4) + 1 * rcof(k,0,3,2,4)
    rcof(k,3,3,2,3) = PCx(k) * rcof(k,2,3,2,4) + 2 * rcof(k,1,3,2,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,3,1,2) = PCy(k) * rcof(k,0,2,1,3) + 2 * rcof(k,0,1,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,0,3,2,2) = PCy(k) * rcof(k,0,2,2,3) + 2 * rcof(k,0,1,2,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,3,0,2) = PCx(k) * rcof(k,0,3,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,3,1,2) = PCx(k) * rcof(k,0,3,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,2,2,2) = PCx(k) * rcof(k,0,2,2,3)
    rcof(k,1,3,2,2) = PCx(k) * rcof(k,0,3,2,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,3,1,0,2) = PCx(k) * rcof(k,2,1,0,3) + 2 * rcof(k,1,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) + 1 * rcof(k,0,2,0,3)
    rcof(k,3,2,0,2) = PCx(k) * rcof(k,2,2,0,3) + 2 * rcof(k,1,2,0,3)
    rcof(k,2,3,0,2) = PCx(k) * rcof(k,1,3,0,3) + 1 * rcof(k,0,3,0,3)
    rcof(k,3,3,0,2) = PCx(k) * rcof(k,2,3,0,3) + 2 * rcof(k,1,3,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,3,0,1,2) = PCx(k) * rcof(k,2,0,1,3) + 2 * rcof(k,1,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,3,1,1,2) = PCx(k) * rcof(k,2,1,1,3) + 2 * rcof(k,1,1,1,3)
    rcof(k,2,2,1,2) = PCx(k) * rcof(k,1,2,1,3) + 1 * rcof(k,0,2,1,3)
    rcof(k,3,2,1,2) = PCx(k) * rcof(k,2,2,1,3) + 2 * rcof(k,1,2,1,3)
    rcof(k,2,3,1,2) = PCx(k) * rcof(k,1,3,1,3) + 1 * rcof(k,0,3,1,3)
    rcof(k,3,3,1,2) = PCx(k) * rcof(k,2,3,1,3) + 2 * rcof(k,1,3,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,3,0,2,2) = PCx(k) * rcof(k,2,0,2,3) + 2 * rcof(k,1,0,2,3)
    rcof(k,2,1,2,2) = PCx(k) * rcof(k,1,1,2,3) + 1 * rcof(k,0,1,2,3)
    rcof(k,3,1,2,2) = PCx(k) * rcof(k,2,1,2,3) + 2 * rcof(k,1,1,2,3)
    rcof(k,2,2,2,2) = PCx(k) * rcof(k,1,2,2,3) + 1 * rcof(k,0,2,2,3)
    rcof(k,3,2,2,2) = PCx(k) * rcof(k,2,2,2,3) + 2 * rcof(k,1,2,2,3)
    rcof(k,2,3,2,2) = PCx(k) * rcof(k,1,3,2,3) + 1 * rcof(k,0,3,2,3)
    rcof(k,3,3,2,2) = PCx(k) * rcof(k,2,3,2,3) + 2 * rcof(k,1,3,2,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,3,1,1) = PCy(k) * rcof(k,0,2,1,2) + 2 * rcof(k,0,1,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,0,3,2,1) = PCy(k) * rcof(k,0,2,2,2) + 2 * rcof(k,0,1,2,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,3,0,1) = PCx(k) * rcof(k,0,3,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,3,1,1) = PCx(k) * rcof(k,0,3,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,2,2,1) = PCx(k) * rcof(k,0,2,2,2)
    rcof(k,1,3,2,1) = PCx(k) * rcof(k,0,3,2,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,3,1,0,1) = PCx(k) * rcof(k,2,1,0,2) + 2 * rcof(k,1,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) + 1 * rcof(k,0,2,0,2)
    rcof(k,3,2,0,1) = PCx(k) * rcof(k,2,2,0,2) + 2 * rcof(k,1,2,0,2)
    rcof(k,2,3,0,1) = PCx(k) * rcof(k,1,3,0,2) + 1 * rcof(k,0,3,0,2)
    rcof(k,3,3,0,1) = PCx(k) * rcof(k,2,3,0,2) + 2 * rcof(k,1,3,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,3,0,1,1) = PCx(k) * rcof(k,2,0,1,2) + 2 * rcof(k,1,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,3,1,1,1) = PCx(k) * rcof(k,2,1,1,2) + 2 * rcof(k,1,1,1,2)
    rcof(k,2,2,1,1) = PCx(k) * rcof(k,1,2,1,2) + 1 * rcof(k,0,2,1,2)
    rcof(k,3,2,1,1) = PCx(k) * rcof(k,2,2,1,2) + 2 * rcof(k,1,2,1,2)
    rcof(k,2,3,1,1) = PCx(k) * rcof(k,1,3,1,2) + 1 * rcof(k,0,3,1,2)
    rcof(k,3,3,1,1) = PCx(k) * rcof(k,2,3,1,2) + 2 * rcof(k,1,3,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,3,0,2,1) = PCx(k) * rcof(k,2,0,2,2) + 2 * rcof(k,1,0,2,2)
    rcof(k,2,1,2,1) = PCx(k) * rcof(k,1,1,2,2) + 1 * rcof(k,0,1,2,2)
    rcof(k,3,1,2,1) = PCx(k) * rcof(k,2,1,2,2) + 2 * rcof(k,1,1,2,2)
    rcof(k,2,2,2,1) = PCx(k) * rcof(k,1,2,2,2) + 1 * rcof(k,0,2,2,2)
    rcof(k,3,2,2,1) = PCx(k) * rcof(k,2,2,2,2) + 2 * rcof(k,1,2,2,2)
    rcof(k,2,3,2,1) = PCx(k) * rcof(k,1,3,2,2) + 1 * rcof(k,0,3,2,2)
    rcof(k,3,3,2,1) = PCx(k) * rcof(k,2,3,2,2) + 2 * rcof(k,1,3,2,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,3,1,0) = PCy(k) * rcof(k,0,2,1,1) + 2 * rcof(k,0,1,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,0,3,2,0) = PCy(k) * rcof(k,0,2,2,1) + 2 * rcof(k,0,1,2,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,3,0,0) = PCx(k) * rcof(k,0,3,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,3,1,0) = PCx(k) * rcof(k,0,3,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,2,2,0) = PCx(k) * rcof(k,0,2,2,1)
    rcof(k,1,3,2,0) = PCx(k) * rcof(k,0,3,2,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,3,1,0,0) = PCx(k) * rcof(k,2,1,0,1) + 2 * rcof(k,1,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) + 1 * rcof(k,0,2,0,1)
    rcof(k,3,2,0,0) = PCx(k) * rcof(k,2,2,0,1) + 2 * rcof(k,1,2,0,1)
    rcof(k,2,3,0,0) = PCx(k) * rcof(k,1,3,0,1) + 1 * rcof(k,0,3,0,1)
    rcof(k,3,3,0,0) = PCx(k) * rcof(k,2,3,0,1) + 2 * rcof(k,1,3,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,3,0,1,0) = PCx(k) * rcof(k,2,0,1,1) + 2 * rcof(k,1,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,3,1,1,0) = PCx(k) * rcof(k,2,1,1,1) + 2 * rcof(k,1,1,1,1)
    rcof(k,2,2,1,0) = PCx(k) * rcof(k,1,2,1,1) + 1 * rcof(k,0,2,1,1)
    rcof(k,3,2,1,0) = PCx(k) * rcof(k,2,2,1,1) + 2 * rcof(k,1,2,1,1)
    rcof(k,2,3,1,0) = PCx(k) * rcof(k,1,3,1,1) + 1 * rcof(k,0,3,1,1)
    rcof(k,3,3,1,0) = PCx(k) * rcof(k,2,3,1,1) + 2 * rcof(k,1,3,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,3,0,2,0) = PCx(k) * rcof(k,2,0,2,1) + 2 * rcof(k,1,0,2,1)
    rcof(k,2,1,2,0) = PCx(k) * rcof(k,1,1,2,1) + 1 * rcof(k,0,1,2,1)
    rcof(k,3,1,2,0) = PCx(k) * rcof(k,2,1,2,1) + 2 * rcof(k,1,1,2,1)
    rcof(k,2,2,2,0) = PCx(k) * rcof(k,1,2,2,1) + 1 * rcof(k,0,2,2,1)
    rcof(k,3,2,2,0) = PCx(k) * rcof(k,2,2,2,1) + 2 * rcof(k,1,2,2,1)
    rcof(k,2,3,2,0) = PCx(k) * rcof(k,1,3,2,1) + 1 * rcof(k,0,3,2,1)
    rcof(k,3,3,2,0) = PCx(k) * rcof(k,2,3,2,1) + 2 * rcof(k,1,3,2,1)
  end do

end subroutine

pure subroutine r3_2_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,0,7) = (-two*p(k))**7 * Fn(7,k)
    rcof(k,0,0,0,8) = (-two*p(k))**8 * Fn(8,k)
    rcof(k,0,0,1,7) = PCz(k) * rcof(k,0,0,0,8)
    rcof(k,0,0,1,6) = PCz(k) * rcof(k,0,0,0,7)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,7) = PCz(k) * rcof(k,0,0,1,8) + 1 * rcof(k,0,0,0,8)
    rcof(k,0,0,3,7) = PCz(k) * rcof(k,0,0,2,8) + 2 * rcof(k,0,0,1,8)
    rcof(k,0,1,0,7) = PCy(k) * rcof(k,0,0,0,8)
    rcof(k,0,1,1,7) = PCy(k) * rcof(k,0,0,1,8)
    rcof(k,0,1,2,7) = PCy(k) * rcof(k,0,0,2,8)
    rcof(k,0,1,3,7) = PCy(k) * rcof(k,0,0,3,8)
    rcof(k,0,2,0,7) = PCy(k) * rcof(k,0,1,0,8) + 1 * rcof(k,0,0,0,8)
    rcof(k,0,2,1,7) = PCy(k) * rcof(k,0,1,1,8) + 1 * rcof(k,0,0,1,8)
    rcof(k,0,2,2,7) = PCy(k) * rcof(k,0,1,2,8) + 1 * rcof(k,0,0,2,8)
    rcof(k,0,2,3,7) = PCy(k) * rcof(k,0,1,3,8) + 1 * rcof(k,0,0,3,8)
    rcof(k,1,0,0,7) = PCx(k) * rcof(k,0,0,0,8)
    rcof(k,1,1,0,7) = PCx(k) * rcof(k,0,1,0,8)
    rcof(k,1,2,0,7) = PCx(k) * rcof(k,0,2,0,8)
    rcof(k,1,0,1,7) = PCx(k) * rcof(k,0,0,1,8)
    rcof(k,1,1,1,7) = PCx(k) * rcof(k,0,1,1,8)
    rcof(k,1,2,1,7) = PCx(k) * rcof(k,0,2,1,8)
    rcof(k,1,0,2,7) = PCx(k) * rcof(k,0,0,2,8)
    rcof(k,1,1,2,7) = PCx(k) * rcof(k,0,1,2,8)
    rcof(k,1,2,2,7) = PCx(k) * rcof(k,0,2,2,8)
    rcof(k,1,0,3,7) = PCx(k) * rcof(k,0,0,3,8)
    rcof(k,1,1,3,7) = PCx(k) * rcof(k,0,1,3,8)
    rcof(k,1,2,3,7) = PCx(k) * rcof(k,0,2,3,8)
    rcof(k,2,0,0,7) = PCx(k) * rcof(k,1,0,0,8) + 1 * rcof(k,0,0,0,8)
    rcof(k,3,0,0,7) = PCx(k) * rcof(k,2,0,0,8) + 2 * rcof(k,1,0,0,8)
    rcof(k,2,1,0,7) = PCx(k) * rcof(k,1,1,0,8) + 1 * rcof(k,0,1,0,8)
    rcof(k,3,1,0,7) = PCx(k) * rcof(k,2,1,0,8) + 2 * rcof(k,1,1,0,8)
    rcof(k,2,2,0,7) = PCx(k) * rcof(k,1,2,0,8) + 1 * rcof(k,0,2,0,8)
    rcof(k,3,2,0,7) = PCx(k) * rcof(k,2,2,0,8) + 2 * rcof(k,1,2,0,8)
    rcof(k,2,0,1,7) = PCx(k) * rcof(k,1,0,1,8) + 1 * rcof(k,0,0,1,8)
    rcof(k,3,0,1,7) = PCx(k) * rcof(k,2,0,1,8) + 2 * rcof(k,1,0,1,8)
    rcof(k,2,1,1,7) = PCx(k) * rcof(k,1,1,1,8) + 1 * rcof(k,0,1,1,8)
    rcof(k,3,1,1,7) = PCx(k) * rcof(k,2,1,1,8) + 2 * rcof(k,1,1,1,8)
    rcof(k,2,2,1,7) = PCx(k) * rcof(k,1,2,1,8) + 1 * rcof(k,0,2,1,8)
    rcof(k,3,2,1,7) = PCx(k) * rcof(k,2,2,1,8) + 2 * rcof(k,1,2,1,8)
    rcof(k,2,0,2,7) = PCx(k) * rcof(k,1,0,2,8) + 1 * rcof(k,0,0,2,8)
    rcof(k,3,0,2,7) = PCx(k) * rcof(k,2,0,2,8) + 2 * rcof(k,1,0,2,8)
    rcof(k,2,1,2,7) = PCx(k) * rcof(k,1,1,2,8) + 1 * rcof(k,0,1,2,8)
    rcof(k,3,1,2,7) = PCx(k) * rcof(k,2,1,2,8) + 2 * rcof(k,1,1,2,8)
    rcof(k,2,2,2,7) = PCx(k) * rcof(k,1,2,2,8) + 1 * rcof(k,0,2,2,8)
    rcof(k,3,2,2,7) = PCx(k) * rcof(k,2,2,2,8) + 2 * rcof(k,1,2,2,8)
    rcof(k,2,0,3,7) = PCx(k) * rcof(k,1,0,3,8) + 1 * rcof(k,0,0,3,8)
    rcof(k,3,0,3,7) = PCx(k) * rcof(k,2,0,3,8) + 2 * rcof(k,1,0,3,8)
    rcof(k,2,1,3,7) = PCx(k) * rcof(k,1,1,3,8) + 1 * rcof(k,0,1,3,8)
    rcof(k,3,1,3,7) = PCx(k) * rcof(k,2,1,3,8) + 2 * rcof(k,1,1,3,8)
    rcof(k,2,2,3,7) = PCx(k) * rcof(k,1,2,3,8) + 1 * rcof(k,0,2,3,8)
    rcof(k,3,2,3,7) = PCx(k) * rcof(k,2,2,3,8) + 2 * rcof(k,1,2,3,8)
    rcof(k,0,0,2,6) = PCz(k) * rcof(k,0,0,1,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,0,3,6) = PCz(k) * rcof(k,0,0,2,7) + 2 * rcof(k,0,0,1,7)
    rcof(k,0,1,0,6) = PCy(k) * rcof(k,0,0,0,7)
    rcof(k,0,1,1,6) = PCy(k) * rcof(k,0,0,1,7)
    rcof(k,0,1,2,6) = PCy(k) * rcof(k,0,0,2,7)
    rcof(k,0,1,3,6) = PCy(k) * rcof(k,0,0,3,7)
    rcof(k,0,2,0,6) = PCy(k) * rcof(k,0,1,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,2,1,6) = PCy(k) * rcof(k,0,1,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,0,2,2,6) = PCy(k) * rcof(k,0,1,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,0,2,3,6) = PCy(k) * rcof(k,0,1,3,7) + 1 * rcof(k,0,0,3,7)
    rcof(k,1,0,0,6) = PCx(k) * rcof(k,0,0,0,7)
    rcof(k,1,1,0,6) = PCx(k) * rcof(k,0,1,0,7)
    rcof(k,1,2,0,6) = PCx(k) * rcof(k,0,2,0,7)
    rcof(k,1,0,1,6) = PCx(k) * rcof(k,0,0,1,7)
    rcof(k,1,1,1,6) = PCx(k) * rcof(k,0,1,1,7)
    rcof(k,1,2,1,6) = PCx(k) * rcof(k,0,2,1,7)
    rcof(k,1,0,2,6) = PCx(k) * rcof(k,0,0,2,7)
    rcof(k,1,1,2,6) = PCx(k) * rcof(k,0,1,2,7)
    rcof(k,1,2,2,6) = PCx(k) * rcof(k,0,2,2,7)
    rcof(k,1,0,3,6) = PCx(k) * rcof(k,0,0,3,7)
    rcof(k,1,1,3,6) = PCx(k) * rcof(k,0,1,3,7)
    rcof(k,1,2,3,6) = PCx(k) * rcof(k,0,2,3,7)
    rcof(k,2,0,0,6) = PCx(k) * rcof(k,1,0,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,3,0,0,6) = PCx(k) * rcof(k,2,0,0,7) + 2 * rcof(k,1,0,0,7)
    rcof(k,2,1,0,6) = PCx(k) * rcof(k,1,1,0,7) + 1 * rcof(k,0,1,0,7)
    rcof(k,3,1,0,6) = PCx(k) * rcof(k,2,1,0,7) + 2 * rcof(k,1,1,0,7)
    rcof(k,2,2,0,6) = PCx(k) * rcof(k,1,2,0,7) + 1 * rcof(k,0,2,0,7)
    rcof(k,3,2,0,6) = PCx(k) * rcof(k,2,2,0,7) + 2 * rcof(k,1,2,0,7)
    rcof(k,2,0,1,6) = PCx(k) * rcof(k,1,0,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,3,0,1,6) = PCx(k) * rcof(k,2,0,1,7) + 2 * rcof(k,1,0,1,7)
    rcof(k,2,1,1,6) = PCx(k) * rcof(k,1,1,1,7) + 1 * rcof(k,0,1,1,7)
    rcof(k,3,1,1,6) = PCx(k) * rcof(k,2,1,1,7) + 2 * rcof(k,1,1,1,7)
    rcof(k,2,2,1,6) = PCx(k) * rcof(k,1,2,1,7) + 1 * rcof(k,0,2,1,7)
    rcof(k,3,2,1,6) = PCx(k) * rcof(k,2,2,1,7) + 2 * rcof(k,1,2,1,7)
    rcof(k,2,0,2,6) = PCx(k) * rcof(k,1,0,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,3,0,2,6) = PCx(k) * rcof(k,2,0,2,7) + 2 * rcof(k,1,0,2,7)
    rcof(k,2,1,2,6) = PCx(k) * rcof(k,1,1,2,7) + 1 * rcof(k,0,1,2,7)
    rcof(k,3,1,2,6) = PCx(k) * rcof(k,2,1,2,7) + 2 * rcof(k,1,1,2,7)
    rcof(k,2,2,2,6) = PCx(k) * rcof(k,1,2,2,7) + 1 * rcof(k,0,2,2,7)
    rcof(k,3,2,2,6) = PCx(k) * rcof(k,2,2,2,7) + 2 * rcof(k,1,2,2,7)
    rcof(k,2,0,3,6) = PCx(k) * rcof(k,1,0,3,7) + 1 * rcof(k,0,0,3,7)
    rcof(k,3,0,3,6) = PCx(k) * rcof(k,2,0,3,7) + 2 * rcof(k,1,0,3,7)
    rcof(k,2,1,3,6) = PCx(k) * rcof(k,1,1,3,7) + 1 * rcof(k,0,1,3,7)
    rcof(k,3,1,3,6) = PCx(k) * rcof(k,2,1,3,7) + 2 * rcof(k,1,1,3,7)
    rcof(k,2,2,3,6) = PCx(k) * rcof(k,1,2,3,7) + 1 * rcof(k,0,2,3,7)
    rcof(k,3,2,3,6) = PCx(k) * rcof(k,2,2,3,7) + 2 * rcof(k,1,2,3,7)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,0,3,5) = PCz(k) * rcof(k,0,0,2,6) + 2 * rcof(k,0,0,1,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,0,1,3,5) = PCy(k) * rcof(k,0,0,3,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,0,2,2,5) = PCy(k) * rcof(k,0,1,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,0,2,3,5) = PCy(k) * rcof(k,0,1,3,6) + 1 * rcof(k,0,0,3,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,2,1,5) = PCx(k) * rcof(k,0,2,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,1,2,5) = PCx(k) * rcof(k,0,1,2,6)
    rcof(k,1,2,2,5) = PCx(k) * rcof(k,0,2,2,6)
    rcof(k,1,0,3,5) = PCx(k) * rcof(k,0,0,3,6)
    rcof(k,1,1,3,5) = PCx(k) * rcof(k,0,1,3,6)
    rcof(k,1,2,3,5) = PCx(k) * rcof(k,0,2,3,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,3,0,0,5) = PCx(k) * rcof(k,2,0,0,6) + 2 * rcof(k,1,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,3,1,0,5) = PCx(k) * rcof(k,2,1,0,6) + 2 * rcof(k,1,1,0,6)
    rcof(k,2,2,0,5) = PCx(k) * rcof(k,1,2,0,6) + 1 * rcof(k,0,2,0,6)
    rcof(k,3,2,0,5) = PCx(k) * rcof(k,2,2,0,6) + 2 * rcof(k,1,2,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,3,0,1,5) = PCx(k) * rcof(k,2,0,1,6) + 2 * rcof(k,1,0,1,6)
    rcof(k,2,1,1,5) = PCx(k) * rcof(k,1,1,1,6) + 1 * rcof(k,0,1,1,6)
    rcof(k,3,1,1,5) = PCx(k) * rcof(k,2,1,1,6) + 2 * rcof(k,1,1,1,6)
    rcof(k,2,2,1,5) = PCx(k) * rcof(k,1,2,1,6) + 1 * rcof(k,0,2,1,6)
    rcof(k,3,2,1,5) = PCx(k) * rcof(k,2,2,1,6) + 2 * rcof(k,1,2,1,6)
    rcof(k,2,0,2,5) = PCx(k) * rcof(k,1,0,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,3,0,2,5) = PCx(k) * rcof(k,2,0,2,6) + 2 * rcof(k,1,0,2,6)
    rcof(k,2,1,2,5) = PCx(k) * rcof(k,1,1,2,6) + 1 * rcof(k,0,1,2,6)
    rcof(k,3,1,2,5) = PCx(k) * rcof(k,2,1,2,6) + 2 * rcof(k,1,1,2,6)
    rcof(k,2,2,2,5) = PCx(k) * rcof(k,1,2,2,6) + 1 * rcof(k,0,2,2,6)
    rcof(k,3,2,2,5) = PCx(k) * rcof(k,2,2,2,6) + 2 * rcof(k,1,2,2,6)
    rcof(k,2,0,3,5) = PCx(k) * rcof(k,1,0,3,6) + 1 * rcof(k,0,0,3,6)
    rcof(k,3,0,3,5) = PCx(k) * rcof(k,2,0,3,6) + 2 * rcof(k,1,0,3,6)
    rcof(k,2,1,3,5) = PCx(k) * rcof(k,1,1,3,6) + 1 * rcof(k,0,1,3,6)
    rcof(k,3,1,3,5) = PCx(k) * rcof(k,2,1,3,6) + 2 * rcof(k,1,1,3,6)
    rcof(k,2,2,3,5) = PCx(k) * rcof(k,1,2,3,6) + 1 * rcof(k,0,2,3,6)
    rcof(k,3,2,3,5) = PCx(k) * rcof(k,2,2,3,6) + 2 * rcof(k,1,2,3,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,0,3,4) = PCz(k) * rcof(k,0,0,2,5) + 2 * rcof(k,0,0,1,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,1,3,4) = PCy(k) * rcof(k,0,0,3,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,0,2,3,4) = PCy(k) * rcof(k,0,1,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,2,2,4) = PCx(k) * rcof(k,0,2,2,5)
    rcof(k,1,0,3,4) = PCx(k) * rcof(k,0,0,3,5)
    rcof(k,1,1,3,4) = PCx(k) * rcof(k,0,1,3,5)
    rcof(k,1,2,3,4) = PCx(k) * rcof(k,0,2,3,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,3,0,0,4) = PCx(k) * rcof(k,2,0,0,5) + 2 * rcof(k,1,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,3,1,0,4) = PCx(k) * rcof(k,2,1,0,5) + 2 * rcof(k,1,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) + 1 * rcof(k,0,2,0,5)
    rcof(k,3,2,0,4) = PCx(k) * rcof(k,2,2,0,5) + 2 * rcof(k,1,2,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,3,0,1,4) = PCx(k) * rcof(k,2,0,1,5) + 2 * rcof(k,1,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,3,1,1,4) = PCx(k) * rcof(k,2,1,1,5) + 2 * rcof(k,1,1,1,5)
    rcof(k,2,2,1,4) = PCx(k) * rcof(k,1,2,1,5) + 1 * rcof(k,0,2,1,5)
    rcof(k,3,2,1,4) = PCx(k) * rcof(k,2,2,1,5) + 2 * rcof(k,1,2,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,3,0,2,4) = PCx(k) * rcof(k,2,0,2,5) + 2 * rcof(k,1,0,2,5)
    rcof(k,2,1,2,4) = PCx(k) * rcof(k,1,1,2,5) + 1 * rcof(k,0,1,2,5)
    rcof(k,3,1,2,4) = PCx(k) * rcof(k,2,1,2,5) + 2 * rcof(k,1,1,2,5)
    rcof(k,2,2,2,4) = PCx(k) * rcof(k,1,2,2,5) + 1 * rcof(k,0,2,2,5)
    rcof(k,3,2,2,4) = PCx(k) * rcof(k,2,2,2,5) + 2 * rcof(k,1,2,2,5)
    rcof(k,2,0,3,4) = PCx(k) * rcof(k,1,0,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,3,0,3,4) = PCx(k) * rcof(k,2,0,3,5) + 2 * rcof(k,1,0,3,5)
    rcof(k,2,1,3,4) = PCx(k) * rcof(k,1,1,3,5) + 1 * rcof(k,0,1,3,5)
    rcof(k,3,1,3,4) = PCx(k) * rcof(k,2,1,3,5) + 2 * rcof(k,1,1,3,5)
    rcof(k,2,2,3,4) = PCx(k) * rcof(k,1,2,3,5) + 1 * rcof(k,0,2,3,5)
    rcof(k,3,2,3,4) = PCx(k) * rcof(k,2,2,3,5) + 2 * rcof(k,1,2,3,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,1,3,3) = PCy(k) * rcof(k,0,0,3,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,0,2,3,3) = PCy(k) * rcof(k,0,1,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,2,2,3) = PCx(k) * rcof(k,0,2,2,4)
    rcof(k,1,0,3,3) = PCx(k) * rcof(k,0,0,3,4)
    rcof(k,1,1,3,3) = PCx(k) * rcof(k,0,1,3,4)
    rcof(k,1,2,3,3) = PCx(k) * rcof(k,0,2,3,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,3,1,0,3) = PCx(k) * rcof(k,2,1,0,4) + 2 * rcof(k,1,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) + 1 * rcof(k,0,2,0,4)
    rcof(k,3,2,0,3) = PCx(k) * rcof(k,2,2,0,4) + 2 * rcof(k,1,2,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,3,0,1,3) = PCx(k) * rcof(k,2,0,1,4) + 2 * rcof(k,1,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,3,1,1,3) = PCx(k) * rcof(k,2,1,1,4) + 2 * rcof(k,1,1,1,4)
    rcof(k,2,2,1,3) = PCx(k) * rcof(k,1,2,1,4) + 1 * rcof(k,0,2,1,4)
    rcof(k,3,2,1,3) = PCx(k) * rcof(k,2,2,1,4) + 2 * rcof(k,1,2,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,3,0,2,3) = PCx(k) * rcof(k,2,0,2,4) + 2 * rcof(k,1,0,2,4)
    rcof(k,2,1,2,3) = PCx(k) * rcof(k,1,1,2,4) + 1 * rcof(k,0,1,2,4)
    rcof(k,3,1,2,3) = PCx(k) * rcof(k,2,1,2,4) + 2 * rcof(k,1,1,2,4)
    rcof(k,2,2,2,3) = PCx(k) * rcof(k,1,2,2,4) + 1 * rcof(k,0,2,2,4)
    rcof(k,3,2,2,3) = PCx(k) * rcof(k,2,2,2,4) + 2 * rcof(k,1,2,2,4)
    rcof(k,2,0,3,3) = PCx(k) * rcof(k,1,0,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,3,0,3,3) = PCx(k) * rcof(k,2,0,3,4) + 2 * rcof(k,1,0,3,4)
    rcof(k,2,1,3,3) = PCx(k) * rcof(k,1,1,3,4) + 1 * rcof(k,0,1,3,4)
    rcof(k,3,1,3,3) = PCx(k) * rcof(k,2,1,3,4) + 2 * rcof(k,1,1,3,4)
    rcof(k,2,2,3,3) = PCx(k) * rcof(k,1,2,3,4) + 1 * rcof(k,0,2,3,4)
    rcof(k,3,2,3,3) = PCx(k) * rcof(k,2,2,3,4) + 2 * rcof(k,1,2,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,1,3,2) = PCy(k) * rcof(k,0,0,3,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,0,2,3,2) = PCy(k) * rcof(k,0,1,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,2,2,2) = PCx(k) * rcof(k,0,2,2,3)
    rcof(k,1,0,3,2) = PCx(k) * rcof(k,0,0,3,3)
    rcof(k,1,1,3,2) = PCx(k) * rcof(k,0,1,3,3)
    rcof(k,1,2,3,2) = PCx(k) * rcof(k,0,2,3,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,3,1,0,2) = PCx(k) * rcof(k,2,1,0,3) + 2 * rcof(k,1,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) + 1 * rcof(k,0,2,0,3)
    rcof(k,3,2,0,2) = PCx(k) * rcof(k,2,2,0,3) + 2 * rcof(k,1,2,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,3,0,1,2) = PCx(k) * rcof(k,2,0,1,3) + 2 * rcof(k,1,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,3,1,1,2) = PCx(k) * rcof(k,2,1,1,3) + 2 * rcof(k,1,1,1,3)
    rcof(k,2,2,1,2) = PCx(k) * rcof(k,1,2,1,3) + 1 * rcof(k,0,2,1,3)
    rcof(k,3,2,1,2) = PCx(k) * rcof(k,2,2,1,3) + 2 * rcof(k,1,2,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,3,0,2,2) = PCx(k) * rcof(k,2,0,2,3) + 2 * rcof(k,1,0,2,3)
    rcof(k,2,1,2,2) = PCx(k) * rcof(k,1,1,2,3) + 1 * rcof(k,0,1,2,3)
    rcof(k,3,1,2,2) = PCx(k) * rcof(k,2,1,2,3) + 2 * rcof(k,1,1,2,3)
    rcof(k,2,2,2,2) = PCx(k) * rcof(k,1,2,2,3) + 1 * rcof(k,0,2,2,3)
    rcof(k,3,2,2,2) = PCx(k) * rcof(k,2,2,2,3) + 2 * rcof(k,1,2,2,3)
    rcof(k,2,0,3,2) = PCx(k) * rcof(k,1,0,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,3,0,3,2) = PCx(k) * rcof(k,2,0,3,3) + 2 * rcof(k,1,0,3,3)
    rcof(k,2,1,3,2) = PCx(k) * rcof(k,1,1,3,3) + 1 * rcof(k,0,1,3,3)
    rcof(k,3,1,3,2) = PCx(k) * rcof(k,2,1,3,3) + 2 * rcof(k,1,1,3,3)
    rcof(k,2,2,3,2) = PCx(k) * rcof(k,1,2,3,3) + 1 * rcof(k,0,2,3,3)
    rcof(k,3,2,3,2) = PCx(k) * rcof(k,2,2,3,3) + 2 * rcof(k,1,2,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,1,3,1) = PCy(k) * rcof(k,0,0,3,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,0,2,3,1) = PCy(k) * rcof(k,0,1,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,2,2,1) = PCx(k) * rcof(k,0,2,2,2)
    rcof(k,1,0,3,1) = PCx(k) * rcof(k,0,0,3,2)
    rcof(k,1,1,3,1) = PCx(k) * rcof(k,0,1,3,2)
    rcof(k,1,2,3,1) = PCx(k) * rcof(k,0,2,3,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,3,1,0,1) = PCx(k) * rcof(k,2,1,0,2) + 2 * rcof(k,1,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) + 1 * rcof(k,0,2,0,2)
    rcof(k,3,2,0,1) = PCx(k) * rcof(k,2,2,0,2) + 2 * rcof(k,1,2,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,3,0,1,1) = PCx(k) * rcof(k,2,0,1,2) + 2 * rcof(k,1,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,3,1,1,1) = PCx(k) * rcof(k,2,1,1,2) + 2 * rcof(k,1,1,1,2)
    rcof(k,2,2,1,1) = PCx(k) * rcof(k,1,2,1,2) + 1 * rcof(k,0,2,1,2)
    rcof(k,3,2,1,1) = PCx(k) * rcof(k,2,2,1,2) + 2 * rcof(k,1,2,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,3,0,2,1) = PCx(k) * rcof(k,2,0,2,2) + 2 * rcof(k,1,0,2,2)
    rcof(k,2,1,2,1) = PCx(k) * rcof(k,1,1,2,2) + 1 * rcof(k,0,1,2,2)
    rcof(k,3,1,2,1) = PCx(k) * rcof(k,2,1,2,2) + 2 * rcof(k,1,1,2,2)
    rcof(k,2,2,2,1) = PCx(k) * rcof(k,1,2,2,2) + 1 * rcof(k,0,2,2,2)
    rcof(k,3,2,2,1) = PCx(k) * rcof(k,2,2,2,2) + 2 * rcof(k,1,2,2,2)
    rcof(k,2,0,3,1) = PCx(k) * rcof(k,1,0,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,3,0,3,1) = PCx(k) * rcof(k,2,0,3,2) + 2 * rcof(k,1,0,3,2)
    rcof(k,2,1,3,1) = PCx(k) * rcof(k,1,1,3,2) + 1 * rcof(k,0,1,3,2)
    rcof(k,3,1,3,1) = PCx(k) * rcof(k,2,1,3,2) + 2 * rcof(k,1,1,3,2)
    rcof(k,2,2,3,1) = PCx(k) * rcof(k,1,2,3,2) + 1 * rcof(k,0,2,3,2)
    rcof(k,3,2,3,1) = PCx(k) * rcof(k,2,2,3,2) + 2 * rcof(k,1,2,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,1,3,0) = PCy(k) * rcof(k,0,0,3,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,0,2,3,0) = PCy(k) * rcof(k,0,1,3,1) + 1 * rcof(k,0,0,3,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,2,2,0) = PCx(k) * rcof(k,0,2,2,1)
    rcof(k,1,0,3,0) = PCx(k) * rcof(k,0,0,3,1)
    rcof(k,1,1,3,0) = PCx(k) * rcof(k,0,1,3,1)
    rcof(k,1,2,3,0) = PCx(k) * rcof(k,0,2,3,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,3,1,0,0) = PCx(k) * rcof(k,2,1,0,1) + 2 * rcof(k,1,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) + 1 * rcof(k,0,2,0,1)
    rcof(k,3,2,0,0) = PCx(k) * rcof(k,2,2,0,1) + 2 * rcof(k,1,2,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,3,0,1,0) = PCx(k) * rcof(k,2,0,1,1) + 2 * rcof(k,1,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,3,1,1,0) = PCx(k) * rcof(k,2,1,1,1) + 2 * rcof(k,1,1,1,1)
    rcof(k,2,2,1,0) = PCx(k) * rcof(k,1,2,1,1) + 1 * rcof(k,0,2,1,1)
    rcof(k,3,2,1,0) = PCx(k) * rcof(k,2,2,1,1) + 2 * rcof(k,1,2,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,3,0,2,0) = PCx(k) * rcof(k,2,0,2,1) + 2 * rcof(k,1,0,2,1)
    rcof(k,2,1,2,0) = PCx(k) * rcof(k,1,1,2,1) + 1 * rcof(k,0,1,2,1)
    rcof(k,3,1,2,0) = PCx(k) * rcof(k,2,1,2,1) + 2 * rcof(k,1,1,2,1)
    rcof(k,2,2,2,0) = PCx(k) * rcof(k,1,2,2,1) + 1 * rcof(k,0,2,2,1)
    rcof(k,3,2,2,0) = PCx(k) * rcof(k,2,2,2,1) + 2 * rcof(k,1,2,2,1)
    rcof(k,2,0,3,0) = PCx(k) * rcof(k,1,0,3,1) + 1 * rcof(k,0,0,3,1)
    rcof(k,3,0,3,0) = PCx(k) * rcof(k,2,0,3,1) + 2 * rcof(k,1,0,3,1)
    rcof(k,2,1,3,0) = PCx(k) * rcof(k,1,1,3,1) + 1 * rcof(k,0,1,3,1)
    rcof(k,3,1,3,0) = PCx(k) * rcof(k,2,1,3,1) + 2 * rcof(k,1,1,3,1)
    rcof(k,2,2,3,0) = PCx(k) * rcof(k,1,2,3,1) + 1 * rcof(k,0,2,3,1)
    rcof(k,3,2,3,0) = PCx(k) * rcof(k,2,2,3,1) + 2 * rcof(k,1,2,3,1)
  end do

end subroutine

pure subroutine r2_3_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl
    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = -two*p(k) * Fn(1,k)
    rcof(k,0,0,0,2) = four * p(k) * p(k) * Fn(2,k)
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,0,7) = (-two*p(k))**7 * Fn(7,k)
    rcof(k,0,0,0,8) = (-two*p(k))**8 * Fn(8,k)
    rcof(k,0,0,1,7) = PCz(k) * rcof(k,0,0,0,8)
    rcof(k,0,0,1,6) = PCz(k) * rcof(k,0,0,0,7)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * rcof(k,0,0,0,2)
    rcof(k,0,0,1,0) = PCz(k) * rcof(k,0,0,0,1)
    rcof(k,0,0,2,7) = PCz(k) * rcof(k,0,0,1,8) + 1 * rcof(k,0,0,0,8)
    rcof(k,0,0,3,7) = PCz(k) * rcof(k,0,0,2,8) + 2 * rcof(k,0,0,1,8)
    rcof(k,0,1,0,7) = PCy(k) * rcof(k,0,0,0,8)
    rcof(k,0,1,1,7) = PCy(k) * rcof(k,0,0,1,8)
    rcof(k,0,1,2,7) = PCy(k) * rcof(k,0,0,2,8)
    rcof(k,0,1,3,7) = PCy(k) * rcof(k,0,0,3,8)
    rcof(k,0,2,0,7) = PCy(k) * rcof(k,0,1,0,8) + 1 * rcof(k,0,0,0,8)
    rcof(k,0,3,0,7) = PCy(k) * rcof(k,0,2,0,8) + 2 * rcof(k,0,1,0,8)
    rcof(k,0,2,1,7) = PCy(k) * rcof(k,0,1,1,8) + 1 * rcof(k,0,0,1,8)
    rcof(k,0,3,1,7) = PCy(k) * rcof(k,0,2,1,8) + 2 * rcof(k,0,1,1,8)
    rcof(k,0,2,2,7) = PCy(k) * rcof(k,0,1,2,8) + 1 * rcof(k,0,0,2,8)
    rcof(k,0,3,2,7) = PCy(k) * rcof(k,0,2,2,8) + 2 * rcof(k,0,1,2,8)
    rcof(k,0,2,3,7) = PCy(k) * rcof(k,0,1,3,8) + 1 * rcof(k,0,0,3,8)
    rcof(k,0,3,3,7) = PCy(k) * rcof(k,0,2,3,8) + 2 * rcof(k,0,1,3,8)
    rcof(k,1,0,0,7) = PCx(k) * rcof(k,0,0,0,8)
    rcof(k,1,1,0,7) = PCx(k) * rcof(k,0,1,0,8)
    rcof(k,1,2,0,7) = PCx(k) * rcof(k,0,2,0,8)
    rcof(k,1,3,0,7) = PCx(k) * rcof(k,0,3,0,8)
    rcof(k,1,0,1,7) = PCx(k) * rcof(k,0,0,1,8)
    rcof(k,1,1,1,7) = PCx(k) * rcof(k,0,1,1,8)
    rcof(k,1,2,1,7) = PCx(k) * rcof(k,0,2,1,8)
    rcof(k,1,3,1,7) = PCx(k) * rcof(k,0,3,1,8)
    rcof(k,1,0,2,7) = PCx(k) * rcof(k,0,0,2,8)
    rcof(k,1,1,2,7) = PCx(k) * rcof(k,0,1,2,8)
    rcof(k,1,2,2,7) = PCx(k) * rcof(k,0,2,2,8)
    rcof(k,1,3,2,7) = PCx(k) * rcof(k,0,3,2,8)
    rcof(k,1,0,3,7) = PCx(k) * rcof(k,0,0,3,8)
    rcof(k,1,1,3,7) = PCx(k) * rcof(k,0,1,3,8)
    rcof(k,1,2,3,7) = PCx(k) * rcof(k,0,2,3,8)
    rcof(k,1,3,3,7) = PCx(k) * rcof(k,0,3,3,8)
    rcof(k,2,0,0,7) = PCx(k) * rcof(k,1,0,0,8) + 1 * rcof(k,0,0,0,8)
    rcof(k,2,1,0,7) = PCx(k) * rcof(k,1,1,0,8) + 1 * rcof(k,0,1,0,8)
    rcof(k,2,2,0,7) = PCx(k) * rcof(k,1,2,0,8) + 1 * rcof(k,0,2,0,8)
    rcof(k,2,3,0,7) = PCx(k) * rcof(k,1,3,0,8) + 1 * rcof(k,0,3,0,8)
    rcof(k,2,0,1,7) = PCx(k) * rcof(k,1,0,1,8) + 1 * rcof(k,0,0,1,8)
    rcof(k,2,1,1,7) = PCx(k) * rcof(k,1,1,1,8) + 1 * rcof(k,0,1,1,8)
    rcof(k,2,2,1,7) = PCx(k) * rcof(k,1,2,1,8) + 1 * rcof(k,0,2,1,8)
    rcof(k,2,3,1,7) = PCx(k) * rcof(k,1,3,1,8) + 1 * rcof(k,0,3,1,8)
    rcof(k,2,0,2,7) = PCx(k) * rcof(k,1,0,2,8) + 1 * rcof(k,0,0,2,8)
    rcof(k,2,1,2,7) = PCx(k) * rcof(k,1,1,2,8) + 1 * rcof(k,0,1,2,8)
    rcof(k,2,2,2,7) = PCx(k) * rcof(k,1,2,2,8) + 1 * rcof(k,0,2,2,8)
    rcof(k,2,3,2,7) = PCx(k) * rcof(k,1,3,2,8) + 1 * rcof(k,0,3,2,8)
    rcof(k,2,0,3,7) = PCx(k) * rcof(k,1,0,3,8) + 1 * rcof(k,0,0,3,8)
    rcof(k,2,1,3,7) = PCx(k) * rcof(k,1,1,3,8) + 1 * rcof(k,0,1,3,8)
    rcof(k,2,2,3,7) = PCx(k) * rcof(k,1,2,3,8) + 1 * rcof(k,0,2,3,8)
    rcof(k,2,3,3,7) = PCx(k) * rcof(k,1,3,3,8) + 1 * rcof(k,0,3,3,8)
    rcof(k,0,0,2,6) = PCz(k) * rcof(k,0,0,1,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,0,3,6) = PCz(k) * rcof(k,0,0,2,7) + 2 * rcof(k,0,0,1,7)
    rcof(k,0,1,0,6) = PCy(k) * rcof(k,0,0,0,7)
    rcof(k,0,1,1,6) = PCy(k) * rcof(k,0,0,1,7)
    rcof(k,0,1,2,6) = PCy(k) * rcof(k,0,0,2,7)
    rcof(k,0,1,3,6) = PCy(k) * rcof(k,0,0,3,7)
    rcof(k,0,2,0,6) = PCy(k) * rcof(k,0,1,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,3,0,6) = PCy(k) * rcof(k,0,2,0,7) + 2 * rcof(k,0,1,0,7)
    rcof(k,0,2,1,6) = PCy(k) * rcof(k,0,1,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,0,3,1,6) = PCy(k) * rcof(k,0,2,1,7) + 2 * rcof(k,0,1,1,7)
    rcof(k,0,2,2,6) = PCy(k) * rcof(k,0,1,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,0,3,2,6) = PCy(k) * rcof(k,0,2,2,7) + 2 * rcof(k,0,1,2,7)
    rcof(k,0,2,3,6) = PCy(k) * rcof(k,0,1,3,7) + 1 * rcof(k,0,0,3,7)
    rcof(k,0,3,3,6) = PCy(k) * rcof(k,0,2,3,7) + 2 * rcof(k,0,1,3,7)
    rcof(k,1,0,0,6) = PCx(k) * rcof(k,0,0,0,7)
    rcof(k,1,1,0,6) = PCx(k) * rcof(k,0,1,0,7)
    rcof(k,1,2,0,6) = PCx(k) * rcof(k,0,2,0,7)
    rcof(k,1,3,0,6) = PCx(k) * rcof(k,0,3,0,7)
    rcof(k,1,0,1,6) = PCx(k) * rcof(k,0,0,1,7)
    rcof(k,1,1,1,6) = PCx(k) * rcof(k,0,1,1,7)
    rcof(k,1,2,1,6) = PCx(k) * rcof(k,0,2,1,7)
    rcof(k,1,3,1,6) = PCx(k) * rcof(k,0,3,1,7)
    rcof(k,1,0,2,6) = PCx(k) * rcof(k,0,0,2,7)
    rcof(k,1,1,2,6) = PCx(k) * rcof(k,0,1,2,7)
    rcof(k,1,2,2,6) = PCx(k) * rcof(k,0,2,2,7)
    rcof(k,1,3,2,6) = PCx(k) * rcof(k,0,3,2,7)
    rcof(k,1,0,3,6) = PCx(k) * rcof(k,0,0,3,7)
    rcof(k,1,1,3,6) = PCx(k) * rcof(k,0,1,3,7)
    rcof(k,1,2,3,6) = PCx(k) * rcof(k,0,2,3,7)
    rcof(k,1,3,3,6) = PCx(k) * rcof(k,0,3,3,7)
    rcof(k,2,0,0,6) = PCx(k) * rcof(k,1,0,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,2,1,0,6) = PCx(k) * rcof(k,1,1,0,7) + 1 * rcof(k,0,1,0,7)
    rcof(k,2,2,0,6) = PCx(k) * rcof(k,1,2,0,7) + 1 * rcof(k,0,2,0,7)
    rcof(k,2,3,0,6) = PCx(k) * rcof(k,1,3,0,7) + 1 * rcof(k,0,3,0,7)
    rcof(k,2,0,1,6) = PCx(k) * rcof(k,1,0,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,2,1,1,6) = PCx(k) * rcof(k,1,1,1,7) + 1 * rcof(k,0,1,1,7)
    rcof(k,2,2,1,6) = PCx(k) * rcof(k,1,2,1,7) + 1 * rcof(k,0,2,1,7)
    rcof(k,2,3,1,6) = PCx(k) * rcof(k,1,3,1,7) + 1 * rcof(k,0,3,1,7)
    rcof(k,2,0,2,6) = PCx(k) * rcof(k,1,0,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,2,1,2,6) = PCx(k) * rcof(k,1,1,2,7) + 1 * rcof(k,0,1,2,7)
    rcof(k,2,2,2,6) = PCx(k) * rcof(k,1,2,2,7) + 1 * rcof(k,0,2,2,7)
    rcof(k,2,3,2,6) = PCx(k) * rcof(k,1,3,2,7) + 1 * rcof(k,0,3,2,7)
    rcof(k,2,0,3,6) = PCx(k) * rcof(k,1,0,3,7) + 1 * rcof(k,0,0,3,7)
    rcof(k,2,1,3,6) = PCx(k) * rcof(k,1,1,3,7) + 1 * rcof(k,0,1,3,7)
    rcof(k,2,2,3,6) = PCx(k) * rcof(k,1,2,3,7) + 1 * rcof(k,0,2,3,7)
    rcof(k,2,3,3,6) = PCx(k) * rcof(k,1,3,3,7) + 1 * rcof(k,0,3,3,7)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,0,3,5) = PCz(k) * rcof(k,0,0,2,6) + 2 * rcof(k,0,0,1,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,0,1,3,5) = PCy(k) * rcof(k,0,0,3,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,3,0,5) = PCy(k) * rcof(k,0,2,0,6) + 2 * rcof(k,0,1,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,0,3,1,5) = PCy(k) * rcof(k,0,2,1,6) + 2 * rcof(k,0,1,1,6)
    rcof(k,0,2,2,5) = PCy(k) * rcof(k,0,1,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,0,3,2,5) = PCy(k) * rcof(k,0,2,2,6) + 2 * rcof(k,0,1,2,6)
    rcof(k,0,2,3,5) = PCy(k) * rcof(k,0,1,3,6) + 1 * rcof(k,0,0,3,6)
    rcof(k,0,3,3,5) = PCy(k) * rcof(k,0,2,3,6) + 2 * rcof(k,0,1,3,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,3,0,5) = PCx(k) * rcof(k,0,3,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,2,1,5) = PCx(k) * rcof(k,0,2,1,6)
    rcof(k,1,3,1,5) = PCx(k) * rcof(k,0,3,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,1,2,5) = PCx(k) * rcof(k,0,1,2,6)
    rcof(k,1,2,2,5) = PCx(k) * rcof(k,0,2,2,6)
    rcof(k,1,3,2,5) = PCx(k) * rcof(k,0,3,2,6)
    rcof(k,1,0,3,5) = PCx(k) * rcof(k,0,0,3,6)
    rcof(k,1,1,3,5) = PCx(k) * rcof(k,0,1,3,6)
    rcof(k,1,2,3,5) = PCx(k) * rcof(k,0,2,3,6)
    rcof(k,1,3,3,5) = PCx(k) * rcof(k,0,3,3,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,2,2,0,5) = PCx(k) * rcof(k,1,2,0,6) + 1 * rcof(k,0,2,0,6)
    rcof(k,2,3,0,5) = PCx(k) * rcof(k,1,3,0,6) + 1 * rcof(k,0,3,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,2,1,1,5) = PCx(k) * rcof(k,1,1,1,6) + 1 * rcof(k,0,1,1,6)
    rcof(k,2,2,1,5) = PCx(k) * rcof(k,1,2,1,6) + 1 * rcof(k,0,2,1,6)
    rcof(k,2,3,1,5) = PCx(k) * rcof(k,1,3,1,6) + 1 * rcof(k,0,3,1,6)
    rcof(k,2,0,2,5) = PCx(k) * rcof(k,1,0,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,2,1,2,5) = PCx(k) * rcof(k,1,1,2,6) + 1 * rcof(k,0,1,2,6)
    rcof(k,2,2,2,5) = PCx(k) * rcof(k,1,2,2,6) + 1 * rcof(k,0,2,2,6)
    rcof(k,2,3,2,5) = PCx(k) * rcof(k,1,3,2,6) + 1 * rcof(k,0,3,2,6)
    rcof(k,2,0,3,5) = PCx(k) * rcof(k,1,0,3,6) + 1 * rcof(k,0,0,3,6)
    rcof(k,2,1,3,5) = PCx(k) * rcof(k,1,1,3,6) + 1 * rcof(k,0,1,3,6)
    rcof(k,2,2,3,5) = PCx(k) * rcof(k,1,2,3,6) + 1 * rcof(k,0,2,3,6)
    rcof(k,2,3,3,5) = PCx(k) * rcof(k,1,3,3,6) + 1 * rcof(k,0,3,3,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,0,3,4) = PCz(k) * rcof(k,0,0,2,5) + 2 * rcof(k,0,0,1,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,1,3,4) = PCy(k) * rcof(k,0,0,3,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,3,0,4) = PCy(k) * rcof(k,0,2,0,5) + 2 * rcof(k,0,1,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,3,1,4) = PCy(k) * rcof(k,0,2,1,5) + 2 * rcof(k,0,1,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,0,3,2,4) = PCy(k) * rcof(k,0,2,2,5) + 2 * rcof(k,0,1,2,5)
    rcof(k,0,2,3,4) = PCy(k) * rcof(k,0,1,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,0,3,3,4) = PCy(k) * rcof(k,0,2,3,5) + 2 * rcof(k,0,1,3,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,3,0,4) = PCx(k) * rcof(k,0,3,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,3,1,4) = PCx(k) * rcof(k,0,3,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,2,2,4) = PCx(k) * rcof(k,0,2,2,5)
    rcof(k,1,3,2,4) = PCx(k) * rcof(k,0,3,2,5)
    rcof(k,1,0,3,4) = PCx(k) * rcof(k,0,0,3,5)
    rcof(k,1,1,3,4) = PCx(k) * rcof(k,0,1,3,5)
    rcof(k,1,2,3,4) = PCx(k) * rcof(k,0,2,3,5)
    rcof(k,1,3,3,4) = PCx(k) * rcof(k,0,3,3,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) + 1 * rcof(k,0,2,0,5)
    rcof(k,2,3,0,4) = PCx(k) * rcof(k,1,3,0,5) + 1 * rcof(k,0,3,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,2,2,1,4) = PCx(k) * rcof(k,1,2,1,5) + 1 * rcof(k,0,2,1,5)
    rcof(k,2,3,1,4) = PCx(k) * rcof(k,1,3,1,5) + 1 * rcof(k,0,3,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,2,1,2,4) = PCx(k) * rcof(k,1,1,2,5) + 1 * rcof(k,0,1,2,5)
    rcof(k,2,2,2,4) = PCx(k) * rcof(k,1,2,2,5) + 1 * rcof(k,0,2,2,5)
    rcof(k,2,3,2,4) = PCx(k) * rcof(k,1,3,2,5) + 1 * rcof(k,0,3,2,5)
    rcof(k,2,0,3,4) = PCx(k) * rcof(k,1,0,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,2,1,3,4) = PCx(k) * rcof(k,1,1,3,5) + 1 * rcof(k,0,1,3,5)
    rcof(k,2,2,3,4) = PCx(k) * rcof(k,1,2,3,5) + 1 * rcof(k,0,2,3,5)
    rcof(k,2,3,3,4) = PCx(k) * rcof(k,1,3,3,5) + 1 * rcof(k,0,3,3,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,1,3,3) = PCy(k) * rcof(k,0,0,3,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,3,1,3) = PCy(k) * rcof(k,0,2,1,4) + 2 * rcof(k,0,1,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,0,3,2,3) = PCy(k) * rcof(k,0,2,2,4) + 2 * rcof(k,0,1,2,4)
    rcof(k,0,2,3,3) = PCy(k) * rcof(k,0,1,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,0,3,3,3) = PCy(k) * rcof(k,0,2,3,4) + 2 * rcof(k,0,1,3,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,3,0,3) = PCx(k) * rcof(k,0,3,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,3,1,3) = PCx(k) * rcof(k,0,3,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,2,2,3) = PCx(k) * rcof(k,0,2,2,4)
    rcof(k,1,3,2,3) = PCx(k) * rcof(k,0,3,2,4)
    rcof(k,1,0,3,3) = PCx(k) * rcof(k,0,0,3,4)
    rcof(k,1,1,3,3) = PCx(k) * rcof(k,0,1,3,4)
    rcof(k,1,2,3,3) = PCx(k) * rcof(k,0,2,3,4)
    rcof(k,1,3,3,3) = PCx(k) * rcof(k,0,3,3,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) + 1 * rcof(k,0,2,0,4)
    rcof(k,2,3,0,3) = PCx(k) * rcof(k,1,3,0,4) + 1 * rcof(k,0,3,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,2,2,1,3) = PCx(k) * rcof(k,1,2,1,4) + 1 * rcof(k,0,2,1,4)
    rcof(k,2,3,1,3) = PCx(k) * rcof(k,1,3,1,4) + 1 * rcof(k,0,3,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,2,1,2,3) = PCx(k) * rcof(k,1,1,2,4) + 1 * rcof(k,0,1,2,4)
    rcof(k,2,2,2,3) = PCx(k) * rcof(k,1,2,2,4) + 1 * rcof(k,0,2,2,4)
    rcof(k,2,3,2,3) = PCx(k) * rcof(k,1,3,2,4) + 1 * rcof(k,0,3,2,4)
    rcof(k,2,0,3,3) = PCx(k) * rcof(k,1,0,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,2,1,3,3) = PCx(k) * rcof(k,1,1,3,4) + 1 * rcof(k,0,1,3,4)
    rcof(k,2,2,3,3) = PCx(k) * rcof(k,1,2,3,4) + 1 * rcof(k,0,2,3,4)
    rcof(k,2,3,3,3) = PCx(k) * rcof(k,1,3,3,4) + 1 * rcof(k,0,3,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,1,3,2) = PCy(k) * rcof(k,0,0,3,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,3,1,2) = PCy(k) * rcof(k,0,2,1,3) + 2 * rcof(k,0,1,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,0,3,2,2) = PCy(k) * rcof(k,0,2,2,3) + 2 * rcof(k,0,1,2,3)
    rcof(k,0,2,3,2) = PCy(k) * rcof(k,0,1,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,0,3,3,2) = PCy(k) * rcof(k,0,2,3,3) + 2 * rcof(k,0,1,3,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,3,0,2) = PCx(k) * rcof(k,0,3,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,3,1,2) = PCx(k) * rcof(k,0,3,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,2,2,2) = PCx(k) * rcof(k,0,2,2,3)
    rcof(k,1,3,2,2) = PCx(k) * rcof(k,0,3,2,3)
    rcof(k,1,0,3,2) = PCx(k) * rcof(k,0,0,3,3)
    rcof(k,1,1,3,2) = PCx(k) * rcof(k,0,1,3,3)
    rcof(k,1,2,3,2) = PCx(k) * rcof(k,0,2,3,3)
    rcof(k,1,3,3,2) = PCx(k) * rcof(k,0,3,3,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) + 1 * rcof(k,0,2,0,3)
    rcof(k,2,3,0,2) = PCx(k) * rcof(k,1,3,0,3) + 1 * rcof(k,0,3,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,2,2,1,2) = PCx(k) * rcof(k,1,2,1,3) + 1 * rcof(k,0,2,1,3)
    rcof(k,2,3,1,2) = PCx(k) * rcof(k,1,3,1,3) + 1 * rcof(k,0,3,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,2,1,2,2) = PCx(k) * rcof(k,1,1,2,3) + 1 * rcof(k,0,1,2,3)
    rcof(k,2,2,2,2) = PCx(k) * rcof(k,1,2,2,3) + 1 * rcof(k,0,2,2,3)
    rcof(k,2,3,2,2) = PCx(k) * rcof(k,1,3,2,3) + 1 * rcof(k,0,3,2,3)
    rcof(k,2,0,3,2) = PCx(k) * rcof(k,1,0,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,2,1,3,2) = PCx(k) * rcof(k,1,1,3,3) + 1 * rcof(k,0,1,3,3)
    rcof(k,2,2,3,2) = PCx(k) * rcof(k,1,2,3,3) + 1 * rcof(k,0,2,3,3)
    rcof(k,2,3,3,2) = PCx(k) * rcof(k,1,3,3,3) + 1 * rcof(k,0,3,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,0,1,0,1) = PCy(k) * rcof(k,0,0,0,2)
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,1,3,1) = PCy(k) * rcof(k,0,0,3,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,3,1,1) = PCy(k) * rcof(k,0,2,1,2) + 2 * rcof(k,0,1,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,0,3,2,1) = PCy(k) * rcof(k,0,2,2,2) + 2 * rcof(k,0,1,2,2)
    rcof(k,0,2,3,1) = PCy(k) * rcof(k,0,1,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,0,3,3,1) = PCy(k) * rcof(k,0,2,3,2) + 2 * rcof(k,0,1,3,2)
    rcof(k,1,0,0,1) = PCx(k) * rcof(k,0,0,0,2)
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,3,0,1) = PCx(k) * rcof(k,0,3,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,3,1,1) = PCx(k) * rcof(k,0,3,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,2,2,1) = PCx(k) * rcof(k,0,2,2,2)
    rcof(k,1,3,2,1) = PCx(k) * rcof(k,0,3,2,2)
    rcof(k,1,0,3,1) = PCx(k) * rcof(k,0,0,3,2)
    rcof(k,1,1,3,1) = PCx(k) * rcof(k,0,1,3,2)
    rcof(k,1,2,3,1) = PCx(k) * rcof(k,0,2,3,2)
    rcof(k,1,3,3,1) = PCx(k) * rcof(k,0,3,3,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * rcof(k,0,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) + 1 * rcof(k,0,2,0,2)
    rcof(k,2,3,0,1) = PCx(k) * rcof(k,1,3,0,2) + 1 * rcof(k,0,3,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,2,2,1,1) = PCx(k) * rcof(k,1,2,1,2) + 1 * rcof(k,0,2,1,2)
    rcof(k,2,3,1,1) = PCx(k) * rcof(k,1,3,1,2) + 1 * rcof(k,0,3,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,2,1,2,1) = PCx(k) * rcof(k,1,1,2,2) + 1 * rcof(k,0,1,2,2)
    rcof(k,2,2,2,1) = PCx(k) * rcof(k,1,2,2,2) + 1 * rcof(k,0,2,2,2)
    rcof(k,2,3,2,1) = PCx(k) * rcof(k,1,3,2,2) + 1 * rcof(k,0,3,2,2)
    rcof(k,2,0,3,1) = PCx(k) * rcof(k,1,0,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,2,1,3,1) = PCx(k) * rcof(k,1,1,3,2) + 1 * rcof(k,0,1,3,2)
    rcof(k,2,2,3,1) = PCx(k) * rcof(k,1,2,3,2) + 1 * rcof(k,0,2,3,2)
    rcof(k,2,3,3,1) = PCx(k) * rcof(k,1,3,3,2) + 1 * rcof(k,0,3,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,0,1,0,0) = PCy(k) * rcof(k,0,0,0,1)
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,1,3,0) = PCy(k) * rcof(k,0,0,3,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,3,1,0) = PCy(k) * rcof(k,0,2,1,1) + 2 * rcof(k,0,1,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,0,3,2,0) = PCy(k) * rcof(k,0,2,2,1) + 2 * rcof(k,0,1,2,1)
    rcof(k,0,2,3,0) = PCy(k) * rcof(k,0,1,3,1) + 1 * rcof(k,0,0,3,1)
    rcof(k,0,3,3,0) = PCy(k) * rcof(k,0,2,3,1) + 2 * rcof(k,0,1,3,1)
    rcof(k,1,0,0,0) = PCx(k) * rcof(k,0,0,0,1)
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,3,0,0) = PCx(k) * rcof(k,0,3,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,3,1,0) = PCx(k) * rcof(k,0,3,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,2,2,0) = PCx(k) * rcof(k,0,2,2,1)
    rcof(k,1,3,2,0) = PCx(k) * rcof(k,0,3,2,1)
    rcof(k,1,0,3,0) = PCx(k) * rcof(k,0,0,3,1)
    rcof(k,1,1,3,0) = PCx(k) * rcof(k,0,1,3,1)
    rcof(k,1,2,3,0) = PCx(k) * rcof(k,0,2,3,1)
    rcof(k,1,3,3,0) = PCx(k) * rcof(k,0,3,3,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * rcof(k,0,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) + 1 * rcof(k,0,2,0,1)
    rcof(k,2,3,0,0) = PCx(k) * rcof(k,1,3,0,1) + 1 * rcof(k,0,3,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,2,2,1,0) = PCx(k) * rcof(k,1,2,1,1) + 1 * rcof(k,0,2,1,1)
    rcof(k,2,3,1,0) = PCx(k) * rcof(k,1,3,1,1) + 1 * rcof(k,0,3,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,2,1,2,0) = PCx(k) * rcof(k,1,1,2,1) + 1 * rcof(k,0,1,2,1)
    rcof(k,2,2,2,0) = PCx(k) * rcof(k,1,2,2,1) + 1 * rcof(k,0,2,2,1)
    rcof(k,2,3,2,0) = PCx(k) * rcof(k,1,3,2,1) + 1 * rcof(k,0,3,2,1)
    rcof(k,2,0,3,0) = PCx(k) * rcof(k,1,0,3,1) + 1 * rcof(k,0,0,3,1)
    rcof(k,2,1,3,0) = PCx(k) * rcof(k,1,1,3,1) + 1 * rcof(k,0,1,3,1)
    rcof(k,2,2,3,0) = PCx(k) * rcof(k,1,2,3,1) + 1 * rcof(k,0,2,3,1)
    rcof(k,2,3,3,0) = PCx(k) * rcof(k,1,3,3,1) + 1 * rcof(k,0,3,3,1)
  end do

end subroutine

pure subroutine r3_3_3(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: lmax,nprijkl

  ! input
  integer,   intent(in) :: t, u, v, n
  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)
  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)

  ! output 
  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)

  ! local
  real(kdp) :: r0001,r0002,r0003,r0004,r0005,r0006 
  integer :: k
  rcof = zero 

  do k = 1, nprijkl

    r0001 = -two*p(k) * Fn(1,k)
    r0002 = four * p(k) * p(k) * Fn(2,k)

    rcof(k,0,0,0,0) = Fn(0,k)
    rcof(k,0,0,0,1) = r0001 
    rcof(k,0,0,0,2) = r0002 
    rcof(k,0,0,0,3) = -eight  * p(k) * p(k) * p(k) * Fn(3,k) 
    rcof(k,0,0,0,4) = sixteen * p(k) * p(k) * p(k) * p(k) * Fn(4,k)
    rcof(k,0,0,0,5) = (-two*p(k))**5 * Fn(5,k)
    rcof(k,0,0,0,6) = (-two*p(k))**6 * Fn(6,k)
    rcof(k,0,0,0,7) = (-two*p(k))**7 * Fn(7,k)
    rcof(k,0,0,0,8) = (-two*p(k))**8 * Fn(8,k)
    rcof(k,0,0,0,9) = (-two*p(k))**9 * Fn(9,k)
    rcof(k,0,0,1,8) = PCz(k) * rcof(k,0,0,0,9)
    rcof(k,0,0,1,7) = PCz(k) * rcof(k,0,0,0,8)
    rcof(k,0,0,1,6) = PCz(k) * rcof(k,0,0,0,7)
    rcof(k,0,0,1,5) = PCz(k) * rcof(k,0,0,0,6)
    rcof(k,0,0,1,4) = PCz(k) * rcof(k,0,0,0,5)
    rcof(k,0,0,1,3) = PCz(k) * rcof(k,0,0,0,4)
    rcof(k,0,0,1,2) = PCz(k) * rcof(k,0,0,0,3)
    rcof(k,0,0,1,1) = PCz(k) * r0002 
    rcof(k,0,0,1,0) = PCz(k) * r0001 
    rcof(k,0,0,2,8) = PCz(k) * rcof(k,0,0,1,9) + 1 * rcof(k,0,0,0,9)
    rcof(k,0,0,3,8) = PCz(k) * rcof(k,0,0,2,9) + 2 * rcof(k,0,0,1,9)
    rcof(k,0,1,0,8) = PCy(k) * rcof(k,0,0,0,9)
    rcof(k,0,1,1,8) = PCy(k) * rcof(k,0,0,1,9)
    rcof(k,0,1,2,8) = PCy(k) * rcof(k,0,0,2,9)
    rcof(k,0,1,3,8) = PCy(k) * rcof(k,0,0,3,9)
    rcof(k,0,2,0,8) = PCy(k) * rcof(k,0,1,0,9) + 1 * rcof(k,0,0,0,9)
    rcof(k,0,3,0,8) = PCy(k) * rcof(k,0,2,0,9) + 2 * rcof(k,0,1,0,9)
    rcof(k,0,2,1,8) = PCy(k) * rcof(k,0,1,1,9) + 1 * rcof(k,0,0,1,9)
    rcof(k,0,3,1,8) = PCy(k) * rcof(k,0,2,1,9) + 2 * rcof(k,0,1,1,9)
    rcof(k,0,2,2,8) = PCy(k) * rcof(k,0,1,2,9) + 1 * rcof(k,0,0,2,9)
    rcof(k,0,3,2,8) = PCy(k) * rcof(k,0,2,2,9) + 2 * rcof(k,0,1,2,9)
    rcof(k,0,2,3,8) = PCy(k) * rcof(k,0,1,3,9) + 1 * rcof(k,0,0,3,9)
    rcof(k,0,3,3,8) = PCy(k) * rcof(k,0,2,3,9) + 2 * rcof(k,0,1,3,9)
    rcof(k,1,0,0,8) = PCx(k) * rcof(k,0,0,0,9)
    rcof(k,1,1,0,8) = PCx(k) * rcof(k,0,1,0,9)
    rcof(k,1,2,0,8) = PCx(k) * rcof(k,0,2,0,9)
    rcof(k,1,3,0,8) = PCx(k) * rcof(k,0,3,0,9)
    rcof(k,1,0,1,8) = PCx(k) * rcof(k,0,0,1,9)
    rcof(k,1,1,1,8) = PCx(k) * rcof(k,0,1,1,9)
    rcof(k,1,2,1,8) = PCx(k) * rcof(k,0,2,1,9)
    rcof(k,1,3,1,8) = PCx(k) * rcof(k,0,3,1,9)
    rcof(k,1,0,2,8) = PCx(k) * rcof(k,0,0,2,9)
    rcof(k,1,1,2,8) = PCx(k) * rcof(k,0,1,2,9)
    rcof(k,1,2,2,8) = PCx(k) * rcof(k,0,2,2,9)
    rcof(k,1,3,2,8) = PCx(k) * rcof(k,0,3,2,9)
    rcof(k,1,0,3,8) = PCx(k) * rcof(k,0,0,3,9)
    rcof(k,1,1,3,8) = PCx(k) * rcof(k,0,1,3,9)
    rcof(k,1,2,3,8) = PCx(k) * rcof(k,0,2,3,9)
    rcof(k,1,3,3,8) = PCx(k) * rcof(k,0,3,3,9)
    rcof(k,2,0,0,8) = PCx(k) * rcof(k,1,0,0,9) + 1 * rcof(k,0,0,0,9)
    rcof(k,3,0,0,8) = PCx(k) * rcof(k,2,0,0,9) + 2 * rcof(k,1,0,0,9)
    rcof(k,2,1,0,8) = PCx(k) * rcof(k,1,1,0,9) + 1 * rcof(k,0,1,0,9)
    rcof(k,3,1,0,8) = PCx(k) * rcof(k,2,1,0,9) + 2 * rcof(k,1,1,0,9)
    rcof(k,2,2,0,8) = PCx(k) * rcof(k,1,2,0,9) + 1 * rcof(k,0,2,0,9)
    rcof(k,3,2,0,8) = PCx(k) * rcof(k,2,2,0,9) + 2 * rcof(k,1,2,0,9)
    rcof(k,2,3,0,8) = PCx(k) * rcof(k,1,3,0,9) + 1 * rcof(k,0,3,0,9)
    rcof(k,3,3,0,8) = PCx(k) * rcof(k,2,3,0,9) + 2 * rcof(k,1,3,0,9)
    rcof(k,2,0,1,8) = PCx(k) * rcof(k,1,0,1,9) + 1 * rcof(k,0,0,1,9)
    rcof(k,3,0,1,8) = PCx(k) * rcof(k,2,0,1,9) + 2 * rcof(k,1,0,1,9)
    rcof(k,2,1,1,8) = PCx(k) * rcof(k,1,1,1,9) + 1 * rcof(k,0,1,1,9)
    rcof(k,3,1,1,8) = PCx(k) * rcof(k,2,1,1,9) + 2 * rcof(k,1,1,1,9)
    rcof(k,2,2,1,8) = PCx(k) * rcof(k,1,2,1,9) + 1 * rcof(k,0,2,1,9)
    rcof(k,3,2,1,8) = PCx(k) * rcof(k,2,2,1,9) + 2 * rcof(k,1,2,1,9)
    rcof(k,2,3,1,8) = PCx(k) * rcof(k,1,3,1,9) + 1 * rcof(k,0,3,1,9)
    rcof(k,3,3,1,8) = PCx(k) * rcof(k,2,3,1,9) + 2 * rcof(k,1,3,1,9)
    rcof(k,2,0,2,8) = PCx(k) * rcof(k,1,0,2,9) + 1 * rcof(k,0,0,2,9)
    rcof(k,3,0,2,8) = PCx(k) * rcof(k,2,0,2,9) + 2 * rcof(k,1,0,2,9)
    rcof(k,2,1,2,8) = PCx(k) * rcof(k,1,1,2,9) + 1 * rcof(k,0,1,2,9)
    rcof(k,3,1,2,8) = PCx(k) * rcof(k,2,1,2,9) + 2 * rcof(k,1,1,2,9)
    rcof(k,2,2,2,8) = PCx(k) * rcof(k,1,2,2,9) + 1 * rcof(k,0,2,2,9)
    rcof(k,3,2,2,8) = PCx(k) * rcof(k,2,2,2,9) + 2 * rcof(k,1,2,2,9)
    rcof(k,2,3,2,8) = PCx(k) * rcof(k,1,3,2,9) + 1 * rcof(k,0,3,2,9)
    rcof(k,3,3,2,8) = PCx(k) * rcof(k,2,3,2,9) + 2 * rcof(k,1,3,2,9)
    rcof(k,2,0,3,8) = PCx(k) * rcof(k,1,0,3,9) + 1 * rcof(k,0,0,3,9)
    rcof(k,3,0,3,8) = PCx(k) * rcof(k,2,0,3,9) + 2 * rcof(k,1,0,3,9)
    rcof(k,2,1,3,8) = PCx(k) * rcof(k,1,1,3,9) + 1 * rcof(k,0,1,3,9)
    rcof(k,3,1,3,8) = PCx(k) * rcof(k,2,1,3,9) + 2 * rcof(k,1,1,3,9)
    rcof(k,2,2,3,8) = PCx(k) * rcof(k,1,2,3,9) + 1 * rcof(k,0,2,3,9)
    rcof(k,3,2,3,8) = PCx(k) * rcof(k,2,2,3,9) + 2 * rcof(k,1,2,3,9)
    rcof(k,2,3,3,8) = PCx(k) * rcof(k,1,3,3,9) + 1 * rcof(k,0,3,3,9)
    rcof(k,3,3,3,8) = PCx(k) * rcof(k,2,3,3,9) + 2 * rcof(k,1,3,3,9)
    rcof(k,0,0,2,7) = PCz(k) * rcof(k,0,0,1,8) + 1 * rcof(k,0,0,0,8)
    rcof(k,0,0,3,7) = PCz(k) * rcof(k,0,0,2,8) + 2 * rcof(k,0,0,1,8)
    rcof(k,0,1,0,7) = PCy(k) * rcof(k,0,0,0,8)
    rcof(k,0,1,1,7) = PCy(k) * rcof(k,0,0,1,8)
    rcof(k,0,1,2,7) = PCy(k) * rcof(k,0,0,2,8)
    rcof(k,0,1,3,7) = PCy(k) * rcof(k,0,0,3,8)
    rcof(k,0,2,0,7) = PCy(k) * rcof(k,0,1,0,8) + 1 * rcof(k,0,0,0,8)
    rcof(k,0,3,0,7) = PCy(k) * rcof(k,0,2,0,8) + 2 * rcof(k,0,1,0,8)
    rcof(k,0,2,1,7) = PCy(k) * rcof(k,0,1,1,8) + 1 * rcof(k,0,0,1,8)
    rcof(k,0,3,1,7) = PCy(k) * rcof(k,0,2,1,8) + 2 * rcof(k,0,1,1,8)
    rcof(k,0,2,2,7) = PCy(k) * rcof(k,0,1,2,8) + 1 * rcof(k,0,0,2,8)
    rcof(k,0,3,2,7) = PCy(k) * rcof(k,0,2,2,8) + 2 * rcof(k,0,1,2,8)
    rcof(k,0,2,3,7) = PCy(k) * rcof(k,0,1,3,8) + 1 * rcof(k,0,0,3,8)
    rcof(k,0,3,3,7) = PCy(k) * rcof(k,0,2,3,8) + 2 * rcof(k,0,1,3,8)
    rcof(k,1,0,0,7) = PCx(k) * rcof(k,0,0,0,8)
    rcof(k,1,1,0,7) = PCx(k) * rcof(k,0,1,0,8)
    rcof(k,1,2,0,7) = PCx(k) * rcof(k,0,2,0,8)
    rcof(k,1,3,0,7) = PCx(k) * rcof(k,0,3,0,8)
    rcof(k,1,0,1,7) = PCx(k) * rcof(k,0,0,1,8)
    rcof(k,1,1,1,7) = PCx(k) * rcof(k,0,1,1,8)
    rcof(k,1,2,1,7) = PCx(k) * rcof(k,0,2,1,8)
    rcof(k,1,3,1,7) = PCx(k) * rcof(k,0,3,1,8)
    rcof(k,1,0,2,7) = PCx(k) * rcof(k,0,0,2,8)
    rcof(k,1,1,2,7) = PCx(k) * rcof(k,0,1,2,8)
    rcof(k,1,2,2,7) = PCx(k) * rcof(k,0,2,2,8)
    rcof(k,1,3,2,7) = PCx(k) * rcof(k,0,3,2,8)
    rcof(k,1,0,3,7) = PCx(k) * rcof(k,0,0,3,8)
    rcof(k,1,1,3,7) = PCx(k) * rcof(k,0,1,3,8)
    rcof(k,1,2,3,7) = PCx(k) * rcof(k,0,2,3,8)
    rcof(k,1,3,3,7) = PCx(k) * rcof(k,0,3,3,8)
    rcof(k,2,0,0,7) = PCx(k) * rcof(k,1,0,0,8) + 1 * rcof(k,0,0,0,8)
    rcof(k,3,0,0,7) = PCx(k) * rcof(k,2,0,0,8) + 2 * rcof(k,1,0,0,8)
    rcof(k,2,1,0,7) = PCx(k) * rcof(k,1,1,0,8) + 1 * rcof(k,0,1,0,8)
    rcof(k,3,1,0,7) = PCx(k) * rcof(k,2,1,0,8) + 2 * rcof(k,1,1,0,8)
    rcof(k,2,2,0,7) = PCx(k) * rcof(k,1,2,0,8) + 1 * rcof(k,0,2,0,8)
    rcof(k,3,2,0,7) = PCx(k) * rcof(k,2,2,0,8) + 2 * rcof(k,1,2,0,8)
    rcof(k,2,3,0,7) = PCx(k) * rcof(k,1,3,0,8) + 1 * rcof(k,0,3,0,8)
    rcof(k,3,3,0,7) = PCx(k) * rcof(k,2,3,0,8) + 2 * rcof(k,1,3,0,8)
    rcof(k,2,0,1,7) = PCx(k) * rcof(k,1,0,1,8) + 1 * rcof(k,0,0,1,8)
    rcof(k,3,0,1,7) = PCx(k) * rcof(k,2,0,1,8) + 2 * rcof(k,1,0,1,8)
    rcof(k,2,1,1,7) = PCx(k) * rcof(k,1,1,1,8) + 1 * rcof(k,0,1,1,8)
    rcof(k,3,1,1,7) = PCx(k) * rcof(k,2,1,1,8) + 2 * rcof(k,1,1,1,8)
    rcof(k,2,2,1,7) = PCx(k) * rcof(k,1,2,1,8) + 1 * rcof(k,0,2,1,8)
    rcof(k,3,2,1,7) = PCx(k) * rcof(k,2,2,1,8) + 2 * rcof(k,1,2,1,8)
    rcof(k,2,3,1,7) = PCx(k) * rcof(k,1,3,1,8) + 1 * rcof(k,0,3,1,8)
    rcof(k,3,3,1,7) = PCx(k) * rcof(k,2,3,1,8) + 2 * rcof(k,1,3,1,8)
    rcof(k,2,0,2,7) = PCx(k) * rcof(k,1,0,2,8) + 1 * rcof(k,0,0,2,8)
    rcof(k,3,0,2,7) = PCx(k) * rcof(k,2,0,2,8) + 2 * rcof(k,1,0,2,8)
    rcof(k,2,1,2,7) = PCx(k) * rcof(k,1,1,2,8) + 1 * rcof(k,0,1,2,8)
    rcof(k,3,1,2,7) = PCx(k) * rcof(k,2,1,2,8) + 2 * rcof(k,1,1,2,8)
    rcof(k,2,2,2,7) = PCx(k) * rcof(k,1,2,2,8) + 1 * rcof(k,0,2,2,8)
    rcof(k,3,2,2,7) = PCx(k) * rcof(k,2,2,2,8) + 2 * rcof(k,1,2,2,8)
    rcof(k,2,3,2,7) = PCx(k) * rcof(k,1,3,2,8) + 1 * rcof(k,0,3,2,8)
    rcof(k,3,3,2,7) = PCx(k) * rcof(k,2,3,2,8) + 2 * rcof(k,1,3,2,8)
    rcof(k,2,0,3,7) = PCx(k) * rcof(k,1,0,3,8) + 1 * rcof(k,0,0,3,8)
    rcof(k,3,0,3,7) = PCx(k) * rcof(k,2,0,3,8) + 2 * rcof(k,1,0,3,8)
    rcof(k,2,1,3,7) = PCx(k) * rcof(k,1,1,3,8) + 1 * rcof(k,0,1,3,8)
    rcof(k,3,1,3,7) = PCx(k) * rcof(k,2,1,3,8) + 2 * rcof(k,1,1,3,8)
    rcof(k,2,2,3,7) = PCx(k) * rcof(k,1,2,3,8) + 1 * rcof(k,0,2,3,8)
    rcof(k,3,2,3,7) = PCx(k) * rcof(k,2,2,3,8) + 2 * rcof(k,1,2,3,8)
    rcof(k,2,3,3,7) = PCx(k) * rcof(k,1,3,3,8) + 1 * rcof(k,0,3,3,8)
    rcof(k,3,3,3,7) = PCx(k) * rcof(k,2,3,3,8) + 2 * rcof(k,1,3,3,8)
    rcof(k,0,0,2,6) = PCz(k) * rcof(k,0,0,1,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,0,3,6) = PCz(k) * rcof(k,0,0,2,7) + 2 * rcof(k,0,0,1,7)
    rcof(k,0,1,0,6) = PCy(k) * rcof(k,0,0,0,7)
    rcof(k,0,1,1,6) = PCy(k) * rcof(k,0,0,1,7)
    rcof(k,0,1,2,6) = PCy(k) * rcof(k,0,0,2,7)
    rcof(k,0,1,3,6) = PCy(k) * rcof(k,0,0,3,7)
    rcof(k,0,2,0,6) = PCy(k) * rcof(k,0,1,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,0,3,0,6) = PCy(k) * rcof(k,0,2,0,7) + 2 * rcof(k,0,1,0,7)
    rcof(k,0,2,1,6) = PCy(k) * rcof(k,0,1,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,0,3,1,6) = PCy(k) * rcof(k,0,2,1,7) + 2 * rcof(k,0,1,1,7)
    rcof(k,0,2,2,6) = PCy(k) * rcof(k,0,1,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,0,3,2,6) = PCy(k) * rcof(k,0,2,2,7) + 2 * rcof(k,0,1,2,7)
    rcof(k,0,2,3,6) = PCy(k) * rcof(k,0,1,3,7) + 1 * rcof(k,0,0,3,7)
    rcof(k,0,3,3,6) = PCy(k) * rcof(k,0,2,3,7) + 2 * rcof(k,0,1,3,7)
    rcof(k,1,0,0,6) = PCx(k) * rcof(k,0,0,0,7)
    rcof(k,1,1,0,6) = PCx(k) * rcof(k,0,1,0,7)
    rcof(k,1,2,0,6) = PCx(k) * rcof(k,0,2,0,7)
    rcof(k,1,3,0,6) = PCx(k) * rcof(k,0,3,0,7)
    rcof(k,1,0,1,6) = PCx(k) * rcof(k,0,0,1,7)
    rcof(k,1,1,1,6) = PCx(k) * rcof(k,0,1,1,7)
    rcof(k,1,2,1,6) = PCx(k) * rcof(k,0,2,1,7)
    rcof(k,1,3,1,6) = PCx(k) * rcof(k,0,3,1,7)
    rcof(k,1,0,2,6) = PCx(k) * rcof(k,0,0,2,7)
    rcof(k,1,1,2,6) = PCx(k) * rcof(k,0,1,2,7)
    rcof(k,1,2,2,6) = PCx(k) * rcof(k,0,2,2,7)
    rcof(k,1,3,2,6) = PCx(k) * rcof(k,0,3,2,7)
    rcof(k,1,0,3,6) = PCx(k) * rcof(k,0,0,3,7)
    rcof(k,1,1,3,6) = PCx(k) * rcof(k,0,1,3,7)
    rcof(k,1,2,3,6) = PCx(k) * rcof(k,0,2,3,7)
    rcof(k,1,3,3,6) = PCx(k) * rcof(k,0,3,3,7)
    rcof(k,2,0,0,6) = PCx(k) * rcof(k,1,0,0,7) + 1 * rcof(k,0,0,0,7)
    rcof(k,3,0,0,6) = PCx(k) * rcof(k,2,0,0,7) + 2 * rcof(k,1,0,0,7)
    rcof(k,2,1,0,6) = PCx(k) * rcof(k,1,1,0,7) + 1 * rcof(k,0,1,0,7)
    rcof(k,3,1,0,6) = PCx(k) * rcof(k,2,1,0,7) + 2 * rcof(k,1,1,0,7)
    rcof(k,2,2,0,6) = PCx(k) * rcof(k,1,2,0,7) + 1 * rcof(k,0,2,0,7)
    rcof(k,3,2,0,6) = PCx(k) * rcof(k,2,2,0,7) + 2 * rcof(k,1,2,0,7)
    rcof(k,2,3,0,6) = PCx(k) * rcof(k,1,3,0,7) + 1 * rcof(k,0,3,0,7)
    rcof(k,3,3,0,6) = PCx(k) * rcof(k,2,3,0,7) + 2 * rcof(k,1,3,0,7)
    rcof(k,2,0,1,6) = PCx(k) * rcof(k,1,0,1,7) + 1 * rcof(k,0,0,1,7)
    rcof(k,3,0,1,6) = PCx(k) * rcof(k,2,0,1,7) + 2 * rcof(k,1,0,1,7)
    rcof(k,2,1,1,6) = PCx(k) * rcof(k,1,1,1,7) + 1 * rcof(k,0,1,1,7)
    rcof(k,3,1,1,6) = PCx(k) * rcof(k,2,1,1,7) + 2 * rcof(k,1,1,1,7)
    rcof(k,2,2,1,6) = PCx(k) * rcof(k,1,2,1,7) + 1 * rcof(k,0,2,1,7)
    rcof(k,3,2,1,6) = PCx(k) * rcof(k,2,2,1,7) + 2 * rcof(k,1,2,1,7)
    rcof(k,2,3,1,6) = PCx(k) * rcof(k,1,3,1,7) + 1 * rcof(k,0,3,1,7)
    rcof(k,3,3,1,6) = PCx(k) * rcof(k,2,3,1,7) + 2 * rcof(k,1,3,1,7)
    rcof(k,2,0,2,6) = PCx(k) * rcof(k,1,0,2,7) + 1 * rcof(k,0,0,2,7)
    rcof(k,3,0,2,6) = PCx(k) * rcof(k,2,0,2,7) + 2 * rcof(k,1,0,2,7)
    rcof(k,2,1,2,6) = PCx(k) * rcof(k,1,1,2,7) + 1 * rcof(k,0,1,2,7)
    rcof(k,3,1,2,6) = PCx(k) * rcof(k,2,1,2,7) + 2 * rcof(k,1,1,2,7)
    rcof(k,2,2,2,6) = PCx(k) * rcof(k,1,2,2,7) + 1 * rcof(k,0,2,2,7)
    rcof(k,3,2,2,6) = PCx(k) * rcof(k,2,2,2,7) + 2 * rcof(k,1,2,2,7)
    rcof(k,2,3,2,6) = PCx(k) * rcof(k,1,3,2,7) + 1 * rcof(k,0,3,2,7)
    rcof(k,3,3,2,6) = PCx(k) * rcof(k,2,3,2,7) + 2 * rcof(k,1,3,2,7)
    rcof(k,2,0,3,6) = PCx(k) * rcof(k,1,0,3,7) + 1 * rcof(k,0,0,3,7)
    rcof(k,3,0,3,6) = PCx(k) * rcof(k,2,0,3,7) + 2 * rcof(k,1,0,3,7)
    rcof(k,2,1,3,6) = PCx(k) * rcof(k,1,1,3,7) + 1 * rcof(k,0,1,3,7)
    rcof(k,3,1,3,6) = PCx(k) * rcof(k,2,1,3,7) + 2 * rcof(k,1,1,3,7)
    rcof(k,2,2,3,6) = PCx(k) * rcof(k,1,2,3,7) + 1 * rcof(k,0,2,3,7)
    rcof(k,3,2,3,6) = PCx(k) * rcof(k,2,2,3,7) + 2 * rcof(k,1,2,3,7)
    rcof(k,2,3,3,6) = PCx(k) * rcof(k,1,3,3,7) + 1 * rcof(k,0,3,3,7)
    rcof(k,3,3,3,6) = PCx(k) * rcof(k,2,3,3,7) + 2 * rcof(k,1,3,3,7)
    rcof(k,0,0,2,5) = PCz(k) * rcof(k,0,0,1,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,0,3,5) = PCz(k) * rcof(k,0,0,2,6) + 2 * rcof(k,0,0,1,6)
    rcof(k,0,1,0,5) = PCy(k) * rcof(k,0,0,0,6)
    rcof(k,0,1,1,5) = PCy(k) * rcof(k,0,0,1,6)
    rcof(k,0,1,2,5) = PCy(k) * rcof(k,0,0,2,6)
    rcof(k,0,1,3,5) = PCy(k) * rcof(k,0,0,3,6)
    rcof(k,0,2,0,5) = PCy(k) * rcof(k,0,1,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,0,3,0,5) = PCy(k) * rcof(k,0,2,0,6) + 2 * rcof(k,0,1,0,6)
    rcof(k,0,2,1,5) = PCy(k) * rcof(k,0,1,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,0,3,1,5) = PCy(k) * rcof(k,0,2,1,6) + 2 * rcof(k,0,1,1,6)
    rcof(k,0,2,2,5) = PCy(k) * rcof(k,0,1,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,0,3,2,5) = PCy(k) * rcof(k,0,2,2,6) + 2 * rcof(k,0,1,2,6)
    rcof(k,0,2,3,5) = PCy(k) * rcof(k,0,1,3,6) + 1 * rcof(k,0,0,3,6)
    rcof(k,0,3,3,5) = PCy(k) * rcof(k,0,2,3,6) + 2 * rcof(k,0,1,3,6)
    rcof(k,1,0,0,5) = PCx(k) * rcof(k,0,0,0,6)
    rcof(k,1,1,0,5) = PCx(k) * rcof(k,0,1,0,6)
    rcof(k,1,2,0,5) = PCx(k) * rcof(k,0,2,0,6)
    rcof(k,1,3,0,5) = PCx(k) * rcof(k,0,3,0,6)
    rcof(k,1,0,1,5) = PCx(k) * rcof(k,0,0,1,6)
    rcof(k,1,1,1,5) = PCx(k) * rcof(k,0,1,1,6)
    rcof(k,1,2,1,5) = PCx(k) * rcof(k,0,2,1,6)
    rcof(k,1,3,1,5) = PCx(k) * rcof(k,0,3,1,6)
    rcof(k,1,0,2,5) = PCx(k) * rcof(k,0,0,2,6)
    rcof(k,1,1,2,5) = PCx(k) * rcof(k,0,1,2,6)
    rcof(k,1,2,2,5) = PCx(k) * rcof(k,0,2,2,6)
    rcof(k,1,3,2,5) = PCx(k) * rcof(k,0,3,2,6)
    rcof(k,1,0,3,5) = PCx(k) * rcof(k,0,0,3,6)
    rcof(k,1,1,3,5) = PCx(k) * rcof(k,0,1,3,6)
    rcof(k,1,2,3,5) = PCx(k) * rcof(k,0,2,3,6)
    rcof(k,1,3,3,5) = PCx(k) * rcof(k,0,3,3,6)
    rcof(k,2,0,0,5) = PCx(k) * rcof(k,1,0,0,6) + 1 * rcof(k,0,0,0,6)
    rcof(k,3,0,0,5) = PCx(k) * rcof(k,2,0,0,6) + 2 * rcof(k,1,0,0,6)
    rcof(k,2,1,0,5) = PCx(k) * rcof(k,1,1,0,6) + 1 * rcof(k,0,1,0,6)
    rcof(k,3,1,0,5) = PCx(k) * rcof(k,2,1,0,6) + 2 * rcof(k,1,1,0,6)
    rcof(k,2,2,0,5) = PCx(k) * rcof(k,1,2,0,6) + 1 * rcof(k,0,2,0,6)
    rcof(k,3,2,0,5) = PCx(k) * rcof(k,2,2,0,6) + 2 * rcof(k,1,2,0,6)
    rcof(k,2,3,0,5) = PCx(k) * rcof(k,1,3,0,6) + 1 * rcof(k,0,3,0,6)
    rcof(k,3,3,0,5) = PCx(k) * rcof(k,2,3,0,6) + 2 * rcof(k,1,3,0,6)
    rcof(k,2,0,1,5) = PCx(k) * rcof(k,1,0,1,6) + 1 * rcof(k,0,0,1,6)
    rcof(k,3,0,1,5) = PCx(k) * rcof(k,2,0,1,6) + 2 * rcof(k,1,0,1,6)
    rcof(k,2,1,1,5) = PCx(k) * rcof(k,1,1,1,6) + 1 * rcof(k,0,1,1,6)
    rcof(k,3,1,1,5) = PCx(k) * rcof(k,2,1,1,6) + 2 * rcof(k,1,1,1,6)
    rcof(k,2,2,1,5) = PCx(k) * rcof(k,1,2,1,6) + 1 * rcof(k,0,2,1,6)
    rcof(k,3,2,1,5) = PCx(k) * rcof(k,2,2,1,6) + 2 * rcof(k,1,2,1,6)
    rcof(k,2,3,1,5) = PCx(k) * rcof(k,1,3,1,6) + 1 * rcof(k,0,3,1,6)
    rcof(k,3,3,1,5) = PCx(k) * rcof(k,2,3,1,6) + 2 * rcof(k,1,3,1,6)
    rcof(k,2,0,2,5) = PCx(k) * rcof(k,1,0,2,6) + 1 * rcof(k,0,0,2,6)
    rcof(k,3,0,2,5) = PCx(k) * rcof(k,2,0,2,6) + 2 * rcof(k,1,0,2,6)
    rcof(k,2,1,2,5) = PCx(k) * rcof(k,1,1,2,6) + 1 * rcof(k,0,1,2,6)
    rcof(k,3,1,2,5) = PCx(k) * rcof(k,2,1,2,6) + 2 * rcof(k,1,1,2,6)
    rcof(k,2,2,2,5) = PCx(k) * rcof(k,1,2,2,6) + 1 * rcof(k,0,2,2,6)
    rcof(k,3,2,2,5) = PCx(k) * rcof(k,2,2,2,6) + 2 * rcof(k,1,2,2,6)
    rcof(k,2,3,2,5) = PCx(k) * rcof(k,1,3,2,6) + 1 * rcof(k,0,3,2,6)
    rcof(k,3,3,2,5) = PCx(k) * rcof(k,2,3,2,6) + 2 * rcof(k,1,3,2,6)
    rcof(k,2,0,3,5) = PCx(k) * rcof(k,1,0,3,6) + 1 * rcof(k,0,0,3,6)
    rcof(k,3,0,3,5) = PCx(k) * rcof(k,2,0,3,6) + 2 * rcof(k,1,0,3,6)
    rcof(k,2,1,3,5) = PCx(k) * rcof(k,1,1,3,6) + 1 * rcof(k,0,1,3,6)
    rcof(k,3,1,3,5) = PCx(k) * rcof(k,2,1,3,6) + 2 * rcof(k,1,1,3,6)
    rcof(k,2,2,3,5) = PCx(k) * rcof(k,1,2,3,6) + 1 * rcof(k,0,2,3,6)
    rcof(k,3,2,3,5) = PCx(k) * rcof(k,2,2,3,6) + 2 * rcof(k,1,2,3,6)
    rcof(k,2,3,3,5) = PCx(k) * rcof(k,1,3,3,6) + 1 * rcof(k,0,3,3,6)
    rcof(k,3,3,3,5) = PCx(k) * rcof(k,2,3,3,6) + 2 * rcof(k,1,3,3,6)
    rcof(k,0,0,2,4) = PCz(k) * rcof(k,0,0,1,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,0,3,4) = PCz(k) * rcof(k,0,0,2,5) + 2 * rcof(k,0,0,1,5)
    rcof(k,0,1,0,4) = PCy(k) * rcof(k,0,0,0,5)
    rcof(k,0,1,1,4) = PCy(k) * rcof(k,0,0,1,5)
    rcof(k,0,1,2,4) = PCy(k) * rcof(k,0,0,2,5)
    rcof(k,0,1,3,4) = PCy(k) * rcof(k,0,0,3,5)
    rcof(k,0,2,0,4) = PCy(k) * rcof(k,0,1,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,0,3,0,4) = PCy(k) * rcof(k,0,2,0,5) + 2 * rcof(k,0,1,0,5)
    rcof(k,0,2,1,4) = PCy(k) * rcof(k,0,1,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,0,3,1,4) = PCy(k) * rcof(k,0,2,1,5) + 2 * rcof(k,0,1,1,5)
    rcof(k,0,2,2,4) = PCy(k) * rcof(k,0,1,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,0,3,2,4) = PCy(k) * rcof(k,0,2,2,5) + 2 * rcof(k,0,1,2,5)
    rcof(k,0,2,3,4) = PCy(k) * rcof(k,0,1,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,0,3,3,4) = PCy(k) * rcof(k,0,2,3,5) + 2 * rcof(k,0,1,3,5)
    rcof(k,1,0,0,4) = PCx(k) * rcof(k,0,0,0,5)
    rcof(k,1,1,0,4) = PCx(k) * rcof(k,0,1,0,5)
    rcof(k,1,2,0,4) = PCx(k) * rcof(k,0,2,0,5)
    rcof(k,1,3,0,4) = PCx(k) * rcof(k,0,3,0,5)
    rcof(k,1,0,1,4) = PCx(k) * rcof(k,0,0,1,5)
    rcof(k,1,1,1,4) = PCx(k) * rcof(k,0,1,1,5)
    rcof(k,1,2,1,4) = PCx(k) * rcof(k,0,2,1,5)
    rcof(k,1,3,1,4) = PCx(k) * rcof(k,0,3,1,5)
    rcof(k,1,0,2,4) = PCx(k) * rcof(k,0,0,2,5)
    rcof(k,1,1,2,4) = PCx(k) * rcof(k,0,1,2,5)
    rcof(k,1,2,2,4) = PCx(k) * rcof(k,0,2,2,5)
    rcof(k,1,3,2,4) = PCx(k) * rcof(k,0,3,2,5)
    rcof(k,1,0,3,4) = PCx(k) * rcof(k,0,0,3,5)
    rcof(k,1,1,3,4) = PCx(k) * rcof(k,0,1,3,5)
    rcof(k,1,2,3,4) = PCx(k) * rcof(k,0,2,3,5)
    rcof(k,1,3,3,4) = PCx(k) * rcof(k,0,3,3,5)
    rcof(k,2,0,0,4) = PCx(k) * rcof(k,1,0,0,5) + 1 * rcof(k,0,0,0,5)
    rcof(k,3,0,0,4) = PCx(k) * rcof(k,2,0,0,5) + 2 * rcof(k,1,0,0,5)
    rcof(k,2,1,0,4) = PCx(k) * rcof(k,1,1,0,5) + 1 * rcof(k,0,1,0,5)
    rcof(k,3,1,0,4) = PCx(k) * rcof(k,2,1,0,5) + 2 * rcof(k,1,1,0,5)
    rcof(k,2,2,0,4) = PCx(k) * rcof(k,1,2,0,5) + 1 * rcof(k,0,2,0,5)
    rcof(k,3,2,0,4) = PCx(k) * rcof(k,2,2,0,5) + 2 * rcof(k,1,2,0,5)
    rcof(k,2,3,0,4) = PCx(k) * rcof(k,1,3,0,5) + 1 * rcof(k,0,3,0,5)
    rcof(k,3,3,0,4) = PCx(k) * rcof(k,2,3,0,5) + 2 * rcof(k,1,3,0,5)
    rcof(k,2,0,1,4) = PCx(k) * rcof(k,1,0,1,5) + 1 * rcof(k,0,0,1,5)
    rcof(k,3,0,1,4) = PCx(k) * rcof(k,2,0,1,5) + 2 * rcof(k,1,0,1,5)
    rcof(k,2,1,1,4) = PCx(k) * rcof(k,1,1,1,5) + 1 * rcof(k,0,1,1,5)
    rcof(k,3,1,1,4) = PCx(k) * rcof(k,2,1,1,5) + 2 * rcof(k,1,1,1,5)
    rcof(k,2,2,1,4) = PCx(k) * rcof(k,1,2,1,5) + 1 * rcof(k,0,2,1,5)
    rcof(k,3,2,1,4) = PCx(k) * rcof(k,2,2,1,5) + 2 * rcof(k,1,2,1,5)
    rcof(k,2,3,1,4) = PCx(k) * rcof(k,1,3,1,5) + 1 * rcof(k,0,3,1,5)
    rcof(k,3,3,1,4) = PCx(k) * rcof(k,2,3,1,5) + 2 * rcof(k,1,3,1,5)
    rcof(k,2,0,2,4) = PCx(k) * rcof(k,1,0,2,5) + 1 * rcof(k,0,0,2,5)
    rcof(k,3,0,2,4) = PCx(k) * rcof(k,2,0,2,5) + 2 * rcof(k,1,0,2,5)
    rcof(k,2,1,2,4) = PCx(k) * rcof(k,1,1,2,5) + 1 * rcof(k,0,1,2,5)
    rcof(k,3,1,2,4) = PCx(k) * rcof(k,2,1,2,5) + 2 * rcof(k,1,1,2,5)
    rcof(k,2,2,2,4) = PCx(k) * rcof(k,1,2,2,5) + 1 * rcof(k,0,2,2,5)
    rcof(k,3,2,2,4) = PCx(k) * rcof(k,2,2,2,5) + 2 * rcof(k,1,2,2,5)
    rcof(k,2,3,2,4) = PCx(k) * rcof(k,1,3,2,5) + 1 * rcof(k,0,3,2,5)
    rcof(k,3,3,2,4) = PCx(k) * rcof(k,2,3,2,5) + 2 * rcof(k,1,3,2,5)
    rcof(k,2,0,3,4) = PCx(k) * rcof(k,1,0,3,5) + 1 * rcof(k,0,0,3,5)
    rcof(k,3,0,3,4) = PCx(k) * rcof(k,2,0,3,5) + 2 * rcof(k,1,0,3,5)
    rcof(k,2,1,3,4) = PCx(k) * rcof(k,1,1,3,5) + 1 * rcof(k,0,1,3,5)
    rcof(k,3,1,3,4) = PCx(k) * rcof(k,2,1,3,5) + 2 * rcof(k,1,1,3,5)
    rcof(k,2,2,3,4) = PCx(k) * rcof(k,1,2,3,5) + 1 * rcof(k,0,2,3,5)
    rcof(k,3,2,3,4) = PCx(k) * rcof(k,2,2,3,5) + 2 * rcof(k,1,2,3,5)
    rcof(k,2,3,3,4) = PCx(k) * rcof(k,1,3,3,5) + 1 * rcof(k,0,3,3,5)
    rcof(k,3,3,3,4) = PCx(k) * rcof(k,2,3,3,5) + 2 * rcof(k,1,3,3,5)
    rcof(k,0,0,2,3) = PCz(k) * rcof(k,0,0,1,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,0,3,3) = PCz(k) * rcof(k,0,0,2,4) + 2 * rcof(k,0,0,1,4)
    rcof(k,0,1,0,3) = PCy(k) * rcof(k,0,0,0,4)
    rcof(k,0,1,1,3) = PCy(k) * rcof(k,0,0,1,4)
    rcof(k,0,1,2,3) = PCy(k) * rcof(k,0,0,2,4)
    rcof(k,0,1,3,3) = PCy(k) * rcof(k,0,0,3,4)
    rcof(k,0,2,0,3) = PCy(k) * rcof(k,0,1,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,0,3,0,3) = PCy(k) * rcof(k,0,2,0,4) + 2 * rcof(k,0,1,0,4)
    rcof(k,0,2,1,3) = PCy(k) * rcof(k,0,1,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,0,3,1,3) = PCy(k) * rcof(k,0,2,1,4) + 2 * rcof(k,0,1,1,4)
    rcof(k,0,2,2,3) = PCy(k) * rcof(k,0,1,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,0,3,2,3) = PCy(k) * rcof(k,0,2,2,4) + 2 * rcof(k,0,1,2,4)
    rcof(k,0,2,3,3) = PCy(k) * rcof(k,0,1,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,0,3,3,3) = PCy(k) * rcof(k,0,2,3,4) + 2 * rcof(k,0,1,3,4)
    rcof(k,1,0,0,3) = PCx(k) * rcof(k,0,0,0,4)
    rcof(k,1,1,0,3) = PCx(k) * rcof(k,0,1,0,4)
    rcof(k,1,2,0,3) = PCx(k) * rcof(k,0,2,0,4)
    rcof(k,1,3,0,3) = PCx(k) * rcof(k,0,3,0,4)
    rcof(k,1,0,1,3) = PCx(k) * rcof(k,0,0,1,4)
    rcof(k,1,1,1,3) = PCx(k) * rcof(k,0,1,1,4)
    rcof(k,1,2,1,3) = PCx(k) * rcof(k,0,2,1,4)
    rcof(k,1,3,1,3) = PCx(k) * rcof(k,0,3,1,4)
    rcof(k,1,0,2,3) = PCx(k) * rcof(k,0,0,2,4)
    rcof(k,1,1,2,3) = PCx(k) * rcof(k,0,1,2,4)
    rcof(k,1,2,2,3) = PCx(k) * rcof(k,0,2,2,4)
    rcof(k,1,3,2,3) = PCx(k) * rcof(k,0,3,2,4)
    rcof(k,1,0,3,3) = PCx(k) * rcof(k,0,0,3,4)
    rcof(k,1,1,3,3) = PCx(k) * rcof(k,0,1,3,4)
    rcof(k,1,2,3,3) = PCx(k) * rcof(k,0,2,3,4)
    rcof(k,1,3,3,3) = PCx(k) * rcof(k,0,3,3,4)
    rcof(k,2,0,0,3) = PCx(k) * rcof(k,1,0,0,4) + 1 * rcof(k,0,0,0,4)
    rcof(k,3,0,0,3) = PCx(k) * rcof(k,2,0,0,4) + 2 * rcof(k,1,0,0,4)
    rcof(k,2,1,0,3) = PCx(k) * rcof(k,1,1,0,4) + 1 * rcof(k,0,1,0,4)
    rcof(k,3,1,0,3) = PCx(k) * rcof(k,2,1,0,4) + 2 * rcof(k,1,1,0,4)
    rcof(k,2,2,0,3) = PCx(k) * rcof(k,1,2,0,4) + 1 * rcof(k,0,2,0,4)
    rcof(k,3,2,0,3) = PCx(k) * rcof(k,2,2,0,4) + 2 * rcof(k,1,2,0,4)
    rcof(k,2,3,0,3) = PCx(k) * rcof(k,1,3,0,4) + 1 * rcof(k,0,3,0,4)
    rcof(k,3,3,0,3) = PCx(k) * rcof(k,2,3,0,4) + 2 * rcof(k,1,3,0,4)
    rcof(k,2,0,1,3) = PCx(k) * rcof(k,1,0,1,4) + 1 * rcof(k,0,0,1,4)
    rcof(k,3,0,1,3) = PCx(k) * rcof(k,2,0,1,4) + 2 * rcof(k,1,0,1,4)
    rcof(k,2,1,1,3) = PCx(k) * rcof(k,1,1,1,4) + 1 * rcof(k,0,1,1,4)
    rcof(k,3,1,1,3) = PCx(k) * rcof(k,2,1,1,4) + 2 * rcof(k,1,1,1,4)
    rcof(k,2,2,1,3) = PCx(k) * rcof(k,1,2,1,4) + 1 * rcof(k,0,2,1,4)
    rcof(k,3,2,1,3) = PCx(k) * rcof(k,2,2,1,4) + 2 * rcof(k,1,2,1,4)
    rcof(k,2,3,1,3) = PCx(k) * rcof(k,1,3,1,4) + 1 * rcof(k,0,3,1,4)
    rcof(k,3,3,1,3) = PCx(k) * rcof(k,2,3,1,4) + 2 * rcof(k,1,3,1,4)
    rcof(k,2,0,2,3) = PCx(k) * rcof(k,1,0,2,4) + 1 * rcof(k,0,0,2,4)
    rcof(k,3,0,2,3) = PCx(k) * rcof(k,2,0,2,4) + 2 * rcof(k,1,0,2,4)
    rcof(k,2,1,2,3) = PCx(k) * rcof(k,1,1,2,4) + 1 * rcof(k,0,1,2,4)
    rcof(k,3,1,2,3) = PCx(k) * rcof(k,2,1,2,4) + 2 * rcof(k,1,1,2,4)
    rcof(k,2,2,2,3) = PCx(k) * rcof(k,1,2,2,4) + 1 * rcof(k,0,2,2,4)
    rcof(k,3,2,2,3) = PCx(k) * rcof(k,2,2,2,4) + 2 * rcof(k,1,2,2,4)
    rcof(k,2,3,2,3) = PCx(k) * rcof(k,1,3,2,4) + 1 * rcof(k,0,3,2,4)
    rcof(k,3,3,2,3) = PCx(k) * rcof(k,2,3,2,4) + 2 * rcof(k,1,3,2,4)
    rcof(k,2,0,3,3) = PCx(k) * rcof(k,1,0,3,4) + 1 * rcof(k,0,0,3,4)
    rcof(k,3,0,3,3) = PCx(k) * rcof(k,2,0,3,4) + 2 * rcof(k,1,0,3,4)
    rcof(k,2,1,3,3) = PCx(k) * rcof(k,1,1,3,4) + 1 * rcof(k,0,1,3,4)
    rcof(k,3,1,3,3) = PCx(k) * rcof(k,2,1,3,4) + 2 * rcof(k,1,1,3,4)
    rcof(k,2,2,3,3) = PCx(k) * rcof(k,1,2,3,4) + 1 * rcof(k,0,2,3,4)
    rcof(k,3,2,3,3) = PCx(k) * rcof(k,2,2,3,4) + 2 * rcof(k,1,2,3,4)
    rcof(k,2,3,3,3) = PCx(k) * rcof(k,1,3,3,4) + 1 * rcof(k,0,3,3,4)
    rcof(k,3,3,3,3) = PCx(k) * rcof(k,2,3,3,4) + 2 * rcof(k,1,3,3,4)
    rcof(k,0,0,2,2) = PCz(k) * rcof(k,0,0,1,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,0,3,2) = PCz(k) * rcof(k,0,0,2,3) + 2 * rcof(k,0,0,1,3)
    rcof(k,0,1,0,2) = PCy(k) * rcof(k,0,0,0,3)
    rcof(k,0,1,1,2) = PCy(k) * rcof(k,0,0,1,3)
    rcof(k,0,1,2,2) = PCy(k) * rcof(k,0,0,2,3)
    rcof(k,0,1,3,2) = PCy(k) * rcof(k,0,0,3,3)
    rcof(k,0,2,0,2) = PCy(k) * rcof(k,0,1,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,0,3,0,2) = PCy(k) * rcof(k,0,2,0,3) + 2 * rcof(k,0,1,0,3)
    rcof(k,0,2,1,2) = PCy(k) * rcof(k,0,1,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,0,3,1,2) = PCy(k) * rcof(k,0,2,1,3) + 2 * rcof(k,0,1,1,3)
    rcof(k,0,2,2,2) = PCy(k) * rcof(k,0,1,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,0,3,2,2) = PCy(k) * rcof(k,0,2,2,3) + 2 * rcof(k,0,1,2,3)
    rcof(k,0,2,3,2) = PCy(k) * rcof(k,0,1,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,0,3,3,2) = PCy(k) * rcof(k,0,2,3,3) + 2 * rcof(k,0,1,3,3)
    rcof(k,1,0,0,2) = PCx(k) * rcof(k,0,0,0,3)
    rcof(k,1,1,0,2) = PCx(k) * rcof(k,0,1,0,3)
    rcof(k,1,2,0,2) = PCx(k) * rcof(k,0,2,0,3)
    rcof(k,1,3,0,2) = PCx(k) * rcof(k,0,3,0,3)
    rcof(k,1,0,1,2) = PCx(k) * rcof(k,0,0,1,3)
    rcof(k,1,1,1,2) = PCx(k) * rcof(k,0,1,1,3)
    rcof(k,1,2,1,2) = PCx(k) * rcof(k,0,2,1,3)
    rcof(k,1,3,1,2) = PCx(k) * rcof(k,0,3,1,3)
    rcof(k,1,0,2,2) = PCx(k) * rcof(k,0,0,2,3)
    rcof(k,1,1,2,2) = PCx(k) * rcof(k,0,1,2,3)
    rcof(k,1,2,2,2) = PCx(k) * rcof(k,0,2,2,3)
    rcof(k,1,3,2,2) = PCx(k) * rcof(k,0,3,2,3)
    rcof(k,1,0,3,2) = PCx(k) * rcof(k,0,0,3,3)
    rcof(k,1,1,3,2) = PCx(k) * rcof(k,0,1,3,3)
    rcof(k,1,2,3,2) = PCx(k) * rcof(k,0,2,3,3)
    rcof(k,1,3,3,2) = PCx(k) * rcof(k,0,3,3,3)
    rcof(k,2,0,0,2) = PCx(k) * rcof(k,1,0,0,3) + 1 * rcof(k,0,0,0,3)
    rcof(k,3,0,0,2) = PCx(k) * rcof(k,2,0,0,3) + 2 * rcof(k,1,0,0,3)
    rcof(k,2,1,0,2) = PCx(k) * rcof(k,1,1,0,3) + 1 * rcof(k,0,1,0,3)
    rcof(k,3,1,0,2) = PCx(k) * rcof(k,2,1,0,3) + 2 * rcof(k,1,1,0,3)
    rcof(k,2,2,0,2) = PCx(k) * rcof(k,1,2,0,3) + 1 * rcof(k,0,2,0,3)
    rcof(k,3,2,0,2) = PCx(k) * rcof(k,2,2,0,3) + 2 * rcof(k,1,2,0,3)
    rcof(k,2,3,0,2) = PCx(k) * rcof(k,1,3,0,3) + 1 * rcof(k,0,3,0,3)
    rcof(k,3,3,0,2) = PCx(k) * rcof(k,2,3,0,3) + 2 * rcof(k,1,3,0,3)
    rcof(k,2,0,1,2) = PCx(k) * rcof(k,1,0,1,3) + 1 * rcof(k,0,0,1,3)
    rcof(k,3,0,1,2) = PCx(k) * rcof(k,2,0,1,3) + 2 * rcof(k,1,0,1,3)
    rcof(k,2,1,1,2) = PCx(k) * rcof(k,1,1,1,3) + 1 * rcof(k,0,1,1,3)
    rcof(k,3,1,1,2) = PCx(k) * rcof(k,2,1,1,3) + 2 * rcof(k,1,1,1,3)
    rcof(k,2,2,1,2) = PCx(k) * rcof(k,1,2,1,3) + 1 * rcof(k,0,2,1,3)
    rcof(k,3,2,1,2) = PCx(k) * rcof(k,2,2,1,3) + 2 * rcof(k,1,2,1,3)
    rcof(k,2,3,1,2) = PCx(k) * rcof(k,1,3,1,3) + 1 * rcof(k,0,3,1,3)
    rcof(k,3,3,1,2) = PCx(k) * rcof(k,2,3,1,3) + 2 * rcof(k,1,3,1,3)
    rcof(k,2,0,2,2) = PCx(k) * rcof(k,1,0,2,3) + 1 * rcof(k,0,0,2,3)
    rcof(k,3,0,2,2) = PCx(k) * rcof(k,2,0,2,3) + 2 * rcof(k,1,0,2,3)
    rcof(k,2,1,2,2) = PCx(k) * rcof(k,1,1,2,3) + 1 * rcof(k,0,1,2,3)
    rcof(k,3,1,2,2) = PCx(k) * rcof(k,2,1,2,3) + 2 * rcof(k,1,1,2,3)
    rcof(k,2,2,2,2) = PCx(k) * rcof(k,1,2,2,3) + 1 * rcof(k,0,2,2,3)
    rcof(k,3,2,2,2) = PCx(k) * rcof(k,2,2,2,3) + 2 * rcof(k,1,2,2,3)
    rcof(k,2,3,2,2) = PCx(k) * rcof(k,1,3,2,3) + 1 * rcof(k,0,3,2,3)
    rcof(k,3,3,2,2) = PCx(k) * rcof(k,2,3,2,3) + 2 * rcof(k,1,3,2,3)
    rcof(k,2,0,3,2) = PCx(k) * rcof(k,1,0,3,3) + 1 * rcof(k,0,0,3,3)
    rcof(k,3,0,3,2) = PCx(k) * rcof(k,2,0,3,3) + 2 * rcof(k,1,0,3,3)
    rcof(k,2,1,3,2) = PCx(k) * rcof(k,1,1,3,3) + 1 * rcof(k,0,1,3,3)
    rcof(k,3,1,3,2) = PCx(k) * rcof(k,2,1,3,3) + 2 * rcof(k,1,1,3,3)
    rcof(k,2,2,3,2) = PCx(k) * rcof(k,1,2,3,3) + 1 * rcof(k,0,2,3,3)
    rcof(k,3,2,3,2) = PCx(k) * rcof(k,2,2,3,3) + 2 * rcof(k,1,2,3,3)
    rcof(k,2,3,3,2) = PCx(k) * rcof(k,1,3,3,3) + 1 * rcof(k,0,3,3,3)
    rcof(k,3,3,3,2) = PCx(k) * rcof(k,2,3,3,3) + 2 * rcof(k,1,3,3,3)
    rcof(k,0,0,2,1) = PCz(k) * rcof(k,0,0,1,2) + 1 * r0002 
    rcof(k,0,0,3,1) = PCz(k) * rcof(k,0,0,2,2) + 2 * rcof(k,0,0,1,2)
    rcof(k,0,1,0,1) = PCy(k) * r0002 
    rcof(k,0,1,1,1) = PCy(k) * rcof(k,0,0,1,2)
    rcof(k,0,1,2,1) = PCy(k) * rcof(k,0,0,2,2)
    rcof(k,0,1,3,1) = PCy(k) * rcof(k,0,0,3,2)
    rcof(k,0,2,0,1) = PCy(k) * rcof(k,0,1,0,2) + 1 * r0002 
    rcof(k,0,3,0,1) = PCy(k) * rcof(k,0,2,0,2) + 2 * rcof(k,0,1,0,2)
    rcof(k,0,2,1,1) = PCy(k) * rcof(k,0,1,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,0,3,1,1) = PCy(k) * rcof(k,0,2,1,2) + 2 * rcof(k,0,1,1,2)
    rcof(k,0,2,2,1) = PCy(k) * rcof(k,0,1,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,0,3,2,1) = PCy(k) * rcof(k,0,2,2,2) + 2 * rcof(k,0,1,2,2)
    rcof(k,0,2,3,1) = PCy(k) * rcof(k,0,1,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,0,3,3,1) = PCy(k) * rcof(k,0,2,3,2) + 2 * rcof(k,0,1,3,2)
    rcof(k,1,0,0,1) = PCx(k) * r0002 
    rcof(k,1,1,0,1) = PCx(k) * rcof(k,0,1,0,2)
    rcof(k,1,2,0,1) = PCx(k) * rcof(k,0,2,0,2)
    rcof(k,1,3,0,1) = PCx(k) * rcof(k,0,3,0,2)
    rcof(k,1,0,1,1) = PCx(k) * rcof(k,0,0,1,2)
    rcof(k,1,1,1,1) = PCx(k) * rcof(k,0,1,1,2)
    rcof(k,1,2,1,1) = PCx(k) * rcof(k,0,2,1,2)
    rcof(k,1,3,1,1) = PCx(k) * rcof(k,0,3,1,2)
    rcof(k,1,0,2,1) = PCx(k) * rcof(k,0,0,2,2)
    rcof(k,1,1,2,1) = PCx(k) * rcof(k,0,1,2,2)
    rcof(k,1,2,2,1) = PCx(k) * rcof(k,0,2,2,2)
    rcof(k,1,3,2,1) = PCx(k) * rcof(k,0,3,2,2)
    rcof(k,1,0,3,1) = PCx(k) * rcof(k,0,0,3,2)
    rcof(k,1,1,3,1) = PCx(k) * rcof(k,0,1,3,2)
    rcof(k,1,2,3,1) = PCx(k) * rcof(k,0,2,3,2)
    rcof(k,1,3,3,1) = PCx(k) * rcof(k,0,3,3,2)
    rcof(k,2,0,0,1) = PCx(k) * rcof(k,1,0,0,2) + 1 * r0002
    rcof(k,3,0,0,1) = PCx(k) * rcof(k,2,0,0,2) + 2 * rcof(k,1,0,0,2)
    rcof(k,2,1,0,1) = PCx(k) * rcof(k,1,1,0,2) + 1 * rcof(k,0,1,0,2)
    rcof(k,3,1,0,1) = PCx(k) * rcof(k,2,1,0,2) + 2 * rcof(k,1,1,0,2)
    rcof(k,2,2,0,1) = PCx(k) * rcof(k,1,2,0,2) + 1 * rcof(k,0,2,0,2)
    rcof(k,3,2,0,1) = PCx(k) * rcof(k,2,2,0,2) + 2 * rcof(k,1,2,0,2)
    rcof(k,2,3,0,1) = PCx(k) * rcof(k,1,3,0,2) + 1 * rcof(k,0,3,0,2)
    rcof(k,3,3,0,1) = PCx(k) * rcof(k,2,3,0,2) + 2 * rcof(k,1,3,0,2)
    rcof(k,2,0,1,1) = PCx(k) * rcof(k,1,0,1,2) + 1 * rcof(k,0,0,1,2)
    rcof(k,3,0,1,1) = PCx(k) * rcof(k,2,0,1,2) + 2 * rcof(k,1,0,1,2)
    rcof(k,2,1,1,1) = PCx(k) * rcof(k,1,1,1,2) + 1 * rcof(k,0,1,1,2)
    rcof(k,3,1,1,1) = PCx(k) * rcof(k,2,1,1,2) + 2 * rcof(k,1,1,1,2)
    rcof(k,2,2,1,1) = PCx(k) * rcof(k,1,2,1,2) + 1 * rcof(k,0,2,1,2)
    rcof(k,3,2,1,1) = PCx(k) * rcof(k,2,2,1,2) + 2 * rcof(k,1,2,1,2)
    rcof(k,2,3,1,1) = PCx(k) * rcof(k,1,3,1,2) + 1 * rcof(k,0,3,1,2)
    rcof(k,3,3,1,1) = PCx(k) * rcof(k,2,3,1,2) + 2 * rcof(k,1,3,1,2)
    rcof(k,2,0,2,1) = PCx(k) * rcof(k,1,0,2,2) + 1 * rcof(k,0,0,2,2)
    rcof(k,3,0,2,1) = PCx(k) * rcof(k,2,0,2,2) + 2 * rcof(k,1,0,2,2)
    rcof(k,2,1,2,1) = PCx(k) * rcof(k,1,1,2,2) + 1 * rcof(k,0,1,2,2)
    rcof(k,3,1,2,1) = PCx(k) * rcof(k,2,1,2,2) + 2 * rcof(k,1,1,2,2)
    rcof(k,2,2,2,1) = PCx(k) * rcof(k,1,2,2,2) + 1 * rcof(k,0,2,2,2)
    rcof(k,3,2,2,1) = PCx(k) * rcof(k,2,2,2,2) + 2 * rcof(k,1,2,2,2)
    rcof(k,2,3,2,1) = PCx(k) * rcof(k,1,3,2,2) + 1 * rcof(k,0,3,2,2)
    rcof(k,3,3,2,1) = PCx(k) * rcof(k,2,3,2,2) + 2 * rcof(k,1,3,2,2)
    rcof(k,2,0,3,1) = PCx(k) * rcof(k,1,0,3,2) + 1 * rcof(k,0,0,3,2)
    rcof(k,3,0,3,1) = PCx(k) * rcof(k,2,0,3,2) + 2 * rcof(k,1,0,3,2)
    rcof(k,2,1,3,1) = PCx(k) * rcof(k,1,1,3,2) + 1 * rcof(k,0,1,3,2)
    rcof(k,3,1,3,1) = PCx(k) * rcof(k,2,1,3,2) + 2 * rcof(k,1,1,3,2)
    rcof(k,2,2,3,1) = PCx(k) * rcof(k,1,2,3,2) + 1 * rcof(k,0,2,3,2)
    rcof(k,3,2,3,1) = PCx(k) * rcof(k,2,2,3,2) + 2 * rcof(k,1,2,3,2)
    rcof(k,2,3,3,1) = PCx(k) * rcof(k,1,3,3,2) + 1 * rcof(k,0,3,3,2)
    rcof(k,3,3,3,1) = PCx(k) * rcof(k,2,3,3,2) + 2 * rcof(k,1,3,3,2)
    rcof(k,0,0,2,0) = PCz(k) * rcof(k,0,0,1,1) + 1 * r0001 
    rcof(k,0,0,3,0) = PCz(k) * rcof(k,0,0,2,1) + 2 * rcof(k,0,0,1,1)
    rcof(k,0,1,0,0) = PCy(k) * r0001 
    rcof(k,0,1,1,0) = PCy(k) * rcof(k,0,0,1,1)
    rcof(k,0,1,2,0) = PCy(k) * rcof(k,0,0,2,1)
    rcof(k,0,1,3,0) = PCy(k) * rcof(k,0,0,3,1)
    rcof(k,0,2,0,0) = PCy(k) * rcof(k,0,1,0,1) + 1 * r0001 
    rcof(k,0,3,0,0) = PCy(k) * rcof(k,0,2,0,1) + 2 * rcof(k,0,1,0,1)
    rcof(k,0,2,1,0) = PCy(k) * rcof(k,0,1,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,0,3,1,0) = PCy(k) * rcof(k,0,2,1,1) + 2 * rcof(k,0,1,1,1)
    rcof(k,0,2,2,0) = PCy(k) * rcof(k,0,1,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,0,3,2,0) = PCy(k) * rcof(k,0,2,2,1) + 2 * rcof(k,0,1,2,1)
    rcof(k,0,2,3,0) = PCy(k) * rcof(k,0,1,3,1) + 1 * rcof(k,0,0,3,1)
    rcof(k,0,3,3,0) = PCy(k) * rcof(k,0,2,3,1) + 2 * rcof(k,0,1,3,1)
    rcof(k,1,0,0,0) = PCx(k) * r0001
    rcof(k,1,1,0,0) = PCx(k) * rcof(k,0,1,0,1)
    rcof(k,1,2,0,0) = PCx(k) * rcof(k,0,2,0,1)
    rcof(k,1,3,0,0) = PCx(k) * rcof(k,0,3,0,1)
    rcof(k,1,0,1,0) = PCx(k) * rcof(k,0,0,1,1)
    rcof(k,1,1,1,0) = PCx(k) * rcof(k,0,1,1,1)
    rcof(k,1,2,1,0) = PCx(k) * rcof(k,0,2,1,1)
    rcof(k,1,3,1,0) = PCx(k) * rcof(k,0,3,1,1)
    rcof(k,1,0,2,0) = PCx(k) * rcof(k,0,0,2,1)
    rcof(k,1,1,2,0) = PCx(k) * rcof(k,0,1,2,1)
    rcof(k,1,2,2,0) = PCx(k) * rcof(k,0,2,2,1)
    rcof(k,1,3,2,0) = PCx(k) * rcof(k,0,3,2,1)
    rcof(k,1,0,3,0) = PCx(k) * rcof(k,0,0,3,1)
    rcof(k,1,1,3,0) = PCx(k) * rcof(k,0,1,3,1)
    rcof(k,1,2,3,0) = PCx(k) * rcof(k,0,2,3,1)
    rcof(k,1,3,3,0) = PCx(k) * rcof(k,0,3,3,1)
    rcof(k,2,0,0,0) = PCx(k) * rcof(k,1,0,0,1) + 1 * r0001
    rcof(k,3,0,0,0) = PCx(k) * rcof(k,2,0,0,1) + 2 * rcof(k,1,0,0,1)
    rcof(k,2,1,0,0) = PCx(k) * rcof(k,1,1,0,1) + 1 * rcof(k,0,1,0,1)
    rcof(k,3,1,0,0) = PCx(k) * rcof(k,2,1,0,1) + 2 * rcof(k,1,1,0,1)
    rcof(k,2,2,0,0) = PCx(k) * rcof(k,1,2,0,1) + 1 * rcof(k,0,2,0,1)
    rcof(k,3,2,0,0) = PCx(k) * rcof(k,2,2,0,1) + 2 * rcof(k,1,2,0,1)
    rcof(k,2,3,0,0) = PCx(k) * rcof(k,1,3,0,1) + 1 * rcof(k,0,3,0,1)
    rcof(k,3,3,0,0) = PCx(k) * rcof(k,2,3,0,1) + 2 * rcof(k,1,3,0,1)
    rcof(k,2,0,1,0) = PCx(k) * rcof(k,1,0,1,1) + 1 * rcof(k,0,0,1,1)
    rcof(k,3,0,1,0) = PCx(k) * rcof(k,2,0,1,1) + 2 * rcof(k,1,0,1,1)
    rcof(k,2,1,1,0) = PCx(k) * rcof(k,1,1,1,1) + 1 * rcof(k,0,1,1,1)
    rcof(k,3,1,1,0) = PCx(k) * rcof(k,2,1,1,1) + 2 * rcof(k,1,1,1,1)
    rcof(k,2,2,1,0) = PCx(k) * rcof(k,1,2,1,1) + 1 * rcof(k,0,2,1,1)
    rcof(k,3,2,1,0) = PCx(k) * rcof(k,2,2,1,1) + 2 * rcof(k,1,2,1,1)
    rcof(k,2,3,1,0) = PCx(k) * rcof(k,1,3,1,1) + 1 * rcof(k,0,3,1,1)
    rcof(k,3,3,1,0) = PCx(k) * rcof(k,2,3,1,1) + 2 * rcof(k,1,3,1,1)
    rcof(k,2,0,2,0) = PCx(k) * rcof(k,1,0,2,1) + 1 * rcof(k,0,0,2,1)
    rcof(k,3,0,2,0) = PCx(k) * rcof(k,2,0,2,1) + 2 * rcof(k,1,0,2,1)
    rcof(k,2,1,2,0) = PCx(k) * rcof(k,1,1,2,1) + 1 * rcof(k,0,1,2,1)
    rcof(k,3,1,2,0) = PCx(k) * rcof(k,2,1,2,1) + 2 * rcof(k,1,1,2,1)
    rcof(k,2,2,2,0) = PCx(k) * rcof(k,1,2,2,1) + 1 * rcof(k,0,2,2,1)
    rcof(k,3,2,2,0) = PCx(k) * rcof(k,2,2,2,1) + 2 * rcof(k,1,2,2,1)
    rcof(k,2,3,2,0) = PCx(k) * rcof(k,1,3,2,1) + 1 * rcof(k,0,3,2,1)
    rcof(k,3,3,2,0) = PCx(k) * rcof(k,2,3,2,1) + 2 * rcof(k,1,3,2,1)
    rcof(k,2,0,3,0) = PCx(k) * rcof(k,1,0,3,1) + 1 * rcof(k,0,0,3,1)
    rcof(k,3,0,3,0) = PCx(k) * rcof(k,2,0,3,1) + 2 * rcof(k,1,0,3,1)
    rcof(k,2,1,3,0) = PCx(k) * rcof(k,1,1,3,1) + 1 * rcof(k,0,1,3,1)
    rcof(k,3,1,3,0) = PCx(k) * rcof(k,2,1,3,1) + 2 * rcof(k,1,1,3,1)
    rcof(k,2,2,3,0) = PCx(k) * rcof(k,1,2,3,1) + 1 * rcof(k,0,2,3,1)
    rcof(k,3,2,3,0) = PCx(k) * rcof(k,2,2,3,1) + 2 * rcof(k,1,2,3,1)
    rcof(k,2,3,3,0) = PCx(k) * rcof(k,1,3,3,1) + 1 * rcof(k,0,3,3,1)
    rcof(k,3,3,3,0) = PCx(k) * rcof(k,2,3,3,1) + 2 * rcof(k,1,3,3,1)
  end do

end subroutine


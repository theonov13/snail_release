
subroutine trn2sphr(xtraf,maxl)
!====================================================================+
! Purpose:
! --------
!
! Calculates the transformation matrix from cartesian
! to spherical harmonic basis functions
!
!====================================================================+

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer,   intent(in) :: maxl

  ! output
  real(kdp), intent(out) :: xtraf(2*maxl+1,(maxl+1)*(maxl+2)/2)

  ! local
  integer :: m,i,ncart
  real(kdp), allocatable :: tmp(:)  
  
  ncart = (maxl+1)*(maxl+2)/2
  allocate(tmp(ncart))

  do m = -maxl, maxl
    call calcYlm(tmp,maxl,m)
    do i = 1, ncart
      xtraf(m+maxl+1,i) = tmp(i)
    end do
  end do
end subroutine


subroutine calcYlm(ret,l,mval)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'

  ! dimensions
  integer,   intent(in) :: l, mval

  ! output
  real(kdp), intent(out) :: ret((l+1)*(l+2)/2)

  ! local
  integer   :: m,a,b,p,k,ind,zexp,yexp,xexp,cosfac,sinfac
  real(kdp) :: prefac,ffac,afac,fac

  ! functions
  real(kdp) :: fact, binomial
  integer   :: getind

  m = abs(mval)
  ret = zero

  ! Compute prefactor
  prefac = sqrt((2*l+1)/(4.0*PI))*(2.0)**(-l)

  if(m /= 0) then
    prefac = prefac * sqrt(fact(l-m)*2.0/fact(l+m))
  end if

  ! Calculate bar Pi contribution
  do k = 0, (l-m)/2
    ! get front factor 
    ffac = (-1.0)**(k) * binomial(l,k) * binomial(2*(l-k),l)
    if (m /=0) then
      ffac = ffac * fact(l-2*k) / fact(l-2*k-m)
    end if
    ffac = ffac * prefac

    ! Distribute exponents
    do a = 0, k
      afac = binomial(k,a) * ffac

      do b = 0, a
        fac = binomial(a,b) * afac

        ! Current exponents
        zexp = 2 * b - 2 * k + l - m
        yexp = 2 * (a - b)
        xexp = 2 * (k - a)

        ! Now, add in the contribution of A or B.
        if (mval > 0) then
          !Contribution from A_m
          do p = 0, m

            ! Check if term contributes
            select case (modulo(m-p,4)) 
              case(0) ! cos(0) = 1
                cosfac = 1
              case(1) ! cos(pi/2) = 0
                cosfac = 0
              case(2) ! cos(pi) = -1
                cosfac = -1
              case(3) ! cos(3*pi/2) = 0
                cosfac = 0
              case default
                call quit("Unexpected case","calYlm")
            end select

            if (cosfac /= 0) then
              ind = getind(xexp+p,yexp+m-p,zexp)
              ret(ind) = ret(ind) + cosfac * binomial(m,p) * fac
            end if

          end do

        elseif (m == 0) then
          ! No A_m or B_m term.
          ind = getind(xexp,yexp,zexp)
          ret(ind) = ret(ind) + fac

        else 
          ! B_m contributes
          do p = 0, m

            ! Check contribution of current term
            select case (modulo(m-p,4)) 
              case(0) ! sin(0) = 0
                sinfac = 0
              case(1) ! sin(pi/2) = 1
                sinfac = 1
              case(2) ! sin(pi) = 0
                sinfac = 0 
              case(3) ! sin(3*pi/2) = -1
                sinfac = -1 
              case default
                call quit("Unexpected case","calYlm")
            end select

            if (sinfac /= 0) then
              ind = getind(xexp+p,yexp+m-p,zexp)
              ret(ind) = ret(ind) + sinfac * binomial(m,p) * fac
            end if
          end do 

        end if

      end do
    end do  
  end do


end subroutine


! exactly the same function is defined for the type type_shell
! At some point this function should be used instead
function getind(nx,ny,nz) result(ind)
  implicit none

  integer :: ind

  integer, intent(in) :: nx,ny,nz
  integer :: ii,jj

  ii = ny + nz
  jj = nz

  !  return index in one dimensional array
  ind = ii * (ii + 1) / 2 + jj + 1
  return
end function



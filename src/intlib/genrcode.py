
def gen_code3(ncd,mcd,lcd):
    #v = 4
    #u = 4
    #t = 4
    
    print "do klp = 1, nprimkl"
    for phi in xrange(ncd+1):
      for nu in xrange(mcd+1):
        for tau in xrange(lcd+1):
          dfac = (-1.0)**(tau+nu+phi)
          print "   ftuv(klp," + str(tau) + "," + str(nu) + "," + str(phi) + ") = " + str(dfac) + " * ecdt(klp,llc,lld," + str(tau) + ") * ecdu(klp,mmc,mmd," + str(nu) + ") * ecdv(klp,nnc,nnd," + str(phi)+ ")"
    
    print "end do"

def gen_code2(ncd,mcd,lcd,nab,mab,lab):

    ituv2 = 0
    for phi in xrange(ncd+1):
        for nu in xrange(mcd+1):
            for tau in xrange(lcd+1):
                ituv2 += 1
                print "taunuphi = " + "(" + str(ituv2-1) + ")*nprimkl + klp"
                
                print "dfac = ftuv(taunuphi) * pfac(ijkl)"

                ituv = 0
                for v in xrange(nab+1):
                    voff = str(v+phi) + "* voffijkl "
                    for u in xrange(mab+1):
                        uoff = str(u+nu) + "* uoffijkl "
                        for t in xrange(lab+1):
                            toff = str(t+tau) + "* nprimijkl "

                            ituv += 1
                            print "tuv = " + str(ituv-1) + "* nprimijkl + ijkl"

                            print "tuvbig = " + voff + uoff + toff + " + ijkl"

                            print "gtuv(tuv) = gtuv(tuv) + dfac * rtuv(tuvbig)"
                             

  #                ituv2 = 0
  #                do phi = 0, ncd
  #                  do nu = 0, mcd
  #                    do tau = 0, lcd

  #                      ituv2 = ituv2 + 1
  #                      taunuphi = (ituv2-1)*nprimkl + klp
  #                      !  = getindtuv(klp,tau,nu,phi,lcd,mcd,ncd,nprimc,nprimd)

  #                      dfac = ftuv(taunuphi) * pfac(ijkl)

  #                      ituv = 0
  #                      do v = 0, nab
  #                        voff = (v+phi) * voffijkl
  #                        do u = 0, mab
  #                          uoff = (u+nu) * uoffijkl
  #                          do t = 0, lab
  #                            toff = (t+tau) * nprimijkl

  #                            ituv = ituv + 1
  #                            tuv = (ituv-1)*nprimijkl + ijkl
  #                            ! = getindtuv(ijkl,t,u,v,lab,mab,nab,nprimij,nprimkl)

  #                            tuvbig = voff + uoff + toff + ijkl
  #                            ! = getindtuv(ijkl,t+tau,u+nu,v+phi,tmax,umax,vmax,nprimij,nprimkl)

  #                            gtuv(tuv) = gtuv(tuv) + dfac * rtuv(tuvbig)
  #                          end do
  #                        end do
  #                      end do
  #                    end do
  #                  end do
  #                end do


def gen_code(t,u,v):
    #v = 4
    #u = 4
    #t = 4
    lmax = v + u + t 
    
    
    print "pure subroutine r" + str(elem[0]) + "_" + str(elem[1]) + "_" + str(elem[2]) + "(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)"
    print ""
    print "  implicit none" 
    print ""
    print "  include 'fortrankinds.h'"
    print "  include 'numbers.h'"
    print ""
    print "  ! dimensions"
    print "  integer,   intent(in) :: lmax,nprijkl"
    print ""
    print "  ! input"
    print "  integer,   intent(in) :: t, u, v, n"
    print "  real(kdp), intent(in) :: p(nprijkl), PCx(nprijkl), PCy(nprijkl), PCz(nprijkl)"
    print "  real(kdp), intent(in) :: Fn(0:lmax,nprijkl)"
    print ""
    print "  ! output "
    print "  real(kdp), intent(out) :: rcof(nprijkl,0:t,0:u,0:v,0:lmax)"
    print ""
    print "  ! local "
    print "  integer :: k"
    print "  rcof = zero \n"
    
    print "  do k = 1, nprijkl"
    for i in xrange(lmax+1):
      print "    rcof(k,0,0,0," + str(i) + ") = (-two*p(k))**" + str(i) + " * Fn(" + str(i) + ",k)"   
    
    if (v > 0):
      for m in xrange(lmax,0,-1):
        print "    rcof(k,0,0,1," +  str(m-1) + ") = PCz(k) * rcof(k,0,0,0," + str(m) + ")"

    for m in xrange(lmax,0,-1):
      for iv in xrange(2,v+1):
        print "    rcof(k,0,0," + str(iv) + "," + str(m-1) + ") = PCz(k) * rcof(k,0,0," + str(iv-1) + "," + str(m) + ") + " + str(iv-1) + " * rcof(k,0,0," + str(iv-2) + "," + str(m) + ")"

      if (u > 0):
        for iv in xrange(0,v+1):
          print "    rcof(k,0,1," + str(iv) + "," + str(m-1) + ") = PCy(k) * rcof(k,0,0," + str(iv) + "," + str(m) + ")"

      for iv in xrange(0,v+1):
        for iu in xrange(2,u+1):
          print "    rcof(k,0," + str(iu) + "," + str(iv) + "," + str(m-1) + ") = PCy(k) * rcof(k,0," + str(iu-1) + "," + str(iv) + "," + str(m) + ") + " \
                + str(iu-1) + " * rcof(k,0," + str(iu-2) + "," + str(iv) + ","  + str(m) + ")"
  
      if (t > 0):
        for iv in xrange(v+1):
          for iu in xrange(u+1):
            print "    rcof(k,1," + str(iu) + "," +str(iv) + "," + str(m-1) + ") = PCx(k) * rcof(k,0," + str(iu) + "," + str(iv) + "," + str(m) + ")"

      for iv in xrange(v+1):
        for iu in xrange(u+1):
          for it in xrange(2,t+1):
            print "    rcof(k," + str(it) + "," + str(iu) + "," + str(iv) + "," + str(m-1) + ") = PCx(k) * rcof(k," + str(it-1) + "," + str(iu) + "," + str(iv) + "," + str(m) + ") + " \
                  + str(it-1) + " * rcof(k," + str(it-2) + "," + str(iu) + "," + str(iv) + "," + str(m) + ")"

    print "  end do"
    print ""
    print "end subroutine" 
    
   
#def gencode2(la,ma,na,lb,mb,nb,lc,mc,nc,ld,md,nd):
#
#   
#	print "! Build:"
#	print "!   F_{tau,nu,phi}^{c_r d_s} = (-1)^(tau+nu+phi) * E_{tau,phi,nu}^{c_r d_s}"
#   for phi in xrange(
#	do phi = 0, nc + nd
#	  do nu = 0, mc + md
#	    do tau = 0, lc + ld
#	      dfac = (-1.0)**(tau+nu+phi)
#	      do l = 1, nprimd
#	        do k = 1, nprimc
#	          klp = (l-1)*nprimc + k
#	          ftuv(klp,tau,nu,phi) = dfac * ecdt(klp,lc,ld,tau) * ecdu(klp,mc,md,nu) * ecdv(klp,nc,nd,phi) 
#	          !ftuv(klp,tau,nu,phi) = dfac * ecof2(lc,ld,tau,dcd(1),cexps(k),dexps(l)) 
#	          !                            * ecof2(mc,md,nu,dcd(2),cexps(k),dexps(l)) 
#	          !                            * ecof2(nc,nd,phi,dcd(3),cexps(k),dexps(l)) 
#	        end do
#	      end do
#	    end do
#	  end do
#	end do
#
#   return 



    
class unique_element:
    def __init__(self,value,occurrences):
        self.value = value
        self.occurrences = occurrences

def perm_unique(elements):
    eset=set(elements)
    listunique = [unique_element(i,elements.count(i)) for i in eset]
    u=len(elements)
    return perm_unique_helper(listunique,[0]*u,u-1)

def perm_unique_helper(listunique,result_list,d):
    if d < 0:
        yield tuple(result_list)
    else:
        for i in listunique:
            if i.occurrences > 0:
                result_list[d]=i.value
                i.occurrences-=1
                for g in  perm_unique_helper(listunique,result_list,d-1):
                    yield g
                i.occurrences+=1


kmax = 3

# rcode:

for i in xrange(kmax+1):
    for j in xrange(i+1):
        for k in xrange(j+1):
            a = list(perm_unique([i,j,k]))
            #print(a)
            for elem in a:
               #gen_code(elem[0], elem[1], elem[2])
               gen_code3(elem[0], elem[1], elem[2])
               print ""

# code in hermit

#kmax = 1
#
#for i in xrange(kmax+1):
#    for j in xrange(i+1):
#        for k in xrange(j+1):
#            for l in xrange(kmax+1):
#                for m in xrange(l+1):
#                    for n in xrange(m+1):
#                        a = list(perm_unique([i,j,k,l,m,n]))
#                        print(a)
#                        for elem in a:
#                           gen_code2(elem[0], elem[1], elem[2], elem[3], elem[4], elem[5])
#                           print ""

#for i in xrange(kmax+1):
#    for j in xrange(i+1):
#        for k in xrange(j+1):
#            a = list(perm_unique([i,j,k]))
#            #print(a)
#            for elem in a:
#					print "case(" + str(elem[0]) + str(elem[1]) + str(elem[2]) + ")\n"
#					print "  call r" + str(elem[0]) + "_" + str(elem[1]) + "_" + str(elem[2]) + "(rcof, t, u, v, n, p, PCx, PCy, PCz, Fn, lmax, nprijkl)\n"

!-------------------------------------------------------------------------------+
!  Purpose: Main driver for vibrational analysis 
!
!   At the moment:
!   --------------
!    + Normal mode analysis of given hessian  
!
! Gunnar Schmitz
!-------------------------------------------------------------------------------+

subroutine vibengine(fhess,atoms,natoms)

  use type_atom

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'

  ! constants
  logical, parameter :: locdbg = .false.

  ! dimensions
  integer, intent(in) :: natoms

  ! input
  character(len=80), intent(in) :: fhess
  type(atom_info),   intent(in) :: atoms(natoms) 

  ! local
  real(kdp), allocatable :: hess(:),coord(:,:),amass(:),qmass(:)
  integer :: ncoord, iat, i, j, k, ipqii
  real(kdp) :: xmass

  !-----------------------------------------------------------------------+
  ! print header information
  !-----------------------------------------------------------------------+
  call PrintHeader('Starting vibrational module')

  !-----------------------------------------------------------------------+
  ! allocate memory
  !-----------------------------------------------------------------------+
  ncoord = 3 * natoms
  allocate(hess(ncoord*(ncoord+1)/2),coord(3,natoms),amass(natoms),qmass(ncoord))

  !-----------------------------------------------------------------------+
  ! Prepare hessian and coordinates
  !-----------------------------------------------------------------------+
  do iat = 1, natoms
    coord(1:3,iat) = atoms(iat)%xyz(1:3)
    amass(iat) = atoms(iat)%mass
    xmass = 1.d0 / dsqrt(amass(iat))
    do k = 1, 3
      qmass((iat-1)*3 + k) = xmass
    end do
  end do

  call rdhess(hess,fhess,ncoord)
  
  ! Mass weight hessian: H_m = m^{-1/2} H m^{-1/2}
  do i = 1, ncoord
    ipqii = i*(i-1)/2
    do j = 1, i
      hess(ipqii+j) = hess(ipqii+j) * qmass(i) * qmass(j)
    end do
  end do
 
  !-----------------------------------------------------------------------+
  ! Perform normal mode analysis
  !-----------------------------------------------------------------------+

  call freqana(hess,coord,amass,ncoord,natoms)

  deallocate(hess,coord,amass,qmass)
end subroutine




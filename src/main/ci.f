!-------------------------------------------------------------------------------+
!  Purpose: Main driver for CI calculations (or probably more other things)
!
!   At the moment:
!   --------------
!    + CIS excitations energies
!      - Using full diagonalization
!      - Initial implementation of a Davidson solver
!
!    + TDHF/RPA excitation energies
!      - Using full diagonalization
!      - Initial implementation of iterative solver 
!
! Gunnar Schmitz
!-------------------------------------------------------------------------------+

subroutine ciengine(escf,CisCalcDef,basis,tolint,have_eri,twoeint,memmax,filmo,nocc,nvir,nspin)

  use type_cgto
  use type_shell
  use type_basis
  use type_int4idx

  use type_ciscalcdef

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'

  ! constants
  logical, parameter :: locdbg = .false.

  ! dimensions
  integer, intent(in) :: nocc,nvir,nspin

  ! input
  real(kdp),         intent(in) :: escf
  type(cis_calcdef), intent(in) :: CisCalcDef
  type(basis_info),  intent(in) :: basis
  character(len=80), intent(in) :: filmo
  real(kdp),         intent(in) :: tolint
  logical,           intent(in) :: have_eri
  integer(kli),      intent(in) :: memmax

  ! input/output
  type(int4idx) ,    intent(inout) :: twoeint

  ! local
  integer      :: nexc, idx, jdx, lwork, info, ilow
  integer      :: nbf,nshell,mxcart,mxprim,lmax
  integer(kli) :: memused,memavail
  real(kdp)    :: dlen, tolres, toleig, dummy, dtmp
  integer      :: maxred,maxiter

  integer,   allocatable :: iexc(:,:)
  real(kdp), allocatable :: cimat(:,:),cien(:),work(:),tempmat(:,:),imag(:)

  !-----------------------------------------------------------------------+
  ! define shortcuts
  !-----------------------------------------------------------------------+
  nbf    = basis%nbf
  nshell = basis%nshell
  mxprim = basis%mxprim
  mxcart = basis%mxcart
  lmax   = basis%lmax

  !-----------------------------------------------------------------------+
  ! sanity checks
  !-----------------------------------------------------------------------+

  if (nspin.ne.1) call quit("At the moment only RHF-reference for CI","ciengine")

  !-----------------------------------------------------------------------+
  ! print header information
  !-----------------------------------------------------------------------+
  if (CisCalcDef%use_TDHF) then
    call PrintHeader('Starting the TDHF calculation')
  else
    call PrintHeader('Starting the CIS calculation')
  end if

  !-----------------------------------------------------------------------+
  ! Get Single excitation space
  !-----------------------------------------------------------------------+
  nexc = nocc * nvir

  ! Get list of Single Exciations
  allocate(iexc(nexc,2))
  call ci_getsingexcspace(iexc,nocc,nvir,nexc)

  !-----------------------------------------------------------------------+
  ! allocate memory
  !-----------------------------------------------------------------------+
  if (CisCalcDef%use_fulldiag) then
    allocate(cimat(nexc,nexc),cien(nexc))
  end if

  !----------------------------------------------------------------------+
  ! Build CIS-Matrix
  !----------------------------------------------------------------------+
  if (CisCalcDef%use_fulldiag) then
    call ci_getcismatrix(cimat, CisCalcDef%use_TDHF, escf, basis%shells, tolint, lmax, &
                         have_eri, twoeint, basis, memmax, filmo,         &
                         nocc, nvir, nbf, nshell, mxprim, mxcart)

    if (locdbg) then
      write(6,*) "CIS-Matrix:"
      call matprt(cimat,nexc,nexc)
    end if
  end if

  !----------------------------------------------------------------------+
  ! Diagonalize CIS-Matrix
  !----------------------------------------------------------------------+
  if (CisCalcDef%use_fulldiag) then
    lwork = -1

    if (CisCalcDef%use_TDHF) then

      !--------------------------------------------------------------------------------------------+
      ! We have rewritten the large non-Hermitian eigenvale problem
      !
      !  / A  B \   / X \       / X \ 
      ! |        | |     | = E |     |
      !  \-B -A /   \ Y /       \ Y / 
      !
      ! to the smaller dimensional problem
      !
      !  (A+B)(A-B) (X-Y) = E^2 (X-Y)
      !
      ! of the same size as the CIS problem. However, it is still non-Hermitian and 
      ! since (A+B) and (A-B) do not commute we can not use the efficient algorithm 
      ! in dsyev and need to fall back to dgeev 
      !--------------------------------------------------------------------------------------------+

      allocate(imag(nexc),tempmat(nexc,nexc))

      call dcopy(nexc*nexc,cimat,1,tempmat,1)

      call dgeev( 'N', 'V', nexc, tempmat, nexc, cien, imag, dummy, nexc, cimat, nexc, dlen, lwork, info )
      lwork = int(dlen)
      allocate(work(lwork))

      call dgeev( 'N', 'V', nexc, tempmat, nexc, cien, imag, dummy, nexc, cimat, nexc, work, lwork, info )

      ! Sort eigenvalues like it is done in dsyev:
      do idx = 1, nexc-1
        ilow = idx
        do jdx =idx + 1,nexc
          if (cien(jdx) < cien(ilow)) ilow = jdx
        end do
        if (ilow.ne.idx) then
          ! swap real eigenvalues
          call dswap(1,cien(ilow),1,cien(idx),1) 
      
          ! swap imaginary eigenvalues
          call dswap(1,imag(ilow),1,imag(idx),1) 
           
          ! swap eigenvectors
          call dswap(nexc,cimat(1,idx),1,cimat(1,ilow),1)
        end if
      end do

      deallocate(imag,tempmat)
    else
      call dsyev('V','U',nexc,cimat,nexc,cien,dlen,lwork,info)
      lwork = int(dlen)
      allocate(work(lwork))

      call dsyev( 'V', 'U', nexc, cimat, nexc, cien, work, lwork, info )

    end if

    if (info.ne.0 ) then
       call quit('Problem during diagonalization of CI-matrix','ciengine')
    end if

    do idx = 1, nexc 
      if (CisCalcDef%use_TDHF) then
        write(istdout,'(/,3x,a,i5,3x,a,f16.12)') 'State:', idx, 'energy',  dsqrt(cien(idx))
      else
        write(istdout,'(/,3x,a,i5,3x,a,f16.12)') 'State:', idx, 'energy',  cien(idx)
      end if
      write(istdout,'(3x,a)') '----------------------------------------------'
      ! assign excitations to this state
      do jdx = 1, nexc
        if (abs(cimat(jdx,idx)) .gt. 0.05) then
          write(istdout,'(5x,i4,2x,a,i4,3x,a,f16.12))') iexc(jdx,1), "->", iexc(jdx,2), 'c =', cimat(jdx,idx)
        end if
      end do 
    end do

    deallocate(work)

  else

    tolres  = CisCalcDef%tolres 
    toleig  = CisCalcDef%toleig
    maxiter = CisCalcDef%maxiter
  
    maxred = CisCalcDef%nroots * 200

    call ci_davsol(iexc,tolres,toleig,CisCalcDef%nroots,maxiter,CisCalcDef%use_TDHF,  &
                   CisCalcDef%use_TDHF,escf,tolint,lmax,twoeint,basis,                &
                   memmax,filmo,nocc,nvir,nbf,mxprim,mxcart,maxred,nexc,nexc) 
  
  end if

  deallocate(iexc)
  if (CisCalcDef%use_fulldiag) deallocate(cimat)

end subroutine




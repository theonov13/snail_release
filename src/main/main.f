!==================================================================================
! SNAIL - the slow and inefficient QM program -
!
! As the name suggests this program is not intended to be very efficient,
! but to deliver an hopefully easy to understand code base, so that (under)graduate
! students are able to follow/implement e.g. a simple Hartree-Fock program.  
! 
! The first time I had access to a widely used QM code, I was intimidated, when 
! I tried to understand the HF code or even just the routine for the Hückel guess. 
! I knew that the code was intended to be efficient and probably thus harder to 
! follow, but it was a discouraging experience...  
! 
! Snail aims to be the opposite. It is an inefficient code, but (hopefully)
! so easy to understand, that it helps students to get a deeper insight
! and to play around with these methods. I intentionally still use Fortran as programming language
! to still be as close to what you probably can expect in a more sophisticated QM program. 
! Nevertheless, snail shows that it does not require black magic to implement Hartree-Fock or MP2. 
!
! Implemented methods:
!  - RHF (conventional, direct, RI-JK)
!     - Dipole moments
!     - Mulliken population analysis
!     - Loewdin population analysis (not really sure if it is correct)
!  - UHF (conventional, direct, RI-JK)
!     - Dipole moments
!  - MP2 and SCS-MP2 for RHF reference
!  - RI-MP2 and SCS-RI-MP2 for RHF reference
!  - CIS and TDHF excitation energies for RHF reference
!  - Normal mode analysis based on a user provided Hessian
!
! Issues/Remarks/Improvements:
!  - At the moment all integrals are kept in memory (-> put them to disk)
!  - No batching of integrals ( -> introduce batching at least for RI methods)
!  - Only Cartesian Gaussian are used. (-> transform them after integral evaluation
!                                           into spherical Gaussian )
!
!
! Feel free to contribute, but keep it simple and well documented.   
!
!================================================================================== 

program snail

  use type_pgto
  use type_cgto
  use type_atom
  use type_shell
  use type_basis
  use mod_fileutil
  use type_int4idx
  use type_timer
  use globaldata
  use sysmod
  use type_integ

  use type_scfcalcdef
  use type_ciscalcdef
  use type_mp2calcdef
  use type_pltcalcdef
  use type_optcalcdef

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'

  ! constants
  integer,   parameter :: ispina = 1,&
                          ispinb = 2
  logical,   parameter :: lprtcoord = .true., &
                          do_tabboys = .false.  ! Create File with tabulated values of Boys-function

  ! for the command line
  integer           :: iarg
  character(len=80) :: argmt

  ! maximal dimensions 
  integer           :: mxatoms

  ! molecule dependend dimensions
  integer           :: natoms,nocc(2),nvirt,nelec,nshell,nspin,nxshell

  ! memory management
  integer(kli)      :: memused, memmax, memavail

  ! other stuff
  character(len=80) :: wavefunc,title,filmo,fhess
  logical           :: havefil,lexist,have_eri
  integer           :: ibf,iat,ncharge,iprint

  real(kdp)         :: escf,emp2
  integer           :: scfmaxiter

  ! Definition of calculations
  type(scf_calcdef) :: ScfCalcDef
  type(cis_calcdef) :: CisCalcDef
  type(mp2_calcdef) :: Mp2CalcDef
  type(plt_calcdef) :: PltCalcDef
  type(opt_calcdef) :: OptCalcDef

  ! thresholds
  real(kdp)         :: tolint 

  ! for method setup
  logical           :: lscf,lplot,lmp2,lcis,lvib,use_ri,use_rijk
  logical           :: lnumgrad,lnumhess,do_opt

  type(basis_info) :: basis 
  type(basis_info) :: cbas,jkbas
 
  type(atom_info),  allocatable :: atoms(:) 

  ! for 4-index integrals
  type(int4idx)    :: twoeint
  type(integ_info) :: intopt

  ! for time measurments
  real(kdp) :: cpu,cpux,cpu0,wall

  ! input filese
  character(len=80) :: filinp

  type(timer_info) :: timer

  character(len=:),allocatable :: hname
  
  ! functions
  real(kdp) :: doublefact

  call prtdate()

  write(istdout,*) "" 
  write(istdout,*) repeat("=",70)
  write(istdout,*) "                                "
  write(istdout,*) "                .----.   @   @  "
  write(istdout,*) '               / .-"-.`.  \v/   '
  write(istdout,*) "               | | '\ \ \_/ )   "
  write(istdout,*) "             ,-\ `-.' /.'  /    "
  write(istdout,*) "           '---`----'----'      "
  write(istdout,*) "                                "
  write(istdout,*) "  SNAIL - the slow and inefficient QM program -"
  write(istdout,*) repeat("=",70)

  ! self-explanatory get host we are running on
  hname = hostname()

  write(istdout,'(/3x,a,a)') "Hostname: ", hname

  ! init
  cpu0 = 0.0d0 

  ! Get Snail main dir
  call get_environment_variable("SNAILDIR", homedir)

  ! start time measurement
  call timer_exec(timer,'init','all')

  ! init file utils
  call init_modfileutil()

  ! If necessary create Table with Boys Function values
  inquire(file=trim(homedir) // "/bin/fboys-tab.dat", exist=lexist)
  if ( .not. lexist .or. do_tabboys) then
    write(istdout,'(/3x,a)') "First run? Need to generate tabulated Boys Function"
    call WriteTabBoys(homedir)
    write(istdout,'(3x,a)') "Boys Function done..."
  end if

  !--------------------------------------------------------------------------+ 
  ! check if we have an input file 
  !--------------------------------------------------------------------------+ 
  filinp = '/--file not set--/'

  call getarg(1,argmt)
  filinp = adjustl(argmt)

  ! check if file exists
  inquire(file=filinp, exist=havefil)

  !--------------------------------------------------------------------------+ 
  ! read input and start requested calculation
  !--------------------------------------------------------------------------+ 
  if (havefil) then
    mxatoms = 250 

    allocate(atoms(mxatoms))

    call readinp(basis,cbas,jkbas,use_ri,use_rijk,&
                 lscf,lmp2,lcis,lvib,lplot,lnumgrad,lnumhess,do_opt,&
                 iprint,ScfCalcDef,CisCalcDef,Mp2CalcDef,PltCalcDef,&
                 OptCalcDef,&
                 tolint,intopt,natoms,ncharge,wavefunc,title,&
                 memmax,memused,atoms,filinp,fhess,mxatoms)  
  
    ! Print title for this calculation
    write(istdout,'(3x,a10,a)') "Title: ", title

    ! init memory counter
    memused = 0

    nelec = 0
    do iat = 1, natoms
      nelec = nelec + atoms(iat)%atono
    end do
    
    nelec = nelec - ncharge
 
    if (wavefunc(1:3) == 'rhf') then
      if (mod(nelec,2) == 0) then
        nocc = nelec / 2 
        nspin = 1
      else
        Stop 'odd number of electrons not supported by RHF try UHF'
      end if
    elseif (wavefunc(1:3) == 'uhf') then
      nocc(ispina) = nelec / 2 + mod(nelec,2) 
      nocc(ispinb) = nelec / 2
      nspin = 2
    else
      call quit('unknown wave function','main')
    end if

    !-------------------------------------------------------+
    !  Geometrical info on molecule
    !-------------------------------------------------------+
    if (lprtcoord) then
      call prtcoord(atoms,natoms,mxatoms)
     
      call getpointgroup(atoms,1.d-1,natoms)
    end if

    !-------------------------------------------------------+
    ! choose the requested method e.g. Hartree Fock, MP2 etc. 
    !-------------------------------------------------------+
    escf = zero

    !
    ! Read previously calculated properties
    !
    call readproperties(escf)

    ! 
    ! Energy driver
    !
    call CalcEnergy(escf,emp2,.false.,lscf,ScfCalcDef,lcis,CisCalcDef,lmp2,Mp2CalcDef, &
                    atoms,basis,jkbas,cbas,use_ri,use_rijk,                            &
                    timer,iprint,intopt,tolint,ncharge,memmax,                         &
                    nocc,natoms,nspin)


    !
    ! Plot density, MOs etc.
    !
    if (lplot) then
      filmo = 'mos'
      call timer_exec(timer,'init','PLOT')
      call plot_engine(PltCalcDef,atoms,basis,memmax,filmo,nocc,nvirt,nspin,natoms)
      call timer_exec(timer,'measure','PLOT')
    end if

    !
    ! Numerical Gradient
    !
    if (lnumgrad) then
      call timer_exec(timer,'init','NUMGRAD')
      memavail = memmax - memused
      call numgrad(escf,emp2,lscf,ScfCalcDef,lcis,CisCalcDef,lmp2,Mp2CalcDef, &
                   do_opt,OptCalcDef,                                         &
                   atoms,basis,jkbas,cbas,use_ri,use_rijk,                    &
                   timer,iprint,intopt,tolint,ncharge,memmax,                 &
                   nocc,natoms,nspin)
      call timer_exec(timer,'measure','NUMGRAD')
    end if

    !
    ! Numerical Hessian
    !
    if (lnumhess) then
      call timer_exec(timer,'init','NUMHESS')
      memavail = memmax - memused
      !call numhess(escf,ScfCalcDef,timer,iprint,atoms,basis,jkbas,use_rijk, &
      !             intopt,tolint,ncharge,memavail,nocc,natoms,nspin)
      call numhess(escf,emp2,lscf,ScfCalcDef,lcis,CisCalcDef,lmp2,Mp2CalcDef, &
                   atoms,basis,jkbas,cbas,use_ri,use_rijk,                    &
                   timer,iprint,intopt,tolint,ncharge,memmax,                 &
                   nocc,natoms,nspin)
      call timer_exec(timer,'measure','NUMHESS')
    end if

    !
    ! Vibrational analysis 
    !
    if (lvib) then
      call timer_exec(timer,'init','VIB')
      call vibengine(fhess,atoms,natoms)
      call timer_exec(timer,'measure','VIB')
    end if

    !
    ! Write file with calculated properties
    !
    call writeproperties(escf)

    !-----------------------------------------------------------------------+
    ! clean up
    !-----------------------------------------------------------------------+
    deallocate(atoms)

    call deallocate_basis(basis)
    call deallocate_basis(cbas)
    call deallocate_basis(jkbas)
 
  else
    write(6,'(/3x,a)') 'No valid input file given'
  end if

  ! stop time measurement
  call timer_exec(timer,'measure','all',cpu,wall)

  write(istdout,'(/3x,a)')  'Profiling:'
  write(istdout,'(5x,a,f10.2)')  'Total CPU-time (sec)    : ', cpu

  call timer_exec(timer,'report_all','xxx')

  write(istdout,'(/1x,a)') repeat('-',70)
  call prttime(cpu, " CPU")
  call prttime(wall,"WALL")
  write(istdout,'(1x,a)') repeat('-',70)

  call timer_exec(timer,'reset','xxx')

  write(istdout,'(/1x,a,a,a,/)') repeat('*',3), " Snail : ended properly ", repeat('*',3)

  call prtdate()
 
 end






subroutine CalcEnergy(escf,emp2,silent,lscf,ScfCalcDef,lcis,CisCalcDef,lmp2,Mp2CalcDef,&
                      atoms,basis,jkbas,cbas,use_ri,use_rijk, &
                      timer,iprint,intopt,tolint,charge,memmax,&
                      nocc,natoms,nspin)

  use type_atom
  use type_cgto
  use type_shell
  use type_basis
  use type_int4idx
  use type_queue
  use type_integ
  use type_timer
  use IntegralBuffer
  use mod_fileutil

  use type_scfcalcdef
  use type_ciscalcdef
  use type_mp2calcdef

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'

  ! dimensions
  integer, intent(in) :: natoms,nspin
  integer, intent(in) :: nocc(nspin)

  ! output
  real(kdp),         intent(out) :: escf,emp2

  ! input
  type(scf_calcdef), intent(in) :: ScfCalcDef
  type(cis_calcdef), intent(in) :: CisCalcDef
  type(mp2_calcdef), intent(in) :: Mp2CalcDef
  type(atom_info),   intent(in) :: atoms(natoms)
  type(basis_info),  intent(in) :: basis
  type(basis_info),  intent(in) :: jkbas 
  type(basis_info),  intent(in) :: cbas 
  integer,           intent(in) :: charge
  integer,           intent(in) :: iprint 
  integer(kli),      intent(in) :: memmax
  real(kdp),         intent(in) :: tolint
  logical,           intent(in) :: lscf,lmp2,lcis
  logical,           intent(in) :: use_ri,use_rijk,silent
  type(integ_info),  intent(in) :: intopt

  ! input/output
  type(timer_info),  intent(inout) :: timer


  ! for 4-index integrals
  type(int4idx)    :: twoeint

  character(len=80) :: filmo
  integer :: nvirt
  logical :: have_eri 
  integer(kli) :: memavail,memused

  memused = 0

  !
  ! Hartree Fock
  !
  if (lscf) then
    call timer_exec(timer,'init','SCF')
    memavail = memmax - memused
    call hfengine(escf,ScfCalcDef,silent,timer,iprint,atoms,basis,jkbas,use_rijk, &
                  intopt,tolint,charge,memavail,nocc,natoms,nspin)
    call timer_exec(timer,'measure','SCF')
  end if

  !
  ! CIS and TDHF
  !
  if (lcis) then
    call timer_exec(timer,'init','CIS')

    nvirt = basis%nbf - nocc(1)

    filmo = 'mos'
    call allocate_int4idx(twoeint,.false.,basis%nbf)

    have_eri = .false.
    call ciengine(escf,CisCalcDef,basis,tolint,have_eri,twoeint,memmax,filmo,nocc,nvirt,nspin)

    call deallocate_int4idx(twoeint)

    call timer_exec(timer,'measure','CIS')
  end if

  !
  ! MP2 and RI-MP2
  !
  if (lmp2) then 
    filmo = 'mos'

    nvirt = basis%nbf-nocc(1)

    if (use_ri) then
      call timer_exec(timer,'init','RI-MP2')
      call rimp2engine(escf,emp2,silent,Mp2CalcDef,basis,cbas,tolint,memmax,filmo,nocc,nvirt,nspin)
      call timer_exec(timer,'measure','RI-MP2')
    else
      call timer_exec(timer,'init','MP2')
      call allocate_int4idx(twoeint,.false.,basis%nbf)
      call mp2engine(escf,emp2,silent,Mp2CalcDef,basis,tolint,.false.,twoeint,memmax,filmo,nocc,nvirt,nspin)
      call timer_exec(timer,'measure','MP2')
      call deallocate_int4idx(twoeint)
    end if

  end if  

 end subroutine






subroutine getdipmom(atoms,shells,dens,natoms,nbf,nspin,nshell,mxcart,mxprim)
!------------------------------------------------------------------------------------+
!   Purpose: Caculates the dipole moment
!
!            1) Calculates the dipole moment integrals in the AO Basis
!            2) Contracts dipole integrals with density matrix in AO Basis
!
!------------------------------------------------------------------------------------+
  use type_atom
  use type_shell 
  use type_cgto

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'constants.h'
  include 'stdout.h'

  ! dimensions
  integer, intent(in) :: natoms,nbf,nspin,nshell,mxcart,mxprim

  ! input
  type(atom_info),   intent(in) :: atoms(natoms)
  type(shell_info),  intent(in) :: shells(nshell)
  real(kdp),         intent(in) :: dens(nbf,nbf,nspin)
 
  ! local
  integer   :: ijcart,ibf,jbf,ishl,jshl,ibfst,ibfnd,jbfst,jbfnd,icomp,jcomp,iat,ixyz,ispin
  integer   :: ncarti,ncartj,nprimi,nprimj
  real(kdp) :: dipole,center_of_charge(3),charge_sum,dip(3),dip_el(3),dip_nuc(3)
  type(cgto_info), pointer :: bfi, bfj
  real(kdp), allocatable :: mu(:,:,:),dmu(:,:)
  real(kdp), allocatable :: mux(:),muy(:),muz(:)

  real(kdp), allocatable :: bfiexps(:), bficoefs(:), bfinorms(:), &
                            bfjexps(:), bfjcoefs(:), bfjnorms(:)

  integer,   allocatable :: bfila(:), bfima(:), bfina(:), &
                            bfjla(:), bfjma(:), bfjna(:)


  !------------------------------------------------------------------------+
  ! memory allocation 
  !------------------------------------------------------------------------+
  allocate(mu(nbf,nbf,3))

  allocate(mux(mxcart*mxcart),muy(mxcart*mxcart),muz(mxcart*mxcart))

  allocate( bfiexps(mxprim*mxcart), bficoefs(mxprim*mxcart), bfinorms(mxprim*mxcart), &
            bfjexps(mxprim*mxcart), bfjcoefs(mxprim*mxcart), bfjnorms(mxprim*mxcart)  )

  allocate( bfila(mxcart), bfima(mxcart), bfina(mxcart), &
            bfjla(mxcart), bfjma(mxcart), bfjna(mxcart)  )

  !------------------------------------------------------------------------+
  ! actual routine 
  !------------------------------------------------------------------------+

  ! 1) calculate center of charge
  charge_sum = zero
  center_of_charge = zero
  do iat = 1, natoms
    charge_sum = charge_sum + atoms(iat)%charge
    center_of_charge = center_of_charge + atoms(iat)%charge * atoms(iat)%xyz
  end do
  center_of_charge = center_of_charge / charge_sum

  ! 2) Generate diople integrals in AO basis
  do ishl = 1, nshell
    ibfst = shells(ishl)%ibfstart; ibfnd = shells(ishl)%ibfend
    nprimi = shells(ishl)%nprim; ncarti = shells(ishl)%ncart
    call copybasinfo(bfiexps, bficoefs, bfinorms, bfila, bfima, bfina, shells, ishl, nprimi, ncarti, nshell)

    do jshl = 1, nshell
      jbfst = shells(jshl)%ibfstart; jbfnd = shells(jshl)%ibfend
      nprimj = shells(jshl)%nprim; ncartj = shells(jshl)%ncart
      call copybasinfo(bfjexps, bfjcoefs, bfjnorms, bfjla, bfjma, bfjna, shells, jshl, nprimj, ncartj, nshell)

      call mmd_dipole(mux,&
                      bfiexps,bficoefs,bfinorms,shells(ishl)%origin,bfila,bfima,bfina, &
                      bfjexps,bfjcoefs,bfjnorms,shells(jshl)%origin,bfjla,bfjma,bfjna, &
                      center_of_charge,'x',nprimi,nprimj,ncarti,ncartj)
      call mmd_dipole(muy,&
                      bfiexps,bficoefs,bfinorms,shells(ishl)%origin,bfila,bfima,bfina, &
                      bfjexps,bfjcoefs,bfjnorms,shells(jshl)%origin,bfjla,bfjma,bfjna, &
                      center_of_charge,'y',nprimi,nprimj,ncarti,ncartj)
      call mmd_dipole(muz,&
                      bfiexps,bficoefs,bfinorms,shells(ishl)%origin,bfila,bfima,bfina, &
                      bfjexps,bfjcoefs,bfjnorms,shells(jshl)%origin,bfjla,bfjma,bfjna, &
                      center_of_charge,'z',nprimi,nprimj,ncarti,ncartj)
 
      do ibf = ibfst, ibfnd
        icomp = ibf - ibfst + 1
        do jbf = jbfst, jbfnd
          jcomp = jbf - jbfst + 1

          ijcart = (jcomp-1)*ncarti + icomp

          mu(ibf,jbf,1) = mux(ijcart)
          mu(ibf,jbf,2) = muy(ijcart)
          mu(ibf,jbf,3) = muz(ijcart)
 
        end do
      end do
    
    end do
  end do

  ! 3) contract with density matrix
  allocate(dmu(nbf,nbf))
  dip = zero
  dip_el  = zero
  dip_nuc = zero
  do ixyz = 1, 3

    ! electronic contribution
    do ispin = 1, nspin
      do ibf = 1, nbf
        do jbf = 1, nbf
          dip(ixyz) = dip(ixyz) - dens(ibf,jbf,ispin) * mu(ibf,jbf,ixyz)
          dip_el(ixyz) = dip_el(ixyz) - dens(ibf,jbf,ispin) * mu(ibf,jbf,ixyz)
        end do
      end do
    end do

    ! add nuclear contribution
    do iat = 1, natoms
      dip(ixyz) = dip(ixyz) + atoms(iat)%charge*(atoms(iat)%xyz(ixyz)-center_of_charge(ixyz))
      dip_nuc(ixyz) = dip_nuc(ixyz) + atoms(iat)%charge*(atoms(iat)%xyz(ixyz)-center_of_charge(ixyz))
    end do
  end do

  write(6,'(/5x,6x,a12,a12,a12)') "nuc.", "el.","total" 
  write(6,'(5x,a,f12.8,f12.8,f12.8)')  "Dipole X:", dip_nuc(1), dip_el(1), dip(1) ! * 2.541580252938067
  write(6,'(5x,a,f12.8,f12.8,f12.8)')  "Dipole Y:", dip_nuc(2), dip_el(2), dip(2) ! * 2.541580252938067
  write(6,'(5x,a,f12.8,f12.8,f12.8)')  "Dipole Z:", dip_nuc(3), dip_el(3), dip(3) ! * 2.541580252938067
 
  dipole = sqrt(dip(1)*dip(1) + dip(2)*dip(2) + dip(3)*dip(3))  

  write(6,'(/5x,a,f14.8,2x,a,f14.8,2x,a)')  "|dipole|", dipole, "au", dipole * 2.541580252938067, "debye"
end subroutine

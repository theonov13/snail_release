
subroutine getlowdin(orb,shells,atoms,nmo,nbf,nshell,nocc,natoms)
!--------------------------------------------------------+
! Purpose: Performs Loewdin analysis
!--------------------------------------------------------+

  use type_shell
  use type_atom

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'
 
  ! constants
  logical, parameter :: isuhf = .false.

  ! dimensions
  integer, intent(in) :: nmo,nbf,nshell,nocc,natoms

  ! input
  real(kdp),        intent(in) :: orb(nmo,nbf)
  type(atom_info),  intent(in) :: atoms(natoms)
  type(shell_info), intent(in) :: shells(nshell)

  ! local
  real(kdp), allocatable :: ovlp(:,:), pop(:,:), dens(:,:), ovlpsqrt(:,:),  scratch(:,:)
  integer,   allocatable :: imapatom(:),imapbf(:)
  real(kdp) :: qelec, gap, qcharge
  integer   :: ibf, jbf, ishl, iatom, iatold, ibfst, ibfnd


  allocate(ovlp(nbf,nbf),pop(nbf,nbf),dens(nbf,nbf))

  allocate(imapatom(nbf),imapbf(natoms + 1))

  !-----------------------------------------------------------------------+
  ! print header information
  !-----------------------------------------------------------------------+
  call PrintHeader('Starting Loewdin population analysis')

  !-----------------------------------------------------------------------+
  ! Actual subroutine
  !-----------------------------------------------------------------------+

  ! get maping of basis functions to atoms
  iatold = -1
  imapbf(natoms + 1) = nbf + 1
  do ishl = 1, nshell
    ibfst = shells(ishl)%ibfstart; ibfnd = shells(ishl)%ibfend
    iatom = shells(ishl)%iatom

    if (iatold .ne. iatom) then
      iatold = iatom
      imapbf(iatom) = shells(ishl)%ibfstart
    end if 

    do ibf = ibfst, ibfnd
      imapatom(ibf) = iatom
    end do    
  end do

  ! Build density
  call builddens(isuhf, dens, orb, 1, nocc, nbf)

  ! Get overlap matrix
  call getovlp(ovlp, shells, nbf, nshell)

  allocate( ovlpsqrt(nbf,nbf), scratch(nbf,nbf) )
  call dcopy(nbf*nbf,ovlp,1,ovlpsqrt,1)
  call sqrtmat(ovlpsqrt,.false.,nbf)

  !---------------------------------+
  ! Build S^{1/2} D S^{1/2}
  !---------------------------------+
  ! scr = D x S^{1/2)
  call dgemm('n', 'n', nbf, nbf, nbf, one, dens, nbf, ovlpsqrt, nbf, zero, scratch, nbf)
  
  ! Build pop = S^{1/2) x scr
  call dgemm('n', 'n', nbf, nbf, nbf, one, ovlpsqrt, nbf, scratch, nbf, zero, pop, nbf)

  ! get number of electrons  Tr{ S^{1/2} D S^{1/2} }
  qelec = zero
  do jbf = 1, nbf
    qelec = qelec + pop(jbf,jbf)
  end do

  write(istdout,'(2x,a,f12.8,/)') "number of electrons loewdin:", qelec

  ! get atom population on specific atoms 
  do iatom = 1, natoms
    ibfst = imapbf(iatom); ibfnd = imapbf(iatom + 1) - 1
    gap = zero
    do ibf = ibfst, ibfnd
      gap = gap + pop(ibf,ibf) 
    end do
    ! calculate lowdin charge Q = Z - GAP
    qcharge = atoms(iatom)%charge - gap 
    write(istdout,'(1x,i4,2x,a3,f12.8)') iatom, atoms(iatom)%elem, qcharge
  end do

  deallocate(ovlp,pop,dens,imapatom,ovlpsqrt,scratch)
end subroutine





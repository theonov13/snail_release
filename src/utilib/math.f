!=====================================================================
! Some special functions mainly needed by the integral code 
!
!=====================================================================

  pure function doublefact(n) result(dfact2)
!-----------------------------------------------------------------+
! Purpose: Computes the double factorial (2n-1)!!
!-----------------------------------------------------------------+
    implicit none

    include 'fortrankinds.h'

    real(kdp) :: dfact2

    integer, intent(in) :: n

    ! local
    integer :: i
    real(kdp) :: dval(11) 

    ! cache some values for better performance...
    dval(1)  = 1.d0
    dval(2)  = 1.d0
    dval(3)  = 3.d0
    dval(4)  = 3.d0
    dval(5)  = 15.d0
    dval(6)  = 15.d0
    dval(7)  = 105.d0
    dval(8)  = 105.d0
    dval(9)  = 945.d0
    dval(10) = 945.d0
    dval(11) = 10395.d0

    if ( n.ge.-1.and.n.le.1) then
      dfact2 = 1.d0; return
    end if

    if (n .le. 11) then
      dfact2 = dval(n)
    else

      dfact2 = 1.0d0

      do i = 1, n, 2
        dfact2 = dfact2 * i
      end do

    end if

  end function

  function fj(j,l,m,a,b) result(dret)
   
     
    implicit none

    include 'fortrankinds.h'

    real(kdp) :: dret

    integer,   intent(in) :: j,l,m
    real(kdp), intent(in) :: a,b

    ! local
    integer :: low,high,im 
    ! functions
    real(kdp) :: binomial 


    ! In the expansion we have 0 <= im <= m, 0 <= il <= l.
    ! For the factor of x^j we must have im + il == j, thus we require
    ! that il = j - im. Obviously 0 <= j <= m + l.

    !  Sanity check
    if ((j.lt.0) .Or. ( j .gt. m + l)) then
      write(6,*) "Trying to compute fj for j=" , j , ", l=" , l , ", m=" , m , "!"      
      stop
    end If

    ! Now, requiring
    ! 0 <= im <= m
    ! 0 <= il <= l
    ! we get
    ! 0 <= im <= m
    ! 0 <= j - im <= l

    ! and we get the requirements
    ! 0 <= im <= m
    ! im <= j <= l+im
    ! and we get the loop limits as
    ! max(0,j-l) <= im <= min(m,j)

    if (j - l .gt. 0) then ; low = j - l ; else ; low = 0 ; end if

    If (m .lt. j) then ; high = m ; else ; high = j ; end If

    dret = 0.0d0

    do im = low, high
      dret = dret + binomial(m, im) * b**(m-im) * binomial(l, j - im) * a**(l - j + im)
    end do

    return
  end function

  pure function fact(num) result (dret)
!-----------------------------------------------------------------+
! Purpose: Calculates the factorial num!
!-----------------------------------------------------------------+
    implicit none

    include 'fortrankinds.h'

    real(kdp) :: dret

    integer, intent(in) :: num

    integer :: i


    dret = 1.0d0

    do i = 1, num
      dret = dret * i
    end do

    return
  end function

  function binomial(a,b) result (dret)
!-----------------------------------------------------------------+
! Purpose: Calculates the binomial (a over b)
!-----------------------------------------------------------------+
    implicit none

    include 'fortrankinds.h'

    real(kdp) :: dret

    integer, intent(in) :: a,b

    ! functions
    real(kdp) :: fact

    dret = fact(a) / fact(b) / fact(a - b)

    return
  end function


  function SolidRCartesian(l, m, x)
!---------------------------------------------------------------------------+
! Purpose: Calculates Solid Harmonics
!
!          $ R_{l m} = \sqrt{\frac{4 \pi}{2 l + 1}} r^l Y_{l m} $
!
! The implementation is based on Eq. 5.1.16 in Ref[1]
!
!  r^l Y_lm (\theta,\phi) = [ (2l+1)/(4\pi) (l+m)!(l-m)!]^(1/2)
!                         * \sum_pqs (-(x+iy)/2)^p ((x-iy)/2)^q z^s/p!q!r!
!
!
! [1] D.A. Varshalovich, A.N. Moskalev and V.K. Khersonsky, 
!     Quantum Theory of Angular Momentum, Nauka, Leningrad, 1975 (Russian) 
!---------------------------------------------------------------------------+

    implicit none

    include 'fortrankinds.h'
    include 'constants.h'
    include 'numbers.h'


    complex(kdp) :: SolidRCartesian
    integer, intent(in) :: l, m
    real(kdp), intent(in) :: x(3)
    integer :: p, q, s

    ! functions
    real(kdp) :: fact    

    SolidRCartesian = cplx_zero

    do p = 0, l
       q = p - m
       s = l - p - q

       if ((q >= 0) .and. (s >= 0)) then
          SolidRCartesian = SolidRCartesian + ( (cmplx(-0.5_kdp * x(1), -0.5_kdp * x(2), kdp)**p) &
                                            *   (cmplx( 0.5_kdp * x(1), -0.5_kdp * x(2), kdp)**q) &
                                            *   (x(3)**s) &
                                            /   (fact(p) * fact(q) * fact(s)) )
       end if
    end do

    SolidRCartesian = SolidRCartesian * sqrt(fact(l + m) * fact(l - m))

  end function SolidRCartesian


  function SphericalYCartesian(l, m, x)
!---------------------------------------------------------------------------+
! Purpose: Calculates Spherical Harmonics by rescaling Solid Harmonics
!
!          $ Y_{l m} = \sqrt{\frac{2l+1}{4\pi}} \frac{1}{r^l}  R_{l m} $
!
!---------------------------------------------------------------------------+

    implicit none

    include 'fortrankinds.h'
    include 'constants.h'
    include 'numbers.h'

    complex(kdp) :: SphericalYCartesian
    integer, intent(in) :: l, m
    real(kdp), intent(in) :: x(3)

    ! functions
    real(kdp)    :: fact
    complex(kdp) :: SolidRCartesian    

    SphericalYCartesian = SolidRCartesian(l, m, x) * sqrt(((2.0_kdp * l) + 1) / (4.0_kdp * pi)) &
                                                   * (dot_product(x,x)**(-0.5_kdp * l))

  end function SphericalYCartesian


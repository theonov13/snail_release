

 subroutine canorg(xmat,smat,nbf)
!---------------------------------------------------------------------------+
! Purpose: Performs the canonical/Loewdin/symmetric 
!          Orthogonalisation of the basis set
!
!  Input:
!        smat   overlap matrix
!        nbf    number of basis functions
!
!  Output:
!        xmat   Transformation matrix
!     
!
!---------------------------------------------------------------------------+

   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'
   include 'constants.h'

   integer, intent(in) :: nbf

   real(kdp), intent(out) :: xmat(nbf,nbf)
   real(kdp), intent(in)  :: smat(nbf,nbf)

   real(kdp), allocatable :: sei(:),umat(:,:),dmat(:,:),amat(:,:),work(:)
   integer   :: ibf,jbf,lwork,info
   real(kdp) :: dlen


   allocate(sei(nbf),umat(nbf,nbf),dmat(nbf,nbf),amat(nbf,nbf))

   ! diagonalize overlap
   call dcopy(nbf*nbf,smat,1,umat,1)

   lwork = -1
   call dsyev('V','U',nbf,umat,nbf,sei,dlen,lwork,info)
   lwork = int(dlen)
   allocate(work(lwork))

   info = 0
   call dsyev( 'V', 'U', nbf, umat, nbf, sei, work, lwork, info )

   if (info.ne.0 ) then
     call quit('diagonalization failed!','canorg')
   end if
  
   ! find linear dependend basis functions
   dmat = zero
   do ibf = 1, nbf
     if (sei(ibf) .le. ldpthrs) Then
       dmat(ibf,ibf) = zero
     else
       dmat(ibf,ibf) = one / dsqrt(sei(ibf))
     end if
   end do 
 
   ! A = D x U^T
   call dgemm('n', 't', nbf, nbf, nbf, one, dmat, nbf, umat, nbf, zero, amat, nbf)
   ! X = U x A'
   call dgemm('n', 'n', nbf, nbf, nbf, one, umat, nbf, amat, nbf, zero, xmat, nbf)

   do jbf = 1, nbf
     do ibf = 1, nbf
       if (dabs(xmat(ibf, jbf)) .lt. MACHEP) xmat(ibf, jbf) = zero
     end do
   end do

   deallocate(sei,umat,dmat,amat,work)
 end subroutine

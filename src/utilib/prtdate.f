subroutine prtdate()

  implicit none

  include 'stdout.h'

  ! local
  character(len=30)     :: date
  integer, dimension(8) :: values
  character(len=5)      :: zone

  call date_and_time(date(1:8),date(9:18),zone,values)
  date = date(1:4)//'-'//date(5:6)//'-'//date(7:8)//' '//date(9:10)//':'//date(11:12)//':'//date(13:18)

  write(istdout,'(/,4x,a,/)') date(1:24)
  call flush(istdout)


end subroutine prtdate 


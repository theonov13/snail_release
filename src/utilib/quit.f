subroutine quit(message,prog) 

  implicit none

  character(*), intent(in) :: message,prog 
 
  write(6,'(/1x,a)') message
  write(6,'(1x,a)') trim(prog) // ' ended abnormally'

  close(6)

  stop 'program stopped.'

end subroutine quit

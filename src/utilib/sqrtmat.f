
subroutine sqrtmat(mat,doinvert,ndim)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! dimensions
  integer, intent(in) :: ndim

  ! input/output
  real(kdp), intent(inout) :: mat(ndim,ndim)
  logical,   intent(in)    :: doinvert

  ! local
  integer   :: lwork, idx, jdx, info
  real(kdp) :: dlen

  real(kdp), allocatable :: temp1(:,:),temp2(:,:),work(:),eigval(:)

  allocate(eigval(ndim),temp1(ndim,ndim),temp2(ndim,ndim))

  !------------------------------------------------------------------------+
  ! Build square root of A and invert:  
  !   1) Do eigen decomposition A = V D V^T
  !   2) Build the square root of D and invert it
  !   3) Sandwich D^{1/2} between the eigen vectors
  !------------------------------------------------------------------------+

  ! first run to get dimension
  lwork = -1
  call dsyev( 'V', 'U', ndim, mat, ndim, eigval, dlen, lwork, info)

  lwork = int(dlen)
  allocate(work(lwork))

  ! actual run
  call dsyev( 'V', 'U', ndim, mat, ndim, eigval, work, lwork, info)

  ! save eigenvectors
  call dcopy(ndim*ndim,mat,1,temp2,1)

  ! (inverse) square root of diagonal matrix 
  do idx = 1, ndim 
    if (eigval(idx) > zero) then
      if (doinvert) then
        eigval(idx) = one / dsqrt(eigval(idx))
      else
        eigval(idx) = dsqrt(eigval(idx))
      end if
    else
      eigval(idx) = zero
    end if
  end do

  ! multiply the eigenvector with the diagonal matrix of eigenvalues
  do jdx = 1, ndim 
    do idx = 1, ndim 
      temp1(idx,jdx) = temp2(idx,jdx) * eigval(jdx) 
    end do
  end do

  !  Build output matrix 
  call dgemm( 'n', 't', ndim, ndim, ndim, one  &
            , temp1, ndim, temp2, ndim         &
            , zero, mat, ndim )

end subroutine

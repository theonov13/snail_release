!-------------------------------------------------------------------+
! Purpose: Performs a "Forward" contraction of an arbitray tensor A
! 
!           X_{ii..a..n} = A_{ii..l..n} C_l^a 
!
!
!   tensour_out : Output tensor
!   idims_out   : array with dimensions of the output tensor
!   tensor_in   : arbitrary tensor A_{ii..l..n}
!   matrix      : matrix C_l^a
!   idx         : Index along which the tensor will be contracted
!   ndim        : Number of dimension/order of the tensor
!   nrows       : Number of rows of matrix C_l^a
!   ncols       : Number of columns of matrix C_l^a
!   idims_in    : array with dimensions of the input tensor  
!   ndata_in    : Complete size of input tensor 
!   ndata_out   : Complete size of output tensor
!
!-------------------------------------------------------------------+
subroutine ContractForward(tensor_out,idims_out,&
                           tensor_in,matrix,idx,idims_in,&
                           ndim,nrows,ncols,ndata_in,ndata_out)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'

  ! constant
  logical, parameter :: locdbg = .false.

  ! dimensions
  integer,   intent(in) :: ndim,nrows,ncols,ndata_in,ndata_out

  ! input
  integer,   intent(in) :: idims_in(ndim)
  real(kdp), intent(in) :: matrix(nrows,ncols),&
                           tensor_in(ndata_in)
  integer,   intent(in) :: idx 

  ! input/output
  integer,   intent(out)   :: idims_out(ndim)
  real(kdp), intent(inout) :: tensor_out(ndata_out)

  ! local
  integer :: jdx,nloopdim,iadrin,iadrout,ihlp,&
             ncurin,ncurout,nlastin,nlastout, &
             nleft

  integer :: nthreads, nthreads_orig

  integer, allocatable :: ind(:),lndim(:),noff_in(:),noff_out(:),ldims_in(:),ldims_out(:)

  ! functions
  integer :: get_max_threads, mkl_get_max_threads

  !-----------------------------------------------------------------------+
  ! Define things for parrallel code 
  !-----------------------------------------------------------------------+
  nthreads = get_max_threads()
  nthreads_orig = mkl_get_max_threads()

  call mkl_set_num_threads(nthreads)

  ! over how much indices we have to loop (the other are implicitly in the dgemm)
  nloopdim = ndim - idx

  ! calculate how much "space" is to the left
  nleft = 1
  do ihlp = 1, idx - 1
    nleft = nleft * idims_in(ihlp)
  enddo

  if (nloopdim == 0) then
    ! special case where we only have on dgemm call
    call dgemm('n','n',nleft,ncols,nrows,one,tensor_in,nleft,&
               matrix,nrows,zero,tensor_out,nleft)
    
  else
    ! allocate memory for all the address fields
    allocate(ind(nloopdim),lndim(nloopdim),&
             ldims_in( nloopdim+1),&
             ldims_out(nloopdim+1),&
             noff_in( nloopdim),&
             noff_out(nloopdim))
    ind = 1

    ! set dimensions over which we have to loop
    do jdx = 1, nloopdim
      lndim(jdx) = idims_in(ndim - jdx + 1)
    end do

    ! set vector with output dimensions
    idims_out(:)   = idims_in(:)
    idims_out(idx) = ncols 

    ! calculate "local" dimensions
    jdx = 1
    do ihlp = idx, ndim
      ldims_in(jdx)  = idims_in(ihlp)
      ldims_out(jdx) = idims_out(ihlp)
      jdx = jdx + 1
    end do

    if (locdbg) then
      write(6,*) 'ldims_in',ldims_in
      write(6,*) 'ldims_out', ldims_out
    end if

    ! create vector with offsets (needed to calculate the addresses)
    nlastin  = ldims_in(1); nlastout = ldims_out(1)
    noff_in(nloopdim)  = nlastin 
    noff_out(nloopdim) = nlastout
    
    do jdx = nloopdim - 1, 1, -1
      ncurin  = nlastin*ldims_in(nloopdim - jdx + 1)
      ncurout = nlastout*ldims_out(nloopdim - jdx + 1)

      noff_in(jdx)  = ncurin
      noff_out(jdx) = ncurout

      nlastin  = ncurin
      nlastout = ncurout
    end do

    do jdx = 1, nloopdim
      noff_in(jdx)  = noff_in(jdx)*nleft
      noff_out(jdx) = noff_out(jdx)*nleft
    end do

    if (locdbg) then
      write(6,*) 'noff_in',noff_in
      write(6,*) 'noff_out',noff_out
    end if

    !---------------------------------------------------------------------*
    ! realization of arbitrary many nested loops via the basic counting
    ! principle to ensure that this routine works for any kind of tensor
    !---------------------------------------------------------------------*

    do while (.true.)

      ! calculate start address in input and output array 
      iadrin = 1
      iadrout = 1
      do ihlp = 1, nloopdim
        iadrin  = iadrin  + (ind(ihlp) - 1) * noff_in(ihlp) 
        iadrout = iadrout + (ind(ihlp) - 1) * noff_out(ihlp)
      end do

      ! perform the operation for this loop cycle aka dgemm call
      call dgemm('n','n',nleft,ncols,nrows,one,tensor_in(iadrin),nleft,&
                 matrix,nrows,zero,tensor_out(iadrout),nleft)

      ! increment indices
      do jdx = nloopdim, 1, -1
        ind(jdx) = ind(jdx) + 1
        if (ind(jdx) <= lndim(jdx)) then
          exit
        else
          ind(jdx) = 1
        end if
      end do

      if (jdx < 1) exit

    end do

    ! clean up
    deallocate(ind,lndim,noff_in,noff_out,ldims_in,ldims_out)
  end if 

  ! reset threads in MKL
  call mkl_set_num_threads(nthreads_orig)

end subroutine


 pure function dist2_3d(x1,y1,z1,x2,y2,z2) result(dret)
 !---------------------------------------------------------------------+
 ! Purpose: Calculates the squared distance between two points
 !---------------------------------------------------------------------+
   implicit none

   include 'fortrankinds.h'

   real(kdp) :: dret

   real(kdp), intent(in) :: x1,y1,z1,x2,y2,z2

   dret = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) + (z1 - z2) * (z1 - z2)

   return
 end function


 function dist2(xyza,xyzb) result(dret)
 !---------------------------------------------------------------------+
 ! Purpose: Calculates the squared distance between two vectors
 !---------------------------------------------------------------------+

   implicit none
    
   include 'fortrankinds.h'
   include 'numbers.h'

   real(kdp) :: dret

   real(kdp), intent(in) :: xyza(3),xyzb(3)

   real(kdp) :: dxyz(3)
 
   ! functions
   real(kdp) :: ddot

   call dcopy(3,xyza,1,dxyz,1)
   call daxpy(3,-one,xyzb,1,dxyz,1)

   dret = ddot(3,dxyz,1,dxyz,1) 

   return
 end


 function dist(xyza,xyzb) result(dret)
 !---------------------------------------------------------------------+
 ! Purpose: Calculates the distance between two vectors
 !---------------------------------------------------------------------+

   implicit none
   
   include 'fortrankinds.h'
   
   real(kdp) :: dret

   real(kdp), intent(in) :: xyza(3),xyzb(3)

   ! functions
   real(kdp) :: dist2

   dret = dsqrt(dist2(xyza,xyzb))

   return
 end

 pure subroutine swap(i,j)
 !---------------------------------------------------------------------+
 ! Purpose: swaps two indices
 !---------------------------------------------------------------------+
   integer, intent(inout) :: i,j
   integer :: k
   k = i; i = j; j = k
 end subroutine

 pure function product_center_1D(alpha,xa,beta,xb) result(dret)
 !---------------------------------------------------------------------+
 ! Purpose: Calculates the product center of two gaussians
 !---------------------------------------------------------------------+
   implicit none

   include 'fortrankinds.h'

   real(kdp) :: dret

   real(kdp), intent(in) :: alpha,xa,beta,xb

   dret = (alpha*xa+beta*xb)/(alpha+beta)

   return
 end function

 pure subroutine product_center(dret,alpha,xa,beta,xb) 
 !---------------------------------------------------------------------+
 ! Purpose: Calculates the product center of two gaussians
 !---------------------------------------------------------------------+
   implicit none

   include 'fortrankinds.h'
   include 'numbers.h'

   real(kdp), intent(out) :: dret(3)

   real(kdp), intent(in)  :: alpha,xa(3),beta,xb(3)

   ! Note: Here is no benefit using BLAS routines (it will even slow down) 
 
   dret = (alpha * xa + beta * xb)/(alpha+beta)

   return
 end subroutine


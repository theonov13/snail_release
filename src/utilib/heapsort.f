

subroutine heapsort(iarr,idx,ninfo,ndim)

  implicit none

  include 'fortrankinds.h'
  
  ! dimension:
  integer,   intent(in) :: ninfo,ndim
  
  ! input:
  integer,   intent(in) :: idx

  ! output:
  integer,   intent(inout) :: iarr(ninfo,ndim)
  
  ! local:
  integer  :: istart,ibottom,istr,info
  integer  :: temp

  istr = ndim / 2
  do istart = istr, 1, -1
    call siftdown(idx,istart,ndim,iarr,ninfo,ndim)
  end do
 
  do ibottom = ndim, 2, -1
    do info = 1, ninfo
      temp = iarr(info,1)
      iarr(info,1) = iarr(info,ibottom)
      iarr(info,ibottom) = temp
    end do
    call siftdown(idx,1,ibottom,iarr,ninfo,ndim)
  end do
 
end subroutine heapsort

subroutine siftdown(idx,istart,ibottom,iarr,ninfo,ndim)

  implicit none

  include 'fortrankinds.h'
  
  integer,  intent(in)    :: idx,istart,ibottom,ninfo,ndim
  integer,  intent(inout) :: iarr(ninfo,ndim)
 
  integer  :: ichild,iroot,info
  integer  :: temp
 
  iroot = istart
  do while(iroot*2 < ibottom)
    ichild = iroot*2
 
    if(ichild + 1 < ibottom) then
      if(iarr(idx,ichild) < iarr(idx,ichild+1)) ichild = ichild + 1
    end if
 
    if(iarr(idx,iroot) < iarr(idx,ichild)) then
      do info = 1, ninfo
        temp              = iarr(info,ichild)
        iarr(info,ichild) = iarr(info,iroot)
        iarr(info,iroot)  = temp
      end do
      iroot = ichild
    else
      return
    end if  
  end do      
 
end subroutine siftdown

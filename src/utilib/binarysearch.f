
!-----------------------------------------------------------------------------------------+
!  Binary search in sorted array with O(log(n)) costs and O(1) space requirements
!-----------------------------------------------------------------------------------------+
function binarysearch(array,find,nsize) result(ind) 

  implicit none

  include 'fortrankinds.h'
 
  ! output
  integer :: ind

  ! dimensions
  integer, intent(in) :: nsize

  ! input
  integer, intent(in) :: array(nsize), find
  

  ind = binarySearchKernel(array, find, 0, nsize)

end function


function binarySearchKernel(array, find, left, right) result(ind)

  implicit none

  include 'fortrankinds.h'

  ! dimension
  integer, intent(in) :: right

  ! input
  integer, intent(in) :: array(right), find, left

  ! local
  integer :: middle, potentialMatch

  do while (left <= right)
    middle = (left + right) / 2
    potentialMatch = array(middle)
 
    if (find == potentialMatch)
      ind = middle
      return
    else if (find < potentialMatch)
      right = middle - 1
    else
      left = middle + 1
    end if
  end do

  ind = -1
end function

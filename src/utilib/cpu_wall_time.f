subroutine cpu_wall_time(cpu,wall)

  implicit none

  include 'fortrankinds.h'

  ! constants
  integer, parameter :: base_year=1970

  ! output
  real(kdp), intent(out) :: cpu,wall 

  ! local
  integer, dimension(8) :: values
  character(len=8)      :: date
  character(len=10)     :: time
  character(len=5)      :: zone
  integer               :: delta_unix_days, elapsed_unix_days, day_delta, iyr

  ! functions
  integer :: day_of_year 

  ! for getting the CPU time we use the fortran instrinsic cpu_time
  call cpu_time(cpu)

  ! getting the wall time is more difficult..
  call date_and_time(date,time,zone,values)

  if (values(1) < base_year) then
     write(6,*) "Dates before UNIX birth 01.01.1970 are not supported."
     elapsed_unix_days = 0
  else
     day_delta = -1
     do iyr = base_year, values(1) - 1
        day_delta = day_delta + day_of_year(iyr,12,31)
     end do
     elapsed_unix_days = day_delta + day_of_year(values(1),values(2),values(3))
  end if

  delta_unix_days = elapsed_unix_days

  wall = (((real(delta_unix_days,kdp) * 24_kdp) + &
       &    real(values(5),kdp)) * 60_kdp       + &
       &    real(values(6),kdp)) * 60_kdp       + &
       &    real(values(7),kdp)                 + &
       &    real(values(8),kdp) / 1000_kdp


end subroutine


function day_of_year(year,month,day) result(iret)

  implicit none

  ! output
  integer :: iret

  ! input
  integer, intent(in) :: day, month, year


  ! local
  integer :: imn

  ! functions
  integer :: num_days_in_month

  iret = 0
 
  do imn = 1, month - 1
    iret = iret + num_days_in_month(imn,year)
  end do

  iret = iret + day

  return
end function


function num_days_in_month(month, year) result(iret)

  implicit none

  ! output
  integer :: iret

  ! input
  integer, intent(in) :: month, year

  ! local
  integer, dimension(12) :: days_per_month

  days_per_month = (/ 31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 /)

  days_per_month(2) = 28
  if ((mod(year,4) == 0 .and. mod(year,100) /= 0) .or. mod(year,400) == 0) then
     days_per_month(2) = 29
  end if

  if ((month < 1) .or. (month > 12)) then
     write(6,*) "Invalid month number : ", month
     iret = 0
  else
     iret = days_per_month(month)
  end if

  return
end function


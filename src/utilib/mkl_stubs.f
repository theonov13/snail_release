#if !defined(MKL)
!
! Provide dummies for MKL procedures
! Reference: https://software.intel.com/en-us/mkl-developer-reference-fortran
!

!-------------------------------------------------------------------+
! Threading Control
!-------------------------------------------------------------------+

subroutine mkl_set_num_threads(n)
  integer :: n
end subroutine mkl_set_num_threads

subroutine mkl_set_dynamic(n)
  integer :: n
end subroutine mkl_set_dynamic

function mkl_get_max_threads()
  integer :: mkl_get_max_threads
  mkl_get_max_threads = 1
end function mkl_get_max_threads

function mkl_get_dynamic()
  integer :: mkl_get_dynamic
  mkl_get_dynamic = 1
end

!-------------------------------------------------------------------+
! Memory Management
!-------------------------------------------------------------------+

subroutine mkl_free_buffers()
  ! empty
end subroutine mkl_free_buffers

function mkl_disable_fast_mm()
  integer :: mkl_disable_fast_mm
  mkl_disable_fast_mm = 1
end function mkl_disable_fast_mm

function mkl_mem_stat()
  integer :: mkl_mem_stat
  mkl_mem_stat = 1
end function mkl_mem_stat

#endif

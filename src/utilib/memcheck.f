subroutine memcheck(memmax,memused,message,routine)

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'

  integer(kli), intent(in) :: memmax,memused
  character(*), intent(in) :: message,routine

  if (memused.gt.memmax) then
    write(istdout,*) message
    write(istdout,*) 'memory exceeded in', routine
    stop 'porgram aborted..'
  end if

end subroutine

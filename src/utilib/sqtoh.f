subroutine sqtoh(n,a,b)
! ---------------------------------------------------------------------
!      contract full matrix a to trigonal matrix b
!      a and b may refer to the same address
! ---------------------------------------------------------------------
    implicit none

    include 'fortrankinds.h'

    integer, intent(in) :: n
    real(kdp), intent(inout) :: a(n,n),b(n*(n+1)/2)
    integer i, j, k

    k = 0
    do i = 1, n
      do j = 1, i
        b(k+j) = a(j,i)
      end do
      k = k + i
    end do

end subroutine sqtoh


integer function get_max_threads()
!----------------------------------------------------------------------*
!     return the maximum number of threads that can be started at all
!----------------------------------------------------------------------*

  implicit none

#if defined (NOOMP)
  get_max_threads = 1
  return
#else
  ! OMP:
  integer :: omp_get_max_threads
  get_max_threads = omp_get_max_threads() 
  return
#endif

end function

integer function get_thread_num()
!----------------------------------------------------------------------*
!     return thread number (ID) within the team, that lies between
!     0 and get_thread_num()-1, the master will get ID 0.
!----------------------------------------------------------------------*
  implicit none

#if defined (NOOMP)
   get_thread_num = 0
   return
#else
   ! OMP:
   integer :: omp_get_thread_num
   get_thread_num = omp_get_thread_num() 
   return
#endif

end

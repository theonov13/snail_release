subroutine prtenergy(escf,epair,edoub,lmp2)

  include 'fortrankinds.h'
  include 'stdout.h'

  ! SCS values from Grimme
  real(kdp), parameter :: scscos = 1.2d0, scscss = 1.0d0/3.0d0

  ! input
  real(kdp), intent(in) :: escf,epair,edoub(2)
  logical,   intent(in) :: lmp2

  ! local
  real(kdp) :: scsepair

  write(istdout,'(3x,a,a,a)') '+', repeat("-",51), '+'
  write(istdout,'(3x,a,a,a)') '+', repeat(" ",51), '+'

  write(istdout,'(2x,a2,x,a28,f16.10,a3,3x,a)') '+','RHF energy:                 ', escf , 'Eh','+'

  if (lmp2) then
    scsepair = scscos * edoub(1) + ((scscos+2.0d0*scscss)/3.0d0) * edoub(2)

    write(istdout,'(2x,a2,1x,a28,f16.10,a3,3x,a)') '+','Final MP2 energy:           ', escf + epair, 'Eh','+'
    write(istdout,'(2x,a2,1x,a28,f16.10,a3,3x,a)') '+','MP2 correlation energy:     ', epair, 'Eh','+'

    write(istdout,'(3x,a,a,a)') '+', repeat(" ",51), '+'

    write(istdout,'(2x,a2,1x,a28,f16.10,a3,3x,a)') '+','Final SCS-MP2 energy:       ', escf + scsepair, 'Eh','+'
    write(istdout,'(2x,a2,1x,a28,f16.10,a3,3x,a)') '+','SCS-MP2 correlation energy: ', scsepair, 'Eh','+'

    write(istdout,'(3x,a,a,a)') '+', repeat(" ",51), '+'

    write(istdout,'(3x,a,f16.10,2x,a,f16.10,2x,a)') '+ E(S)  =',edoub(1),               'E(T)  =',edoub(2),'+'
    write(istdout,'(3x,a,f16.10,2x,a,f16.10,2x,a)') '+ E(OS) =',edoub(1)+edoub(2)/3.0d0,'E(SS) =',edoub(2)*2.0d0/3.0d0,'+'
  end if

  write(istdout,'(3x,a,a,a)') '+', repeat(" ",51), '+'
  write(istdout,'(3x,a,a,a)') '+', repeat("-",51), '+'
  

end subroutine prtenergy


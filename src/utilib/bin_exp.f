
!------------------------------------------------------------+
! Purpose: Performs "binary exponentiation" basis**expo, 
!          which is limited to only integer exponents, but
!          which can be faster than the generic ** operator 
!
!
! Gunnar Schmitz, August 2018
!------------------------------------------------------------+
elemental function bin_exp(basis,expo) result(dret)

  implicit none

  include 'fortrankinds.h'

  real(kdp) :: dret

  integer,   intent(in) :: expo
  real(kdp), intent(in) :: basis

  ! local
  integer :: h
  real(kdp) :: cur

  dret = 1.0
  cur = basis
  h = expo

  do while (h > 0)
    if (iand(h,1) > 0) dret = dret * cur

    cur = cur * cur
    h = ishft(h,-1)
  end do

  return
end function

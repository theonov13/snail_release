subroutine prttime(tdiff,timlab)

  include 'fortrankinds.h'
  include 'stdout.h'
  real(kdp), parameter :: timday=86400.d0,timhr=3600.d0,timmin=60.d0
  
  real(kdp), intent(in) :: tdiff
  character(len=*), intent(in) :: timlab
  
  integer ndays, nhours, nmins, nsecs
  real(kdp) thour, tmin, tsec
  
  ndays=tdiff/timday
  thour=tdiff-ndays*timday
  nhours=thour/timhr
  tmin=thour-nhours*timhr
  nmins=tmin/timmin
  tsec=tmin-nmins*timmin
  nsecs=nint(tsec)
  if (nsecs.eq.60) then
    nsecs = 0
    nmins = nmins + 1
  endif
  if (nmins.eq.60) then
    nmins = 0
    nhours = nhours + 1
  endif
  if (nhours.eq.24) then
    nhours = 0
    ndays  = ndays + 1
  endif
  if (ndays.gt.0) then
    write(istdout,902) timlab,ndays,nhours,nmins,nsecs
  elseif (nhours.gt.0) then
    write(istdout,903) timlab,nhours,nmins,nsecs
  elseif (nmins.gt.0) then
    write(istdout,904) timlab,nmins,nsecs
  else
    write(istdout,905) timlab,tdiff
  endif
  902 format(5x,'Total ',a,'-time  : ',i3,' days ' ,i2,' hours ',i2,' minutes and ',i2,' seconds')
  903 format(5x,'Total ',a,'-time  : ',i2,' hours ',i2,' minutes and ',i2,' seconds')
  904 format(5x,'Total ',a,'-time  : ',i2,' minutes and ',i2,' seconds')
  905 format(5x,'Total ',a,'-time  : ',f6.2,' seconds')

  return
end subroutine prttime


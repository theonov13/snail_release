!-----------------------------------------------------------------------------------+
! Calculate Matrix vector product of CI matrix (for RPA and CIS) and trial vector
!
! Note: At the moment this routine is not integral direct
! 
!-----------------------------------------------------------------------------------+

subroutine ci_ampvec(mv, vec, iexc, onlydiag, lrpa, escf, shells, tolint, lmax,   &
                     orb, orben, kmnrs, scratch1, scratch2,                       &
                     have_eri, twoeint, basis, memmax, filmo,                     &
                     nocc, nvir, nexc, nbf, nshell, mxsim, mxprim, mxcart)

  use type_cgto
  use type_shell
  use type_int4idx
  use type_basis
  use type_integ
  use IntegralBuffer

  implicit none

  include 'fortrankinds.h'
  include 'numbers.h'
  include 'stdout.h'

  ! constants
  logical, parameter :: use_contractor = .true.
  integer, parameter :: ndim = 4
  integer, parameter :: iprint = 1
  logical, parameter :: silent = .false.

  ! dimensions
  integer, intent(in) :: nexc, nocc, nvir, nbf, nshell, mxprim, mxcart, mxsim

  ! output
  real(kdp), intent(out) :: mv(nexc,mxsim)
  
  ! input/output
  type(int4idx) ,    intent(inout) :: twoeint
  real(kdp),         intent(inout) :: kmnrs(nbf,nbf,nbf,nbf),    &
                                      scratch1(nbf,nbf,nbf,nbf), &
                                      scratch2(nbf,nbf,nbf,nbf)

  ! input
  real(kdp), intent(in) :: escf 
  type(shell_info),  intent(in) :: shells(nshell)
  type(basis_info),  intent(in) :: basis
  character(len=80), intent(in) :: filmo
  real(kdp),         intent(in) :: tolint,vec(nexc,mxsim)
  real(kdp),         intent(in) :: orben(nbf), orb(nbf,nbf)

  logical,           intent(in) :: have_eri,onlydiag,lrpa
  integer(kli),      intent(in) :: memmax
  integer,           intent(in) :: lmax,iexc(nexc,2)

  ! local
  integer :: a, b, i, j, ia, jb, idm, imo, jmo, moa, mob, &
             ibf, jbf, lbf, kbf, nmat, ndata, ind, ncbf,  &
             istart, iend, irun, nrun, isim
  integer      :: nscreen
  integer(kli) :: memused,memavail

  real(kdp)    :: cpu2ein,cpu0,cpux
  real(kdp)    :: Aiajb, Biajb 

  integer, allocatable :: idims_in(:),idims_out(:)

  type(TIntBuffer), allocatable :: intbuf(:)
  type(integ_info) :: intopt


  real(kdp), allocatable :: gab(:,:)
  real(kdp), allocatable :: mv_old(:,:)
 
  real(kdp) :: dummy

  integer :: nthreads,ithrd

  ! functions
  integer :: get_max_threads, get_thread_num

  !-----------------------------------------------------------------------+
  ! Define things for parrallel code 
  !-----------------------------------------------------------------------+
  nthreads = get_max_threads()

  ! init
  ncbf = basis%ncbf

  memused = 0

  !-----------------------------------------------------------------------+
  !  calculate ERIs or reuse them
  !-----------------------------------------------------------------------+

  if (.not.have_eri) then

    ! init Buffer to calculate two electron integrals
    allocate(intbuf(0:nthreads-1))
    do ithrd = 0, nthreads - 1
      call allocate_intbuf(intbuf(ithrd),intopt,lmax,mxcart,mxprim,silent)
    end do

    write(istdout,'(5x,a)')  'Start calculation of AO-Integrals...'
    call cpu_time(cpu0)
    memavail = memmax - memused
    allocate(gab(nshell,nshell))
    call gettwoeints(twoeint,intbuf,dummy,.false.,1,.true.,.false.,gab,.false.,dummy,tolint,&
                     nscreen,memavail,lmax,shells,iprint,silent,ncbf,nbf,nshell,mxprim,mxcart,nthreads)
    deallocate(gab)
    call cpu_time(cpux)
    cpu2ein = cpux - cpu0

    write(istdout,'(5x,a,f10.2,/)')  'CPU-time for 2-el Ints (sec):', cpu2ein
  end if

  !-----------------------------------------------------------------------+
  ! Unpack integrals
  !-----------------------------------------------------------------------+

  if (.not.have_eri) then
    ! unpack AO-Integrals 
    do lbf = 1, nbf
      do jbf = 1, nbf
        do kbf = 1, nbf
          do ibf = 1, nbf
            kmnrs(ibf,jbf,kbf,lbf) = int4idx_getvalue(twoeint,ibf,jbf,kbf,lbf)
          end do 
        end do
      end do 
    end do
  end if

  !-----------------------------------------------------------------------+
  ! use inefficient way to transform AO-integrals to MO-integrals....
  !-----------------------------------------------------------------------+

  if (.not.have_eri) then
    if (use_contractor) then
     
      allocate(idims_in(ndim),idims_out(ndim))

      do idm = 1, ndim
        idims_in(idm) = nbf
      end do

      ndata = nbf*nbf*nbf*nbf

      ! 1.) Transform first index
      call ContractForward(scratch1, idims_out, kmnrs,    orb, 4, idims_in, ndim, nbf, nbf, ndata, ndata)

      ! 2.) Transform second index
      call ContractForward(scratch2, idims_out, scratch1, orb, 3 ,idims_in, ndim, nbf, nbf, ndata, ndata)

      ! 3.) Transform third index
      call ContractForward(scratch1, idims_out, scratch2, orb, 2, idims_in, ndim, nbf, nbf, ndata, ndata)

      ! 4.) Transform fourth index
      call ContractForward(kmnrs,    idims_out, scratch1, orb, 1, idims_in, ndim, nbf, nbf, ndata, ndata)

      deallocate(idims_in, idims_out)
    else

      call quit("Not implemented","CI-Mat")
    end if
  end if

  !----------------------------------------------------------------------+
  ! Calculate Matrix vector product
  !----------------------------------------------------------------------+
  if (lrpa) allocate(mv_old(nexc,mxsim))

  nrun = 1
  if (lrpa) nrun = 2
  do irun = 1, nrun

    if (irun == 2) call dcopy(nexc*mxsim,mv,1,mv_old,1)
    mv(:,:) = zero

    do jb = 1, nexc
      j = iexc(jb, 1)
      b = iexc(jb, 2)

      istart = 1
      iend = nexc
      if (onlydiag) then
        istart = jb
        iend = jb
      end if

      do ia = istart, iend
        i = iexc(ia, 1)
        a = iexc(ia, 2)

        Aiajb = two * kmnrs(a,i,j,b) - kmnrs(a,b,j,i)

        if ((a == b) .and. (i == j)) then
          Aiajb = Aiajb  + orben(a) - orben(i)
        end if

        if (lrpa) Biajb = two * kmnrs(a,i,b,j) - kmnrs(a,j,b,i) 

        if (lrpa) then
          if (irun == 1) then
            do isim = 1, mxsim
              mv(jb,isim) = mv(jb,isim) + (Aiajb - Biajb) * vec(ia,isim)
            end do
          elseif (irun == 2) then
            do isim = 1, mxsim
              mv(jb,isim) = mv(jb,isim) + (Aiajb + Biajb) * mv_old(ia,isim)
            end do
          end if
        else
          do isim = 1, mxsim
            mv(jb,isim) = mv(jb,isim) + Aiajb * vec(ia,isim)
          end do
        end if

      end do
    end do
  end do

  if (lrpa) deallocate(mv_old)

  if (.not.have_eri) then
    do ithrd = 0, nthreads - 1
      call deallocate_intbuf(intbuf(ithrd))
    end do
  end if
end subroutine

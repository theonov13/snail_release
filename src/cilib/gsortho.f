!==================================================================================================================================!
subroutine gsortho(obasis, idx, thrldp, lskipped, ndim1, ndim2)
!----------------------------------------------------------------------------------------------------------------------------------!
!  Purpose: Performs Gram Schmidt Orthonormalisation(s)  
!----------------------------------------------------------------------------------------------------------------------------------!
      implicit none

      include 'fortrankinds.h'
      include 'stdout.h'
      include 'numbers.h'

!-----dimensions     
      integer, intent(in)  :: ndim1, ndim2


!-----arguments
      real(kdp), intent(inout)  :: obasis(ndim1,ndim2)
      real(kdp), intent(in)     :: thrldp
      integer,   intent(in)     :: idx
      logical,   intent(out)    :: lskipped

!-----local
      integer   :: jvec
      real(kdp) :: dtemp 

!-----functions
      real(kdp) :: ddot,dnrm2


!-----init

      lskipped = .false.

!----------------------------------------------------------------------------------------------------------------------------------!
!     Subroutine    
!----------------------------------------------------------------------------------------------------------------------------------!

      !---------------------------------------------------------+
      ! orthogonalize vector onto the existing basis (obasis) 
      !---------------------------------------------------------+
      do jvec = 1, idx - 1

        dtemp = ddot(ndim1, obasis(1,idx),1, obasis(1,jvec),1)
 
        call daxpy(ndim1,-dtemp,obasis(1,jvec),1,obasis(1,idx),1)
      end do

      !---------------------------------------------------------+
      !  normalize vector
      !---------------------------------------------------------+

      ! Determine norm of vector 
      dtemp = dnrm2(ndim1,obasis(1,idx), 1)

      ! test if linear dependent and skip if it's the case 
      if (dtemp.ne.dtemp) then
        write(istdout,*) 'Warning: NaN norm in gsortho'
        write(istdout,*) ' --> skip trial vector...'
        lskipped = .true.
      else if (dtemp.lt.thrldp) then
        lskipped = .true.
      else
        call dscal(ndim1, one / dtemp, obasis(1,idx), 1)
      end if
   
end subroutine gsortho
!==================================================================================================================================!

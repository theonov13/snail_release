SNAIL - the slow and inefficient QM program -
==============

Quick guide
--------------

**Compiling**

- go to src directory
- ./configure.sh
- make
- binary is put in bin
- add to your .bashrc 

```
  export SNAILDIR=<PATH to your SNAIL top dir>
  export PATH=$SNAILDIR/bin:$PATH

```

Idea
--------------

As the name suggests this program is not intended to be very efficient,
but to deliver an hopefully easy to understand code base, so that (under)graduate
students are able to follow/implement e.g. a simple Hartree-Fock program.  

The first time I had access to a widely used QM code, I was intimidated, when 
I tried to understand the HF code or even just the routine for the Hückel guess. 
I knew that the code was intended to be efficient and probably thus harder to 
follow, but it was a discouraging experience...  

Snail aims to be the opposite. It is an inefficient code, but (hopefully)
so easy to understand, that it helps students to get a deeper insight
and to play around with these methods. I intentionally still use Fortran as programming language
to still be as close to what you probably can expect in a more sophisticated QM program. 
Nevertheless, snail shows that it does not require black magic to implement Hartree-Fock or MP2. 

In my view Theoretical Chemistry has from a didactically point of view the problem that it consists of three steps:

1. The derivation of mathematical models
2. The implementation of the theoretical methods
3. Application of the theoretical methods

The lectures naturally capture step 1 and in practical courses students get familiar with step 3, but
very often one has to learn step 2 on its own when starting in a PhD program. But also implementing the
well know models (again and again) helps to get a deeper understanding. Why something scales as it does and
why a certain approximation is a good one. It also helps to acknowledge the work
of our predecessors. When I implemented Hartree-Fock the first time for my self I was amazed how slow convergent
or non convergent the SCF procedure can be for rather innocent molecules when no damping is applied, 
no DIIS is used or no level shifts are employed.  

**Implemented methods:**
 - Hartree-Fock (RHF and UHF)
 - MP2 and RI-MP2 energies
 - CIS and RPA/TDHF excitation energies

**Issues/Remarks/Improvements:**
 - At the moment all integrals are kept in memory (-> put them to disk)
 - Only Cartesian Gaussian are used. (-> transform them after integral evaluation into spherical Gaussian )

**Supported Compiler**
 - ifort
 - gfortran
 - pgfortran
 - flang (no OpenMP support yet)


Feel free to contribute, but keep it simple and well documented.   



Release Notes:
--------------

**Version 2020.3.0**

 - Initial OpenMP parallelisation of most modules
 - Support for more compiler. Snail can now be compiled with ifort, gfortran, pgfortran and flang
 - For Developers: Automatic testing
 - Frozen core approximation for MP2/RI-MP2 

**Version 2020.1.0**

 - Export of electron density and MOs as cube file
 - Improved one-electron code
    - Implemented three center overlap integrals
 - RI-MP2 with overlap metric (not recommended to use)
 - Normal mode analysis based on a user provided Hessian

**Version 2019.7.0 (Initial release)**

  - RHF (conventional, direct, RI-JK)
     - Dipole moments
     - Mulliken population analysis
     - Loewdin population analysis
  - UHF (conventional, direct, RI-JK)
     - Dipole moments
  - MP2 and SCS-MP2 for RHF reference
  - RI-MP2 and SCS-RI-MP2 for RHF reference
  - CIS and TDHF excitation energies for RHF reference


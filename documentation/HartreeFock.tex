
\chapter{Hartree Fock calculations}

\section{Usage}

Snail currently allows to do conventional and integral direct restricted Hartree Fock (RHF) 
and unrestricted Hartree Fock (UHF) calculations. Using either the conventional or the
integral direct mode the RI-JK approximation can be used. However, for the conventional
calculations this leads formally to a worse scaling. Snail also automatically calculates
the dipole moment after the HF calculation is converged and performs a Mulliken and L\"{o}wdin
population analysis. Below you can find a sample input for water using an integral direct
approach. 

\begin{Example}{Hartree Fock input}
 $title
   "Calculation on Water"
 
 $wavefunction
   RHF
 
 $basis
  def2-SVP
 
 $scf
   direct

 $tolint
   1.d-8
 
 $coord
   3
   O -0.01020 0.0000 -0.0079
   H -0.00760 0.0000  1.8294
   H  1.76900 0.0000 -0.4660
 
\end{Example}

Note that the conventional algorithm is default and that the direct mode has to be activated via
\begin{verbatim}
   $scf
     direct
\end{verbatim}
The different blocks in the input file are hopefully self-explanatory, but we go through them one by one.
\begin{itemize}
  \item \verb=$wavefunction= defines if a RHF or UHF wave function should be used/constructed.
  \item \verb=$basis= defines the orbital basis set to be used in the calculation. 
        Note that currently snail only supports a few basis sets. But you can extend them on your own. Have a look at
        \verb=basis/basis.all= in your snail directory. The format is similar to the Turbomole\cite{turbo} format.
  \item \verb=$scf= sets some option for the SCF calculation. In the moment the possible options are 
        \begin{itemize}
           \item \verb=direct=  for an in integral direct calculations of integrals or \verb=not_direct= for the conventional integral evaluation.
           \item \verb=scftol <int>= sets the tolerance w.r.t. energy changes in the HF energy and density for convergence. The default is $1\cdot 10^{-5}$, which is rather loose.
           \item \verb=maxiter <int>= sets the maximal number of iterations in SCF calculation. Per default it is 100. 
           \item \verb=no_scf= deactivates a SCF calculation. Usefull if pre-determined orbitals are available for MP2 or CIS
           \item \verb=guess <string>= specifies the method used for guessing start orbitals. Available options are:
                  \begin{itemize}
                    \item \verb=gwh= Generalized Wolfsberg Helmholtz guess
                    \item \verb=hcore= Hamiltonian core guess
                    \item \verb=reuse= Use orbitals in file \verb=mos= as start guess     
                  \end{itemize}
        \end{itemize}

  \item \verb=$tolint= sets the integral neglect threshold used in the Schwarz-Screening of integrals.

  \item \verb=$coord= define the coordinates of the molecule. The format is similar to the XYZ format, but Bohr is used instead of Angstom.
 
\end{itemize} 

Snail also supports the RI-JK approximation. To activate it you just need to add an JK auxiliary basis as
\begin{verbatim}
  $jkbas
    def2-SVP
\end{verbatim}
If snail detects an RI-JK basis it will use the RI approximation to calculate integrals. Note that for the
conventional case this will increase the scaling to $\mathcal O(N^5)$ and is therefore in general not advisable,
but can result in speed ups due to the more efficient generation of the 3 index integrals and the efficiency
of the BLAS routines.

\section{Theory}

\subsection{The Hartree Fock approximation}

In Hartree Fock theory we use a single Slater determinant 
\begin{equation}
\begin{split}| \Psi^{\mathrm{SD}} \rangle =
\frac{1}{\sqrt{N!}} \left | \begin{array}{cccc}
\psi_1 (\vec x_1) & \psi_2(\vec x_1) & \ldots & \psi_N (\vec x_1) \\
\psi_1 (\vec x_2) & \psi_2(\vec x_2) & \ldots & \psi_N (\vec x_2) \\
\vdots & \vdots & \ddots & \vdots \\
\psi_1 (\vec x_N) & \psi_2(\vec x_N) & \ldots & \psi_N (\vec x_N) \\
\end{array}\right |\end{split}
\end{equation}
as our wave function Ansatz and optimize it variationally to solve the electronic Schr\"{o}dinger equation.
As Hamilton operator we use
\begin{equation}
 \hat{H} = \sum_i \hat{h}_i + \sum_{i<j} \hat{g}_{ij} + \hat{V}_{NN}\quad, 
\end{equation}
where
\begin{equation}
\hat{V}_{NN} = \sum_{A<B} \frac{Z_{A}Z_{B}}{|\mathbf{r}_{A}-\mathbf{r}_{B}|} = \sum_{A<B} \frac{Z_{A}Z_{B}}{r_{AB}} \quad,
\end{equation}
\begin{equation}
 \hat{h}_i = -\frac{1}{2} \nabla^2_i - \sum_{A} \frac{Z_{A}}{r_{iA}} \quad,
\end{equation}
\begin{equation}
 \hat{g}_{ij} = \frac{1}{r_{ij}} \quad.
\end{equation}
Calculating the expectation value under the condition that the orbitals form an orthonormal basis $\braket{\psi_i|\psi_j}=\delta_{ij}$
\begin{equation}
 E[\Psi^{\mathrm{SD}}] = \frac{\bra{\Psi^{\mathrm{SD}}} \hat{H} \ket{\Psi^{\mathrm{SD}}} }{ \braket{ \Psi^{\mathrm{SD}} | \Psi^{\mathrm{SD}}  }   }
 \label{eqn:EXPEC}
\end{equation}
we obtain
\begin{equation}
 E[\Psi^{\mathrm{SD}}] = \sum_i h_i + \frac{1}{2} \sum_{ij} \bra{\psi_i\psi_j }\ket{\psi_i\psi_j} + \hat{V}_{NN}
\end{equation}
with
\begin{equation}
 h_i = \bra{\psi_j} \hat{h}_i \ket{\psi_i} 
\end{equation}
and
\begin{equation}
\bra{\psi_i\psi_j }\ket{\psi_i\psi_j} = \braket{\psi_i\psi_j | \psi_i\psi_j} - \braket{\psi_i\psi_j | \psi_j\psi_i}
\end{equation}
with 
\begin{equation}
\braket{\psi_i\psi_j | \psi_k\psi_l} = \int\int \psi^*_i(1) \psi^*_j(2) \hat{g}_{12} \psi_k(1) \psi_l(2) \quad.  
\end{equation}
This is the so-called physicist notation. Often also the charge cloud notation
\begin{equation}
\braket{\psi_i\psi_j | \psi_k\psi_l} = \left( \psi_i\psi_k | \psi_j \psi_l \right)
\end{equation}
can be found in the quantum chemical literature and we will also make heavily use of it. 
Note that the ortohonormality of the basis does not change the generality of our Ansatz, but simplifies the equations. 
To optimize the wave function variationally we minimize the expectation value in Eq. \ref{eqn:EXPEC}. However,
we do this under the condition of an orthonormal basis. To include this constraint we introduce the following Lagrangian
\begin{equation}
L = E[\Psi^{\mathrm{SD}}] - \sum_{ij} \varepsilon_{ij} \left(\braket{\psi_i|\psi_j} - \delta_{ij}  \right)
\end{equation}
which contains this condition as Lagrange multipliers $\varepsilon_{ij}$. We now require that the Lagrangian
is stationary w.r.t. variations:
\begin{equation}
 \begin{split}
   \delta L  & = 0 \\
             & = \big[ \left( \sum_i  \bra{\delta\psi_i}\hat{h}\ket{\psi_i} + \sum_k \bra{(\delta\psi_i)\psi_j }\ket{\psi_i\psi_j}   \right) - \sum_{ij} \varepsilon_{ij} \braket{\delta\psi_i|\psi_j} \big] + c.c. \\
             & = \big[ \sum_i \bra{\delta\psi_i}\hat{F}\ket{\psi_i} - \sum_{ij} \varepsilon_{ij} \braket{\delta\psi_i|\psi_j} \big]  + c.c. 
 \end{split}
\end{equation} 
Here we introduced the Fock operator defined as
\begin{equation}
 \hat{f}(i) = \hat{h}(i) + \hat{j}(i) - \hat{k}(i) \quad\text{and}\quad \hat{F} = \sum_i \hat{f}(i) 
\end{equation}
\begin{equation}
 \hat{j}(i) = \sum_j \int \psi_j^{*}(2) \frac{1}{r_{12}} \psi_j(2) d\tau_2 \hfill \text{Coulomb operator}
\end{equation}
\begin{equation}
 \hat{k}(1)\psi(1) = \sum_j \psi_j(1) \int \psi_j^{*}(2) \frac{1}{r_{12}} \psi(2)d\tau_2 \hfill \text{Exchange operator}
\end{equation}
The Fock operator is a Hermitian one particle operator. The minimization is thus equivalent to the Hartree Fock equations 
\begin{equation}
\hat{F}\ket{\psi_i} = \sum_j \varepsilon_{ij}\ket{\psi_j} \quad\quad i,j\in\{occ\}
\end{equation}
\begin{equation}
\hat{F}\ket{\Psi^{\mathrm{SD}}} = \sum_i \varepsilon_{ij}\ket{\Psi^{\mathrm{SD}}} 
\end{equation}
The occupied molecular orbitals are only determined up to an unitary transformation among each other.
However, usually they are completely determined by requiring that the Matrix $\varepsilon_{ij}$
is diagonal, which leads to canonical MOs and the canonical Hartree Fock equation
\begin{equation}
\hat{F}\ket{\Psi^{\mathrm{SD}}} =  \varepsilon_{i}\ket{\Psi^{\mathrm{SD}}} 
\end{equation}
Thus, the orbitals are determined as eigenfunctions of the Fock operator. The eigenvalues $\varepsilon_i$
are interpreted as orbital energies. 
 
\subsection{Solution of the Hartree Fock equations in a basis set: Self consistent field method}
\label{sec:scf}

We will now take a closer look on how to solve the HF equations. We expand our MOs in a basis of atomic orbitals (AOs)
\begin{equation}
  \psi_i = \sum_{\nu} \ket{\nu} c_{\nu i} \quad\quad\text{with}\quad \ket{\nu} = \chi_{\nu}(\vec{r}) \cdot \Big\{ 
  \begin{array}{c}
    \alpha \\
    \beta \\
  \end{array}
\end{equation}
\begin{equation}
 \delta \psi_i = d\psi_i = \sum_{\nu} \ket{\nu} dc_{\nu i}
\end{equation}
For the Hartree Fock equations expanded in this basis we get:
\begin{equation}
   \delta L  = \big[ \sum_i \bra{\delta\psi_i}\hat{F}\ket{\psi_i} - \sum_{ij} \varepsilon_{ij} \braket{\delta\psi_i|\psi_j} \big]  + c.c. = 0
\end{equation}
\begin{equation}
 \frac{dL}{dc_{\mu i}} = \bra{\mu} \hat{F} \ket{\nu} - \sum_j \varepsilon_{ij} \braket{\mu|\psi_j} = 0 \quad \forall_{\mu,i}
\end{equation}
Introducing the matrices
\begin{equation}
  F_{\mu\nu} = \bra{\mu}\hat{F} \ket{\nu}
\end{equation}
\begin{equation}
  S_{\mu\nu} = \braket{\mu|\nu}
\end{equation}
the Hartree Fock equations can be written in matrix form as
\begin{equation}
 \mathbf{F} \vec{c}_i = \sum_j \varepsilon_{ij} \mathbf{S} \vec{c}_j
\end{equation}
In case of the canonical Hartree Fock equations 
\begin{equation}
 \mathbf{F} \vec{c}_i =  \varepsilon_{i} \mathbf{S} \vec{c}_i
\end{equation}
The two most time consuming computational steps are
\begin{enumerate}
  \item Calculation of the two electron AO integrals $\big(\mu\nu|\kappa\lambda\big)$.
  \item The construction of the Fock matrix using the two electron AO integrals $\big(\mu\nu|\kappa\lambda\big)$  
        and the MO coefficients $c_{\mu i}$
        \begin{equation}
          F_{\mu\nu} = h_{\mu\nu} + J_{\mu\nu} - K_{\mu\nu}
        \end{equation}
        \begin{equation}
          J_{\mu\nu} = \bra{\mu} \hat{J} \ket{\nu} = \sum_j \braket{\mu \psi_j|\nu\psi_j} = \sum_j \big(\mu\nu|\psi_j\psi_j \big) = \sum_{\kappa\lambda j} \big(\mu\nu|\kappa\lambda\big) c_{\lambda j} c_{\kappa j}
        \end{equation}
        \begin{equation}
          K_{\mu\nu} = \bra{\mu} \hat{K} \ket{\nu} = \sum_j \braket{\mu \psi_j|\psi_j\nu} = \sum_j \big(\mu\psi_j|\psi_j\nu \big) = \sum_{\kappa\lambda j} \big(\mu\kappa|\lambda\nu\big) c_{\lambda j} c_{\kappa j}
        \end{equation}
\end{enumerate}
Since the Fock matrix depends on the orbital coefficients $c_{\mu i}$, which should be determined by solving the Hartree Fock equations,
we have an non-linear problem, which needs to be solved iteratively. The standard approach is the \textbf{self-consistent-field} (SCF) 
method outlined below:
\begin{enumerate}
  \item Guess start orbitals $\psi_i^{(0)}$
  \item Calculate for $\psi_i^{(m)}$ the matrices $\mathbf{J}$ and $\mathbf{K}$ and build $\mathbf{F}$  
  \item Solve $\mathbf{F} \psi_i^{(m+1)}=\varepsilon_i^{(m+1)} \psi_i^{(m+1)}$  (diagonalize Fock matrix) to get a new set of orbitals.
  \item Calculate the energy
        \begin{equation*}
           E_{\mathrm{HF}}^{(m+1)} = \sum_i \frac{1}{2} \left( h_{ii} + F_{ii} \right)
        \end{equation*} 
  \item Is $|E_{\mathrm{HF}}^{(m+1)}-E_{\mathrm{HF}}^{(m)}| < T_1$ and $||\psi^{(m+1)}-\psi^{(m)}||< T_2$, where $T_1$ and $T_2$ are convergence
        thresholds stop, else continue with 2.
\end{enumerate}

\subsection{Unrestricted and restricted Hartree Fock}

The Ansatz we used so far
\begin{itemize}
  \item A single Slater determinant
  \item For all spin orbitals the spatial parts are optimized independently of each other. The only requirement is the ortohonormality of the spatial part
        and that the spin parts are eigenfunctions of $\hat{S}_z$
  \item The spatial part for $\alpha$ and $\beta$ can be different
\end{itemize} 
is denoted as unrestricted Hartree Fock (UHF). An other Ansatz is restricted Hartree Fock (RHF). 
It varies from UHF in two points.
\begin{enumerate}
  \item For the orbitals the additional constraint is introduced that the spatial part for $\alpha$ and $\beta$ is identical (thus restricted).
        \begin{equation}
          \phi_i = \sum_{\nu} \chi_{\nu} c_{\nu i} \quad \phi^{\alpha}_i = \phi_i \alpha \quad \phi^{\beta}_i = \phi_i \beta 
        \end{equation}
        Due to this restriction the parameters, which have to be optimized are reduced by one half, which reduces the computational costs.
  \item The Ansatz for the wave function is one configuration state function (CSF). Only in the closed-shell and high-spin open-shell
        case this is possible with a single Slater determinant. 
\end{enumerate}
Using RHF the Fock matrix looks a bit different
\begin{equation}
  F_{\mu\nu} = h_{\mu\nu} + \sum_i \left[ 2\big(\mu\nu|ii\big) - \big(\mu i|i \nu\big)  \right] \quad,
\end{equation}
where the factor of 2 stems from the spin integration.
RHF is the preferred choice due to the reduced computational costs. Often it is of advantage to express the Fock matrix
in terms of the density matrix
\begin{equation}
  D_{\mu\nu} = \sum_i c_{\mu i} c_{\nu i} \quad .
\end{equation}
For the Fock matrix we then obtain
\begin{equation}
  \begin{split}
    F_{\mu\nu} & = h_{\mu\nu} + \sum_{\kappa\lambda} \left[ 2\big(\mu\nu|\kappa\lambda\big) D_{\kappa\lambda} - \big(\mu \kappa|\lambda \nu\big) D_{\kappa\lambda}  \right] \\
               & = h_{\mu\nu} + \sum_{\kappa\lambda} D_{\kappa\lambda}  \left[ 2\big(\mu\nu|\kappa\lambda\big) - \big(\mu \kappa|\lambda \nu\big) \right] 
  \end{split}
\end{equation}


\subsection{Damping and Direct inversion of the iterative subspace}

Let's go back to the SCF method. The naive implementation described in section \ref{sec:scf} often does not converge in
slightly more complicated cases. In the flowing section we will discuss solutions to this problem. 
For this discussion we  use a more simplified picture of the steps in HF outlined as  
\begin{equation}
 D_n \rightarrow F(D_n)  \rightarrow D_{n+1}
\end{equation} 
We use a density matrix $D_n$ to construct a Fock matrix $F_n$, which is used to obtain a new density matrix $D_{n+1}$.
A rather useful property of the Fock matrix in this context is that
\begin{equation}
 F\left(  \sum_i c_i D_i\right) = \sum_i c_i F(D_i)
\end{equation}
for any $c_i$, which fulfill $\sum_i c_i = 1$.

The first solution for convergence problems we want to discuss is to apply damping to the new set of orbitals. It sounds a bit odd, but often helps
to prevent oscillations. Simple damping mixes the output density with parts of the input density
\begin{equation}
 \tilde{D}_{n+1} = (1-\alpha) D_n + D_{n+1}
\end{equation}
We use then this density for a new Fock matrix build instead of $D_{n+1}$. 

\subsubsection{DIIS: Direct inversion in the iterative subspace}

Direct inversion in the iterative subspace is an extrapolation technique introduced by Pulay\cite{DIIS1,DIIS2}
which proved to be very efficient in forcing convergence of the SCF procedure. They idea
is that during the different SCF iterations Fock and density matrices ($\mathbf{F}_0$, $\mathbf{F}_1$, $\mathbf{F}_2$ and $\mathbf{D}_0$, $\mathbf{D}_1$, $\mathbf{D}_2$)
are saved together with an error measure $\mathbf{e}_i$, which indicates how far the Fock/density matrix is from convergence.
One popular choice is the commutator
\begin{equation}
   \mathbf{F}\mathbf{D}\mathbf{S} - \mathbf{S}\mathbf{D}\mathbf{F}
\end{equation}  
and also used in snail. The converged solution has an error of zero and the essence 
of DIIS is to linear combine the previous Fock/density matrices in order to minimize this error.\cite{DIIS1,DIIS2} 
Formulating a Lagrangian including the constraint that the coefficients sum to one
\begin{align}
L&=\left\|\mathbf e_{m+1}\right\|^2-2\lambda\left(\sum_i\ c_i-1\right),\\
&=\sum_{ij}c_jB_{ji}c_i-2\lambda\left(\sum_i\ c_i-1\right),\text{ where } B_{ij}=\langle\mathbf e_j, \mathbf e_i\rangle.
\end{align}
and minimizing it, yields the following linear system of equations
\begin{equation}
\begin{bmatrix} 
B_{11} & B_{12} & B_{13} & ... & B_{1m} & -1 \\
B_{21} & B_{22} & B_{23} & ... & B_{2m} & -1 \\ 
B_{31} & B_{32} & B_{33} & ... & B_{3m} & -1 \\ 
\vdots & \vdots & \vdots & \vdots & \ddots & \vdots \\
B_{m1} & B_{m2} & B_{m3} & ... & B_{mm} & -1 \\
1      & 1      & 1      & ... & 1      & 0
\end{bmatrix} \begin{bmatrix} c_1 \\ c_2 \\ c_3 \\ \vdots \\ c_m \\ \lambda \end{bmatrix}=
\begin{bmatrix} 0 \\ 0 \\ 0 \\ \vdots \\ 0 \\ 1 \end{bmatrix} \quad,
\end{equation}
which can be solved with standard tools.
The coefficients are then used to get an extrapolated Fock/density matrix
\begin{equation}
 \mathbf{F}_{m+1} = \sum_i^m c_i \mathbf{F}_i 
\end{equation}

\subsection{Direct SCF}

In a conventional algorithm the AO integrals $(\mu\nu|\kappa\lambda)$ are calculated once and then read in during
each SCF iterations. Although these integrals don't change during the SCF procedure, this kind of algorithm might
not be optimal since the constant I/O as well as the integral storage can  be a bottleneck.\cite{doi:10.1002/jcc.540030314}

However, in HF/KS-DFT calculations we actually don't need these integrals per se. We just need to contract them with the density matrix
to obtain the Fock matrix. The storage requirements for the Fock matrix are  $N^2$ instead of $N^4$ for the 4 index AO integrals.
Therefore savings can be expected if we avoid the storage of the AO integrals.
Thus, in direct SCF methods these integrals are re-calculated in each SCF iteration.\cite{doi:10.1002/jcc.540030314,SchwarzScreening}
This requires an efficient pre-screening. We should avoid to calculate small integrals let's say $10^{-10}$.
With means of simple mathematical relations there is a tool to achieve this. We recall that the two-electron integrals
\begin{equation}
  g_{abcd} = \int \int \frac{\Omega_{ab}(1)\Omega_{cd}(2)}{r_{12}} d\pmb{r}_2 d\pmb{r}_2
\end{equation}
are elements of a positive definite matrix with diagonal elements $g_{abab}\ge 0$. Thus the conditions for an inner product are satisfied
and we can use the Cauchy-Schwartz inequality
\begin{equation}
  \|g_{abcd}\| \le \sqrt{g_{abab}}\sqrt{g_{cdcd} } = Q_{ab} Q_{cd}
\end{equation}
to get an strict upper bound to the integral! Additionally in case of direct SCF we can exploit that
the density matrix is available prior to the integral evaluation and we can use this information for additional screening.
The screening is performed on the level of shells. This reduces memory requirements and ensures rotational invariance.
\begin{equation}
   D_{MN} = \max_{\mu \in M, \nu \in N} | D_{\mu\nu} |  \quad\quad Q_{MN} = \max_{\mu \in M, \nu \in N} | Q_{\mu\nu} |
\end{equation}
The integral $(\mu\nu|\kappa\lambda)$ is only needed to be evaluated if
\begin{equation}
   Q_{MN} Q_{KL} D_{\max} \ge \tau
\end{equation}
where
\begin{equation}
  D_{\max} = \max\{4|D_{MN}| ,4 |D_{KL}|, |D_{NL}|, |D_{NK}|, |D_{ML}|, |D_{MK}|  \} \quad.
\end{equation}
Furthermore we can use an incremental Fock-build and then use the difference in the density matrix between iterations for screening:
\begin{equation}
  \Delta D_{\mu\nu}^{(i)} = D_{\mu\nu}^{(i)} - D_{\mu\nu}^{(i-1)}
\end{equation}
In this way in each iteration more and more integrals are screened away and the SCF iterations become faster and faster.

For the evaluation of the Fock-matrix (RHF) let's recall that
\begin{equation}
    F_{\mu\nu} = h_{\mu\nu} + \sum_{\kappa\lambda} D_{\kappa\lambda} \left[ 2 (\mu\nu|\kappa\lambda) - (\mu\kappa|\nu\lambda)  \right]
\end{equation}
Exploiting permutational symmetry the contributions to the Fock-matrix can be written as
\begin{align*}
    F_{\mu\nu}        & = F_{\mu\nu}        + 4 D_{\kappa\lambda} (\mu\nu|\kappa\lambda) \\
    F_{\kappa\lambda} & = F_{\kappa\lambda} + 4 D_{\mu\nu} (\mu\nu|\kappa\lambda)
\end{align*}
for the Coulomb contributions and
\begin{align*}
    F_{\mu\kappa}  & = F_{\mu\kappa}  -  D_{\nu\lambda} (\mu\nu|\kappa\lambda) \\
    F_{\mu\lambda} & = F_{\mu\lambda} -  D_{\nu\kappa}  (\mu\nu|\kappa\lambda) \\
    F_{\nu\kappa}  & = F_{\nu\kappa}  -  D_{\mu\lambda} (\mu\nu|\kappa\lambda) \\
    F_{\nu\lambda} & = F_{\nu\lambda} -  D_{\mu\kappa}  (\mu\nu|\kappa\lambda)
\end{align*}
for the Exchange contributions. Thus, if we calculate a shell quadruple we can calculates its
contributions to the Fock matrix according to the given recipe. This completely removes the
necessity to store the integrals on disk. Note that same programs provide semi integral direct
algorithms, in which the most time consuming integrals are stored and read in, in each SCF iteration.


\subsection{RI-JK approximation}

In the RI-JK approximation\cite{B204199P} we perform a projection onto an auxiliary basis 
\begin{equation}
 \hat{I} \approx \sum\limits_{PQ} {\left. {\left| P \right.} \right){{\left[ {{V^{ - 1}}} \right]}_{PQ}}\left( {\left. Q \right|} \right. \hat{g}_{12} } \quad .
	\label{eqn:Idenity}
\end{equation}
Where it must be ensured that the $V_{PQ}$ matrix 
\begin{equation}
  \left( {\left. P \right|\left. Q \right)} \right. = {V_{PQ}} = \int \int {{P({r_1}) \cdot \hat{g}_{12} \cdot Q({r_2})}}d{r_1}d{r_2}
\end{equation}
is invertible. The matrix  ${\left[ {{V^{ - 1}}} \right]}_{PQ}$ ensures that $\hat{I}$ is idempotent e.g. $\hat{I}^{2}=\hat{I}$.
The operator $\hat{I}$ is only in the limit of a complete basis a resolution-of-the-identity and therefore we usually introduce an
error when invoking the RI approximation, but it gives us the possibility to rewrite the 4 index integrals as sums of 2 and 3 index integrals: 
\begin{equation}
 \begin{split}
   \left( {\left. {\mu\nu} \right|\left. {\kappa\lambda} \right)} \right. \approx {\left( {\left. {\mu\nu} \right|\left. {\kappa\lambda} \right)} \right._{RI}} & =
         {\sum\limits_{PQ} {\left( {\left. {\mu\nu} \right|\left. Q \right)} \right.\left[ {{V^{ - 1}}} \right]} _{PQ}}\left( {\left. {\kappa\lambda} \right|\left. P \right)} \right. \\
  \end{split}
  \label{eqn:RI}
\end{equation}
For convenience we form the symmetric B intermediates via multiplication with the square root of 
the inverse of the $V_{PQ}$ matrix 
\begin{equation}
 B^Q_{\mu\nu} = V_{PQ}^{-1/2} \left(Q|\mu\nu\right) \quad.
\end{equation}
Note that alternatively also its Cholesky decomposition can
be used and is due to efficiency reasons the preferred choice.
The B intermediates allow us to express the 4 index integral as 
\begin{equation}
  \left( {\mu\nu} | {\kappa\lambda} \right)_{RI} = \sum_{Q} B^Q_{\mu\nu} B^Q_{\kappa\lambda} \quad. 
\end{equation}
However, the formation of this integral would scale as $\mathcal O(N_x N^4)$ and thus worse than in conventional Hartree Fock.
So how can we profit form the RI approximation? Again we have to recall that we are not really interested in the integrals
itself but need to contract them with the density matrix to form the Fock matrix. And again it is beneficial to take 
separate look at the Coulomb matrix $J_{\mu\nu}$ and the Exchange $ K_{\mu\nu}$.\\  

We can calculate the Coulomb matrix in two steps. First we contract the B intermediate
with the density matrix in a $\mathcal O(N_x \cdot N^2)$ step
\begin{equation}
  c_Q = \sum_{\kappa\lambda} B^Q_{\kappa\lambda} D_{\kappa\lambda}
\end{equation}
and afterwards we contract the obtained vector with an other B intermediate
in a $\mathcal O(N^2\cdot N_x)$ step
\begin{equation}
  J_{\mu\nu} = \sum_Q c_Q B^Q_{\mu\nu} \quad.
\end{equation}
Thus, the costs for evaluating the Coulomb matrix is $\mathcal O(N^3)$. This is an improvement to 
conventional HF, but still not optimal since we know that the non vanishing elements of the Coulomb
matrix scale with $\mathcal O(N^2)$. However, we can combine the generation of the B intermediates
with Schwarz-Screening to only calculate the integrals for the non-vanishing AO shell pairs $\mu\nu$
and use sparse linear algebra routines.

For the Exchange the situation is a bit different. Here, we can not contract away two indices 
from the density matrix at the same time. However, we can first construct the X intermediate
\begin{equation}
  X^Q_{\mu i} = B^Q_{\mu\nu} C_{\nu i}
\end{equation}
in a $\mathcal O(N_x \cdot N^2 \cdot N_o)$ step and then form the Exchange
\begin{equation}
  K_{\mu\nu} = \sum_{Qi} X^Q_{\mu i} X^Q_{\nu i}
\end{equation}
in a further $\mathcal O(N_x \cdot N^2 \cdot N_o)$ step. Thus, the costs
are $\mathcal O(N^4)$, which is unfortunate since we know that the
non vanishing elements in the Exchange scale as $\mathcal O(N)$.
Also screening does not help here a lot since $i$ is a delocalized occupied MO.
Still, the RI approximation may have advantages for this contribution and can
accelerate the computational time for small molecules in large basis sets.
In such cases the number of occupied orbitals $N_o$ can be neglected and we
arrive at $\mathcal O(N^3)$ scaling costs.




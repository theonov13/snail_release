
\chapter{CIS and RPA calculations}

\section{Usage}

Snail allows to calculate CIS and TDHF/RPA excitation energies. There is the possibility to use a full diagonaliztion
to get all excitations, which is only manageable for small molecules in small basis sets, or a Davidson algorithm
can be used to only calculate $n$ excited states. Currently Snail only supports to calculate CIS and TDHF/RPA
using a RHF reference. Find below the input for calculation excitation energies for water.

\begin{Example}{Hartree Fock input}
 $title
   "Calculation on Water"
 
 $wavefunction
   RHF
 
 $basis
  def2-SVP

 $cis
   nroots 5
   no_fulldiag

 $tolint
   1.d-8
 
 $coord
   3
   O -0.01020 0.0000 -0.0079
   H -0.00760 0.0000  1.8294
   H  1.76900 0.0000 -0.4660
 
\end{Example}

\newpage

It is again a similar input to the RHF calculation, but one keyword block was added.
\begin{itemize}
  \item \verb=$cis= activates the calculation of CIS or TDHF/RPA excitation energies. Afterwards several options can follow:
        \begin{itemize}
           \item \verb=nroots <int>= specifies the number of excitations to be calculated.
           \item \verb=tdhf= specifies that a TDHF calculation should be done instead of CIS.
           \item \verb=no_fulldiag= or \verb=fulldiag= specify if a full diagonalization should be used. Default is \verb=no_fulldiag=
                  and a full diagonalization is only feasible for small molecules in small basis sets. 
        \end{itemize} 

\end{itemize} 


\section{Theory}

One of the simplest response methods is the Configuration Interaction Singles (CIS) method.
If the unperturbed Hartree-Fock wave function $\ket{{\mathrm{0}}_{\mathrm{HF}}}$ fulfills the Brillouin condition
$\braket{\mathrm{0}_{\mathrm{HF}}|H^{(0)}|ai}$, then the unperturbed CIS ground state wave function is
$\ket{\mathrm{0}}=\ket{\mathrm{0}_{\mathrm{HF}}}$. For the time dependent expectation value of $\hat{H}(t,\epsilon)-i\frac{d}{dt}$
over the phase-isolated wave function we get for CIS
\begin{equation}
  \mathcal{W}_{\mathrm{CIS}} = \bra{\mathrm{0}} \exp{\left( -\hat{S}(t,\epsilon) \right)} \left( \hat{H}(t,\epsilon)-i\frac{d}{dt} \right) \exp{\left( -\hat{S}(t,\epsilon) \right)}   \ket{\mathrm{0}} \quad,
\end{equation}
\begin{equation}
 \hat{S}(t,\epsilon) = \sum_{ai} \left( S_{ia} \hat{R}_{ia}^{\dagger} - S_{ia}^{*} \hat{R}_{ia} \right)
\end{equation}
Since $\ket{\mathrm{0}}=\ket{\mathrm{0}_{\mathrm{HF}}}$ the state transfer operator $\hat{R}_{ia}$ can be expressed as
\begin{equation}
 \hat{R}_{ia} = \ket{\mathrm{0}}\bra{ai} = \ket{\mathrm{0}}\bra{\mathrm{0}}\tau_{ia} = \ket{\mathrm{0}}\bra{\mathrm{0}} a_i^{\dagger} a_a
\end{equation}
We request that $\ket{\mathrm{0}}$ is the normalized Hartree-Fock determinant. This yields
\begin{equation}
  \hat{R}_{ia} \hat{R}_{jb}^{\dagger} = \ket{\mathrm{0}}\bra{\mathrm{0}} a_i^{\dagger} a_a a_b^{\dagger} a_j  \ket{\mathrm{0}}\bra{\mathrm{0}} = \ket{\mathrm{0}}\bra{\mathrm{0}} \delta_{ia,jb} \ket{\mathrm{0}}\bra{\mathrm{0}}
\end{equation}
\begin{equation}
  \hat{R}_{ia}^{\dagger} \hat{R}_{jb} = a_a^{\dagger} a_i \ket{\mathrm{0}} \bra{\mathrm{0}} a_j^{\dagger} a_b
\end{equation}
\begin{equation}
  \hat{R}_{ia} \ket{\mathrm{0}} = 0
\end{equation}
\begin{equation}
  \bra{\mathrm{0}} \hat{R}_{ia}^{\dagger} = 0
\end{equation}
According to response theory we can identify the excitations energies $\omega$ as poles of the response function.
For the linear response $\ll A;B \gg_{\omega}$ we have a form like
\begin{equation}
  \ll{A;B}\gg_{\omega} = - \mathbf{A}^{[1]T} \left(  \mathbf{E}^{[2]} -  \omega \mathbf{S}^{[2]}  \right)^{-1} \mathbf{B}^{[1]} \quad,
\end{equation}
where the form of $\mathbf{A}^{[1]}$ and $\mathbf{B}^{[1]}$ don't matter at this point. Important is
that the linear response function will become singular for those $\omega_j$, which fulfill the eigenvalue equations 
\begin{equation}
  \mathbf{E}^{[2]} \mathbf{U}^{(j)} = \omega_j \mathbf{S}^{[2]} \mathbf{U}^{(j)}
\end{equation}
where $\mathbf{E}^{[2]}$ is the electronic hessian, $\mathbf{S}^{[2]}$ the metric and $\mathbf{U}^{(j)}$ an eigenvector.
The Hessian $\mathbf{E}^{[2]}$ plays the role of a matrix representation of the unperturbed Hamiltonian (reduced
by the ground state energy), $H = H^{(0)}-E^{(0)}$ , in the many-electron basis and $\mathbf{S}^{[2]}$ plays the role of a metric or overlap matrix for this basis.
We will now derive the CIS expressions for  
\begin{equation}
 \mathbf{E}^{[2]}  
= \begin{pmatrix}
   - \bra{\mathrm{0}} [H, R_{ia}, R_{jb}^{\dagger} ] \ket{\mathrm{0}}           & - \bra{\mathrm{0}} [H, R_{ia}, R_{jb} ] \ket{\mathrm{0}}  \\
   - \bra{\mathrm{0}} [H, R_{ia}^{\dagger}, R_{jb}^{\dagger} ] \ket{\mathrm{0}} & - \bra{\mathrm{0}} [H, R_{ia}^{\dagger}, R_{jb} ] \ket{\mathrm{0}}
  \end{pmatrix}
= \begin{pmatrix}
   \mathbf{A}  & \mathbf{B} \\
   \mathbf{B}* & \mathbf{A}*
  \end{pmatrix}
\end{equation}
and
\begin{equation}
 \mathbf{S}^{[2]}   
 = \begin{pmatrix}
   \bra{\mathrm{0}} [R_{ia}, R_{jb}^{\dagger} ] \ket{\mathrm{0}}            & \bra{\mathrm{0}} [R_{ia}, R_{jb} ] \ket{\mathrm{0}}  \\
   \bra{\mathrm{0}} [R_{ia}^{\dagger}, R_{jb}^{\dagger} ] \ket{\mathrm{0}}  & \bra{\mathrm{0}} [R_{ia}^{\dagger}, R_{jb} ] \ket{\mathrm{0}} 
  \end{pmatrix}
 = \begin{pmatrix}
   \mathbf{\Sigma}    & \mathbf{\Delta} \\
   -\mathbf{\Delta}*  & -\mathbf{\Sigma}*
  \end{pmatrix}
\end{equation}
Note that we introduced the symmetrized $n$-fold commutators
\begin{equation}
 [O,A_1,\dots,A_n] = \frac{1}{n!} \hat{P}^{1\dots n}[\dots[O,A_1],\dots,A_n] \quad .
\end{equation}
In the following we will derive these matrix elements. Expanding the commutators we obtain for the sub blocks of $\mathbf{S}^{[2]}$:
\begin{equation}
 \Sigma_{ia,jb} = \bra{\mathrm{0}} R_{ia} R_{jb}^{\dagger}  \ket{\mathrm{0}} -  \bra{\mathrm{0}} R_{jb}^{\dagger} R_{ia}  \ket{\mathrm{0}} =  \bra{\mathrm{0}} a_i^{\dagger} a_a a_b^{\dagger} a_j  \ket{\mathrm{0}} = \delta_{ia,jb}
\end{equation}
\begin{equation}
 \Delta_{ia,jb} = \bra{\mathrm{0}} R_{ia}^{\Delta} R_{jb}^{\dagger}  \ket{\mathrm{0}} -  \bra{\mathrm{0}} R_{jb}^{\dagger} R_{ia}^{\dagger}  \ket{\mathrm{0}} =  0
\end{equation}
We will now take a look at the elements of $\mathbf{E}^{[2]}$. For this purpose we use that $[H,O_m,O_n]=\frac{1}{2}[[H,O_m],O_n]+\frac{1}{2} [[H,O_n],O_m]$.
For the diagonal subblocks of $\mathbf{E}^{[2]}$ we first evaluate the double commutators
\begin{equation}
  \begin{split}
    \bra{\mathrm{0}} [[H, R_{ia}], R_{jb}^{\dagger} ] \ket{\mathrm{0}} & = \bra{\mathrm{0}} [H, R_{ia}] R_{jb}^{\dagger}  \ket{\mathrm{0}} \\
                                                               & = \bra{\mathrm{0}} H R_{ia}] R_{jb}^{\dagger}  \ket{\mathrm{0}} - \bra{\mathrm{0}} R_{ia} H R_{jb}^{\dagger} \ket{\mathrm{0}}\\
                                                               & = \bra{\mathrm{0}} H  \ket{\mathrm{0}} \delta_{ia,jb} - \bra{\mathrm{0}} R_{ia} H R_{jb}^{\dagger} \ket{\mathrm{0}} \\
                                                               & = \bra{\mathrm{0}} H  \ket{\mathrm{0}} \delta_{ia,jb} - \bra{ai} H \ket{bj} = E_{0,0} - E_{0,0} - H_{ai,bj} = - H_{ai,bj}
  \end{split}
\end{equation}
In a similar fashion we obtain
\begin{equation}
 \begin{split}
   \bra{\mathrm{0}} [[H, R_{jb}^{\dagger}], R_{ia} ] \ket{\mathrm{0}} & = - \bra{\mathrm{0}} R_{ia} [H, R_{jb}^{\dagger}] \ket{\mathrm{0}} \\
                                                              & =   \bra{\mathrm{0}} R_{ia} R_{jb}^{\dagger} H \ket{\mathrm{0}} - \bra{\mathrm{0}} R_{ia} H R_{jb}^{\dagger} \ket{\mathrm{0}} \\
                                                              & = E_{0,0} - E_{0,0} - H_{ai,bj} = - H_{ai,bj}
 \end{split} 
\end{equation}
Thus, we obtain for the elements of the matrix $\mathbf{A}$
\begin{equation}
\begin{split}
  A_{ia,jb}  & = \frac{1}{2} \bra{\mathrm{0}} [[H, R_{ia}], R_{jb}^{\dagger} ] \ket{\mathrm{0}} - \bra{\mathrm{0}} [[H, R_{jb}^{\dagger}], R_{ia} ] \ket{\mathrm{0}} \\
             & = H_{ai,bj} 
\end{split}
\end{equation}
Now taking a look at the off-diagonal sub blocks we get:
\begin{equation}
\begin{split}
 \bra{\mathrm{0}} [[H, R_{ia}^{\dagger}], R_{jb}^{\dagger} ] \ket{\mathrm{0}} & = \bra{\mathrm{0}} [H, R_{ia}^{\dagger}] R_{jb}^{\dagger}  \ket{\mathrm{0}} \\
                                                                      & = \bra{\mathrm{0}} H R_{ia}^{\dagger} R_{jb}^{\dagger}  \ket{\mathrm{0}} - \bra{\mathrm{0}} R_{ia}^{\dagger}H  R_{jb}^{\dagger}  \ket{\mathrm{0}} \\
                                                                      & = \bra{\mathrm{0}} H \ket{ai} \braket{\mathrm{0}|bj} \braket{\mathrm{0}|\mathrm{0}} - \braket{\mathrm{0}|ai} \bra{\mathrm{0}} H \ket{bj} \braket{\mathrm{0}|\mathrm{0}} \\
                                                                      & = 0 - 0 
\end{split}
\end{equation}
Thus, this yields in total
\begin{equation}
 \begin{pmatrix}
   \mathbf{A}^{\mathrm{CIS}}  &  \mathbf{0} \\
   \mathbf{0}             &  \mathbf{A}^{\mathrm{CIS}*}
 \end{pmatrix}
  \begin{pmatrix}
   \mathbf{X}  \\
   \mathbf{Y} 
 \end{pmatrix}
= \omega_j  
\begin{pmatrix}
   \mathbf{1}  &  \mathbf{0} \\
   \mathbf{0}  & -\mathbf{1}
 \end{pmatrix}
  \begin{pmatrix}
   \mathbf{X}  \\
   \mathbf{Y} 
 \end{pmatrix}
\end{equation}
It can be shown that the equations decouple in an excitation $\mathbf{X}$ and deexcitation block $\mathbf{Y}$, which only differ by a sign. 
To obtain the CIS excitation energies we then have to solve
\begin{equation}
 \mathbf{A}^{\mathrm{CIS}} \mathbf{X} = \omega \mathbf{X} \quad.
\end{equation}
This is an eigenvalue problem for a symmetric positive definite matrix. Evaluating the Matrix elements we obtain
\begin{equation}
 \mathbf{A}^{\mathrm{CIS}}_{ai,bj} = \delta_{ij}\delta_{ab} \left(\varepsilon_a - \varepsilon_i\right) + \left[ 2(ai|jb) - (ab|ji)  \right]
\end{equation}

The response theory for Hartree-Fock is slightly more complicated than for CIS and we will encounter more non-vanishing terms.
Following the same notation for CIS we can write $\mathcal W$ for HF as
\begin{equation}
 \mathcal W_{\mathrm{HF}} = \bra{\mathrm{0}} \exp{\left(-\hat{\kappa}(t,\epsilon)\right)} \left( H -i \frac{d}{dt} \right) \exp{\left(\hat{\kappa}(t,\epsilon)\right)} \ket{\mathrm{0}} \quad .
\end{equation}
For simplicity reasons we stick to a spin orbital formalism, where orbital indices refer to spin and not spatial orbitals. 
Using the notation we can write $\hat{\kappa}$ as:
\begin{equation}
  \hat{\kappa}(t,\epsilon) = \sum_{ai} \left( \kappa_{ai} \tau_{ia}^{\dagger} - \kappa_{ai}^{*} \tau_{ia} \right) \quad,
\end{equation}
where $\tau_{ia}=a_i^{\dagger}a_a$ and $\tau_{ia}^{\dagger}=a_a^{\dagger}a_i$. We note that
\begin{equation}
 \tau_{ia} \ket{\mathrm{0}} = 0 
\end{equation}
and
\begin{equation}
 \bra{\mathrm{0}} \tau_{ia}^{\dagger} = 0
\end{equation}
We now want to evaluate the complex Hessian and metric matrices
\begin{equation}
 \mathbf{E}^{[2]}  
= \begin{pmatrix}
   - \bra{\mathrm{0}} [H, \tau_{ia}, \tau_{jb}^{\dagger} ] \ket{\mathrm{0}}           & - \bra{\mathrm{0}} [H, \tau_{ia}, \tau_{jb} ] \ket{\mathrm{0}}  \\
   - \bra{\mathrm{0}} [H, \tau_{ia}^{\dagger}, \tau_{jb}^{\dagger} ] \ket{\mathrm{0}} & - \bra{\mathrm{0}} [H, \tau_{ia}^{\dagger}, \tau_{jb} ] \ket{\mathrm{0}}
  \end{pmatrix}
= \begin{pmatrix}
   \mathbf{A}  & \mathbf{B} \\
   \mathbf{B}* & \mathbf{A}*
  \end{pmatrix}
\end{equation}
and
\begin{equation}
 \mathbf{S}^{[2]}   
 = \begin{pmatrix}
   \bra{\mathrm{0}} [\tau_{ia}, \tau_{jb}^{\dagger} ] \ket{\mathrm{0}}            & \bra{\mathrm{0}} [\tau_{ia}, \tau_{jb} ] \ket{\mathrm{0}}  \\
   \bra{\mathrm{0}} [\tau_{ia}^{\dagger}, \tau_{jb}^{\dagger} ] \ket{\mathrm{0}}  & \bra{\mathrm{0}} [\tau_{ia}^{\dagger}, \tau_{jb} ] \ket{\mathrm{0}} 
  \end{pmatrix}
 = \begin{pmatrix}
   \mathbf{\Sigma}    & \mathbf{\Delta} \\
   -\mathbf{\Delta}*  & -\mathbf{\Sigma}*
  \end{pmatrix}
\end{equation}
We start again with $\mathbf{S}^{[2]}$. For this purpose we will take a look at the commutator $[\tau_{ia}, \tau_{jb}^{\dagger} ]$. 
First we note that
\begin{equation}
 \begin{split}
  \tau_{ia} \tau_{jb}^{\dagger} & = a_i^{\dagger} a_a a_b^{\dagger} a_j = a_i^{\dagger} a_j a_a a_b^{\dagger} = \left(\delta_{ij} - a_j a_i^{\dagger} \right) \left(\delta_{ab} - a_b^{\dagger} a_a \right) \\
                      & = \delta_{ij} \delta_{ab} - \delta_{ij} a_b^{\dagger} a_a - \delta_{ab} a_j a_i^{\dagger} + a_b^{\dagger} a_j a_i^{\dagger} a_a
 \end{split}
\end{equation}
\begin{equation}
  \tau_{jb}^{\dagger} \tau_{ia} = a_b^{\dagger} a_j a_i^{\dagger} a_a 
\end{equation}
Thus forming the commutator we get
\begin{equation}
  [\tau_{ia}, \tau_{jb}^{\dagger} ] = \delta_{ij} \delta_{ab} - \delta_{ij} a_b^{\dagger} a_a - \delta_{ab} a_j a_i^{\dagger}
\end{equation}
For the matrices $\mathbf{\Sigma}$ and $\mathbf{\Delta}$ we get:
\begin{equation}
  \sigma_{ai,bj} = \bra{\mathrm{0}} [\tau_{ia}, \tau_{jb}^{\dagger} ] \ket{\mathrm{0}} = \bra{\mathrm{0}} \delta_{ij} \delta_{ab} - \delta_{ij} a_b^{\dagger} a_a - \delta_{ab} a_j a_i^{\dagger}
 \ket{\mathrm{0}} = \delta_{ij}\delta_{ab}  \quad .
\end{equation}
\begin{equation}
  \Delta_{ai,bj} = \bra{\mathrm{0}} [\tau_{ia}, \tau_{jb} ] \ket{\mathrm{0}} =  \bra{\mathrm{0}} 0 \ket{\mathrm{0}} = 0
\end{equation}
Now taking our attention towards $\mathbf{E}^{[2]}$ we first look at the $\mathbf{A}$ matrix.
\begin{equation}
\begin{split}
  \bra{\mathrm{0}} [[H,\tau_{ia}],\tau_{jb}^{\dagger}] \ket{\mathrm{0}} & = \bra{\mathrm{0}} [H,\tau_{ia}] \tau_{jb}^{\dagger} \ket{\mathrm{0}} \\
                                                                & = \bra{\mathrm{0}} H\tau_{ia}\tau_{jb}^{\dagger} \ket{\mathrm{0}} - \bra{\mathrm{0}}\tau_{ia} H\tau_{jb}^{\dagger} \ket{\mathrm{0}} \\
                                                                & = \bra{\mathrm{0}} H [\tau_{ia}, \tau_{jb}^{\dagger}] \ket{\mathrm{0}} 
                                                                  + \bra{\mathrm{0}} H\tau_{jb}^{\dagger} \tau_{ia} \ket{\mathrm{0}}
                                                                  - \bra{ai} H \ket{bj} \\
                                                                & = \bra{\mathrm{0}} \delta_{ij}\delta_{ab} - \delta_{ij} a_b^{\dagger} a_a - \delta_{ab} a_j a_i^{\dagger} \ket{\mathrm{0}}
                                                                  - \bra{ai} H \ket{bj} \\
                                                                & = \delta_{ij} \delta_{ab} \bra{\mathrm{0}} H \ket{\mathrm{0}} - \bra{ai} H \ket{bj}
\end{split}
\end{equation}
\begin{equation}
\begin{split}
  \bra{\mathrm{0}} [[H,\tau_{jb}^{\dagger}],\tau_{ia}] \ket{\mathrm{0}} & = - \bra{\mathrm{0}} \tau_{ia} [H,\tau_{jb}^{\dagger}] \ket{\mathrm{0}} \\
                                                                & = + \bra{\mathrm{0}} \tau_{ia} \tau_{jb}^{\dagger} H \ket{\mathrm{0}} - \bra{\mathrm{0}} \tau_{ia} H \tau_{jb}^{\dagger} \ket{\mathrm{0}} \\
                                                                & = \delta_{ij} \delta_{ab} \bra{\mathrm{0}} H \ket{\mathrm{0}} - \bra{ai} H \ket{bj} 
\end{split}
\end{equation}
Thus in total we get 
\begin{equation}
  A_{ia,jb} = - \bra{\mathrm{0}} [H,\tau_{ia},\tau_{jb}^{\dagger}] \ket{\mathrm{0}} = \bra{ai} H \ket{bj} - \delta_{ij} \delta_{ab} \bra{\mathrm{0}} H \ket{\mathrm{0}} \quad,
\end{equation}
which is the same result as for CIS. For the commutators that contribute to the matrix $\mathbf{B}$ however we get now non-vanishing contributions
\begin{equation}
\begin{split}
 \bra{\mathrm{0}} [[H,\tau_{ia}],\tau_{jb}] \ket{\mathrm{0}} & = - \bra{\mathrm{0}} \tau_{jb} [H,\tau_{ia}] \ket{\mathrm{0}} \\
                                                             & =   \bra{\mathrm{0}} \tau_{jb}\tau_{ia} H \ket{\mathrm{0}} = \bra{{}^{ab}_{ij}} H \ket{\mathrm{0}}
\end{split}
\end{equation}
Interchanging the index pairs $ia$ and $jb$ we get the same result and thus the elements of the matrix are given as
\begin{equation}
  B_{ai,bj} = \bra{{}^{ab}_{ij}} H \ket{\mathrm{0}}
\end{equation}
Thus, with TDHF, which is also called RPA (Random Phase Approximation) in some context we have a bit more complicated problem
to solve than in CIS. For getting the excitation energies we need to find those $\omega$, which fulfill the equation
\begin{equation}
 \begin{pmatrix}
   \mathbf{A}  &  \mathbf{B} \\
   \mathbf{B}  &  \mathbf{A}
 \end{pmatrix}
  \begin{pmatrix}
   \mathbf{X}  \\
   \mathbf{Y} 
 \end{pmatrix}
= \omega_j  
\begin{pmatrix}
   \mathbf{1}  &  \mathbf{0} \\
   \mathbf{0}  & -\mathbf{1}
 \end{pmatrix}
  \begin{pmatrix}
   \mathbf{X}  \\
   \mathbf{Y} 
 \end{pmatrix}
\end{equation}
\begin{equation}
 \begin{pmatrix}
   \mathbf{A}   &   \mathbf{B} \\
   -\mathbf{B}  &  -\mathbf{A}
 \end{pmatrix}
  \begin{pmatrix}
   \mathbf{X}  \\
   \mathbf{Y} 
 \end{pmatrix}
= \omega_j  
  \begin{pmatrix}
   \mathbf{X}  \\
   \mathbf{Y} 
 \end{pmatrix}
\end{equation}
This is a large non Hermitian eigenvalue problem. However, we can rewrite it to a problem with the same size
as the CIS eigenvalue problem
\begin{equation}
  \left( \mathbf{A} + \mathbf{B} \right) \left( \mathbf{A} - \mathbf{B} \right) \left(  \mathbf{X} - \mathbf{Y}  \right) = \omega^2 \left(  \mathbf{X} - \mathbf{Y}  \right) \quad .
\end{equation} 
This problem is still non Hermitian and not even symmetric since $\left( \mathbf{A} + \mathbf{B} \right)$ and $\left( \mathbf{A} - \mathbf{B} \right)$ do not commute.


Since the loweste eigenvalues of $\mathbf{A}$ are upper bounds for the lowest eigenvalues of $\mathbf{E}^{[2]}$ and the highest eigenvalues of $\mathbf{A}$ are lower
bounds for the highest eigenvalues of $\mathbf{E}^{[2]}$ TDHF/RPA will give lowest and highest excitation energies compared to the excitation energies of CIS.

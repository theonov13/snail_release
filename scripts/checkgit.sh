#!/bin/bash

git_commit=$(git log -n 1 --format="GIT_COMMIT \"%H\"%n" HEAD)
git_branch=$(git symbolic-ref --short HEAD)
git_status=$(git status --porcelain 2> /dev/null)

if [ $? -eq 0 ]; then

  echo $git_commit
  echo $git_branch

  if [ -z "$git_status" ]; then
    echo "Git Repo is clean" 
  else
    echo "Git Repo is dirty" 
    IFS=$'\n'      
    modified=($git_status) 

    for (( i=0; i<${#modified[@]}; i++ ))
    do
      echo "${modified[$i]}"
    done
  fi
else
  echo "Git status failed"
fi



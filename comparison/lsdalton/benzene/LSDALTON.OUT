  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | gschmitz
     Host                     | vejle
     System                   | Linux-4.4.0-116-generic
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /usr/bin/gfortran
     Fortran compiler version | GNU Fortran (Ubuntu 5.4.0-6ubuntu1~16.04.9) 5.4.0 
                              | 20160609
     C compiler               | /usr/bin/gcc
     C compiler version       | gcc (Ubuntu 5.4.0-6ubuntu1~16.04.9) 5.4.0 20160609
     C++ compiler             | /usr/bin/g++
     C++ compiler version     | g++ (Ubuntu 5.4.0-6ubuntu1~16.04.9) 5.4.0 20160609
     BLAS                     | /opt/intel/mkl/lib/intel64/libmkl_gf_lp64.so;/opt/
                              | intel/mkl/lib/intel64/libmkl_sequential.so;/opt/in
                              | tel/mkl/lib/intel64/libmkl_core.so;/usr/lib/x86_64
                              | -linux-gnu/libpthread.so;/usr/lib/x86_64-linux-gnu
                              | /libm.so
     LAPACK                   | /opt/intel/mkl/lib/intel64/libmkl_lapack95_lp64.a;
                              | /opt/intel/mkl/lib/intel64/libmkl_gf_lp64.so
     Static linking           | OFF
     Last Git revision        | f0a29dd58a7c4b14a6decc8e671c52951a104ce4
     Git branch               | master
     Configuration time       | 2018-04-09 11:27:53.362965
  

         Start simulation
     Date and time (Linux)  : Wed Aug 22 13:52:54 2018
     Host name              : vejle                                   
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    cc-pVDZ                                 
    LSint test, BENZENE                     
    cc-pVDZ                                 
    Atomtypes=2 Nosymmetry                                                                                                  
    Charge=6.0 Atoms=6                                                                                                      
    C   2.62524979696891      0.00000000000000      0.00000000000000                                                        
    C   1.31262489848445      2.27353301545501      0.00000000000000                                                        
    C  -1.31262489848445      2.27353301545501      0.00000000000000                                                        
    C  -2.62524979696891      0.00000000000000      0.00000000000000                                                        
    C  -1.31262489848445     -2.27353301545501      0.00000000000000                                                        
    C   1.31262489848445     -2.27353301545501      0.00000000000000                                                        
    Charge=1.0 Atoms=6                                                                                                      
    H   4.66241737376797      0.00000000000000      0.00000000000000                                                        
    H   2.33120868688399      4.03777188872899      0.00000000000000                                                        
    H  -2.33120868688399      4.03777188872899      0.00000000000000                                                        
    H  -4.66241737376797      0.00000000000000      0.00000000000000                                                        
    H  -2.33120868688399     -4.03777188872899      0.00000000000000                                                        
    H   2.33120868688399     -4.03777188872899      0.00000000000000                                                        
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **DALTON INPUT
    .DIRECT
    **INTEGRAL
    .CART-E
    **WAVE FUNCTIONS
    .HF
    *DENSOPT
    .RH
    .DIIS
    *END OF INPUT
 
 
                      
    Atoms and basis sets
      Total number of atoms        :     12
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 C      6.000 cc-pVDZ                   26       14 [9s4p1d|3s2p1d]                              
          2 C      6.000 cc-pVDZ                   26       14 [9s4p1d|3s2p1d]                              
          3 C      6.000 cc-pVDZ                   26       14 [9s4p1d|3s2p1d]                              
          4 C      6.000 cc-pVDZ                   26       14 [9s4p1d|3s2p1d]                              
          5 C      6.000 cc-pVDZ                   26       14 [9s4p1d|3s2p1d]                              
          6 C      6.000 cc-pVDZ                   26       14 [9s4p1d|3s2p1d]                              
          7 H      1.000 cc-pVDZ                    7        5 [4s1p|2s1p]                                  
          8 H      1.000 cc-pVDZ                    7        5 [4s1p|2s1p]                                  
          9 H      1.000 cc-pVDZ                    7        5 [4s1p|2s1p]                                  
         10 H      1.000 cc-pVDZ                    7        5 [4s1p|2s1p]                                  
         11 H      1.000 cc-pVDZ                    7        5 [4s1p|2s1p]                                  
         12 H      1.000 cc-pVDZ                    7        5 [4s1p|2s1p]                                  
    ---------------------------------------------------------------------
    total         42                              198      114
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :      114
      Primitive Regular basisfunctions   :      198
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

    Density subspace min. method    : DIIS                    
    Density optimization            : Diagonalization                    

    Maximum size of Fock/density queue in averaging:   10

    Convergence threshold for gradient        :   0.10E-03
    We have detected a Dunnings Basis set so we deactivate the
    use of the Grand Canonical basis, which is normally default.
    The use of Grand Canonical basis can be enforced using the FORCEGCBASIS keyword
    We perform the calculation in the standard input basis
     
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    The SCF Convergence Criteria is applied to the gradnorm in AO basis

    End of configuration!


    First density: Atoms in molecule guess

    Iteration 0 energy:     -233.332705040677
 
    Diagonalize the Atoms in Molecule Fock Matrix to obtain Initial Density
    Preparing to do S^1/2 decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  9.99999975E-05
    *********************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift       RHinfo  AO gradient ###
    *********************************************************************************** ###
      1   -230.6585040902    0.00000000000    0.00      0.00000    0.00    0.0000000    1.728E+00 ###
      2   -230.7174118300   -0.05890773981    0.00      0.00000    0.00    0.0000000    4.907E-01 ###
      3   -230.7218225458   -0.00441071578   -1.00      0.00000    0.00    0.0000000    1.872E-01 ###
      4   -230.7222671234   -0.00044457760   -1.00      0.00000    0.00    0.0000000    1.049E-02 ###
      5   -230.7222717308   -0.00000460740   -1.00      0.00000    0.00    0.0000000    2.297E-03 ###
      6   -230.7222718995   -0.00000016872   -1.00      0.00000    0.00    0.0000000    5.007E-04 ###
      7   -230.7222719073   -0.00000000783   -1.00      0.00000    0.00    0.0000000    3.470E-05 ###
    SCF converged in      7 iterations
    >>>  CPU Time used in SCF iterations is  38.19 seconds
    >>> wall Time used in SCF iterations is  38.19 seconds

    Total no. of matmuls in SCF optimization:         32

    Number of occupied orbitals:      21
    Number of virtual orbitals:       93

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in    16 iterations!

    Calculation of virtual orbital energies converged in    13 iterations!

     E(LUMO):                         0.183288 au
    -E(HOMO):                        -0.491349 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.674637 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.05890773981    0.0000    0.0000    0.0000000
      3   -0.00441071578   -1.0000    0.0000    0.0000000
      4   -0.00044457760   -1.0000    0.0000    0.0000000
      5   -0.00000460740   -1.0000    0.0000    0.0000000
      6   -0.00000016872   -1.0000    0.0000    0.0000000
      7   -0.00000000783   -1.0000    0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 AO Gradient norm
    ======================================================================
        1          -230.65850409019728317617      0.172771493742931D+01
        2          -230.71741183001162767141      0.490695634341863D+00
        3          -230.72182254579192317578      0.187176885627007D+00
        4          -230.72226712339269738550      0.104867616759874D-01
        5          -230.72227173079167528158      0.229736058363549D-02
        6          -230.72227189950726256029      0.500663060345506D-03
        7          -230.72227190733630663999      0.347038495012311D-04

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final HF energy:                      -230.722271907336
          Nuclear repulsion:                     204.360444202574
          Electronic energy:                    -435.082716109911

    Total no. of matmuls used:                        40
    Total no. of Fock/KS matrix evaluations:           8
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                         16.504 MB
      Max allocated memory, type(matrix)                   3.432 MB
      Max allocated memory, real(realk)                   16.420 MB
      Max allocated memory, integer                      300.822 kB
      Max allocated memory, logical                        3.784 kB
      Max allocated memory, character                      9.040 kB
      Max allocated memory, AOBATCH                       84.992 kB
      Max allocated memory, ODBATCH                       81.312 kB
      Max allocated memory, LSAOTENSOR                    87.552 kB
      Max allocated memory, SLSAOTENSOR                   81.696 kB
      Max allocated memory, ATOMTYPEITEM                  76.528 kB
      Max allocated memory, ATOMITEM                       6.912 kB
      Max allocated memory, LSMATRIX                       3.360 kB
      Max allocated memory, OverlapT                       1.304 MB
      Max allocated memory, linkshell                     21.672 kB
      Max allocated memory, integrand                    653.184 kB
      Max allocated memory, integralitem                   2.304 MB
      Max allocated memory, IntWork                      371.296 kB
      Max allocated memory, Overlap                       14.272 MB
      Max allocated memory, ODitem                        59.136 kB
      Max allocated memory, LStensor                     326.782 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is  39.05 seconds
    >>> wall Time used in LSDALTON is  39.10 seconds

    End simulation
     Date and time (Linux)  : Wed Aug 22 13:53:33 2018
     Host name              : vejle                                   

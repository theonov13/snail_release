#!/usr/bin/env python

import os
import shutil
import sys
import subprocess
import ast
import re
import time 
import argparse

from math import *



#################################################################
#  Helper classes and routines
#################################################################

class Logger(object):
  def __init__(self):
    self.terminal = sys.stdout
    self.log = open("TEST.log", "wb")
    self.sublog = None 
 
  def write(self, message):
    self.terminal.write(message)
    self.log.write(message)  
    if self.sublog is not None:
      self.sublog.write(message) 
 
  def flush(self):
    #this flush method is needed for python 3 compatibility.
    #this handles the flush command by doing nothing.
    #you might want to specify some extra behavior here.
    pass    

def getDirectoryList(path):
  directoryList = []
  
  # return nothing if path is a file
  if os.path.isfile(path):
    return []
  
  # add dir to directorylist if it contains CRIT files
  if len([f for f in os.listdir(path) if f == 'CRIT']) > 0:
    directoryList.append(path)
  
  for d in os.listdir(path):
    new_path = os.path.join(path, d)
    if os.path.isdir(new_path):
      directoryList += getDirectoryList(new_path)
  
  return directoryList

def PrintTime(tdiff):

  timday = 86400.0
  timhr  = 3600.0
  timmin = 60.0

  ndays  = int(tdiff / timday)
  thour  = tdiff - ndays*timday
  nhours = int(thour / timhr)
  tmin   = thour - nhours*timhr
  nmins  = int(tmin / timmin)
  tsec   = tmin - nmins*timmin
  nsecs  = int(tsec)

  if nsecs == 60:
    nsecs  = 0
    nmins += 1
  
  if nmins == 60:
    nmins   = 0
    nhours += 1
  
  if nhours == 24:
    nhours  = 0
    ndays  += 1
  
  if ndays > 0: 
    print("  %-10s %3i %6s %2i %7s %2i %13s %2i %8s" % ( "Wall-time: ", ndays, ' days ', nhours, ' hours ', nmins, ' minutes and ', nsecs, ' seconds'  ) )
  elif nhours > 0:
    print("  %-10s %2i %7s %2i %13s %2i %8s" % ( "Wall-time: ", nhours, ' hours ', nmins, ' minutes and ', nsecs, ' seconds'  ) )
  elif nmins > 0:  
    print("  %-10s %2i %13s %2i %8s" % ( "Wall-time: ",  nmins, ' minutes and ', nsecs, ' seconds'  ) )
  else:
    print("  %-10s %6.2f %8s" % ( "Wall-time: ", tdiff, ' seconds'  ) )

  return


#################################################################
#  Initialisation
#################################################################

# Get directory of the snail program
snaildir = os.environ['SNAILDIR']

# Get Path of the testfolder
cdirpath = os.getcwd()

# All directories in this directory
lstdefault = [name for name in os.listdir(".") if os.path.isdir(os.path.join(".", name))]

# Create argument parser and oh wonder parse arguments....
parser = argparse.ArgumentParser()
parser.add_argument('folders', type=str, nargs='*',default=lstdefault,help='list of folders to test')
parser.add_argument('--x', dest='prog', action='store', default=snaildir + "/bin/snail",help='program to check')

args = parser.parse_args()

# Get the folder, where we want to do the test
lstdir = args.folders  

# Search for sub directories...
folders =  []
for mydir in lstdir:
  folders.extend(getDirectoryList(mydir))


# sanity checks
if not os.path.isfile(args.prog):
  raise Exception('Testprogram not found') 

#################################################################
#  Main script
#################################################################

#
# Log to stdout and File
#
sys.stdout = Logger()

allok = True
errorlist = []

for folder in folders:
  #
  # Chage folder to test case 
  #
  os.chdir(folder)
  
  #
  # Read in criteria we want to check 
  #
  with open('CRIT') as f:
    CRITLIST = ast.literal_eval(f.read())
  
  #
  # Create local log for test case (consider to be here more verbose)
  #
  ftest = open("TESTLOG.out","wb")
  sys.stdout.sublog = ftest
  
  #
  # Create TEST scratch and go there
  #
  if os.path.isdir("TESTDIR"):
    # Keep at most one old copy
    if os.path.isdir("TESTDIR.old"):
      shutil.rmtree("TESTDIR.old")

    shutil.move("TESTDIR","TESTDIR.old")

  os.mkdir("TESTDIR")
  shutil.copy('input.inp','TESTDIR/input.inp')
  
  os.chdir("TESTDIR")
  
 
  #
  # Execute the program
  #
  print("---------------------------------------------------")
  print("  Running test in ")
  print("  -> " + folder)
  
  t_start = (time.time())
  
  fout = open("output.test","wb")
  subprocess.call([args.prog, 'input.inp'],stdout=fout,stderr=fout)
  
  time_run = (time.time() - t_start)
  
  PrintTime(time_run) 
  print("")
  
  print("---------------------------------------------------")
  
  
  
  #
  # Loop over all criteria and perform the checks
  #
  print("")
  #print("---------------------------------------------------")
  print("  Check criteria:")
  print("")
  #print("---------------------------------------------------\n")

  print( "  %10s %10s %11s %10s" % ( "Name", "Ref.", "Test", "Status" ) )
  
  allsuccess = True
  
  for crit in CRITLIST:
    header  = crit["header"]
    pattern = crit["pattern"]
    item    = crit["item"]
    tol     = crit["tol"]
  
    def find(fsearch):
      value = None
  
      with open(fsearch) as origin_file:
        for line in origin_file:
            if pattern in line:
              token = line.split()
              value = token[item]
  
      return float(value) 
  
    valtst = find("output.test")
    valref = find("../output.ref")
  
    success =  (abs(valtst-valref) < tol)
  
    allsuccess = allsuccess and success
  
    if success:
      print( "  %-10s %12.6f %12.6f %5s" % ( header, valref, valtst, "OK" ) )
    else:
      print( "  %-10s %12.6f %12.6f %5s" % ( header, valref, valtst, "FAIL" ) )
   
  print("\n---------------------------------------------------")
  if allsuccess:
    print("  Test : succeded")
  else:
    print("  Test : failed")
    errorlist.append(folder)
  print("---------------------------------------------------")
  
  #
  # Close local log file and set it to none in Logger
  #
  ftest.close()
  sys.stdout.sublog = None

  allok = allok and allsuccess
  
  os.chdir(cdirpath)

print("---------------------------------------------------")
if allok:
  print("   All tests finished successfully")
else:
  print("   Error(s) occured. Please check: ")
  print("   ............................... \n")
  for failed in errorlist:
    print("   " + failed)
print("---------------------------------------------------")

